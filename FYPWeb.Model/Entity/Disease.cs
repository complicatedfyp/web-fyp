﻿using System.Collections.Generic;

namespace Model.Entity
{
    public class Disease : Base
    {
        public string DiseaseName { get; set; }
        public string Description { get; set; }
        public int PageVisit { get; set; }
        public virtual IEnumerable<DrugDetailDisease> DrugDetailDiseases { get; set; }

        public object ForJson()
        {
            return new
            {
                Name = DiseaseName,
                Description,
                Id
            };
        }
    }
}
