﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Entity
{
    public class InventoryTrack : Base
    {
        public int PharmacyId { get; set; }
        [ForeignKey("PharmacyId")]
        public virtual Pharmacy Pharmacy { get; set; }
        public int DrugId { get; set; }
        [ForeignKey("DrugId")]
        public virtual Drug Drug { get; set; }
        public float Price { get; set; }
        public int TotalCount { get; set; }
        public DateTime LastUpdated { get; set; }
        public virtual List<DrugCart> DrugCarts { get; set; }
        public virtual List<SessionCart> SessionCarts { get; set; }

        public dynamic ForJson()
        {
            return new
            {
                Id,
                Drug = Drug.ForJson(),
                DrugId,
                Price,
                Pharmacy = Pharmacy.ForJson(),
                TotalCount,
                LastUpdated =  LastUpdated.ToString("G")
            };
        }
    }
}
