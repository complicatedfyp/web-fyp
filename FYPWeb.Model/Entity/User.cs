﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Entity
{
    public class User : Base
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string ContactNumber { get; set; }
        public string AboutMe { get; set; }
        public string Email { get; set; }
        public string AuthCode { get; set; }
        public bool IsVerified { get; set; }
        public string Gender { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime DateOfBrith { get; set; }
        public string UserImage { get; set; }
        public int UserRoleId { get; set; }
        [ForeignKey("UserRoleId")]
        public virtual UserRole UserRole { get; set; }
        public bool IsActive { get; set; }
        public string Signature { get; set; }
        public virtual List<CustomerReview> CustomerReviews { get; set; }
        public virtual IEnumerable<DrugCart> DrugCarts { get; set; }
        public virtual IEnumerable<DrugReview> DrugReviews { get; set; }
        [InverseProperty("SenderUser")]
        public virtual IEnumerable<Notification> SenderNotifications { get; set; }
        [InverseProperty("ReceiverUser")]
        public virtual IEnumerable<Notification> ReceiverNotifications { get; set; }
        public virtual IEnumerable<Order> Orders { get; set; }
        public virtual List<Pharmacy> Pharmacies { get; set; }
        public virtual IEnumerable<PharmacyReview> PharmacyReviews { get; set; }
        public virtual IEnumerable<PostReply> PostReplies { get; set; }
        public virtual IEnumerable<PostThread> PostThreads { get; set; }
        public virtual IEnumerable<Prescription> Prescriptions { get; set; }

        public override string ToString()
        {
            return FirstName + " " + LastName;
        }

        public dynamic ForJson()
        {
            return new
            {
                Username,
                AboutMe,
                Address,
                ContactNumber,
                Email,
                FirstName,
                LastName,
                Id,
                Gender,
                DateOfBrith = DateOfBrith.ToString("G") + "",
                RegisteredAt = RegisteredAt.ToString("G") + "",
                UserImage
            };
        }
    }
}
