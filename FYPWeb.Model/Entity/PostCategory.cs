﻿using System.Collections.Generic;

namespace Model.Entity
{
    public class PostCategory : Base
    {
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public virtual List<PostThread> PostThreads { get; set; }


        public override string ToString()
        {
            return CategoryName;
        }

        public dynamic ForJson()
        {
            return new
            {
                Id,
                CategoryName,
                Description
            };
        }
    }
}
