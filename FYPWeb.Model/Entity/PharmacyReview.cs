﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Entity
{
    public class PharmacyReview : Base
    {
        public int PharmacyId { get; set; }
        [ForeignKey("PharmacyId")]
        public virtual Pharmacy Pharmacy { get; set; }
        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        public int OrderId { get; set; }
        [ForeignKey("OrderId")]
        public virtual Order Order { get; set; }
        public float Rating { get; set; }
        public string Review { get; set; }
        public DateTime PostedAt { get; set; }

    }
}
