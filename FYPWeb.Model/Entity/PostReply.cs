﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Entity
{
    public class PostReply : Base
    {
        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        public int PostThreadId { get; set; }
        [ForeignKey("PostThreadId")]
        public virtual PostThread PostThread { get; set; }
        public string Body { get; set; }
        public DateTime RepliedAt { get; set; }
    }
}
