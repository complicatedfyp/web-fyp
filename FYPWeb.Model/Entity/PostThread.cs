﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Entity
{
    public class PostThread : Base
    {
        public int? UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        public int PostCategoryId { get; set; }
        [ForeignKey("PostCategoryId")]
        public virtual PostCategory PostCategory { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public DateTime PostedAt { get; set; }
        public virtual List<PostReply> PostReplies { get; set; }
        public virtual List<TagPostThread> TagPostThreads { get; set; }
    }
}
