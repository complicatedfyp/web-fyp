﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Entity
{
    public class Recommendation : Base
    {
        public int? FromUserId { get; set; }
        [ForeignKey("FromUserId")]
        public virtual User FromUser { get; set; }
        public int? ToUserId { get; set; }
        [ForeignKey("ToUserId")]
        public virtual User ToUser { get; set; }
        public int? DrugId { get; set; }
        [ForeignKey("DrugId")]
        public virtual Drug Drug { get; set; }

        public DateTime RecommendedAt { get; set; }
    }
}
