﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Model.Entity
{
    public class Notification : Base
    {
        public int? SenderUserId { get; set; }
        [ForeignKey("SenderUserId")]
        public virtual User SenderUser { get; set; }
        public int ReceiverUserId { get; set; }
        [ForeignKey("ReceiverUserId")]
        public virtual User ReceiverUser { get; set; }
        public int? PostId { get; set; }
        [ForeignKey("PostId")]
        public virtual PostThread PostThread { get; set; }
        public bool IsSeen { get; set; }
        public bool IsUser { get; set; }
        public bool IsPharmacy { get; set; }
        public bool IsPost { get; set; }
        public string Body { get; set; }
        public int NotificationType { get; set; }
        public string Url { get; set; }
        public DateTime CreatedAt { get; set; }

        public override string ToString()
        {

            if (SenderUserId == null)
            {
                return Body;
            }
            if (IsUser)
            {
                return SenderUser + " " + Body;
            }
            if (IsPharmacy) 
            {
                return SenderUser?.Pharmacies.First(p => p.IsApproved && !p.IsRejected).ShopName + " " + Body;
            }
            if(IsPost && PostId != null)
            {
                return SenderUser + " " + Body + " '" + PostThread.Subject.Substring(0,10) + "'";
            }
            
            return "";
        }
    }
}
