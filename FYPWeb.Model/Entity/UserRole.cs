﻿using System.Collections.Generic;

namespace Model.Entity
{
    public class UserRole : Base
    {
        public string UserRoleName { get; set; }
        public virtual IEnumerable<User> Users { get; set; }
    }
}
