﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Entity
{
    public class Employee : Base
    {
        public string Name { get; set; }
        public string ContactNumber { get; set; }
        public string Address { get; set; }
        public string ImageLink { get; set; }
        public int? PharmacyId { get; set; }
        [ForeignKey("PharmacyId")]
        public virtual Pharmacy Pharmacy { get; set; }

        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        public float Salary { get; set; }
        public float DutyHours { get; set; }
        public virtual List<Order> Orders { get; set; }

        public dynamic ForJson()
        {
            return new
            {
                Address,
                ContactNumber,
                DutyHours,
                ImageLink,
                Name,
                Pharmacy.ShopName,
                Salary,
                Id,
                PharmacyId
            };
        }
    }
}
