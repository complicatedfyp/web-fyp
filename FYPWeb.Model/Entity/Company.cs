﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Entity
{
    public class Company : Base
    {
        public string CompanyName { get; set; }
        public string  WebLink { get; set; }
        public string  Address { get; set; }
        public string  Phone { get; set; }
        public string  Fax { get; set; }
        public string ImagePath { get; set; }
        public int CityId { get; set; }
        [ForeignKey("CityId")]
        public virtual City City { get; set; }
        public virtual List<Brand> Brands { get; set; }


        public dynamic ForJson()
        {
            return new
            {
                Fax,
                Phone,
                CompanyName,
                Address,
                CityId,
                WebLink,
                Id
            };
        }
    }
}
