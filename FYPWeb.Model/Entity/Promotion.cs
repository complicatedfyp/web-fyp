﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Entity
{
    public class Promotion : Base
    {
        public int PharmacyId { get; set; }
        [ForeignKey("PharmacyId")]
        public virtual Pharmacy Pharmacy { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Code { get; set; }
        public float PercentageOff { get; set; }
        public virtual IEnumerable<Order> Orders { get; set; }

        public dynamic ForJson()
        {
            return new
            {
                Name,
                Code,
                Description,
                PercentageOff,
                StartDate = StartDate.ToString("G"),
                EndDate = EndDate.ToString("G"),
            };
        }
    }
}
