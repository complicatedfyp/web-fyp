﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Entity
{
    public class Prescription : Base
    {
        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        public string PrescriptionPath { get; set; }
        public virtual List<OrderPrescription> OrderPrescriptions { get; set; }
    }
}
