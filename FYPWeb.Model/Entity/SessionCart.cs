﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Entity
{
    public class SessionCart : Base
    {
        public int InventoryId { get; set; }
        [ForeignKey("InventoryId")]
        public virtual InventoryTrack Inventory { get; set; }
        public string SessionId { get; set; }
        public int Quantity { get; set; }
    }
}
