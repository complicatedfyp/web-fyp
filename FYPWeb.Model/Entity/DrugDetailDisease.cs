﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Entity
{
    public class DrugDetailDisease : Base
    {
        public int DrugDetailId { get; set; }
        [ForeignKey("DrugDetailId")]
        public virtual DrugDetail DrugDetail { get; set; }
        public int DiseaseId { get; set; }
        [ForeignKey("DiseaseId")]
        public virtual Disease Disease { get; set; }
        public bool IsIndication { get; set; }
    }
}
