﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Entity
{
    public class PrescriptionDrug : Base
    {
        public int PrescriptionId { get; set; }
        [ForeignKey("PrescriptionId")]
        public virtual Prescription Prescription { get; set; }
        public int DrugId { get; set; }
        [ForeignKey("DrugId")]
        public virtual Drug Drug { get; set; }
    }
}
