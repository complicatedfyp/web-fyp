﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Model.Entity
{
    public class OrderDetail : Base
    {
        public int OrderId { get; set; }
        [ForeignKey("OrderId")]
        public virtual Order Order { get; set; }
        public int? DrugId { get; set; }
        [ForeignKey("DrugId")]
        public virtual Drug Drug { get; set; }
        public int Quantity { get; set; }
        public float Price { get; set; }
    }
}
