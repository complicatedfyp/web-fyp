﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Entity
{
    public class DrugReview : Base
    {
        public int DrugId { get; set; }
        [ForeignKey("DrugId")]
        public virtual Drug  Drug { get; set; }
        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        public float Rating { get; set; }
        public string Review { get; set; }
        public DateTime PostedAt { get; set; }
    }
}
