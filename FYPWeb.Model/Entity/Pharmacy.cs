﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Model.Entity
{
    public class Pharmacy : Base
    {
        public int? UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        public string ShopName { get; set; }
        public string ShopAddress { get; set; }
        public string ContactPersonName { get; set; }
        public string ImagePath { get; set; }
        public string ShopTimings { get; set; }
        public string ContactNumber { get; set; }
        public float DeliveryCharges { get; set; }
        public float ShopLocationX { get; set; }
        public float ShopLocationY { get; set; }
        public bool IsVacationMode { get; set; }
        public bool IsApproved { get; set; }
        public bool IsRejected { get; set; }
        public virtual List<CustomerReview> CustomerReviews { get; set; }
        public virtual List<InventoryTrack> InventoryTracks { get; set; }
        public virtual List<Order> Orders { get; set; }
        public virtual List<PharmacyReview> PharmacyReviews { get; set; }
        public virtual List<Promotion> Promotions { get; set; }
        public virtual List<Employee> Employees { get; set; }

        public dynamic ForJson()
        {
            return new
            {
                Id,
                ShopName,
                ContactNumber,
                ContactPersonName,
                DeliveryCharges,
                ImagePath,
                IsApproved,
                IsRejected,
                ShopAddress,
                ShopLocationX,
                User = User.ForJson(),
                ShopLocationY,
                ShopTimings,
                UserId,
                Rating = PharmacyReviews.Any() ? PharmacyReviews.Average(p => p.Rating).ToString("F") : "0"
            };
        }
    }
}
