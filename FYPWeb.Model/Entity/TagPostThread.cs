﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Entity
{
    public class TagPostThread : Base
    {
        public int TagId { get; set; }
        [ForeignKey("TagId")]
        public virtual Tag Tag { get; set; }

        public int PostThreadId { get; set; }
        [ForeignKey("PostThreadId")]
        public virtual PostThread PostThread { get; set; }
    }
}
