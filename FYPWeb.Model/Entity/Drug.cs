﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Model.Entity
{
    public class Drug : Base
    {
        public string  DrugName { get; set; }
        public int BrandId { get; set; }
        [ForeignKey("BrandId")]
        public virtual Brand Brand { get; set; }
        public string DrugForm { get; set; }
        public int DrugDetailId { get; set; }
        [ForeignKey("DrugDetailId")]
        public virtual DrugDetail DrugDetail { get; set; }

        public virtual IEnumerable<DrugCart> DrugCarts { get; set; }
        public virtual List<DrugReview> DrugReviews { get; set; }
        public virtual  List<InventoryTrack> InventoryTracks { get; set; }
        public virtual List<OrderDetail> OrderDetails { get; set; }
        public virtual List<Recommendation> Recommendations { get; set; }
        public int PageVisit { get; set; }
        public string Packing { get; set; }
        public int ItemsInPack { get; set; }
        public override string ToString()
        {
            return DrugName;
        }

        public dynamic ForJson()
        {
            return new
            {
                Brand = Brand.ForJson(),
                DrugForm,
                DrugName,
                Packing,
                PageVisit,
                Rating = DrugReviews.Any() ? DrugReviews.Average(p => p.Rating).ToString("F") : "0",
                DrugDetail = DrugDetail.ForJson(),
                ItemsInPack,
                Id
            };
        }
    }
}
