﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Entity
{
    public class Message : Base
    {
        public int? OrderId { get; set; }
        [ForeignKey("OrderId")]
        public virtual Order Order { get; set; }

        public int? SenderUserId { get; set; }
        [ForeignKey("SenderUserId")]
        public virtual User SenderUser { get; set; }

        public int? RecieverUserId { get; set; }
        [ForeignKey("RecieverUserId")]
        public virtual User RecieverUser { get; set; }

        public string Content { get; set; }
        public DateTime CreatedOn { get; set; }

    }
}
