﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Entity
{
    public class Brand : Base
    {
        public int CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }
        public string BrandName { get; set; }
        public string ImagePath { get; set; }
        public virtual List<Drug> Drugs { get; set; }

        public override string ToString()
        {
            return BrandName;
        }

        public dynamic ForJson()
        {
            return new
            {
                Id,
                BrandName,
                Company = Company.ForJson(),
                ImagePath,
            };
        }
    }
}
