﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Entity
{
    public class Tag : Base
    {
        public string Name { get; set; }
        public virtual List<TagPostThread> TagPostThreads { get; set; }
    }
}
