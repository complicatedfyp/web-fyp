﻿using System.Collections.Generic;

namespace Model.Entity
{
    public class DrugDetail : Base
    {
        public string Name { get; set; }
        public string  OverView { get; set; }
        public string ImageLink { get; set; }
        public virtual List<DrugDetailDisease> DrugDetailDiseases { get; set; }
        public virtual IEnumerable<Drug> Drugs { get; set; }

        public dynamic ForJson()
        {
            return new
            {
                OverView,
                ImageLink
            };
        }
    }
}
