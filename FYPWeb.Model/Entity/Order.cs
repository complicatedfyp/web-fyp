﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Entity
{
    public class Order : Base
    {
        public int? UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        public int? PharmacyId { get; set; }
        [ForeignKey("PharmacyId")]
        public virtual Pharmacy Pharmacy { get; set; }
        public float DeliveryCharges { get; set; }
        public float DistanceFromDeliveryLocation { get; set; }
        public bool IsPrescriptionSet { get; set; }
        public int? PromotionId { get; set; }
        [ForeignKey("PromotionId")]
        public virtual Promotion Promotion { get; set; }
        public DateTime? ExpectedDeliveryTime { get; set; }
        public string FromLocation { get; set; }
        public string FromLocationX { get; set; }
        public string FromLocationY { get; set; }
        public string DeliveryLocation { get; set; }
        public string DeliveryLocationX { get; set; }
        public string DeliveryLocationY { get; set; }
        public DateTime OrderedAt  { get; set; }
        public bool IsCompleted { get; set; }
        public DateTime? CompletedAt { get; set; }
        public bool IsAccepted { get; set; }
        public DateTime? AcceptedAt { get; set; }
        public bool IsCanceled { get; set; }
        public DateTime? CancelledAt { get; set; }
        public string CommentsCancelled { get; set; }
        public bool IsRejected { get; set; }
        public DateTime? RejectedAt { get; set; }
        public string CommentsRejected { get; set; }
        public bool IsPrescription { get; set; }    
        public int? EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; set; }
        public virtual List<CustomerReview> CustomerReviews { get; set; }
        public virtual List<OrderDetail> OrderDetails { get; set; }
        public virtual List<PharmacyReview> PharmacyReviews { get; set; }
        public virtual List<OrderPrescription> OrderPrescriptions { get; set; }
        public virtual List<Message> Messages { get; set; }

        public dynamic ForJson()
        {
            return new
            {
                Id,
                DeliveryCharges,
                CommentsCancelled,
                CommentsRejected,
                DeliveryLocation,
                DeliveryLocationX,
                DeliveryLocationY,
                DistanceFromDeliveryLocation,
                Employee = Employee?.ForJson(),
                FromLocation,
                FromLocationX,
                FromLocationY,
                User = User.ForJson(),
                Pharmacy = Pharmacy.ForJson(),
                Promotion = Promotion?.ForJson(),
                Status = IsCompleted ? "Completed" : IsRejected ? "Rejected" : IsCanceled ? "Cancelled" : IsAccepted ? "Accepted" : "Pending"
            };
        }
    }
}
