﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Entity
{
    public class DrugCart : Base
    {
        public int InventoryId { get; set; }
        [ForeignKey("InventoryId")]
        public virtual InventoryTrack Inventory { get; set; }
        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        public int Quantity { get; set; }

        public dynamic ForJson()
        {
            return new
            {
                Id,
                Drug = Inventory.Drug.ForJson(),
                Quantity,
                Inventory.Price,
                Pharmacy = Inventory.Pharmacy.ForJson(),
            };
        }
    }
}
