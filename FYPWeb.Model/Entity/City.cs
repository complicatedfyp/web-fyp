﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Entity
{
    public class City : Base
    {
        public string CityName { get; set; }
        public virtual IEnumerable<Company> Companies { get; set; }
    }
}
