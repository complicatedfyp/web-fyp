﻿using System;
using System.Collections.Generic;
using System.Linq;

// K-means clustering demo. ('Lloyd's algorithm')
// Coded using static methods. Normal error-checking removed for clarity.
// This code can be used in at least two ways. You can do a copy-paste and then insert the code into some system that uses clustering.
// Or you can wrap the code up in a Class Library. The single public method is Cluster().

namespace FYPWeb.Services.Util
{
    public static class KMean
    {
        public static double[][] AppleAlgorithm(double[][] rawData,int cluster)
        {
            var numClusters = cluster;
            var clustering = Cluster(rawData, numClusters); // this is it
            return AveragePoints(rawData, clustering, numClusters);

        } // Main

        // ============================================================================

        private static int[] Cluster(double[][] rawData, int numClusters)
        {
            // k-means clustering
            // index of return is tuple ID, cell is cluster ID
            // ex: [2 1 0 0 2 2] means tuple 0 is cluster 2, tuple 1 is cluster 1, tuple 2 is cluster 0, tuple 3 is cluster 0, etc.
            // an alternative clustering DS to save space is to use the .NET BitArray class
            var data = Normalized(rawData); // so large values don't dominate

            var changed = true; // was there a change in at least one cluster assignment?
            var success = true; // were all means able to be computed? (no zero-count clusters)

            // init clustering[] to get things started
            // an alternative is to initialize means to randomly selected tuples
            // then the processing loop is
            // loop
            //    update clustering
            //    update means
            // end loop
            var clustering = InitClustering(data.Length, numClusters, 0); // semi-random initialization
            var means = Allocate(numClusters, data[0].Length); // small convenience

            var maxCount = data.Length * 10; // sanity check
            var ct = 0;
            while (changed && success && ct < maxCount)
            {
                ++ct; // k-means typically converges very quickly
                success = UpdateMeans(data, clustering, means); // compute new cluster means if possible. no effect if fail
                changed = UpdateClustering(data, clustering, means); // (re)assign tuples to clusters. no effect if fail
            }
            // consider adding means[][] as an out parameter - the final means could be computed
            // the final means are useful in some scenarios (e.g., discretization and RBF centroids)
            // and even though you can compute final means from final clustering, in some cases it
            // makes sense to return the means (at the expense of some method signature uglinesss)
            //
            // another alternative is to return, as an out parameter, some measure of cluster goodness
            // such as the average distance between cluster means, or the average distance between tuples in 
            // a cluster, or a weighted combination of both
            return clustering;
        }

        private static double[][] Normalized(double[][] rawData)
        {
            // normalize raw data by computing (x - mean) / stddev
            // primary alternative is min-max:
            // v' = (v - min) / (max - min)

            // make a copy of input data
            var result = new double[rawData.Length][];
            for (var i = 0; i < rawData.Length; ++i)
            {
                result[i] = new double[rawData[i].Length];
                Array.Copy(rawData[i], result[i], rawData[i].Length);
            }

            for (var j = 0; j < result[0].Length; ++j) // each col
            {
                var colSum = 0.0;
                for (var i = 0; i < result.Length; ++i)
                    colSum += result[i][j];
                var mean = colSum / result.Length;
                var sum = 0.0;
                for (var i = 0; i < result.Length; ++i)
                    sum += (result[i][j] - mean) * (result[i][j] - mean);
                var sd = sum / result.Length;
                for (var i = 0; i < result.Length; ++i)
                    result[i][j] = (result[i][j] - mean) / sd;
            }
            return result;
        }

        private static int[] InitClustering(int numTuples, int numClusters, int randomSeed)
        {
            // init clustering semi-randomly (at least one tuple in each cluster)
            // consider alternatives, especially k-means++ initialization,
            // or instead of randomly assigning each tuple to a cluster, pick
            // numClusters of the tuples as initial centroids/means then use
            // those means to assign each tuple to an initial cluster.
            var random = new Random(randomSeed);
            var clustering = new int[numTuples];
            for (var i = 0; i < numClusters; ++i) // make sure each cluster has at least one tuple
                clustering[i] = i;
            for (var i = numClusters; i < clustering.Length; ++i)
                clustering[i] = random.Next(0, numClusters); // other assignments random
            return clustering;
        }

        private static double[][] Allocate(int numClusters, int numColumns)
        {
            // convenience matrix allocator for Cluster()
            var result = new double[numClusters][];
            for (var k = 0; k < numClusters; ++k)
                result[k] = new double[numColumns];
            return result;
        }

        private static bool UpdateMeans(double[][] data, int[] clustering, double[][] means)
        {
            // returns false if there is a cluster that has no tuples assigned to it
            // parameter means[][] is really a ref parameter

            // check existing cluster counts
            // can omit this check if InitClustering and UpdateClustering
            // both guarantee at least one tuple in each cluster (usually true)
            var numClusters = means.Length;
            var clusterCounts = new int[numClusters];
            for (var i = 0; i < data.Length; ++i)
            {
                var cluster = clustering[i];
                ++clusterCounts[cluster];
            }

            for (var k = 0; k < numClusters; ++k)
                if (clusterCounts[k] == 0)
                    return false; // bad clustering. no change to means[][]

            // update, zero-out means so it can be used as scratch matrix 
            for (var k = 0; k < means.Length; ++k)
            for (var j = 0; j < means[k].Length; ++j)
                means[k][j] = 0.0;

            for (var i = 0; i < data.Length; ++i)
            {
                var cluster = clustering[i];
                for (var j = 0; j < data[i].Length; ++j)
                    means[cluster][j] += data[i][j]; // accumulate sum
            }

            for (var k = 0; k < means.Length; ++k)
            for (var j = 0; j < means[k].Length; ++j)
                means[k][j] /= clusterCounts[k]; // danger of div by 0
            return true;
        }

        private static bool UpdateClustering(double[][] data, int[] clustering, double[][] means)
        {
            // (re)assign each tuple to a cluster (closest mean)
            // returns false if no tuple assignments change OR
            // if the reassignment would result in a clustering where
            // one or more clusters have no tuples.

            var numClusters = means.Length;
            var changed = false;

            var newClustering = new int[clustering.Length]; // proposed result
            Array.Copy(clustering, newClustering, clustering.Length);

            var distances = new double[numClusters]; // distances from curr tuple to each mean

            for (var i = 0; i < data.Length; ++i) // walk thru each tuple
            {
                for (var k = 0; k < numClusters; ++k)
                    distances[k] = Distance(data[i], means[k]); // compute distances from curr tuple to all k means

                var newClusterID = MinIndex(distances); // find closest mean ID
                if (newClusterID != newClustering[i])
                {
                    changed = true;
                    newClustering[i] = newClusterID; // update
                }
            }

            if (changed == false)
                return false; // no change so bail and don't update clustering[][]

            // check proposed clustering[] cluster counts
            var clusterCounts = new int[numClusters];
            for (var i = 0; i < data.Length; ++i)
            {
                var cluster = newClustering[i];
                ++clusterCounts[cluster];
            }

            for (var k = 0; k < numClusters; ++k)
                if (clusterCounts[k] == 0)
                    return false; // bad clustering. no change to clustering[][]

            Array.Copy(newClustering, clustering, newClustering.Length); // update
            return true; // good clustering and at least one change
        }

        private static double Distance(double[] tuple, double[] mean)
        {
            // Euclidean distance between two vectors for UpdateClustering()
            // consider alternatives such as Manhattan distance
            var sumSquaredDiffs = 0.0;
            for (var j = 0; j < tuple.Length; ++j)
                sumSquaredDiffs += Math.Pow((tuple[j] - mean[j]), 2);
            return Math.Sqrt(sumSquaredDiffs);
        }

        private static int MinIndex(double[] distances)
        {
            // index of smallest value in array
            // helper for UpdateClustering()
            var indexOfMin = 0;
            var smallDist = distances[0];
            for (var k = 0; k < distances.Length; ++k)
            {
                if (distances[k] < smallDist)
                {
                    smallDist = distances[k];
                    indexOfMin = k;
                }
            }
            return indexOfMin;
        }

        static double[][] AveragePoints(double[][] data, int[] clustering, int numClusters)
        {
            var temp = new List<double[]>();
            var points = new double[numClusters][];
            for (var k = 0; k < numClusters; ++k)
            {
                temp.Clear();
                for (var i = 0; i < data.Length; ++i)
                {
                    var clusterId = clustering[i];
                    if (clusterId != k) continue;
                    temp.Add(new[]{
                        data[i][0],data[i][1]
                    });
                }
                points[k] = new[]
                {
                    temp.Average(p => p[0]),
                    temp.Average(p => p[1]),
                };
            } // k
            return points;
        }




    }
} // ns
