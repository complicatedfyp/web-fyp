﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace FYPWeb.Services.Util
{
    public class UtilService
    {
        public static int GetAge(DateTime reference, DateTime birthday)
        {
            var age = reference.Year - birthday.Year;
            if (reference < birthday.AddYears(age)) age--;
            return age;
        }
        public static string EncryptPassword(string password)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            md5.ComputeHash(Encoding.ASCII.GetBytes(password));
            var result = md5.Hash;
            var strBuilder = new StringBuilder();
            foreach (var t in result)
            {
                strBuilder.Append(t.ToString("x2"));
            }
            return strBuilder.ToString().ToUpper();
        }

        

        public const string DefaultImagePath = "/Assets/Assets/img/logo-64.png";
        public const string DefaultUserImagePath = "/Assets/Assets/img/user-icon.png";
        public const string DefaultPharmacistImagePath = "/Assets/Assets/img/pharmacist-icon.png";
        public static string RandomString()
        {
            using (var rng = new RNGCryptoServiceProvider())
            {
                var data = new byte[8];
                rng.GetBytes(data);
                return BitConverter.ToString(data);
            }
        }
    }
}
