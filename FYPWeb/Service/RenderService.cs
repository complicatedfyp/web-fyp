﻿using System.IO;
using System.Web;
using System.Web.Mvc;
using RazorEngine;
using RazorEngine.Templating;

namespace FYPWeb.Service
{
    public class RenderService
    {
        public const string AdminDashboardUserTableRow = "~/Views/Template/AdminDashboardUserTableRow.cshtml";
        public const string AdminDashboardCategoryTableRow = "~/Views/Template/AdminDashboardCategoryTableRow.cshtml";
        public const string AdminDashboardCompanyTableRow = "~/Views/Template/AdminDashboardCompanyTableRow.cshtml";
        public const string AdminDashboardBrandTableRow = "~/Views/Template/AdminDashboardBrandTableRow.cshtml";
        public const string AdminDashboardDiseaseTableRow = "~/Views/Template/AdminDashboardDiseaseTableRow.cshtml";
        public const string AdminDashboardMedicineTableRow = "~/Views/Template/AdminDashboardMedicineTableRow.cshtml";
        public const string AdminDashboardMedicineDetailTableRow = "~/Views/Template/AdminDashboardMedicineDetailTableRow.cshtml";
        public const string PostThreadTableRow = "~/Views/Template/PostThreadTableRow.cshtml";
        public const string PostThreadReplyTableRow = "~/Views/Template/PostThreadReplyTableRow.cshtml";
        public const string PharmacistDashboardPharmacyTableRow = "~/Views/Template/PharmacistDashboardPharmacyTableRow.cshtml";
        public const string PharmacistDashboardEmployeeTableRow = "~/Views/Template/PharmacistDashboardEmployeeTableRow.cshtml";
        public const string PharmacistDashboardInventoryTableRow = "~/Views/Template/PharmacistDashboardInventoryTableRow.cshtml";
        public const string PharmacistDashboardInventoryTrackTableRow = "~/Views/Template/PharmacistDashboardInventoryTrackTableRow.cshtml";
        public const string PharmacistDashboardPromotionTableRow = "~/Views/Template/PharmacistDashboardPromotionTableRow.cshtml";
        public const string SelectLocationForm = "~/Views/Template/SelectLocationForm.cshtml";
        public const string RejectOrderForm = "~/Views/Template/RejectOrderForm.cshtml";
        public const string PrescriptionImageProcessForm = "~/Views/Template/PrescriptionImageProcessForm.cshtml";
        public const string ForumPostThreadRow = "~/Views/Template/ForumPostThreadRow.cshtml";
        public const string ForumCategoryRow = "~/Views/Template/ForumCategoryRow.cshtml";
        public const string CompleteOrderForm = "~/Views/Template/CompleteOrderForm.cshtml";
        public const string AcceptOrderForm = "~/Views/Template/AcceptOrderForm.cshtml";
        public const string ProcessLocationForm = "~/Views/Template/ProcessLocationForm.cshtml";
        public const string AddToCartForm = "~/Views/Template/AddToCartForm.cshtml";
        public const string PrescriptionOrderSetMedicineTableRow = "~/Views/Template/PrescriptionOrderSetMedicineTableRow.cshtml";
        public const string OrderDetailForm = "~/Views/Template/OrderDetailForm.cshtml";
        public const string CancelOrderForm = "~/Views/Template/CancelOrderForm.cshtml";
        public const string RecommendationForm = "~/Views/Template/RecommendationForm.cshtml";
        public const string AddToInventoryForm = "~/Views/Template/AddToInventoryForm.cshtml";
        public const string RemoveFromInventoryForm = "~/Views/Template/RemoveFromInventoryForm.cshtml";
        public const string PrescriptionSetMedicineForm = "~/Views/Template/PrescriptionSetMedicineForm.cshtml";
        public const string SetPriceForm = "~/Views/Template/SetPriceForm.cshtml";
        public const string MedicineReviewTableRow = "~/Views/Template/MedicineReviewTableRow.cshtml";
        public const string PostThreadReply = "~/Views/Template/PostThreadReply.cshtml";
        public const string UserEditForm = "~/Views/Template/UserEditForm.cshtml";
        public const string PromotionAddForm = "~/Views/Template/PromotionAddForm.cshtml";
        public const string PromotionStartForm = "~/Views/Template/PromotionStartForm.cshtml";
        public const string MapForm = "~/Views/Template/MapForm.cshtml";
        public const string OrderPlaceForm = "~/Views/Template/OrderPlaceForm.cshtml";
        public const string OrderPlacePrescriptionForm = "~/Views/Template/OrderPlacePrescriptionForm.cshtml";
        public const string CompanyAddForm = "~/Views/Template/CompanyAddForm.cshtml";
        public const string InventoryAddForm = "~/Views/Template/InventoryAddForm.cshtml";
        public const string EmployeeAddForm = "~/Views/Template/EmployeeAddForm.cshtml";
        public const string EmployeeAssignForm = "~/Views/Template/EmployeeAssignForm.cshtml";
        public const string ReviewForm = "~/Views/Template/ReviewForm.cshtml";
        public const string ReportForm = "~/Views/Template/ReportForm.cshtml";
        public const string NotificationItem = "~/Views/Template/NotificationItem.cshtml";
        public const string ReviewOrderForm = "~/Views/Template/ReviewOrderForm.cshtml";
        public const string GeneratedReportTable = "~/Views/Template/GeneratedReportTable.cshtml";
        public const string CustomerReviewOrderForm = "~/Views/Template/CustomerReviewOrderForm.cshtml";
        public const string PharmacyReviewOrderForm = "~/Views/Template/PharmacyReviewOrderForm.cshtml";
        public const string CartForm = "~/Views/Template/CartForm.cshtml";
        public const string PharmacyAddForm = "~/Views/Template/PharmacyAddForm.cshtml";
        public const string BrandAddForm = "~/Views/Template/BrandAddForm.cshtml";
        public const string CategoryAddForm = "~/Views/Template/CategoryAddForm.cshtml";
        public const string MedicineAddForm = "~/Views/Template/MedicineAddForm.cshtml";
        public const string MedicineDetailAddForm = "~/Views/Template/MedicineDetailAddForm.cshtml";
        public const string DiseaseAddForm = "~/Views/Template/DiseaseAddForm.cshtml";
        public const string DiseaseShowForm = "~/Views/Template/DiseaseShowForm.cshtml";
        public const string FileUploadForm = "~/Views/Template/FileUploadForm.cshtml";
        public const string MultiFileUploadForm = "~/Views/Template/MultiFileUploadForm.cshtml";

        public static void Compile(string viewPath)
        {
            var key = viewPath;
            if (Engine.Razor.IsTemplateCached(key, null)) return;
            var viewAbsolutePath = HttpContext.Current.Server.MapPath(viewPath);
            if (viewAbsolutePath == null) return;
            var viewSource = File.ReadAllText(viewAbsolutePath);
            Engine.Razor.Compile(viewSource,key);
        }
        public static string Run(string key, object model)
        {
            return !Engine.Razor.IsTemplateCached(key, null) ? "" : Engine.Razor.Run(key, null, model);
        }
    }
    public class FakeController : ControllerBase { protected override void ExecuteCore() { } }

}