﻿using System.CodeDom;
using System.Web;

namespace FYPWeb.Service
{
    public class SessionService
    {
        private const string UserIdKey = "UserId";
        private const string UserRoleIdKey = "UserRoleId";
        private const string UserRoleKey = "UserRole";
        private const string UserNameKey = "Username";
        private const string UserImageKey = "UserImage";
        private const string UserFullNameKey = "UserFullName";
        private const string SessionKey = "Session";

        public const int AdminId = 1;

        public static void ClearSession()
        {
            HttpContext.Current.Session.Abandon();
        }

        public static bool IsAuth => string.IsNullOrEmpty(UserId);

        public static string SessionId
        {
            get
            {
                if (string.IsNullOrEmpty(HttpContext.Current.Session[SessionKey] + ""))
                {
                    HttpContext.Current.Session[SessionKey] = HttpContext.Current.Session.SessionID;
                }
                return (HttpContext.Current.Session[SessionKey] ?? "").ToString();
            }
            set
            {
                HttpContext.Current.Session[SessionKey] = value;
            }
        }
        public static string UserId
        {
            get { return (HttpContext.Current.Session[UserIdKey] ?? "").ToString(); }
            set { HttpContext.Current.Session[UserIdKey] = value; }
        }
        public static string UserRole
        {
            get { return (HttpContext.Current.Session[UserRoleKey] ?? "").ToString(); }
            set { HttpContext.Current.Session[UserRoleKey] = value; }
        }
        public static string UserRoleId
        {
            get { return (HttpContext.Current.Session[UserRoleIdKey] ?? "").ToString(); }
            set { HttpContext.Current.Session[UserRoleIdKey] = value; }
        }

        public static string UserImage
        {
            get { return (HttpContext.Current.Session[UserImageKey] ?? "").ToString(); }
            set { HttpContext.Current.Session[UserImageKey] = value; }
        }

        public static string UserName
        {
            get { return (HttpContext.Current.Session[UserNameKey] ?? "").ToString(); }
            set { HttpContext.Current.Session[UserNameKey] = value; }
        }

        public static string FullName
        {
            get { return (HttpContext.Current.Session[UserFullNameKey] ?? "").ToString(); }
            set { HttpContext.Current.Session[UserFullNameKey] = value; }
        }

        public static string GetClientId()
        {
            var id = UserId;
            var role = UserRole;
            var clientId = "";
            clientId = UserName + "_" + role + "_" + id;
            return clientId;
        }

    }
}