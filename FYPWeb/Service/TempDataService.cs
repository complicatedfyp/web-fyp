﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FYPWeb.Service
{
    public class TempDataService<T>
    {
        public static void SetData(Controller controller,string key, T data)
        {
            controller.TempData[key] = data;
        }

        public static T GetData(Controller controller, string key)
        {
            return (T)controller.TempData[key];
        }
    }
}