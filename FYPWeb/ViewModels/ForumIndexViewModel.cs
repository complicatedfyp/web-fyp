﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Web;
using Model.Entity;

namespace FYPWeb.ViewModels
{
    public class ForumIndexViewModel
    {
        public ForumHeaderViewModel Header { get; set; }
        public IEnumerable<PostCategory> Categories { get; set; }
        public IEnumerable<PostThread> PostThreads { get; set; }
        public IEnumerable<PostThread> HotPostThreads { get; set; }
    }

    public class ForumAllThreadPostViewModel
    {
        public ForumHeaderViewModel Header { get; set; }
        public int Page { get; set; }
        public string Tag { get; set; }
        public string Category { get; set; }
        public int PageCount { get; set; }
        public string Parameter { get; set; }
        public IEnumerable<PostThread> PostThreads { get; set; }
    }

    public class ForumAllCategoriesViewModel
    {
        public ForumHeaderViewModel Header { get; set; }
        public int Page { get; set; }
        public int PageCount { get; set; }
        public string Parameter { get; set; }
        public IEnumerable<PostCategory> PostCategories { get; set; }
    }
    public class ForumAllTagsViewModel
    {
        public ForumHeaderViewModel Header { get; set; }
        public int Page { get; set; }
        public int PageCount { get; set; }
        public string Parameter { get; set; }
        public IEnumerable<Tag> Tags { get; set; }
    }

    public class ForumReplyViewModel
    {
        public int Age { get; set; }
        public DateTime PostedAt { get; set; }
        public DateTime JoinedDate { get; set; }
        public string Name { get; set; }
        public string Reply { get; set; }
        public string Signature { get; set; }
        public string ImagePath { get; set; }
        public string Gender { get; set; }
        public bool IsMine { get; set; }
        public int Id { get; set; }
    }
    public class ForumPostThreadViewModel
    {
        public ForumHeaderViewModel Header { get; set; }
        public PostThread PostThread { get; set; }
        public IEnumerable<Tag> Tags { get; set; }
        public int Page { get; set; }
        public int CurrentPage { get; set; }
    }
    public class ForumCreatePostViewModel
    {
        public ForumHeaderViewModel Header { get; set; }
        public IEnumerable<PostCategory> PostCategories { get; set; }
    }
    public class ForumEditPostViewModel
    {
        public ForumHeaderViewModel Header { get; set; }
        public IEnumerable<PostCategory> PostCategories { get; set; }
        public PostThread PostThread { get; set; }
    }
    public class ForumReplyPostFormViewModel
    {
        public ForumHeaderViewModel Header { get; set; }
        public PostThread PostThread { get; set; }
    }
    public class ForumEditReplyPostFormViewModel
    {
        public ForumHeaderViewModel Header { get; set; }
        public PostThread PostThread { get; set; }
        public PostReply PostReply { get; set; }
    }

}