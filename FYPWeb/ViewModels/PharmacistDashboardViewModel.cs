﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FYPWeb.Repository.Repository;
using FYPWeb.Repository.RepositoryViewModel;
using Model.Entity;

namespace FYPWeb.ViewModels
{
    public class PharmacistDashboardViewModel
    {
        public PharmacistHeaderViewModel PharmacistHeaderViewModel { get; set; }
        public IEnumerable<Order> ActiveOrders { get; set; }
        public IEnumerable<OrderRepository.StringValue> StringValues { get; set; }
        public IEnumerable<OrderRepository.StringValue> EmployeeStringValues { get; set; }
        public IEnumerable<Order> PendingOrders { get; set; }
    }
    public class PharmacistDashboardRelocationSuggestionViewModel
    {
        public PharmacistHeaderViewModel PharmacistHeaderViewModel { get; set; }
    }



    public class PharmacistDashboardOrderReviewViewModel
    {
        public PharmacistHeaderViewModel PharmacistHeaderViewModel { get; set; }
        public int Page { get; set; }
        public int PageCount { get; set; }
        public IEnumerable<PharmacyReview> PharmacyReviews { get; set; }
        public string Parameter { get; set; }
    }
    public class PharmacistDashboardForumViewModel
    {
        public PharmacistHeaderViewModel PharmacistHeaderViewModel { get; set; }
    }
    public class PharmacistDashboardProfileViewModel
    {
        public PharmacistHeaderViewModel PharmacistHeaderViewModel { get; set; }
        public User User { get; set; }
    }
    public class PharmacistDashboardReplyViewModel
    {
        public PharmacistHeaderViewModel PharmacistHeaderViewModel { get; set; }
    }
    public class PharmacistDashboardPromotionViewModel
    {
        public PharmacistHeaderViewModel PharmacistHeaderViewModel { get; set; }
    }
    public class PharmacistDashboardOrderViewModel
    {
        public PharmacistHeaderViewModel PharmacistHeaderViewModel { get; set; }
        public IEnumerable<Order> Orders { get; set; }
        public int Page { get; set; }
        public string Type { get; set; }
        public int PageCount { get; set; }
        public string Parameter { get; set; }
    }
    public class PharmacistDashboardNotificationViewModel
    {
        public PharmacistHeaderViewModel PharmacistHeaderViewModel { get; set; }
        public IEnumerable<Notification> Notifications { get; set; }
        public int Page { get; set; }
        public string Type { get; set; }
        public int PageCount { get; set; }
        public string Parameter { get; set; }
    }
    public class PharmacistDashboardRecommendationViewModel
    {
        public PharmacistHeaderViewModel PharmacistHeaderViewModel { get; set; }
        public IEnumerable<Recommendation> Recommendations { get; set; }
        public int Page { get; set; }
        public string Type { get; set; }
        public int PageCount { get; set; }
        public string Parameter { get; set; }
    }
    public class PharmacistDashboardReportViewModel
    {
        public PharmacistHeaderViewModel PharmacistHeaderViewModel { get; set; }
        public bool HasPharmacies { get; set; }
    }
    public class PharmacistDashboardEmployeeViewModel
    {
        public PharmacistHeaderViewModel PharmacistHeaderViewModel { get; set; }
    }
    public class PharmacistDashboardReviewViewModel
    {
        public PharmacistHeaderViewModel PharmacistHeaderViewModel { get; set; }
    }

    public class PharmacistDashboardInventoryViewModel
    {
        public PharmacistHeaderViewModel PharmacistHeaderViewModel { get; set; }
        public IEnumerable<Pharmacy> Pharmacies { get; set; }
    }

    public class PharmacistDashboardPharmacyViewModel
    {
        public PharmacistHeaderViewModel PharmacistHeaderViewModel { get; set; }
    }
}