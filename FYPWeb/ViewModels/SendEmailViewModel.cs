﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FYPWeb.ViewModels
{
    public class SendEmailViewModel
    {
        public string Email { get; set; }
        public string Name { get; set; }
    }
}