﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Model.Entity;

namespace FYPWeb.ViewModels
{
    
    public class SearchIndexViewModel
    {
        public HomeHeaderViewModel HeaderViewModel { get; set; }
        public IEnumerable<Company> Companies { get; set; }
        public IEnumerable<Drug> Drugs { get; set; }
        public IEnumerable<Brand> Brands { get; set; }
        public IEnumerable<Disease> Diseases { get; set; }
        public string Query { get; set; }
        public int Page { get; set; }
        public int TotalCount { get; set; }
        public SearchType Type { get; set; }
    }

    public enum SearchType
    {
        Brand = 0,
        Drug = 1,
        Disease = 2,
        Company = 3,
        Categories = 4,
        Posts = 5,
        Tags = 6,
        Users = 7,
        Pharmacies = 8,
    }
}