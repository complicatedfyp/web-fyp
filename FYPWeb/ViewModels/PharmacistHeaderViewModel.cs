﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FYPWeb.ViewModels
{
    public class PharmacistHeaderViewModel
    {
        public PharmacistHeader Header { get; set; }    
    }

    public enum PharmacistHeader
    {
        Profile,
        Report,
        Posts,
        PostReplies,
        Employee,
        Pharmacy,
        Stock,
        MedicineReviews,
        OrderPending,
        OrderRejected,
        OrderCancelled,
        OrderCompleted,
        OrderActive,
        OrderReviews,
        PromotionActive,
        PromotionExpired,
        TrackStock,
        TicketActive,
        TicketResolved,
        RecommendationSent,
        RecommendationRecieved,
        Dashboard,
        ChangeLocation
    }
}