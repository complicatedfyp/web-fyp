﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FYPWeb.Repository.Repository;
using Model.Entity;

namespace FYPWeb.ViewModels
{
    public class CustomerDashboardViewModel
    {
        public CustomerHeaderViewModel HeaderViewModel { get; set; }
        public IEnumerable<Promotion> Promotions { get; set; }
        public IEnumerable<Order> ActiveOrders { get; set; }
        public IEnumerable<DiseaseRepository.DiseaseCount> Diseases { get; set; }
        public IEnumerable<OrderRepository.StringValue> StringValues { get; set; }
    }
    public class CustomerDashboardReviewViewModel
    {
        public CustomerHeaderViewModel HeaderViewModel { get; set; }
    }
    public class CustomerDashboardOrderReviewViewModel
    {
        public CustomerHeaderViewModel HeaderViewModel { get; set; }
        public IEnumerable<CustomerReview> CustomerReviews { get; set; }
        public int Page { get; set; }
        public int PageCount { get; set; }
        public string Parameter { get; set; }
    }
    public class CustomerDashboardNotificationViewModel
    {
        public CustomerHeaderViewModel HeaderViewModel { get; set; }
        public int Page { get; set; }
        public IEnumerable<Notification> Notifications { get; set; }
        public int PageCount { get; set; }
        public string Parameter { get; set; }
        public string Type { get; set; }
    }
    public class CustomerDashboardPostViewModel
    {
        public CustomerHeaderViewModel HeaderViewModel { get; set; }
    }
    public class AdminDashboardPostViewModel
    {
        public AdminHeaderViewModel HeaderViewModel { get; set; }
    }
    public class CustomerDashboardOrderViewModel
    {
        public CustomerHeaderViewModel HeaderViewModel { get; set; }
        public IEnumerable<Order> Orders { get; set; }
        public int Page { get; set; }
        public string Type { get; set; }
        public int PageCount { get; set; }
        public string Parameter { get; set; }
    }
    public class CustomerDashboardRecommendationViewModel
    {
        public CustomerHeaderViewModel HeaderViewModel { get; set; }
        public IEnumerable<Recommendation> Recommendations { get; set; }
        public int Page { get; set; }
        public string Type { get; set; }
        public int PageCount { get; set; }
        public string Parameter { get; set; }
    }
    public class CustomerDashboardReplyViewModel
    {
        public CustomerHeaderViewModel HeaderViewModel { get; set; }
    }
    public class CustomerDashboardProfileViewModel
    {
        public CustomerHeaderViewModel HeaderViewModel { get; set; }
        public User User { get; set; }
    }

    public class CustomerDashboardPrescriptionViewModel
    {
        public CustomerHeaderViewModel HeaderViewModel { get; set; }
        public IEnumerable<Prescription> Prescriptions { get; set; }
    }
}