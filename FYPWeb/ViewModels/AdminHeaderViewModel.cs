﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Model.Entity;

namespace FYPWeb.ViewModels
{
    public class AdminHeaderViewModel
    {
        public AdminSideBar SiderBar;
    }

    public enum AdminSideBar
    {
        Dashboard,
        ForumCategory,
        Profile,
        UserActive,
        UserInActive,
        Post,
        PharmacyPending,
        PharmacyApproved,
        PharmacyRejected,
        Medicines,
        MedicineDetails,
        Company,
        Brands,
        Diseases,
        TicketsActive,
        Settings,
        TicketsInActive,
    }
}