﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Model.Entity;

namespace FYPWeb.ViewModels
{
    public class HomeIndexViewModel
    {
        public HomeHeaderViewModel HeaderViewModel { get; set; }
        public IEnumerable<Disease> Diseases { get; set; }
        public IEnumerable<Drug> RecommendedDrugs { get; set; }
        public IEnumerable<Drug> MostVisitedDrugs { get; set; }
        public IEnumerable<Drug> MostSoldDrugs { get; set; }
    }
    public class HomeMessageViewModel
    {
        public HomeHeaderViewModel HeaderViewModel { get; set; }
        public IEnumerable<Order> Orders { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public int PageCount { get; set; }
    }
    public class HomeChatViewModel
    {
        public HomeHeaderViewModel HeaderViewModel { get; set; }
        public int OrderId { get; set; }
        public int RId { get; set; }
    }

    public class DiseaseProfileViewModel
    {
        public HomeHeaderViewModel HeaderViewModel { get; set; }
        public Disease Disease { get; set; }
        public IEnumerable<Drug> IndicationDrugs { get; set; }
        public int IndPage { get; set; }
        public int Id { get; set; }
        public int IndPageCount { get; set; }
        public IEnumerable<Drug> NonIndicationDrugs { get; set; }
        public int ConPage { get; set; }
        public int ConPageCount { get; set; }
    }
    public class CompanyProfileViewModel
    {
        public HomeHeaderViewModel HeaderViewModel { get; set; }
        public Company Company { get; set; }
    }
    public class BrandProfileViewModel
    {
        public HomeHeaderViewModel HeaderViewModel { get; set; }
        public Brand Brand { get; set; }
    }
    public class PlaceOrderViewModel
    {
        public HomeHeaderViewModel HeaderViewModel { get; set; }
        public IEnumerable<DrugCart> DrugCarts { get; set; }
        public IEnumerable<Prescription> Prescriptions { get; set; }
    }
    public class PlaceOrderPrescriptionViewModel
    {
        public HomeHeaderViewModel HeaderViewModel { get; set; }
        public IEnumerable<Pharmacy> Pharmacies { get; set; }
        public IEnumerable<Prescription> Prescriptions { get; set; }
    }

    public class HomeProfileViewModel
    {
        public HomeHeaderViewModel HeaderViewModel { get; set; }
        public User User { get; set; }
        public IEnumerable<CustomerReview> CustomerReviews { get; set; }
        public IEnumerable<PharmacyReview> PharmacyReviews { get; set; }
        public IEnumerable<InventoryTrack> InventoryTracks { get; set; }
        public int Page { get; set; }
        public int PageCount { get; set; }
        public bool IsMore { get; set; }
        public Pharmacy Pharmacy { get; set; }
    }

    public class AllMedicinesViewModal
    {
        public string Parameter { get; set; }
        public HomeHeaderViewModel HeaderViewModel { get; set; }
        public IEnumerable<InventoryTrack> CustomerDrugs { get; set; }
        public IEnumerable<Drug> PharmacyDrugs { get; set; }
        public int Page { get; set; }
        public int BrandId { get; set; }
        public int PharmacyId { get; set; }
        public int Min { get; set; }
        public int Max { get; set; }
        public string Form { get; set; }
        public int Rating { get; set; }
        public int PageCount { get; set; }
    }
    public class AllUsersViewModal
    {
        public string Parameter { get; set; }
        public HomeHeaderViewModel HeaderViewModel { get; set; }
        public IEnumerable<User> Users { get; set; }
        public int Page { get; set; }
        public int PageCount { get; set; }
    }
    public class AllPharmaciesViewModal
    {
        public string Parameter { get; set; }
        public HomeHeaderViewModel HeaderViewModel { get; set; }
        public IEnumerable<Pharmacy> Pharmacies { get; set; }
        public int Page { get; set; }
        public int PageCount { get; set; }
    }
    public class PharmaciesNearYouViewModal
    {
        public HomeHeaderViewModel HeaderViewModel { get; set; }
    }
    public class AllBrandsViewModal
    {
        public string Parameter { get; set; }
        public HomeHeaderViewModel HeaderViewModel { get; set; }
        public IEnumerable<Brand> Brands { get; set; }
        public int Page { get; set; }
        public int PageCount { get; set; }
    }

    public class AllCompanyViewModal
    {
        public string Parameter { get; set; }
        public HomeHeaderViewModel HeaderViewModel { get; set; }
        public IEnumerable<Company> Companies { get; set; }
        public int Page { get; set; }
        public int PageCount { get; set; }
    }

    public class AllDiseaseViewModel
    {
        public HomeHeaderViewModel HeaderViewModel { get; set; }
        public IEnumerable<Disease> Diseases { get; set; }
        public int Page { get; set; }
        public int PageCount { get; set; }
    }

    public class HomeCartViewModel
    {
        public HomeHeaderViewModel HeaderViewModel { get; set; }
        public IEnumerable<DrugCart> DrugCarts { get; set; }
        public IEnumerable<Pharmacy> Pharmacies { get; set; }
    }
    public class HomeMedicineViewModel
    {
        public HomeHeaderViewModel HeaderViewModel { get; set; }
        public Drug Drug { get; set; }
        public bool IsReviewed { get; set; }
        public int ReviewPageCount { get; set; }
        public int TotalReview { get; set; }
        public float Rating { get; set; }
        public IEnumerable<DrugReview> DrugReviews { get; set; }
        public IEnumerable<Pharmacy> Pharmacies { get; set; }
        public IEnumerable<InventoryTrack> Prices { get; set; }
        public int RecommendationCount { get; set; }
    }

    
}
