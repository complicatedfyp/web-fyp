﻿using System;
using System.Collections.Generic;
using Model.Entity;

namespace FYPWeb.ViewModels
{
    public class AdminDashboardDiseaseTableRowViewModel
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public string Description { get; set; }
    }
    public class PrescriptionOrderSetMedicineTableRowViewModel
    {
        public IEnumerable<OrderDetail> OrderDetails { get; set; }
    }   
    public class PharmacistPromotionFormViewModel
    {
        public int Id { get; set; }
        public bool IsAdmin { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int PharmacyId { get; set; }
        public IEnumerable<Pharmacy> Pharmacies { get; set; }
        public float PercentOff { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

    public class ForumPostThreadRowViewModel
    {
        public PostThread PostThread { get; set; }
    }
    public class ForumCategoryRowViewModel
    {
        public PostCategory PostCategory { get; set; }
    }

    public class NotificationItemViewModel
    {
        public int Id { get; set; }
        public string Body { get; set; }
        public string Url { get; set; }
        public int Priority { get; set; }
        public DateTime Date { get; set; }
    }


    public class PharmacistPromotionStartFormViewModel
    {
        public int Id { get; set; }
        public bool IsAdmin { get; set; }
        public int PharmacyId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

    public class HomeMedicineCartFormViewModel
    {
        public int Id { get; set; }
        public string SessionId { get; set; }
        public int UserId { get; set; }
        public int InventoryId { get; set; }
        public int Quantity { get; set; }
    }
    public class HomeMedicineReviewItemViewModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int UserName { get; set; }
        public DateTime PostedAt { get; set; }
        public string ImageLink { get; set; }
        public string Review { get; set; }
        public string Rating { get; set; }
    }
    public class PharmacistEmployeeAddFormViewModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public float Salary { get; set; }
        public string Address { get; set; }
        public string ContactNumber { get; set; }
        public float DutyHours { get; set; }
    }
    public class PharmacistEmployeeAssignFormViewModel
    {
        public int EmployeeId { get; set; }
        public int UserId { get; set; }
        public int PharamcyId { get; set; }
        public IEnumerable<Pharmacy> Pharmacies { get; set; }
    }
    public class CustomerPharmacyReviewFormViewModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int PharmacyId { get; set; }
        public int OrderId { get; set; }
        public string Rating { get; set; }
        public string Review { get; set; }
    }
    public class PharmacistCustomerReviewFormViewModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int PharmacyId { get; set; }
        public int OrderId { get; set; }
        public string Rating { get; set; }
        public string Review { get; set; }
    }

    public class GeneratedReportTableViewModel
    {
        public IEnumerable<Order> Orders { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public bool IsAll { get; set; }
        public string[] EmployeeNames { get; set; } 
    }

    public class ReviewOrderFormViewModel
    {
        public int Id { get; set; }
        public Order Order { get; set; }
    }
    public class HomeMedicineReviewFormViewModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int MedicineId { get; set; }
        public string Rating { get; set; }
        public string Review { get; set; }
    }
    public class AdminDiseaseShowFormViewModel
    {
        public IEnumerable<Disease> Diseases { get; set; }
        public int Id { get; set; }
    }


    public class PharmacistDashboardEmployeeTableRowViewModel
    {
        public bool IsAdmin { get; set; }
        public int Id { get; set; }
        public string Address { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public string ImageLink { get; set; }
        public int UserId { get; set; }
        public string Pharmacy { get; set; }
    }

    public class SetPriceViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShopName { get; set; }
        public float Price { get; set; }
    }
    public class PharmacistAddInventoryViewModel
    {
        public int Id { get; set; }
        public int PharmacyId { get; set; }
        public int DrugId { get; set; }
        public int Quantity { get; set; }
        public IEnumerable<Pharmacy> Pharmacies { get; set; }
    }
    public class PharmacistDashboardPharmacyTableRowViewModel
    {
        public bool IsApproved { get; set; }
        public bool IsAdmin { get; set; }
        public int Id { get; set; }
        public string Address { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public string ContactPerson { get; set; }
        public string ImageLink { get; set; }
        public string X { get; set; }
        public string Y { get; set; }
        public int UserId { get; set; }
    }

    public class PharmacistDashboardPromotionTableRow
    {
        public bool IsAdmin { get; set; }
        public string Code { get; set; }
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public float PercentOff { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int UserId { get; set; }
        public bool IsActive { get; set; }
    }
    public class SelectLocationFormViewModel
    {
        public string Address { get; set; }
        public string X { get; set; }
        public string Y { get; set; }
    }

    public class PharmacistPharmacyAddFormViewModel
    {
        public int UserId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string ContactPerson { get; set; }
        public float Charges { get; set; }
        public string Address { get; set; }
        public string X { get; set; }
        public string Y { get; set; }
        public string PhoneNumber { get; set; }
    }

    public class AdminMedicineDetailAddFormViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Overview { get; set; }
        public string ImageLink { get; set; }
        public string DrugDiseasesIndication { get; set; }
        public string DrugDiseasesContradiction { get; set; }
    }
    public class AdminDashboardMedicineDetailTableRowViewModel
    {
        public int Id { get; set; }
        public string Overview { get; set; }
        public string ImageLink { get; set; }
        public IEnumerable<Disease> CureDiseases { get; set; }  
        public IEnumerable<Disease> NonCureDiseases { get; set; }
        public string Name { get; set; }
    }

    
    public class AdminDashboardUserTableRowViewModel
    {
        public string UserType { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Phone { get; set; }
        public bool IsActive { get; set; }
        public bool IsVerified { get; set; }
    }

    public class AdminDiseaseAddFormViewModel
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public string Description { get; set; }
    }

    public class AdminDashboardMedicineTableRowViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Brand { get; set; }
        public int BrandId { get; set; }
        public string Form { get; set; }
        public string Packing { get; set; }
        public string ImageLink { get; set; }
    }
    public class AdminDashboardCategoryTableRowViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int PostCount { get; set; }
    }
    public class DashboardPostTableRowViewModel
    {
        public string UserName { get; set; }
        public string OrignalPoster { get; set; }
        public int Id { get; set; }
        public DateTime PostedAt { get; set; }
        public string Subject { get; set; }
        public Tag[] Tags { get; set; }
        public string Category { get; set; }
        public int ReplyCount { get; set; }
    }
    public class DashboardPostReplyTableRowViewModel
    {
        public string UserName { get; set; }
        public int ThreadId { get; set; }
        public string OrignalPoster { get; set; }
        public int Id { get; set; }
        public string Subject { get; set; }
        public DateTime RepliedAt { get; set; }
    }
    public class AdminDashboardReviewTableRowViewModel
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Username { get; set; }
        public float Rating { get; set; }
        public string Review { get; set; }
        public int MedicineId { get; set; }
        public string MedicineName { get; set; }
        public DateTime PostedAt { get; set; }
    }

    public class AdminDashboardBrandTableRowViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImageLink { get; set; }
        public string CompanyName { get; set; }
        public int DrugCount { get; set; }
    }
    public class PharmacistDashboardInventoryTrackTableRowViewModel
    {
        public int Id { get; set; }
        public int? OrderId { get; set; }
        public DateTime PostedAt { get; set; }
        public int DrugId { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
    }
    public class PharmacistDashboardInventoryTableRowViewModel
    {
        public int Id { get; set; }
        public int DrugId { get; set; }
        public float Price { get; set; }
        public string Form { get; set; }
        public string Name { get; set; }
        public int TotalCount { get; set; }
    }

    public class AdminCategoryAddFormViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class MapFormViewModel
    {
        public int Id { get; set; }
        public string X { get; set; }
        public string Y { get; set; }
        public string Address { get; set; }
    }
    public class AdminBrandAddFormViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CompanyId { get; set; }
        public IEnumerable<Company> Companies { get; set; }
    }

    public class CancelOrderFormViewModel
    {
        public int OrderId { get; set; }
    }
    public class OrderDetailFormViewModel
    {
        public int OrderId { get; set; }
        public Order Order { get; set; }
    }

    public class PrescriptionSetMedicineFormViewModel
    {
        public int OrderId { get; set; }
        public IEnumerable<OrderPrescription> OrderPrescriptions { get; set; }
        public IEnumerable<OrderDetail> OrderDetails { get; set; }
        public int PharmacyId { get; set; }
    }
    public class RejectOrderFormViewModel
    {
        public int OrderId { get; set; }
    }
    public class ReportFormViewModel
    {
        public int UserId { get; set; }
        public int PharmacyId { get; set; }
        public IEnumerable<Employee> Employees { get; set; }
    }
    public class AcceptOrderFormViewModel
    {
        public int OrderId { get; set; }
        public IEnumerable<Employee> Employees { get; set; }
    }
    public class ProcessLocationFormViewModel
    {
        public IEnumerable<Pharmacy> Pharmacies { get; set; }
    }
    public class AddToCartFormViewModel
    {
        public IEnumerable<InventoryTrack> InventoryTracks { get; set; }
        public string SessionId { get; set; }
        public int UserId { get; set; }
        public int Count { get; set; }

    }
    public class OrderPlaceFormViewModel
    {
        public IEnumerable<InventoryTrack> InventoryTracks { get; set; }
        public IEnumerable<Pharmacy> Pharmacies { get; set; }
        public int UserId { get; set; }
    }
    public class OrderPlaceFormPrescriptionViewModel
    {
        public int UserId { get; set; }
        public Pharmacy Pharmacy { get; set; }
        public IEnumerable<Prescription> Prescriptions { get; set; }
    }
    public class PrescriptionImageProcessForm
    {
        public IEnumerable<string> Texts { get; set; }
        public Prescription Prescription { get; set; }
    }
    public class CompleteOrderFormViewModel
    {
        public int OrderId { get; set; }
        public Order Order { get; set; }
    }
    public class RemoveFromInventoryFormViewModel
    {
        public string PharmacyName { get; set; }
        public int Max { get; set; }
        public int MedicineId { get; set; }
        public string MedicineName { get; set; }
        public int Id { get; set; }
    }
    public class AddToInventoryFormViewModel
    {
        public IEnumerable<Pharmacy> Pharmacies { get; set; }
        public int UserId { get; set; }
        public float Price { get; set; }
        public int MedicineId { get; set; }
        public int Quantity { get; set; }
        public string MedicineName { get; set; }
    }

    public class RecommendationFormViewModel
    {
        public int UserId { get; set; }
        public int MedicineId { get; set; }
        public string MedicineName { get; set; }
    }

    public class AdminMedicineAddFormViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Packing { get; set; }
        public string Form { get; set; }
        public int ItemsInPack { get; set; }
    }
    public class AdminCompanyAddFormViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Fax { get; set; }
        public string Phone { get; set; }
        public string Link { get; set; }
        public string Address { get; set; }
        public int CityId { get; set; }
        public IEnumerable<City> Cities { get; set; }
    }

    public class FileUploadViewModel
    {
        public string ActionUrl { get; set; }
        public int Limit { get; set; }
        public int Id { get; set; }
    }
    public class AdminDashboardCompanyTableRowViewModel
    {
        public string ImageLink { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int Id { get; set; }
        public string Link { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Address { get; set; }
        public int BrandCount { get; set; }
        public string City { get; set; }
    }

    public class UserFormViewModel
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public int Type { get; set; }
        public DateTime DOB { get; set; }
        public string Address { get; set; }
        public string Password { get; set; }
        public string Gender { get; set; }
        public string Signature { get; set; }
        public int UserId { get; set; }
    }
}