﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FYPWeb.Controllers;

namespace FYPWeb.ViewModels
{
    public class LoginViewModel
    {
        public string Message { get; set; }
    }

    public class RegisterViewModel
    {
        public string Message { get; set; }
        public RegisterUserParameter Parameter { get; set; }
    }

}