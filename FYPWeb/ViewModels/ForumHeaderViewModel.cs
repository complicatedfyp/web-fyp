﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FYPWeb.ViewModels
{
    public class ForumHeaderViewModel
    {
        public HomeHeader Header { get; set; }
        public int CartCount { get; set; }
        public string Paramter { get; set; }
        public int Type { get; set; }
    }

    public enum ForumHeader
    {
        Forum,
        Category,
        None,
    }
}