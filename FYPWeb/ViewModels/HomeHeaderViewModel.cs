﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FYPWeb.ViewModels
{
    public class HomeHeaderViewModel
    {
        public HomeHeader Header { get; set; }
        public int CartCount { get; set; }
        public int NotificationCount { get; set; }
    }

    public enum HomeHeader
    {
        Home,
        AboutUs,
        ContactUs,
        AdvanceSearch,
        Tags,
        Categories,
        Forum,
        PharmacyNearMe,
        None
    }
}