﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FYPWeb.Repository.Repository;

namespace FYPWeb.ViewModels
{
    public class AdminIndexViewModel
    {
        public AdminHeaderViewModel AdminHeaderViewModel { get; set; }
        public IEnumerable<OrderRepository.StringValue> MostSold { get; set; }
        public IEnumerable<OrderRepository.StringValue> UsersPerMonth { get; set; }
        public IEnumerable<OrderRepository.StringValue> UsersRatioPharmacist { get; set; }
        public IEnumerable<OrderRepository.StringValue> MostActiveUser { get; set; }

    }

    public class AdminPharmacyViewModel
    {
        public AdminHeaderViewModel AdminHeaderViewModel { get; set; }
        public string Type { get; set; }
    }

    public class AdminUsersViewModel
    {
        public AdminHeaderViewModel AdminHeaderViewModel { get; set; }
    }
    public class AdminCompaniesViewModel
    {
        public AdminHeaderViewModel AdminHeaderViewModel { get; set; }
    }

    public class AdminMedicineDetailViewModel
    {
        public AdminHeaderViewModel AdminHeaderViewModel { get; set; }
    }
    public class AdminDiseaseViewModel
    {
        public AdminHeaderViewModel AdminHeaderViewModel { get; set; }
    }
    public class AdminBrandViewModel
    {
        public AdminHeaderViewModel AdminHeaderViewModel { get; set; }
    }

    public class AdminForumCateogryViewModel
    {
        public AdminHeaderViewModel AdminHeaderViewModel { get; set; }
    }

    public class AdminMedicineViewModel
    {
        public AdminHeaderViewModel AdminHeaderViewModel { get; set; }
    }
}