﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FYPWeb.ViewModels
{
    public class CustomerHeaderViewModel
    {
        public CustomerSiderBar SiderBar { get; set; }
    }

    public enum CustomerSiderBar
    {
        Profile,
        Dashboard,
        Posts,
        PostReplies,
        MedicineReview,
        OrderReview,
        OrderPending,
        OrderRejected,
        OrderCancelled,
        OrderCompleted,
        OrderActive,
        Prescription,
        RecommendationRecieved,
        RecommendationSent,
        MedicineCart,
    }
}