﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using FYPWeb.Repository.Context;
using FYPWeb.Repository.Repository;
using Microsoft.AspNet.SignalR;
using Model.Entity;

namespace FYPWeb.Hub
{
    public class NotificationHub : Microsoft.AspNet.SignalR.Hub
    {
        private static readonly IHubContext HubContext = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
        private static readonly Dictionary<string, string> Onlineusers = new Dictionary<string, string>();
        public override Task OnConnected()
        {
            var clientId = Context.QueryString["Id"];
            if (clientId == "") return base.OnConnected();
            if (Onlineusers.ContainsKey(clientId)) return base.OnConnected();
            try
            {
                Onlineusers.Add(clientId, Context.ConnectionId);
            }
            catch (Exception)
            {
                var count = Onlineusers.Keys.Count(p => p.EndsWith(clientId));
                Onlineusers.Add((count + 1) + "_" + clientId, Context.ConnectionId);
            }
            return base.OnConnected();
        }
        public override Task OnDisconnected(bool stopCalled)
        {
            var clientId = Context.QueryString["Id"];
            if (clientId == "") return base.OnDisconnected(stopCalled);
            if (!Onlineusers.ContainsKey(clientId)) return base.OnDisconnected(stopCalled);
            try
            {
                Onlineusers.Remove(clientId);
            }
            catch (Exception e)
            {
                // ignored
            }
            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            var clientId = Context.QueryString["Id"];
            if (clientId == "") return base.OnReconnected();
            if (Onlineusers.ContainsKey(clientId)) return base.OnReconnected();
            try
            {
                Onlineusers.Add(clientId, Context.ConnectionId);
            }
            catch (Exception)
            {
                var count = Onlineusers.Keys.Count(p => p.EndsWith(clientId));
                Onlineusers.Add((count + 1) + "_" + clientId, Context.ConnectionId);
            }
            return base.OnReconnected();
        }

        public void SendNotification(Notification notification,int userId, int priority)
        {
            var users = Onlineusers.Where(p => p.Key.EndsWith("_" + userId)).ToList();
            users.ForEach(p => Clients.Client(p.Value).SendNotification(notification.ToString(), notification.CreatedAt, priority));
        }

        public void SendMessage(int sId,int rId,string content,int orderId,string name,string picture)
        {
            var users = Onlineusers.Where(p => p.Key.EndsWith("_" + rId)).ToList();
            var message = new Message()
            {
                OrderId = orderId,
                RecieverUserId = rId,
                SenderUserId = sId,
                Content = content,
                CreatedOn = DateTime.Now
            };
            var context = new Repository.Context.AppContext();
            var repo = new MessageRepository(context);
            repo.Add(message);
            users.ForEach(p => Clients.Client(p.Value).RecieveMessage(name,message.CreatedOn.ToString("f"),picture,message.Content));
        }

        public static void StaticSendNotification(Notification notification, int userId, int priority)
        {
            var notiRepo = new NotificationRepository(new Repository.Context.AppContext());
            var users = Onlineusers.Where(p => p.Key.EndsWith("_" + userId)).ToList();
            var not = notiRepo.GetById(notification.Id);
            users.ForEach(p => HubContext.Clients.Client(p.Value).SendNotification(not.ToString(),notification.Url, notification.CreatedAt, priority));
        }
    }
}