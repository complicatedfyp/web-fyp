﻿

function changePage(e, index, parameter) {
    const p = parameter;

    ajaxPost("/api/Template/GetPostTableRow/",
        {
            UserId: userId,
            Parameter: p,
            Page: index,
            Size: 10
        },
        function (result) {
            $("#postTable").html("");

            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#postTable").append(d);
                });
            } else {
                $("#postTable").html("<p>No Data Found</p>");
            }
            $("#postPagination").find("a").each(function () {
                $(this).removeClass("active");
            });
            $(e).addClass("active");
        },
        function (result) {
            $("#postTable").html("");
            $("#postTable").html("<p>Error Occured</p>");
            Log(result);
        });
}

function searchPost(e, event) {
    if (e !== null && event.which !== 13) {
        return;
    }
    var p = e === null ? "" : $(e).val();
    showLoading("overlay");
    ajaxPost("/api/Template/GetPostTableRow/",
        {
            UserId: userId,
            Parameter: p,
            Page: 1,
            Size: 10
        },
        function (result) {
            $("#postTable").html("");
            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#postTable").append(d);
                });
            } else {
                $("#postTable").html("<p>No Data Found</p>");
            }
            hideLoading("overlay");

        },
        function (result) {
            $("#postTable").html("");
            $("#postTable").html("<p>Error Occured</p>");
            hideLoading("overlay");
            Log(result);
        });

    ajaxPost("/api/Template/GetPostsCount/",
        {
            UserId: userId,
            Parameter: p,
            Page: 1,
            Size: 10
        },
        function (result) {
            $("#postPagination").html("");
            for (let i = 0; i < result.result; i++) {
                $("#postPagination").append("<li><a href='#' onclick='changePage(this," + (i + 1) + ",\"" + p + "\")'>" + (i + 1) + "</a></li>");
            }
        },
        function (result) {
        });
}


function showeditDialogPost(id) {
    const win = window.open("/Forum/EditPost/" + id, "_blank");
    win.focus();
}

function showaddDialogPost() {
    const win = window.open("/Forum/CreatePost/", "_blank");
    win.focus();
}
function showdeleteDialogPost(id) {
    showConfirmDialogBox("Delete Post",
        "Do you want to delete this Post?<br>Deleting this Post will also delete its replies",
        "Delete Post",
        "No",
        function (e) {
            ajaxPost("/api/Post/DeletePost",
                {
                    Id: id
                },
                function (result) {
                    if (result.message === "Success") {
                        showNotification("Post Deleted", NotificationEnum.Success);
                        $("#row_post_" + id).animate({ opacity: "0.0" }, "slow", function () {
                            $(this).remove();
                            $("#myConfirmModal").modal("hide");
                        });
                    }
                },
                function (result) {

                });
        });
}
