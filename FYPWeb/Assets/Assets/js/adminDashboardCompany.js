﻿function changePage(e, index, parameter) {
    const p = parameter;

    ajaxPost("/api/Template/GetCompanies/",
        {
            Parameter: p,
            Page: index,
            Size: 10
        },
        function (result) {
            $("#companyTable").html("");

            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#companyTable").append(d);
                });
            } else {
                $("#companyTable").html("<p>No Data Found</p>");
            }
        },
        function (result) {
            $("#companyTable").html("");
            $("#companyTable").html("<p>Error Occured</p>");
            Log(result);
        });
}

function searchCompany(e,event) {
    if (e !== null && event.which !== 13) {
        return;
    }
    var p = e === null ? "" : $(e).val();
    showLoading("overlay");
    ajaxPost("/api/Template/GetCompanies/",
        {
            Parameter: p,
            Page: 1,
            Size: 10
        },
        function (result) {
            $("#companyTable").html("");
            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#companyTable").append(d);
                });
            } else {
                $("#companyTable").html("<p>No Data Found</p>");
            }
            hideLoading("overlay");
            Log(result);

        },
        function (result) {
            $("#companyTable").html("");
            $("#companyTable").html("<p>Error Occured</p>");
            hideLoading("overlay");
            Log(result);
        });

    ajaxPost("/api/Template/GetCompaniesCount/",
        {
            Parameter: p,
            Page: 1,
            Size: 10
        },
        function (result) {
            $("#companyPagination").html("");
            $("#companyPagination").pagination({
                items: result.count,
                itemsOnPage: 1,
                cssStyle: 'light-theme',
                onPageClick: function (page, e) {
                    changePage(e, page, p);
                }
            });
        },
        function (result) {
            Log(result);
        });
}





function showeditDialogCompany(id) {
    showDialogBox("/api/Template/GetCompanyForm/",
        { CompanyId: id },
        "Edit Company Information");
}

function showdeleteDialogCompany(id) {
    showConfirmDialogBox("Delete Company",
        "Do you want to delete this Company?<br>Deleting this company will also delete its brands and its Medicines",
        "Delete Company",
        "No",
        function(e) {
            ajaxPost("/api/Company/DeleteCompany",
                {
                    CompanyId: id
                },
                function(result) {
                   if (result.message === "Success") {
                       showNotification("Company Deleted", NotificationEnum.Success);
                       $(`#row_company_${id}`).animate({ opacity: "0.0" }, "slow", function () {
                            $(this).remove();
                            $("#myConfirmModal").modal("close");
                       });
                   }
                },
                function(result) {

                });
        });
}

function showaddDialogCompany() {
    showDialogBox("/api/Template/GetCompanyForm/",
        null,
        "Add Company Information");
}


function showImageDialogCompany(id) {
    showDialogBox("/api/Template/GetUploadForm/",
        {
            ActionUrl: "/File/SaveCompanyProfile",
            Id:id,
            Limit:1
        },
        "Change Company Picture");
}