﻿

function submitForm() {
    for (var instance in CKEDITOR.instances){
        if (CKEDITOR.instances.hasOwnProperty(instance))
        {
            CKEDITOR.instances[instance].updateElement();
        }
    }
    const postForm = $("#step-1");
    postForm.validator('validate');
    const elmErr = postForm.children('.has-error');
    if (elmErr && elmErr.length > 0) {
        return;
    }
    $("#postForm").submit();
}

function initSelect2(mydata) {
    $("#Tags").select2({
        tags: true,
        maximumSelectionLength: 5,
        tokenSeparators: [",", " "],
        createSearchChoice: function (term, data) {
            if ($(data).filter(function () {
                return this.text.localeCompare(term) === 0;
            }).length === 0) {
                return {
                    id: term,
                    text: term
                };
            }
        },
        multiple: true,
        data: mydata
    });
}