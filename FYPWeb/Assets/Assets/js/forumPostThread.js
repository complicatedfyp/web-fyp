﻿function changePage(e, index, parameter) {
    const p = parameter;
    ajaxPost("/api/Template/GetPosts/",
        {
            UserId: userId,
            PostId: p,
            Page: index,
            Size: 10
        },
        function (result) {
            $("#postTable").html("");

            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#postTable").append(d);
                });
            } else {
                $("#postTable").html("<p>No Data Found</p>");
            }
        },
        function (result) {
            $("#postTable").html("");
            $("#postTable").html("<p>Error Occured</p>");
        });
}
