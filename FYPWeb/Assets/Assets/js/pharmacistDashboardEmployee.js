﻿$(function () {
    searchEmployee(null, null, userId);
});

function changePage(e, index, parameter, userId) {
    const p = parameter;

    ajaxPost("/api/Template/GetEmployees/",
        {
            UserId: userId,
            Parameter: p,
            Page: index,
            Size: 10
        },
        function (result) {
            $("#employeeTable").html("");

            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#employeeTable").append(d);
                });
            } else {
                $("#employeeTable").html("<p>No Data Found</p>");
            }
            $("#employeePagination").find("a").each(function () {
                $(this).removeClass("active");
            });
            $(e).addClass("active");
        },
        function (result) {
            $("#employeeTable").html("");
            $("#employeeTable").html("<p>Error Occured</p>");
            Log(result);
        });
}

function searchEmployee(e, event, userId) {
    if (e !== null && event.which !== 13) {
        return;
    }
    var p = e === null ? "" : $(e).val();
    showLoading("overlay");
    ajaxPost("/api/Template/GetEmployees/",
        {
            UserId: userId,
            Parameter: p,
            Page: 1,
            Size: 10
        },
        function (result) {
            $("#employeeTable").html("");
            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#employeeTable").append(d);
                });
            } else {
                $("#employeeTable").html("<p>No Data Found</p>");
            }
            hideLoading("overlay");

        },
        function (result) {
            $("#employeeTable").html("");
            $("#employeeTable").html("<p>Error Occured</p>");
            hideLoading("overlay");
            Log(result);
        });

    ajaxPost("/api/Template/GetEmployeesCount/",
        {
            Parameter: p,
            UserId: userId,
            Page: 1,
            Size: 10
        },
        function (result) {
            $("#employeePagination").html("");
            for (let i = 0; i < result.count; i++) {
                $("#employeePagination").append("<li><a href='#' onclick='changePage(this," + (i + 1) + ",\"" + p + "\"," + userId + ")'>" + (i + 1) + "</a></li>");
            }
        },
        function (result) {
            Log(result);
        });
}





function showeditDialogEmployee(id, userId) {
    showDialogBox("/api/Template/GetEmployeeForm/",
        { EmployeeId: id, UserId: userId },
        "Edit Employee Information");
}

function showdeleteDialogEmployee(id, userId) {
    showConfirmDialogBox("Delete Employee",
        "Do you want to delete this Employee?<br>Deleting this Employee will also delete its employees and its Medicines",
        "Delete Employee",
        "No",
        function (e) {
            ajaxPost("/api/Employee/DeleteEmployee",
                {
                    Id: id,
                    UserId: userId,
                },
                function (result) {
                    if (result.message === "Success") {
                        showNotification("Employee Deleted", NotificationEnum.Success);
                        $("#row_employee_" + id).animate({ opacity: "0.0" },
                            "slow",
                            function() {
                                $(this).remove();
                                $("#myConfirmModal").modal("hide");
                            });
                    } else if (result.message === "Error"){
                        showNotification("Enable to Delete this Employee", NotificationEnum.Warning);

                    }
                },
                function (result) {
                    showNotification("Error Occured", NotificationEnum.Warning);

                });
        });
}

function showaddDialogEmployee(userId) {
    showDialogBox("/api/Template/GetEmployeeForm/",
        {
            UserId: userId,
        },
        "Add Employee Information");
}

function showassignDialogEmployee(id,userId) {
    showDialogBox("/api/Template/GetEmployeeAssignForm/",
        {
            UserId: userId,
            EmployeeId: id
        },
        "Add Employee to Pharmacy");
}

function showImageDialogEmployee(id) {
    showDialogBox("/api/Template/GetUploadForm/",
        {
            ActionUrl: "/File/SaveEmployeeProfile",
            Id: id,
            Limit: 1
        },
        "Change Employee Picture");
}