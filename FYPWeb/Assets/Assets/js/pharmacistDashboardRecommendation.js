﻿$(function () {
    searchRecommendation(null, null, userId);
});

function changePage(e, index, parameter, userId) {
    const p = parameter;

    ajaxPost("/api/Template/GetRecommendations/",
        {
            UserId: userId,
            Type: type,
            Parameter: p,
            Page: index,
            Size: 10
        },
        function (result) {
            $("#recommendationTable").html("");

            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#recommendationTable").append(d);
                });
            } else {
                $("#recommendationTable").html("<p>No Data Found</p>");
            }
            $("#recommendationPagination").find("a").each(function () {
                $(this).removeClass("active");
            });
            $(e).addClass("active");
        },
        function (result) {
            $("#recommendationTable").html("");
            $("#recommendationTable").html("<p>Error Occured</p>");
            Log(result);
        });
}

function searchRecommendation(e, event, userId) {
    if (e !== null && event.which !== 13) {
        return;
    }
    var p = e === null ? "" : $(e).val();
    showLoading("overlay");
    ajaxPost("/api/Template/GetRecommendations/",
        {
            UserId: userId,
            Parameter: p,
            Type: type,
            Page: 1,
            Size: 10
        },
        function (result) {
            $("#recommendationTable").html("");
            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#recommendationTable").append(d);
                });
            } else {
                $("#recommendationTable").html("<p>No Data Found</p>");
            }
            hideLoading("overlay");

        },
        function (result) {
            $("#recommendationTable").html("");
            $("#recommendationTable").html("<p>Error Occured</p>");
            hideLoading("overlay");
            Log(result);
        });

    ajaxPost("/api/Template/GetRecommendationsCount/",
        {
            Parameter: p,
            UserId: userId,
            Page: 1,
            Size: 10
        },
        function (result) {
            $("#recommendationPagination").html("");
            for (let i = 0; i < result.count; i++) {
                $("#recommendationPagination").append("<li><a href='#' onclick='changePage(this," + (i + 1) + ",\"" + p + "\"," + userId + ")'>" + (i + 1) + "</a></li>");
            }
        },
        function (result) {
            Log(result);
        });
}





function showeditDialogRecommendation(id, userId) {
    showDialogBox("/api/Template/GetRecommendationForm/",
        { RecommendationId: id, UserId: userId },
        "Edit Recommendation Information");
}

function showdeleteDialogRecommendation(id, userId) {
    showConfirmDialogBox("Delete Recommendation",
        "Do you want to delete this Recommendation?<br>Deleting this Recommendation will also delete its recommendations and its Medicines",
        "Delete Recommendation",
        "No",
        function (e) {
            ajaxPost("/api/Recommendation/DeleteRecommendation",
                {
                    Id: id,
                    UserId: userId,
                },
                function (result) {
                    if (result.message === "Success") {
                        showNotification("Recommendation Deleted", NotificationEnum.Success);
                        $("#row_recommendation_" + id).animate({ opacity: "0.0" }, "slow", function () {
                            $(this).remove();
                            $("#myConfirmModal").modal("hide");
                        });
                    }
                },
                function (result) {

                });
        });
}

function showaddDialogRecommendation(userId) {
    showDialogBox("/api/Template/GetRecommendationForm/",
        {
            UserId: userId,
        },
        "Add Recommendation Information");
}

function showassignDialogRecommendation(id, userId) {
    showDialogBox("/api/Template/GetRecommendationAssignForm/",
        {
            UserId: userId,
            RecommendationId: id
        },
        "Add Recommendation to Pharmacy");
}

function showImageDialogRecommendation(id) {
    showDialogBox("/api/Template/GetUploadForm/",
        {
            ActionUrl: "/File/SaveRecommendationProfile",
            Id: id,
            Limit: 1
        },
        "Change Recommendation Picture");
}