$(document).ready(function () {
    $("#slider").slick({
        dots: true,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear'
    });
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            ajaxPost("/api/Promotion/GetNearestPromotionOffers",
                pos,
                function (result) {
                    if (result.message == "Success") {
                        if (result.result.length == 0) {
                            $("#promotionTable").html("<tr><td colspan='5'>No Promotions Found Near you</td></tr>");
                        } else {
                            $("#promotionTable").html("");
                            result.result.forEach(function (e) {
                                $("#promotionTable").append("<tr  style='cursor:pointer' onclick='goToUrl('" + "/Profile/"+ e.Username + "')'>" +
                                    "<td>" + e.ShopName + "</td>" +
                                    "<td>" + e.Name + "</td>" +
                                    "<td>" + e.Code + "</td>" +
                                    "<td>" + e.PercentageOff + "</td>" +
                                    "<td>" + e.EndDate + "</td>" +
                                    "</tr>");
                            });

                        }
                    }
                },
                function (result) {
                    showNotification("Error Occured", NotificationEnum.Warning);
                    $("#promotionTable").html("<tr><td colspan='5'>No Promotions Found Near you</td></tr>");
                });
            ajaxPost("/api/Medicine/GetNearestMedicine",
                pos,
                function (result) {
                    if (result.message == "Success") {
                        if (result.result.length == 0) {
                            $("#medicineTable").html("<tr><td colspan='5'>No Medicines Found Near you</td></tr>");
                        } else {
                            $("#medicineTable").html("");
                            result.result.forEach(function (e) {
                                $("#medicineTable").append("<tr style='cursor:pointer'  onclick='goToUrl(\"" + "/Medicine/" + e.Id + "\")'>" +
                                    "<td><img src='" + e.ImageLink + "' alt='' width='32' class='image'/></td>" +
                                    "<td>" + e.Name + "</td>" +
                                    "<td>" + e.DrugForm + "</td>" +
                                    "<td>" + e.Packing + "</td>" +
                                    "</tr>");
                            });

                        }
                    }
                },
                function (result) {
                    showNotification("Error Occured", NotificationEnum.Warning);
                    $("#medicineTable").html("<tr><td colspan='5'>No Medicines Found Near you</td></tr>");
                });

        }, function () {

        });
    } else {
        showNotification("Geo Location Blocked", NotificationEnum.Warning);
        $("#promotionTable").html("<tr><td colspan='5'>No Promotions Found Near you</td></tr>");
        $("#medicineTable").html("<tr><td colspan='5'>No Medicines Found Near you</td></tr>");
    }
    
});