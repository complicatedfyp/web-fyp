﻿function showcancelOrderDialog(id) {
    showDialogBox("/api/Template/GetCancelOrderForm/",
        {
            OrderId: id,

        },
        "Cancel Order");
}


function showcompleteOrderDialog(id) {
    showDialogBox("/api/Template/GetCompleteOrderForm/",
        {
            OrderId: id,
        },
        "Complete Order");
}
function showorderDetailDialog(id) {
    showDialogBox("/api/Template/GetOrderDetailForm/",
        {
            OrderId: id,

        },
        "Order Details");
}
function initMapPharmacy(x, y) {
    var uluru = { lat: x, lng: y };
    if (x !== "" && y !== "") {
        uluru = { lat: parseFloat(x), lng: parseFloat(y) };
    }
    var map = new google.maps.Map(document.getElementById('map'),
        {
            zoom: 17,
            center: uluru
        });
    var marker = new google.maps.Marker({
        position: uluru,
        map: map,
        icon: "/Assets/Assets/img/logo-32.png"
    });
}

function showpharmacyreviewOrderDialog(id) {
    showDialogBox("/api/Template/GetPharmacyReviewOrderForm/",
        {
            OrderId: id,
        },
        "Review Order");
}
function showeditpharmacyreviewOrderDialog(id) {
    showDialogBox("/api/Template/GetPharmacyReviewOrderForm/",
        {
            Id: id,
        },
        "Review Order");
}

function showcustomerreviewOrderDialog(id) {
    showDialogBox("/api/Template/GetCustomerReviewOrderForm/",
        {
            OrderId: id,
        },
        "Review Order");
}
function showeditcustomerreviewOrderDialog(id) {
    showDialogBox("/api/Template/GetCustomerReviewOrderForm/",
        {
            Id: id,
        },
        "Review Order");
}
function showreviewOrderDialog(id) {
    showDialogBox("/api/Template/GetReviewOrderForm/",
        {
            OrderId: id,
        },
        "Review Order");
}
function showrejectOrderDialog(id) {
    showDialogBox("/api/Template/GetRejectOrderForm/",
        {
            OrderId: id,

        },
        "Reject Order");
}
function showacceptOrderDialog(id, userId) {
    showDialogBox("/api/Template/GetAcceptOrderForm/",
        {
            OrderId: id,
            UserId: userId,
        },
        "Accept Order");
}



function rejectOrder() {
    const brandForm = $("#step-1");
    brandForm.validator('validate');
    const elmErr = brandForm.children('.has-error');
    if (elmErr && elmErr.length > 0) {
        return;
    }
    var id = $("#OrderId").val();
    var comments = $("#Comment").val();
    ajaxPost("/api/Order/RejectOrder",
        {
            OrderId: id,
            Comment: comments
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Order is Cancelled", NotificationEnum.Success);
                $("#row_order_" + id).animate({ opacity: "0.0" }, "slow", function () {
                    $(this).remove();
                    $("#myConfirmModal").modal("hide");
                });
            }
        },
        function (result) {

        });

}

function saveCustomerReview() {
    const employeeForm = $("#step-1");
    employeeForm.validator('validate');
    const elmErr = employeeForm.children('.has-error');
    if (elmErr && elmErr.length > 0) {
        return;
    }
    var userId = $("#UserId").val();
    var pharmacyId = $("#PharmacyId").val();
    var orderId = $("#OrderId").val();
    var review = $("#Review").val();
    const rating = $("#ratingReview").starRating('getRating');
    ajaxPost("/api/CustomerReview/SaveReview/",
        {
            Id: id,
            UserId: userId,
            OrderId: orderId,
            PharmacyId: pharmacyId,
            Review: review,
            Rating: rating
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Review Added", NotificationEnum.Success);
                closeDialogBox();
                location.reload();
            }
        },
        function (result) {
            showNotification("Error Occured", NotificationEnum.Warning);
        });
}

function editCustomerReview() {
    const employeeForm = $("#step-1");
    employeeForm.validator('validate');
    const elmErr = employeeForm.children('.has-error');
    if (elmErr && elmErr.length > 0) {
        return;
    }
    var userId = $("#UserId").val();
    var pharmacyId = $("#PharmacyId").val();
    var orderId = $("#OrderId").val();
    var review = $("#Review").val();
    const rating = $("#ratingReview").starRating('getRating');
    ajaxPost("/api/CustomerReview/UpdateReview/",
        {
            Id: id,
            UserId: userId,
            OrderId: orderId,
            PharmacyId: pharmacyId,
            Review: review,
            Rating: rating
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Review Updated", NotificationEnum.Success);
                closeDialogBox();
            }
        },
        function (result) {
            showNotification("Error Updating Review", NotificationEnum.Warning);
        });
}


function savePharmacyReview() {
    const employeeForm = $("#step-1");
    employeeForm.validator('validate');
    const elmErr = employeeForm.children('.has-error');
    if (elmErr && elmErr.length > 0) {
        return;
    }
    var userId = $("#UserId").val();
    var pharmacyId = $("#PharmacyId").val();
    var orderId = $("#OrderId").val();
    var review = $("#Review").val();
    const rating = $("#ratingReview").starRating('getRating');
    ajaxPost("/api/PharmacistReview/SaveReview/",
        {
            UserId: userId,
            OrderId: orderId,
            PharmacyId: pharmacyId,
            Review: review,
            Rating: rating
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Review Added", NotificationEnum.Success);
                closeDialogBox();
                location.reload();
            }
        },
        function (result) {
            showNotification("Error Occured", NotificationEnum.Warning);
        });
}

function editPharmacyReview() {
    const employeeForm = $("#step-1");
    employeeForm.validator('validate');
    const elmErr = employeeForm.children('.has-error');
    if (elmErr && elmErr.length > 0) {
        return;
    }
    var userId = $("#UserId").val();
    var pharmacyId = $("#PharmacyId").val();
    var orderId = $("#OrderId").val();
    var review = $("#Review").val();
    const rating = $("#ratingReview").starRating('getRating');
    ajaxPost("/api/PharmacistReview/UpdateReview/",
        {
            Id: id,
            UserId: userId,
            OrderId: orderId,
            PharmacyId: pharmacyId,
            Review: review,
            Rating: rating
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Review Updated", NotificationEnum.Success);
                closeDialogBox();
            }
        },
        function (result) {
            showNotification("Error Updating Review", NotificationEnum.Warning);
        });
}


function acceptOrder() {
    const brandForm = $("#step-1");
    brandForm.validator('validate');
    const elmErr = brandForm.children('.has-error');
    if (elmErr && elmErr.length > 0) {
        return;
    }
    var id = $("#OrderId").val();
    var comments = $("#EmployeeId").val();
    ajaxPost("/api/Order/AcceptOrder",
        {
            OrderId: id,
            EmployeeId: comments
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Order is Accepted", NotificationEnum.Success);
                $("#row_order_" + id).animate({ opacity: "0.0" }, "slow", function () {
                    $(this).remove();
                    closeDialogBox();
                });
            }
        },
        function (result) {

        });

}

function cancelOrder() {
    const brandForm = $("#step-1");
    brandForm.validator('validate');
    const elmErr = brandForm.children('.has-error');
    if (elmErr && elmErr.length > 0) {
        return;
    }
    var id = $("#OrderId").val();
    var comments = $("#Comment").val();
    ajaxPost("/api/Order/CancelOrder",
        {
            OrderId: id,
            Comment: comments
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Order is Cancelled", NotificationEnum.Success);
                $("#row_order_" + id).animate({ opacity: "0.0" }, "slow", function () {
                    $(this).remove();
                    closeDialogBox();
                });
            }
        },
        function (result) {

        });

}

function completeOrder() {
    var id = $("#OrderId").val();
    ajaxPost("/api/Order/CompleteOrder",
        {
            OrderId: id,
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Order is Completed", NotificationEnum.Success);
                $("#row_order_" + id).animate({ opacity: "0.0" }, "slow", function () {
                    $(this).remove();
                    closeDialogBox();
                });
            }
        },
        function (result) {

        });

}