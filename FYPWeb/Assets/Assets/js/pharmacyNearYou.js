﻿var map;
var circleMaker;
var markerList = [];
function initMapTemp() {
    map = new google.maps.Map(document.getElementById('map'),
        {
            zoom: 12,
            center: { lat: 31.582045, lng: 74.329376 }

        });
    circleMaker = new google.maps.Circle({
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35,
        map: map,
        radius: 10000
    });
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            map.setCenter(pos);
            new google.maps.Marker({
                position: pos,
                icon: "/Assets/Assets/img/map-user-icon.png",
                map: map
            });
            circleMaker.setCenter(pos);
            ajaxPost("/api/Pharmacy/GetNearestPharmacies",
                {
                    Long: position.coords.longitude,
                    Lat: position.coords.latitude,
                },
                function (result) {
                    if (result.message == "Success") {
                        markerList.forEach(function (e) {
                            e.marker.setMap(null);
                        });
                        markerList = [];
                        result.result.forEach(function (e) {

                            var m = {
                                marker: new google.maps.Marker({
                                    position: {
                                        lat: e.X,
                                        lng: e.Y
                                    },
                                    icon: "/Assets/Assets/img/logo-32.png",
                                    map: map,
                                    title: e.Name
                                }),
                                content: getContent(e.Username, e.Name, e.Address, e.Rating)
                            }
                            m.marker.addListener('click', function () {
                                infowindow.setContent(m.content);
                                infowindow.open(map, m.marker);
                            });
                            markerList.push(m);
                        });
                    }
                },
                function (result) {
                    showNotification("Error Occured", NotificationEnum.Warning);
                });

        }, function () {

        });
    } else {
        // Browser doesn't support Geolocation
    }
    var infowindow = new google.maps.InfoWindow({
        content: ""
    });
    $(".placepicker").placepicker({
        mapOptions: {
            types: ['(cities)'],
            componentRestrictions: { country: "pk" }
        },
        placeChanged: function (place) {
            var pos = {
                lat: place.geometry.location.lat(),
                lng: place.geometry.location.lng()
            };
            map.setCenter(pos);
            circleMaker.setCenter(pos);
            ajaxPost("/api/Pharmacy/GetNearestPharmacies",
                {
                    Long: place.geometry.location.lng(),
                    Lat: place.geometry.location.lat(),
                },
                function (result) {
                    if (result.message == "Success") {
                        markerList.forEach(function (e) {
                            e.marker.setMap(null);
                        });
                        markerList = [];
                        result.result.forEach(function (e) {

                            var m = {
                                marker: new google.maps.Marker({
                                    position: {
                                        lat: e.X,
                                        lng: e.Y
                                    },
                                    icon: "/Assets/Assets/img/logo-32.png",
                                    map: map,
                                    title: e.Name
                                }),
                                content: getContent(e.Username, e.Name, e.Address, e.Rating)
                            }
                            m.marker.addListener('click', function () {
                                infowindow.setContent(m.content);
                                infowindow.open(map, m.marker);
                            });
                            markerList.push(m);
                        });
                    }
                },
                function (result) {
                    showNotification("Error Occured", NotificationEnum.Warning);
                });

        }
    });
}

function resetPosition() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            map.setCenter(pos);
        }, function () {

        });
    } else {
        showNotification("Error Reseting Location", NotificationEnum.Warning);
    }
}

function changePosition() {
    if ($("#PharmacyId").val() > 0) {
        var X = $("#PharmacyId").find(":selected").data("x");
        var Y = $("#PharmacyId").find(":selected").data("y");
        var pos = {
            lat: X,
            lng: Y
        };
        map.setCenter(pos);
    }
}

function getContent(username, name, address, rating) {
    var content = "<div class='panel'>" +
        "<div class='panel-header'>" +
        "<a target='_blank' href='/Profile/" + username + "'>"
        + name +
        "</a>" +
        "</div>" +
        "<div class='panel-body'>" +
        "<div class='row'>" +
        "<div class='col-xs-12'>" +
        "<p>" +
        "" + address +
        "<br>" +
        "<i class='fa fa-star' style='color:gold'></i>" +
        "" + rating + "/5" +
        "</p>" +
        "</div>" +
        "</div>" +
        "</div>" +
        "</div>";
    return content;
}