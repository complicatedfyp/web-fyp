﻿var myname = "";
var mydate = "";
var mypicture = "";
var sId = "";
var rId = "";
var orderId = "";
var page = 1;
function sendMesageviaInput(e) {
    e.preventDefault();
    if (e.keyCode === 13) {
        sendMessage();
    }
}

function getMessages() {
    ajaxPost("/api/Message/GetMessagesByOrderId",
        {
            OrderId: orderId,
            Page:page
        },
        function(result) {
            if (result.message == "Success") {
                page++;
                result.result.forEach(function(e) {
                    if (e.sId == sId) {
                        $("#messageBody").prepend(myMessageTemplate(e.name, e.date, e.picture, e.content));
                    } else {
                        $("#messageBody").prepend(otherMessageTemplate(e.name, e.date, e.picture, e.content));
                    }
                });
            }
        },
        function(result) {
            showNotification("Error Occured", NotificationEnum.Error);
        });
}
function otherMessageTemplate(name, date, picture, message) {
    return `<div class="direct-chat-msg">
        <div class="direct-chat-info clearfix">
        <span class="direct-chat-name pull-left">${ name}</span>
        <span class="direct-chat-timestamp pull-right">${ date}</span>
        </div>
        <!-- /.direct-chat-info -->
        <img class="direct-chat-img" width="128" height="128" src="${ picture}" alt= "Message User Image"><!-- /.direct-chat-img -->
        <div class="direct-chat-text">
            ${ message}
        </div>
        <!-- /.direct-chat-text -->
        </div>`;
}

function myMessageTemplate(name, date, picture, message) {
    return `<div class="direct-chat-msg right">
                                        <div class="direct-chat-info clearfix">
                                            <span class="direct-chat-name pull-right">${ name}</span>
                                            <span class="direct-chat-timestamp pull-left">${ date}</span>
                                        </div>
                                        <!-- /.direct-chat-info -->
                                        <img class="direct-chat-img" src="${ picture}" width="128" height="128" alt="Message User Image"><!-- /.direct-chat-img -->
                                        <div class="direct-chat-text">
                                            ${ message}
                                        </div>
                                        <!-- /.direct-chat-text -->
                                    </div>`;
}
