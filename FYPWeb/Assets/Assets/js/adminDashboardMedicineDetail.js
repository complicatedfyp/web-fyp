﻿$(function () {
    searchMedicineDetail(null);
});


function showCureDiseaseDialog(id) {
     showDialogBox("/api/Template/GetDiseaseShowForm/",
        {
            Id: id,
            IsInidication: true
        },
        "Interaction Diseases");   
}

function showContradictedDiseaseDialog(id) {
     showDialogBox("/api/Template/GetDiseaseShowForm/",
        {
            Id: id,
            IsInidication: false
        },
        "Contradicted Diseases");   
}

function changePage(e, index, parameter) {
    const p = parameter;

    ajaxPost("/api/Template/GetMedicineDetails/",
        {
            Parameter: p,
            Page: index,
            Size: 10
        },
        function (result) {
            $("#medicineDetailTable").html("");

            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#medicineDetailTable").append(d);
                });
            } else {
                $("#medicineDetailTable").html("<p>No Data Found</p>");
            }
        },
        function (result) {
            $("#medicineDetailTable").html("");
            $("#medicineDetailTable").html("<p>Error Occured</p>");
            Log(result);
        });
}

function searchMedicineDetail(e, event) {
    if (e !== null && event.which !== 13) {
        return;
    }
    var p = e === null ? "" : $(e).val();
    showLoading("overlay");
    ajaxPost("/api/Template/GetMedicineDetails/",
        {
            Parameter: p,
            Page: 1,
            Size: 10
        },
        function (result) {
            $("#medicineDetailTable").html("");
            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#medicineDetailTable").append(d);
                });
            } else {
                $("#medicineDetailTable").html("<p>No Data Found</p>");
            }
            hideLoading("overlay");

        },
        function (result) {
            $("#medicineDetailTable").html("");
            $("#medicineDetailTable").html("<p>Error Occured</p>");
            hideLoading("overlay");
            Log(result);
        });

    ajaxPost("/api/Template/GetMedicineDetailsCount/",
        {
            Parameter: p,
            Page: 1,
            Size: 10
        },
        function (result) {
            $("#medicineDetailPagination").html("");
            $("#medicineDetailPagination").pagination({
                items: result.count,
                itemsOnPage: 1,
                cssStyle: 'light-theme',
                onPageClick: function (page, e) {
                    changePage(e, page, p);
                }
            });
        },
        function (result) {
            Log(result);
        });
}





function showeditDialogMedicineDetail(id) {
    showDialogBox("/api/Template/GetMedicineDetailForm/",
        { MedicineDetailId: id },
        "Edit MedicineDetail Information");
}

function showdeleteDialogMedicineDetail(id) {
    showConfirmDialogBox("Delete MedicineDetail",
        "Do you want to delete this MedicineDetail?<br>Deleting this MedicineDetail will also delete its Medicines",
        "Delete MedicineDetail",
        "No",
        function (e) {
            ajaxPost("/api/MedicineDetail/DeleteMedicineDetail",
                {
                    Id: id
                },
                function (result) {
                    if (result.message === "Success") {
                        showNotification("MedicineDetail Deleted", NotificationEnum.Success);
                        $("#row_medicineDetail_" + id).animate({ opacity: "0.0" }, "slow", function () {
                            $(this).remove();
                            $("#myConfirmModal").modal("hide");
                        });
                    }
                },
                function (result) {

                });
        });
}

function showaddDialogMedicineDetail() {
    showDialogBox("/api/Template/GetMedicineDetailForm/",
        null,
        "Add MedicineDetail Information");
}


function showImageDialogMedicineDetail(id) {
    showDialogBox("/api/Template/GetUploadForm/",
        {
            ActionUrl: "/File/SaveMedicineDetailProfile",
            Id: id,
            Limit: 1
        },
        "Change MedicineDetail Picture");
}