﻿
function changePage(e, index, parameter) {
    const p = parameter;
    var pId = $("#PharmacyInventoryId").val();
    if (pId === 0) {
        return;
    }
    ajaxPost("/api/Template/GetInventoriesTrack/",
        {
            Parameter: p,
            Page: index,
            Size: 10,
            PharmacyId: pId
        },
        function (result) {
            $("#inventoryTable").html("");

            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#inventoryTable").append(d);
                });
            } else {
                $("#inventoryTable").html("<p>No Data Found</p>");
            }
            $("#inventoryPagination").find("a").each(function () {
                $(this).removeClass("active");
            });
            $(e).addClass("active");
        },
        function (result) {
            $("#inventoryTable").html("");
            $("#inventoryTable").html("<p>Error Occured</p>");
        });
}

function searchInventory(e, event) {
    if (e !== null && event.which !== 13) {
        return;
    }
    var p = e === null ? "" : $(e).val();
    showLoading("overlay");
    var pId = $("#PharmacyInventoryId").val();
    if (pId === 0) {
        return;
    }
    ajaxPost("/api/Inventory/GetTotalCount/",
        {
            Parameter: p,
            PharmacyId: pId
        },
        function (result) {
            if (result.message === "Success") {
                $("#stockCount").html(result.result);
            }
        },
        function (result) {
            Log(result);
        });
    ajaxPost("/api/Template/GetInventoriesTrack/",
        {
            Parameter: p,
            Page: 1,
            Size: 10,
            PharmacyId: pId
        },
        function (result) {
            $("#inventoryTable").html("");
            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#inventoryTable").append(d);
                });
            } else {
                $("#inventoryTable").html("<p>No Data Found</p>");
            }
            hideLoading("overlay");

        },
        function (result) {
            $("#inventoryTable").html("");
            $("#inventoryTable").html("<p>Error Occured</p>");
            hideLoading("overlay");
        });

    ajaxPost("/api/Template/GetInventoriesCountTrack/",
        {
            Parameter: p,
            Page: 1,
            Size: 10,
            PharmacyId: pId
        },
        function (result) {
            $("#inventoryPagination").html("");
            for (let i = 0; i < result.count; i++) {
                $("#inventoryPagination").append("<li><a href='#' onclick='changePage(this," + (i + 1) + ",\"" + p + "\")'>" + (i + 1) + "</a></li>");
            }
        },
        function (result) {
            Log(result);
        });
}


function showorderDetailDialog(id) {
    showDialogBox("/api/Template/GetOrderDetailForm/",
        {
            OrderId: id,

        },
        "Order Details");
}

function initMapPharmacy(x, y) {
    var uluru = { lat: x, lng: y };
    if (x !== "" && y !== "") {
        uluru = { lat: parseFloat(x), lng: parseFloat(y) };
    }
    var map = new google.maps.Map(document.getElementById('map'),
        {
            zoom: 17,
            center: uluru
        });
    var marker = new google.maps.Marker({
        position: uluru,
        map: map,
        icon: "/Assets/Assets/img/logo-32.png"
    });
}
