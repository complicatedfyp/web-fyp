﻿$(function () {
    searchReply(null);
});

function changePage(e, index, parameter) {
    const p = parameter;

    ajaxPost("/api/Template/GetReplies/",
        {
            UserId: userId,
            Parameter: p,
            Page: index,
            Size: 10
        },
        function (result) {
            $("#replyTable").html("");

            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#replyTable").append(d);
                });
            } else {
                $("#replyTable").html("<p>No Data Found</p>");
            }
            $("#replyPagination").find("a").each(function () {
                $(this).removeClass("active");
            });
            $(e).addClass("active");
        },
        function (result) {
            $("#replyTable").html("");
            $("#replyTable").html("<p>Error Occured</p>");
            Log(result);
        });
}

function searchReply(e, event) {
    if (e !== null && event.which !== 13) {
        return;
    }
    var p = e === null ? "" : $(e).val();
    showLoading("overlay");
    ajaxPost("/api/Template/GetReplies/",
        {
            UserId: userId,
            Parameter: p,
            Page: 1,
            Size: 10
        },
        function (result) {
            $("#replyTable").html("");
            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#replyTable").append(d);
                });
            } else {
                $("#replyTable").html("<p>No Data Found</p>");
            }
            hideLoading("overlay");

        },
        function (result) {
            $("#replyTable").html("");
            $("#replyTable").html("<p>Error Occured</p>");
            hideLoading("overlay");
            Log(result);
        });

    ajaxPost("/api/Template/GetRepliesCount/",
        {
            UserId: userId,
            Parameter: p,
            Page: 1,
            Size: 10
        },
        function (result) {
            $("#replyPagination").html("");
            for (let i = 0; i < result.count; i++) {
                $("#replyPagination").append("<li><a href='#' onclick='changePage(this," + (i + 1) + ",\"" + p + "\")'>" + (i + 1) + "</a></li>");
            }
        },
        function (result) {
            Log(result);
        });
}





function showeditDialogReply(id) {
    goToUrl('/Forum/EditReply/' + id);
}

function showdeleteDialogReply(id) {
    showConfirmDialogBox("Delete Reply",
        "Do you want to delete this Reply?",
        "Delete Reply",
        "No",
        function (e) {
            ajaxPost("/api/Reply/DeleteReply",
                {
                    Id: id
                },
                function (result) {
                    if (result.message === "Success") {
                        showNotification("Reply Deleted", NotificationEnum.Success);
                        $("#row_reply_" + id).animate({ opacity: "0.0" }, "slow", function () {
                            $(this).remove();
                            $("#myConfirmModal").modal("hide");
                        });
                    }
                },
                function (result) {

                });
        });
}
