﻿var geocoder = null;
var map = null;
var markerList = [];
function showProcessLocationDialogBox(id) {
    showDialogBox("/api/Template/GetProcessLocationDialogBox/",
        {
            UserId: id
        },
        "Relocate Suggestion");
}

function applyAlgo() {
    var id = $("#PharmacyId").val();
    var noOfPharmacy = $("#PharmacyCount").val();
    ajaxPost("/api/Pharmacy/ApplyAlgorithm/",
        {
            PharmacyId: id,
            NumberOfPharmacies: noOfPharmacy,
        },
        function(result) {
            if (result.message === "Success") {
                $("#Address").html("");
                markerList.forEach(function (e) {
                    e.setMap(null);
                });
                result.result.forEach(function (e) {
                    markerList.push(new google.maps.Marker({
                        position: {
                            lat: e[0],
                            lng: e[1]
                        },
                        icon: "/Assets/Assets/img/logo-32.png",
                        map: map,
                    }));
                    geocodeLatLng(e[0], e[1], "#Address");
                });
                showNotification("Process Completed", NotificationEnum.Success);
                closeDialogBox();
            }
        },
        function(result) {
            showNotification("Error Occured", NotificationEnum.Warning);
        });
}

function initMap() {
    map = new google.maps.Map(document.getElementById('map'),
        {
            zoom: 17,
        });
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            map.setCenter(pos);
        });
    } else {
        // Browser doesn't support Geolocation
    }

}
function geocodeLatLng(x,y,id) {
    var latlng = { lat: parseFloat(x), lng: parseFloat(y) };
    if (geocoder == null) {
        geocoder = new google.maps.Geocoder;
    }
    geocoder.geocode({ 'location': latlng }, function (results, status) {
        if (status === 'OK') {
            if (results[0]) {
                $(id).append("<tr><td>" + results[0].formatted_address + "</td><td><button class='btn btn-info' onclick='showLocation(this)' data-x='" + x + "' data-y='" + y + "'>Show Location</button></td></tr>");
            } else {
                Log("Nothing Found");
            }
        } else {
            Log("Error Occured");
        }
    });
}

function showLocation(e) {
    var x = $(e).attr('data-x');
    var y = $(e).attr('data-y');
    var latlng = { lat: parseFloat(x), lng: parseFloat(y) };
    map.setCenter(latlng);
}