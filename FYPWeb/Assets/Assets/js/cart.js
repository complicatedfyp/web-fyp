﻿
function showeditDialogCart(medicineId, userId, sessionId = "") {
    showDialogBox("/api/Template/GetCartForm/",
        {
            MedicineId: medicineId,
            UserId: userId,
            SessionId: sessionId,
        },
        "Edit Cart");
}
function showLoginRegsiterForm() {
    $("#myLoginModel").modal("show");
}
function showdeleteDialogCart(medicineId, userId, sessionId = "") {
    showConfirmDialogBox("Remove Item",
        "Do you want to delete this Item in Cart?",
        "Remove",
        "Cancel",
        function (e) {
            ajaxPost("/api/Medicine/RemoveFromCart",
                {
                    MedicineId: medicineId,
                    UserId: userId,
                    SessionId: sessionId,
                },
                function (result) {
                    if (result.message === "Success") {
                        showNotification("Item From Cart Removed", NotificationEnum.Success);
                        $("#myConfirmModal").modal("hide");
                        setTimeout(function () { location.reload() }, 2000);
                    }
                },
                function (result) {
                    showNotification("Error Occured", NotificationEnum.Warning);
                });
        });
}


function editCart() {
    const cartForm = $("#step-1");
    cartForm.validator('validate');
    const elmErr = cartForm.children('.has-error');
    if (elmErr && elmErr.length > 0) {
        return;
    }
    const medicineId = $("#MedicineId").val();
    const userId = $("#UserId").val();
    const qty = $("#Quantity").val();
    const sessionId = $("#SessionId").val();
    ajaxPost("/api/Medicine/UpdateCart/",
        {
            MedicineId: medicineId,
            UserId: userId,
            Quantity: qty,
            SessionId: sessionId,
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Cart Updated", NotificationEnum.Success);
                $("#close").click();
                setTimeout(function () { location.reload() }, 3000);
            }
        },
        function (result) {
            showNotification("Error Occured", NotificationEnum.Warning);
        });
}
