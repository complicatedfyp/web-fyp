﻿$(function () {
    searchDisease(null);
});

function changePage(e, index, parameter) {
    const p = parameter;

    ajaxPost("/api/Template/GetDiseases/",
        {
            Parameter: p,
            Page: index,
            Size: 10
        },
        function (result) {
            $("#diseaseTable").html("");

            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#diseaseTable").append(d);
                });
            } else {
                $("#diseaseTable").html("<p>No Data Found</p>");
            }
        },
        function (result) {
            $("#diseaseTable").html("");
            $("#diseaseTable").html("<p>Error Occured</p>");
            Log(result);
        });
}

function searchDisease(e, event) {
    if (e !== null && event.which !== 13) {
        return;
    }
    var p = e === null ? "" : $(e).val();
    showLoading("overlay");
    ajaxPost("/api/Template/GetDiseases/",
        {
            Parameter: p,
            Page: 1,
            Size: 10
        },
        function (result) {
            $("#diseaseTable").html("");
            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#diseaseTable").append(d);
                });
            } else {
                $("#diseaseTable").html("<p>No Data Found</p>");
            }
            hideLoading("overlay");

        },
        function (result) {
            $("#diseaseTable").html("");
            $("#diseaseTable").html("<p>Error Occured</p>");
            hideLoading("overlay");
            Log(result);
        });

    ajaxPost("/api/Template/GetDiseasesCount/",
        {
            Parameter: p,
            Page: 1,
            Size: 10
        },
        function (result) {
            $("#diseasePagination").html("");
            $("#diseasePagination").pagination({
                items: result.count,
                itemsOnPage: 1,
                cssStyle: 'light-theme',
                onPageClick: function (page, e) {
                    changePage(e, page, p);
                }
            });
        },
        function (result) {
            Log(result);
        });
}





function showeditDialogDisease(id) {
    showDialogBox("/api/Template/GetDiseaseForm/",
        { diseaseId: id },
        "Edit Disease Information");
}

function showdeleteDialogDisease(id) {
    showConfirmDialogBox("Delete Disease",
        "Do you want to delete this Disease?<br>Deleting this Disease will also delete its Medicines",
        "Delete Disease",
        "No",
        function (e) {
            ajaxPost("/api/Disease/DeleteDisease",
                {
                    Id: id
                },
                function (result) {
                    if (result.message === "Success") {
                        showNotification("Disease Deleted", NotificationEnum.Success);
                        $("#row_disease_" + id).animate({ opacity: "0.0" }, "slow", function () {
                            $(this).remove();
                            $("#myConfirmModal").modal("hide");
                        });
                    }
                },
                function (result) {

                });
        });
}

function showaddDialogDisease() {
    showDialogBox("/api/Template/GetDiseaseForm/",
        null,
        "Add Disease Information");
}

