﻿$(function () {
    searchBrand(null);
});

function changePage(e, index, parameter) {
    const p = parameter;

    ajaxPost("/api/Template/GetBrands/",
        {
            Parameter: p,
            Page: index,
            Size: 10
        },
        function (result) {
            $("#brandTable").html("");

            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#brandTable").append(d);
                });
            } else {
                $("#brandTable").html("<p>No Data Found</p>");
            }
        },
        function (result) {
            $("#brandTable").html("");
            $("#brandTable").html("<p>Error Occured</p>");
            Log(result);
        });
}

function searchBrand(e, event) {
    if (e !== null && event.which !== 13) {
        return;
    }
    var p = e === null ? "" : $(e).val();
    showLoading("overlay");
    ajaxPost("/api/Template/GetBrands/",
        {
            Parameter: p,
            Page: 1,
            Size: 10
        },
        function (result) {
            $("#brandTable").html("");
            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#brandTable").append(d);
                });
            } else {
                $("#brandTable").html("<p>No Data Found</p>");
            }
            hideLoading("overlay");

        },
        function (result) {
            $("#brandTable").html("");
            $("#brandTable").html("<p>Error Occured</p>");
            hideLoading("overlay");
            Log(result);
        });

    ajaxPost("/api/Template/GetBrandsCount/",
        {
            Parameter: p,
            Page: 1,
            Size: 10
        },
        function (result) {
            $("#brandPagination").html("");
            $("#brandPagination").pagination({
                items: result.count,
                itemsOnPage: 1,
                cssStyle: 'light-theme',
                onPageClick: function(page,e) {
                    changePage(e, page, p);
                }
            });
        },
        function (result) {
            Log(result);
        });
}





function showeditDialogBrand(id) {
    showDialogBox("/api/Template/GetBrandForm/",
        { BrandId: id },
        "Edit Brand Information");
}

function showdeleteDialogBrand(id) {
    showConfirmDialogBox("Delete Brand",
        "Do you want to delete this Brand?<br>Deleting this Brand will also delete its brands and its Medicines",
        "Delete Brand",
        "No",
        function (e) {
            ajaxPost("/api/Brand/DeleteBrand",
                {
                    Id: id
                },
                function (result) {
                    if (result.message === "Success") {
                        showNotification("Brand Deleted", NotificationEnum.Success);
                        $("#row_brand_" + id).animate({ opacity: "0.0" }, "slow", function () {
                            $(this).remove();
                            $("#myConfirmModal").modal("hide");
                        });
                    }
                },
                function (result) {

                });
        });
}

function showaddDialogBrand() {
    showDialogBox("/api/Template/GetBrandForm/",
        null,
        "Add Brand Information");
}


function showImageDialogBrand(id) {
    showDialogBox("/api/Template/GetUploadForm/",
        {
            ActionUrl: "/File/SaveBrandProfile",
            Id: id,
            Limit: 1
        },
        "Change Brand Picture");
}