﻿function changePage(e, index, parameter) {
    const p = parameter;

    ajaxPost("/api/Template/GetMedicines/",
        {
            Parameter: p,
            Page: index,
            Size: 10
        },
        function (result) {
            $("#medicineTable").html("");

            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#medicineTable").append(d);
                });
            } else {
                $("#medicineTable").html("<p>No Data Found</p>");
            }
        },
        function (result) {
            $("#medicineTable").html("");
            $("#medicineTable").html("<p>Error Occured</p>");
            Log(result);
        });
}

function searchMedicine(e, event) {
    if (e !== null && event.which !== 13) {
        return;
    }
    var p = e === null ? "" : $(e).val();
    showLoading("overlay");
    ajaxPost("/api/Template/GetMedicines/",
        {
            Parameter: p,
            Page: 1,
            Size: 10
        },
        function (result) {
            $("#medicineTable").html("");
            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#medicineTable").append(d);
                });
            } else {
                $("#medicineTable").html("<p>No Data Found</p>");
            }
            hideLoading("overlay");

        },
        function (result) {
            $("#medicineTable").html("");
            $("#medicineTable").html("<p>Error Occured</p>");
            hideLoading("overlay");
            Log(result);
        });

    ajaxPost("/api/Template/GetMedicinesCount/",
        {
            Parameter: p,
            Page: 1,
            Size: 10
        },
        function (result) {
            $("#medicinePagination").html("");
            $("#medicinePagination").pagination({
                items: result.count,
                itemsOnPage: 1,
                cssStyle: 'light-theme',
                onPageClick: function (page, e) {
                    changePage(e, page, p);
                }
            });
        },
        function (result) {
            Log(result);
        });
}





function showeditDialogMedicine(id) {
    showDialogBox("/api/Template/GetMedicineForm/",
        { MedicineId: id },
        "Edit Medicine Information");
}

function showdeleteDialogMedicine(id) {
    showConfirmDialogBox("Delete Medicine",
        "Do you want to delete this Medicine?<br>Deleting this Medicine will also delete its Medicines",
        "Delete Medicine",
        "No",
        function (e) {
            ajaxPost("/api/Medicine/DeleteMedicine",
                {
                    Id: id
                },
                function (result) {
                    if (result.message === "Success") {
                        showNotification("Medicine Deleted", NotificationEnum.Success);
                        $("#row_medicine_" + id).animate({ opacity: "0.0" }, "slow", function () {
                            $(this).remove();
                            $("#myConfirmModal").modal("hide");
                        });
                    }
                },
                function (result) {

                });
        });
}

function showaddDialogMedicine() {
    showDialogBox("/api/Template/GetMedicineForm/",
        null,
        "Add Medicine Information");
}


function showImageDialogMedicine(id) {
    showDialogBox("/api/Template/GetUploadForm/",
        {
            ActionUrl: "/File/SaveMedicineProfile",
            Id: id,
            Limit: 1
        },
        "Change Medicine Picture");
}