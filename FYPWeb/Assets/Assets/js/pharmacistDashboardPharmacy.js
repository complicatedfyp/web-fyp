﻿$(function () {
    searchPharmacy(null,null,userId);
});

function changePage(e, index, parameter,userId) {
    const p = parameter;

    ajaxPost("/api/Template/GetPharmacies/",
        {
            UserId: userId,
            Parameter: p,
            Page: index,
            Size: 10
        },
        function (result) {
            $("#pharmacyTable").html("");

            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#pharmacyTable").append(d);
                });
            } else {
                $("#pharmacyTable").html("<p>No Data Found</p>");
            }
            $("#pharmacyPagination").find("a").each(function () {
                $(this).removeClass("active");
            });
            $(e).addClass("active");
        },
        function (result) {
            $("#pharmacyTable").html("");
            $("#pharmacyTable").html("<p>Error Occured</p>");
            Log(result);
        });
}

function searchPharmacy(e, event,userId) {
    if (e !== null && event.which !== 13) {
        return;
    }
    var p = e === null ? "" : $(e).val();
    showLoading("overlay");
    ajaxPost("/api/Template/GetPharmacies/",
        {
            UserId: userId,
            Parameter: p,
            Page: 1,
            Size: 10
        },
        function (result) {
            $("#pharmacyTable").html("");
            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#pharmacyTable").append(d);
                });
            } else {
                $("#pharmacyTable").html("<p>No Data Found</p>");
            }
            hideLoading("overlay");

        },
        function (result) {
            $("#pharmacyTable").html("");
            $("#pharmacyTable").html("<p>Error Occured</p>");
            hideLoading("overlay");
            Log(result);
        });

    ajaxPost("/api/Template/GetPharmaciesCount/",
        {
            Parameter: p,
            UserId: userId,
            Page: 1,
            Size: 10
        },
        function (result) {
            $("#pharmacyPagination").html("");
            for (let i = 0; i < result.count; i++) {
                $("#pharmacyPagination").append("<li><a href='#' onclick='changePage(this," + (i + 1) + ",\"" + p + "\","+ userId + ")'>" + (i + 1) + "</a></li>");
            }
        },
        function (result) {
            Log(result);
        });
}





function showeditDialogPharmacy(id,userId) {
    showDialogBox("/api/Template/GetPharmacyForm/",
        { PharmacyId: id,UserId: userId },
        "Edit Pharmacy Information");
}

function showdeleteDialogPharmacy(id,userId) {
    showConfirmDialogBox("Delete Pharmacy",
        "Do you want to delete this Pharmacy?<br>Deleting this Pharmacy will also delete its pharmacys and its Medicines",
        "Delete Pharmacy",
        "No",
        function (e) {
            ajaxPost("/api/Pharmacy/DeletePharmacy",
                {
                    Id: id,
                    UserId: userId,
                },
                function (result) {
                    if (result.message === "Success") {
                        showNotification("Pharmacy Deleted", NotificationEnum.Success);
                        $("#row_pharmacy_" + id).animate({ opacity: "0.0" }, "slow", function () {
                            $(this).remove();
                            $("#myConfirmModal").modal("hide");
                        });
                    }
                },
                function (result) {

                });
        });
}

function showaddDialogPharmacy(userId) {
    showDialogBox("/api/Template/GetPharmacyForm/",
        {
            UserId: userId,
        },
        "Add Pharmacy Information");
}


function showImageDialogPharmacy(id) {
    showDialogBox("/api/Template/GetUploadForm/",
        {
            ActionUrl: "/File/SavePharmacyProfile",
            Id: id,
            Limit: 1
        },
        "Change Pharmacy Picture");
}