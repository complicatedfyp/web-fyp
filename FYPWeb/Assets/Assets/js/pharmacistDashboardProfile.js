﻿function updateProfile(id) {
    const brandForm = $("#step-1");
    brandForm.validator('validate');
    const elmErr = brandForm.children('.has-error');
    if (elmErr && elmErr.length > 0) {
        return;
    }
    const fname = $("#FirstName").val();
    const lname = $("#LastName").val();
    const gender = $("input[name='gender']:checked").val();
    const phonenumber = $("#PhoneNumber").val();
    const dob = $("#DOB").val();
    const address = $("#Address").val();
    const aboutme = $("#AboutMe").val();
    const signature = $("#Signature").val();
    ajaxPost("/api/User/UpdateUser/",
        {
            UserId: id,
            FirstName: fname,
            LastName: lname,
            Gender: gender,
            PhoneNumber: phonenumber,
            DOB: dob,
            AboutMe: aboutme,
            Signature: signature,
            Address: address
        },
        function(result) {
            if (result.message === "Success") {
                showNotification("Profile Updated", NotificationEnum.Success);
            }
        },
        function(result) {
            showNotification("Error Updating Profile", NotificationEnum.Warning);
        });
}

function showImageDialogUser(id) {
    showDialogBox("/api/Template/GetUploadForm/",
        {
            ActionUrl: "/File/SaveUserProfile",
            Id: id,
            Limit: 1
        },
        "Change Profile Picture");
}