﻿$(function () {
    $(".slimScrollDiv").slimScroll({
        height: "250px"
    });
});


function loadMoreReviews(id,divId,type) {
    
}

function loadMoreActivites(id, divId, type) {

}

function showMap(id) {
    showDialogBox("/api/Template/GetMapPharmacy/",
        { PharmacyId: id },
        "Map");
}

function showLoginRegsiterForm() {
    $("#myLoginModel").modal("show");
}


function initMapPharmacy(x,y)
{
    var uluru = { lat: x, lng: y };
    if (x !== "" && y !== "") {
        uluru = { lat: parseFloat(x), lng: parseFloat(y) };
    }
    var map = new google.maps.Map(document.getElementById('map'),
        {
            zoom: 17,
            center: uluru
        });
    var marker = new google.maps.Marker({
        position: uluru,
        map: map,
        icon: "/Assets/Assets/img/logo-32.png"
    });
}