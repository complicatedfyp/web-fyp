function changeOrderType(e) {
    if (e.value === "cart") {
        $("#cart").show();
        $("#prescription").hide();
        $("#OrderType").val(0);
    } else if (e.value === "prescription") {
        $("#cart").hide();
        $("#prescription").show();
        $("#OrderType").val(1);
    }
}


$(function () {
    $("#cart").show();
    $("#prescription").hide();
    $(".image-picker").imagepicker();
});

function showMap(id) {
    showDialogBox("/api/Template/GetMapPharmacy/",
        { PharmacyId: id },
        "Map");
}

function showSelectLocationForm() {
    showDialogBox("/api/Template/GetSelectLocationForm/",
        null,
        "Map");
}

function setLocation() {
    const brandForm = $("#step-1");
    brandForm.validator('validate');
    const elmErr = brandForm.children('.has-error');
    if (elmErr && elmErr.length > 0) {
        return;
    }
    var address = $("#Address").val();
    var x = $("#Xaxis").val();
    var y = $("#Yaxis").val();
    $("#myAddress").html(address);
    $("#myX").html(x);
    $("#myY").html(y);
    getTotalDistance(x, y, $("#px").html(), $("#py").html());
    showNotification("Location Set", NotificationEnum.Success);
    closeDialogBox();
}

function placeOrderForm(userId, pharmacistId = 0) {
    showDialogBox("/api/Template/GetOrderPlaceForm",
        {
            UserId: userId,
            PharmacyId: pharmacistId
        }, "Place Order Dialog");
}
function placeOrderFormPrescription(userId) {
    var prescriptions = $("#Prescription").val();
    var pharmacyId = $("#PharmacyId").val();
    showDialogBox("/api/Template/GetOrderPlaceForm",
        {
            UserId: userId,
            PharmacyId: pharmacyId,
            Prescriptions: prescriptions
        }, "Place Order Dialog");
}

function placeOrderPrescription() {
    var pharmacistId = [];
    var prescription = [];
    $("input[name=PharmacyId]").each(function () {
        pharmacistId.push($(this).val());
    });
    $("input[name=Prescription]").each(function () {
        prescription.push($(this).val());
    });
    var promotion = [];
    $("input[name=PromotionId]").each(function () {
        promotion.push($(this).val());
    });
    var address = $("#Address").val();
    var x = $("#Xaxis").val();
    var y = $("#Yaxis").val();
    var userId = $("#UserId").val();
    if (address === "" || x === "" || y === "") {
        showNotification("Please Select Delivery Location", NotificationEnum.Info);
        return;
    }
    ajaxPost("/api/Order/PlaceOrderPrescription",
        {
            UserId: userId,
            Address: address,
            X: x,
            Y: y,
            PromotionId: promotion,
            PharmacyId: pharmacistId,
            Prescriptions: prescription
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Order is Placed", NotificationEnum.Success);
                goToUrl("/Customer/Order?type=Pending");
            }
        },
        function (result) {
            showNotification("Error Placing Order", NotificationEnum.Warning);
        });
    
}
function placeOrder() {
    var pharmacistId = [];
    $("input[name=PharmacyId]").each(function () {
        pharmacistId.push($(this).val());
    });
    var promotion = [];
    $("input[name=PromotionId]").each(function () {
        promotion.push($(this).val());
    });
    var address = $("#Address").val();
    var x = $("#Xaxis").val();
    var y = $("#Yaxis").val();
    var userId = $("#UserId").val();
    if (address === "" || x === "" || y === "") {
        showNotification("Please Select Delivery Location", NotificationEnum.Info);
        return;
    }
    ajaxPost("/api/Order/PlaceOrder",
        {
            UserId: userId,
            Address: address,
            X: x,
            Y: y,
            PromotionId: promotion,
            PharmacyId: pharmacistId
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Order is Placed", NotificationEnum.Success);
                goToUrl("/Customer/Order?type=Pending");
            }
        },
        function (result) {
            showNotification("Error Placing Order", NotificationEnum.Warning);
        });
}

function getTotalDistance(x, y, x1, y1) {
    var origin1 = new google.maps.LatLng(x, y);
    var origin2 = new google.maps.LatLng(x1, y1);

    var distanceService = new google.maps.DistanceMatrixService();
    distanceService.getDistanceMatrix({
        origins: [origin1],
        destinations: [origin2],
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.METRIC,
        durationInTraffic: false,
        avoidHighways: false,
        avoidTolls: false
    },
        function (response, status) {
            if (status !== google.maps.DistanceMatrixStatus.OK) {
                console.log('Error:', status);
            } else {
                var distance = response.rows[0].elements[0].distance.text;
                var duration = response.rows[0].elements[0].duration.text;
                $("#myDistance").html(distance);
                $("#myDistance").data('value', response.rows[0].elements[0].distance.value);
                $("#myTime").data('value', response.rows[0].elements[0].duration.value);
                $("#myTime").html(duration);
            }
        });
}

function initMapPharmacy(x, y) {
    var uluru = { lat: x, lng: y };
    if (x !== "" && y !== "") {
        uluru = { lat: parseFloat(x), lng: parseFloat(y) };
    }
    var map = new google.maps.Map(document.getElementById('map'),
        {
            zoom: 17,
            center: uluru
        });
    var marker = new google.maps.Marker({
        position: uluru,
        map: map,
        icon: "/Assets/Assets/img/logo-32.png"
    });
}

function initFunctionSetLocationForm() {
    var uluru = { lat: 31.582045, lng: 74.329376 };
    var map = new google.maps.Map(document.getElementById('map'),
        {
            zoom: 17,
            center: uluru
        });
    var marker = new google.maps.Marker({
        position: uluru,
        map: map,
        icon: "/Assets/Assets/img/logo-32.png"
    });
    var geocoder = new google.maps.Geocoder;

    google.maps.event.addListener(map,
        'center_changed',
        function () {
            // 0.1 seconds after the center of the map has changed,
            // set back the marker position.
            window.setTimeout(function () {
                var center = map.getCenter();
                marker.setPosition(center);
                var lng = marker.getPosition().lng();
                var lat = marker.getPosition().lat();
                $("#Xaxis").val(lat);
                $("#Yaxis").val(lng);
            },
                100);
        });
    var input = document.getElementById("SearchMap");
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    var searchBox = new google.maps.places.SearchBox(input);
    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function () {
        searchBox.setBounds(map.getBounds());
    });

    searchBox.addListener('places_changed',
        function () {
            var places = searchBox.getPlaces();
            if (places.length === 0) {
                return;
            }


            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function (place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                map.setCenter(place.geometry.location);
            });
        });

    $("#SelectLocation").on("click",
        function (e) {
            geocodeLatLng(geocoder);
        });
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            marker.setPosition(pos);
            map.setCenter(pos);
        }, function () {

        });
    } else {
        // Browser doesn't support Geolocation
    }
}

function redeemPromotion(id) {
    var code = $("#PromotionCode-" + id).val();
    var pId = $("#PharmacyId-" + id).val();
    if (code.length === 0) {
        showNotification("Enter Code to Redeem Promotion", NotificationEnum.Warning);
        return;
    }
    ajaxPost("/api/Promotion/RedeemPromotion",
        {
            Code: code,
            PharmacyId: pId
        },
        function (result) {
            if (result.message === "Success") {
                $("#PromotionId-" + id).val(result.Id);
                $("#promotionLabel-" + id).html("Promotion : " + result.Promotion);
            } else {
                showNotification("No Promotion Found for current Pharmacy", NotificationEnum.Warning);
            }
        },
        function () {
            showNotification("Error Occured", NotificationEnum.Warning);
        });
}

function geocodeLatLng(geocoder) {
    var latlng = { lat: parseFloat($("#Xaxis").val()), lng: parseFloat($("#Yaxis").val()) };
    geocoder.geocode({ 'location': latlng }, function (results, status) {
        if (status === 'OK') {
            if (results[0]) {
                $("#Address").val(results[0].formatted_address);
            } else {
                Log("Nothing Found");
            }
        } else {
            Log("Error Occured");
        }
    });
}