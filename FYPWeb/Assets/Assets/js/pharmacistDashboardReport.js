﻿function showGenerateReportDialog(id) {
    if (hasPharamcy != "True") {
        showNotification("You have no Pharmacy Approved/Added", NotificationEnum.INFO);
        return;
    }
    showDialogBox("/api/Template/GetReportForm",
    {
        UserId: id
    },"Generate Report");


}

function initMapPharmacy(x, y) {
    var uluru = { lat: x, lng: y };
    if (x !== "" && y !== "") {
        uluru = { lat: parseFloat(x), lng: parseFloat(y) };
    }
    var map = new google.maps.Map(document.getElementById('map'),
        {
            zoom: 17,
            center: uluru
        });
    var marker = new google.maps.Marker({
        position: uluru,
        map: map,
        icon: "/Assets/Assets/img/logo-32.png"
    });
}


function initReport() {
    $("#StartDate").daterangepicker({
        locale: {
            format: "MM/DD/YYYY"
        },
        startDate: moment().format("MM/DD/YYYY"),
        endDate: moment().format("MM/DD/YYYY")
    });
}

function showorderDetailDialog(id) {
    showDialogBox("/api/Template/GetOrderDetailForm/",
        {
            OrderId: id,

        },
        "Order Details");
}

function generateReport() {
    const id = $("#PharmacyId").val();
    const employeeId = $("#EmployeeId").val();
    const startDate = $("#StartDate").val().split(" - ")[0];
    const endDate = $("#StartDate").val().split(" - ")[1];
    ajaxPost("/api/Template/GenerateReport/",
        {
            PharmacyId: id,
            EmployeeId: employeeId,
            StartDate: startDate,
            EndDate: endDate,
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Report Generated", NotificationEnum.Success);
                $("#result").html(result.result);
                closeDialogBox();
            }
        },
        function (result) {
            showNotification("Error Generating Report", NotificationEnum.Warning);
        });
}
