﻿function addToPharmacyInventory() {
    var medicineId = $("#MedicineId").val();
    var pharmacyId = $("#PharmacyId").val();
    var quantity = $("#Quantity").val();
    const radioValue = $("input[name='AddType']:checked").val();
    if (pharmacyId > 0) {
        ajaxPost("/api/Inventory/SaveInventory/",
            {
                DrugId: medicineId,
                PharmacyId: pharmacyId,
                AddType: radioValue,
                Quantity: quantity,
            },
            function (result) {
                if (result.message === "Success") {
                    showNotification("Medicine Added To Inventory", NotificationEnum.Success);
                    closeDialogBox();
                }
            },
            function (result) {
                showNotification("Error Occured", NotificationEnum.Warning);
            });
    }
}

function removeFromPharmacyInventory() {
    var id = $("#Id").val();
    var quantity = $("#Quantity").val();
    const radioValue = $("input[name='AddType']:checked").val();
    if (pharmacyId > 0) {
        ajaxPost("/api/Inventory/RemovesInventory/",
            {
                Id: id,
                AddType: radioValue,
                Quantity: quantity,
            },
            function (result) {
                if (result.message === "Success") {
                    showNotification("Medicine Removed From Inventory", NotificationEnum.Success);
                    closeDialogBox();
                } else {
                    showNotification("Removing More than Quantity", NotificationEnum.Success);
                }
            },
            function (result) {
                showNotification("Error Occured", NotificationEnum.Warning);
            });
    }
}