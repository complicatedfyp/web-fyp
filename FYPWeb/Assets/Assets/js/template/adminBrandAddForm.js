﻿
function saveBrand() {
    const brandForm = $("#step-1");
    brandForm.validator('validate');
    const elmErr = brandForm.children('.has-error');
    if (elmErr && elmErr.length > 0) {
        return;
    }

    const name = $("#Name").val();
    const companyId = $("#Company").val();
    ajaxPost("/api/Brand/SaveBrand/",
        {
            Name: name,
            CompanyId: companyId,
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Brand Added", NotificationEnum.Success);
                ajaxPost("/api/Template/GetBrandSingleRow/",
                    {
                        Id: result.result.Id
                    },
                    function (result2) {
                        $("#brandTable").prepend(result2.result);
                    },
                    function (result2) {
                        showNotification("Error Occured", NotificationEnum.Warning);
                    });
                $("#close").click();
            }
        },
        function (result) {
            showNotification("Error Occured", NotificationEnum.Warning);
        });
}
function editBrand() {
    const id = $("#BrandId").val();
    const name = $("#Name").val();
    const companyId = $("#CompanyId").val();
    ajaxPost("/api/Brand/UpdateBrand/",
        {
            Id: id,
            CompanyId: companyId,
            Name: name,
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Brand Updated", NotificationEnum.Success);
            }
        },
        function (result) {
            showNotification("Error Updating Brand", NotificationEnum.Warning);
        });
}


