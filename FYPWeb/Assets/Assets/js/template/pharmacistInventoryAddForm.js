﻿
function saveInventory() {
    const brandForm = $("#step-1");
    brandForm.validator('validate');
    const elmErr = brandForm.children('.has-error');
    if (elmErr && elmErr.length > 0) {
        return;
    }

    const qty = $("#Quantity").val();
    const medicineId = $("#MedicineId").val();
    const pharmacyId = $("#PharmacyId").val();
    const radioValue = $("input[name='AddType']:checked").val();
    ajaxPost("/api/Inventory/SaveInventory/",
        {
            DrugId: medicineId,
            Quantity: qty,
            PharmacyId: pharmacyId,
            AddType: radioValue,
        },
        function (result) {
            if (result.message === "Success") {
                location.reload();
            }
        },
        function (result) {
            showNotification("Error Occured", NotificationEnum.Warning);
        });
}


