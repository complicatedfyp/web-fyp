﻿function initFunctionPharamcyAddForm() {
    var uluru = { lat: 31.582045, lng: 74.329376};
    if (x !== "" && y !== "") {
         uluru = { lat: parseFloat(x), lng: parseFloat(y) };
    }
    var map = new google.maps.Map(document.getElementById('map'),
        {
            zoom: 17,
            center: uluru
        });
    $("#Xaxis").val(uluru.lat);
    $("#Yaxis").val(uluru.lng);
    var marker = new google.maps.Marker({
        position: uluru,
        map: map,
        icon: "/Assets/Assets/img/logo-32.png"
    });
    var geocoder = new google.maps.Geocoder;
    google.maps.event.addListener(map,
        'center_changed',
        function () {
            // 0.1 seconds after the center of the map has changed,
            // set back the marker position.
            window.setTimeout(function () {
                var center = map.getCenter();
                marker.setPosition(center);
                var lng = marker.getPosition().lng();
                var lat = marker.getPosition().lat();
                $("#Xaxis").val(lat);
                $("#Yaxis").val(lng);
            },
                100);
        });
    var input = document.getElementById("SearchMap");
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    var searchBox = new google.maps.places.SearchBox(input);
    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function () {
        searchBox.setBounds(map.getBounds());
    });

    searchBox.addListener('places_changed',
        function () {
            var places = searchBox.getPlaces();
            if (places.length === 0) {
                return;
            }

            
            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function (place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                map.setCenter(place.geometry.location);
            });
        });

    $("#SelectLocation").on("click",
        function(e) {
            geocodeLatLng(geocoder);
        });
}

function geocodeLatLng(geocoder) {
    var latlng = { lat: parseFloat($("#Xaxis").val()), lng: parseFloat($("#Yaxis").val()) };
    geocoder.geocode({ 'location': latlng }, function (results, status) {
        if (status === 'OK') {
            if (results[0]) {
                $("#Address").val(results[0].formatted_address);
            } else {
                Log("Nothing Found");
            }
        } else {
            Log("Error Occured");
        }
    });
}



function savePharmacy() {
    const pharmacyForm = $("#step-1");
    pharmacyForm.validator('validate');
    const elmErr = pharmacyForm.children('.has-error');
    if (elmErr && elmErr.length > 0) {
        return;
    }

    const name = $("#Name").val();
    const contactPerson = $("#ContactPerson").val();
    const phoneNumber = $("#PhoneNumber").val();
    const address = $("#Address").val();
    const x = $("#Xaxis").val();
    const y = $("#Yaxis").val();
    const userId = $("#UserId").val();
    const charges = $("#Charges").val();
    ajaxPost("/api/Pharmacy/SavePharmacy/",
        {
            Name: name,
            ContactPerson: contactPerson,
            PhoneNumber: phoneNumber,
            Address: address,
            X: x,
            Y: y,
            UserId: userId,
            Charges: charges,
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Pharmacy Added - Wait for Admin to Approve Pharamcy", NotificationEnum.Success);
                ajaxPost("/api/Template/GetPharmacySingleRow/",
                    {
                        Id: result.result.Id
                    },
                    function (result2) {
                        $("#pharmacyTable").prepend(result2.result);
                    },
                    function (result2) {
                        showNotification("Error Occured", NotificationEnum.Warning);
                    });
                $("#close").click();
            }
        },
        function (result) {
            showNotification("Error Occured", NotificationEnum.Warning);
        });
}
function editPharmacy() {
    const id = $("#PharmacyId").val();
    const name = $("#Name").val();
    const contactPerson = $("#ContactPerson").val();
    const phoneNumber = $("#PhoneNumber").val();
    const address = $("#Address").val();
    const x = $("#Xaxis").val();
    const y = $("#Yaxis").val();
    const charges = $("#Charges").val();

    const userId = $("#UserId").val();
    ajaxPost("/api/Pharmacy/UpdatePharmacy/",
        {
            Id: id,
            Name: name,
            ContactPerson: contactPerson,
            PhoneNumber: phoneNumber,
            Address: address,
            X: x,
            Charges: charges,
            Y: y,
            UserId: userId
        },
        function (result) {
            if (result.message === "Success") {
                if (result.isLocationChanged) {
                    showNotification("Pharmacy Updated - Wait for Admin to Approve Changes", NotificationEnum.Success);
                }
                else{
                    showNotification("Pharmacy Updated", NotificationEnum.Success);
                }
            }
        },
        function (result) {
            showNotification("Error Updating Pharmacy", NotificationEnum.Warning);
        });
}


