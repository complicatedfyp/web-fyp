﻿function sendRecommendation() {
    var userId = $("#UserId").val();
    var medicineId = $("#MedicineId").val();
    var userRecieverId = $("#UserRecieverId").val();

    if (userRecieverId <= 0) {
        return;
    }
    ajaxPost("/api/Recommendation/SendRecommendation",
        {
            UserId: userId,
            MedicineId: medicineId,
            UserRecieverId: userRecieverId,
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("User Recommended", NotificationEnum.Success);
            } else if (result.message === "Already Exists") {
                showNotification("User Already Recommended by You", NotificationEnum.Info);
            }
            else if (result.message === "Cannot Send Same User") {
                showNotification("Cannot Send yourself Recommendation", NotificationEnum.Info);
            }
        },
        function (result) {
            showNotification("Error Occured", NotificationEnum.Warning);
        });

}