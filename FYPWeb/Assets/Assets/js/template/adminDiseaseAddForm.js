﻿
function saveDisease() {
    const diseaseForm = $("#step-1");
    diseaseForm.validator('validate');
    const elmErr = diseaseForm.children('.has-error');
    if (elmErr && elmErr.length > 0) {
        return;
    }

    const name = $("#Name").val();
    const description = $("#Description").val();
    ajaxPost("/api/Disease/SaveDisease/",
        {
            Name: name,
            Description: description,
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Disease Added", NotificationEnum.Success);
                ajaxPost("/api/Template/GetDiseaseSingleRow/",
                    {
                        Id: result.result.Id
                    },
                    function (result2) {
                        $("#diseaseTable").prepend(result2.result);
                    },
                    function (result2) {
                        showNotification("Error Occured", NotificationEnum.Warning);
                    });
                $("#close").click();
            }
        },
        function (result) {
            showNotification("Error Occured", NotificationEnum.Warning);
        });
}
function editDisease() {
    const id = $("#DiseaseId").val();
    const name = $("#Name").val();
    const description = $("#Description").val();
    ajaxPost("/api/disease/Updatedisease/",
        {
            Id: id,
            Description: description,
            Name: name,
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Disease Updated", NotificationEnum.Success);
            }
        },
        function (result) {
            showNotification("Error Updating Disease", NotificationEnum.Warning);
        });
}


