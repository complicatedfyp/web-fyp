﻿function savePromotion() {
    const promotionForm = $("#step-1");
    promotionForm.validator('validate');
    const elmErr = promotionForm.children('.has-error');
    if (elmErr && elmErr.length > 0) {
        return;
    }

    const name = $("#Name").val();
    const pharmacyId = $("#PharmacyId").val();
    const description = $("#Description").val();
    const percentOff = $("#PercentOff").val();
    const code = $("#Code").val();
    ajaxPost("/api/Promotion/SavePromotion/",
        {
            Name: name,
            PharmacyId: pharmacyId,
            Description: description,
            PercentOff: percentOff,
            Code: code,
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Promotion Added", NotificationEnum.Success);
                closeDialogBox();
                ajaxPost("/api/Template/GetPromotionSingleRow/",
                    {
                        Id: result.result.Id
                    },
                    function(result2) {
                        $("#promotionTable").prepend(result2.result);
                    },
                    function(result2) {
                        showNotification("Error Occured", NotificationEnum.Warning);
                    });
                $("#close").click();
            } else if (result.message === "Error") {
                showNotification("Code Already Used by another Promotion", NotificationEnum.Warning);
            }
        },
        function (result) {
            showNotification("Error Occured", NotificationEnum.Warning);
        });
}
function editPromotion() {
    const id = $("#PromotionId").val();
    const name = $("#Name").val();
    const pharmacyId = $("#PharmacyId").val();
    const description = $("#Description").val();
    const percentOff = $("#PercentOff").val();
    const code = $("#Code").val();

    ajaxPost("/api/Promotion/UpdatePromotion/",
        {
            PromotionId: id,
            Name: name,
            PharmacyId: pharmacyId,
            Description: description,
            PercentOff: percentOff,
            Code: code,
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Promotion Updated", NotificationEnum.Success);
                closeDialogBox();
            } else if (result.message === "Error") {
                showNotification("Code Already Used by another Promotion", NotificationEnum.Warning);
            }
        },
        function (result) {
            showNotification("Error Updating Promotion", NotificationEnum.Warning);
        });
}

function startPromotion() {
    const id = $("#PromotionId").val();
    const startDate = $("#StartDate").val().split(" - ")[0];
    const endDate = $("#StartDate").val().split(" - ")[1];
    ajaxPost("/api/Promotion/StartPromotion/",
        {
            PromotionId: id,
            StartDate: startDate,
            EndDate: endDate,
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Promotion Started", NotificationEnum.Success);
                $("#row_promotion_" + id).animate({ opacity: "0.0" }, "slow", function () {
                    $(this).remove();
                    $("#myModal").modal("close");
                });
            }
        },
        function (result) {
            showNotification("Error Updating Promotion", NotificationEnum.Warning);
        });
}

function endPromotion() {
    const id = $("#PromotionId").val();
    const startDate = $("#StartDate").val().split(" - ")[0];
    const endDate = $("#StartDate").val().split(" - ")[1];
    ajaxPost("/api/Promotion/EndPromotion/",
        {
            PromotionId: id,
            StartDate: startDate,
            EndDate: endDate,
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Promotion Stopped", NotificationEnum.Success);
                $("#row_promotion_" + id).animate({ opacity: "0.0" }, "slow", function () {
                    $(this).remove();
                    $("#myConfirmModal").modal("hide");
                });
            }
        },
        function (result) {
            showNotification("Error Updating Promotion", NotificationEnum.Warning);
        });
}