﻿
function saveCategory() {
    const categoryForm = $("#step-1");
    categoryForm.validator('validate');
    const elmErr = categoryForm.children('.has-error');
    if (elmErr && elmErr.length > 0) {
        return;
    }
    const name = $("#Name").val();
    ajaxPost("/api/Category/SaveCategory/",
        {
            Name: name,
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Category Added", NotificationEnum.Success);
                ajaxPost("/api/Template/GetCategorySingleRow/",
                    {
                        Id: result.result.Id
                    },
                    function (result2) {
                        $("#categoryTable").prepend(result2.result);
                    },
                    function (result2) {
                        showNotification("Error Occured", NotificationEnum.Warning);
                    });
                $("#close").click();
            }
        },
        function (result) {
            showNotification("Error Occured", NotificationEnum.Warning);
        });
}
function editCategory() {
    const id = $("#CategoryId").val();
    const name = $("#Name").val();
    ajaxPost("/api/Category/UpdateCategory/",
        {
            Id: id,
            Name: name,
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Category Updated", NotificationEnum.Success);
            }
        },
        function (result) {
            showNotification("Error Updating Category", NotificationEnum.Warning);
        });
}


