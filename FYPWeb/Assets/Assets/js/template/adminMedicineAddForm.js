﻿
function saveMedicine() {
    const medicineForm = $("#step-1");
    medicineForm.validator('validate');
    const elmErr = medicineForm.children('.has-error');
    if (elmErr && elmErr.length > 0) {
        return;
    }

    const name = $("#Name").val();
    ajaxPost("/api/Medicine/SaveMedicine/",
        {
            Name: name,
            CostPrice: $("#CostPrice").val(),
            Form: $("#Form").val(),
            Packing: $("#Packing").val(),
            SalePrice: $("#SalePrice").val(),
            BrandId: $("#Brand").val(),
            DrugDetailId: $("#MedicineDetail").val(),

        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Medicine Added", NotificationEnum.Success);
                ajaxPost("/api/Template/GetMedicineSingleRow/",
                    {
                        Id: result.result.Id
                    },
                    function (result2) {
                        $("#medicineTable").prepend(result2.result);
                    },
                    function (result2) {
                        showNotification("Error Occured", NotificationEnum.Warning);
                    });
                $("#close").click();
            }
        },
        function (result) {
            showNotification("Error Occured", NotificationEnum.Warning);
        });
}
function editMedicine() {
    const id = $("#MedicineId").val();
    const name = $("#Name").val();
    ajaxPost("/api/Medicine/UpdateMedicine/",
        {
            Id: id,
            Name: name,
            CostPrice: $("#CostPrice").val(),
            Form: $("#Form").val(),
            Packing: $("#Packing").val(),
            SalePrice: $("#SalePrice").val(),
            BrandId: $("#Brand").val(),
            DrugDetailId: $("#MedicineDetail").val(),
    },
        function (result) {
            if (result.message === "Success") {
                showNotification("Medicine Updated", NotificationEnum.Success);
            }
        },
        function (result) {
            showNotification("Error Updating Medicine", NotificationEnum.Warning);
        });
}


