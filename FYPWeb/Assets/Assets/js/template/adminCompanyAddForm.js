﻿function saveCompany() {
    const elmForm = $("#step-1");
    elmForm.validator('validate');
    const elmErr = elmForm.children('.has-error');
    if (elmErr && elmErr.length > 0) {
        return;
    }

    const name = $("#Name").val();
    const phonenumber = $("#PhoneNumber").val();
    const fax = $("#Fax").val();
    const link = $("#Link").val();
    const address = $("#Address").val();
    const cityId = $("#City").val();
    ajaxPost("/api/Company/SaveCompany/",
        {
            Name: name,
            Link: link,
            PhoneNumber: phonenumber.replace("_", ""),
            Fax: fax.replace("_",""),
            Address: address,
            CityId: cityId
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Company Added", NotificationEnum.Success);
                ajaxPost("/api/Template/GetCompanySingleRow",
                    {
                        Id: result.result.Id
                    },
                    function (result2) {
                        $("#companyTable").prepend(result2.result);
                    },
                    function (result2) {
                        showNotification("Error Occured", NotificationEnum.Warning);
                    });
                $("#close").click();
            }
        },
        function (result) {
            showNotification("Error Occured", NotificationEnum.Warning);
        });
}
function editCompany() {
    const id = $("#CompanyId").val();
    const name = $("#Name").val();
    const phonenumber = $("#PhoneNumber").val();
    const fax = $("#Fax").val();
    const link = $("#Link").val();
    const cityId = $("#City").val();
    const address = $("#Address").val();
    ajaxPost("/api/Company/UpdateCompany/",
        {
            CompanyId: id,
            Name: name,
            Link: link,
            PhoneNumber: phonenumber,
            Fax: fax,
            Address: address,
            CityId: cityId
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Company Updated", NotificationEnum.Success);

            }
        },
        function (result) {
            showNotification("Error Updating Company", NotificationEnum.Warning);
        });
}