﻿function initFun(limit,id) {
    Dropzone.options.myAwesomeDropzone = {
        maxFiles: limit,
        accept: function (file, done) {
            console.log("uploaded");

        },
        init: function () {
            this.on("maxfilesexceeded", function (file) {
                alert("No more files please!");
                this.removeFile(file);
            });
            this.on("sending", function (file, xhr, formData) {
                const value = id;
                formData.append("Id", value); // Append all the additional input data of your form here!
            });
        }
    };
    $("#dropzoneForm").dropzone();
}