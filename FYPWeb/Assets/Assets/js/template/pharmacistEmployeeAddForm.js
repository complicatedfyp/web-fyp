﻿
function saveEmployee() {
    const employeeForm = $("#step-1");
    employeeForm.validator('validate');
    const elmErr = employeeForm.children('.has-error');
    if (elmErr && elmErr.length > 0) {
        return;
    }

    const id = $("#EmployeeId").val();
    const name = $("#Name").val();
    const number = $("#PhoneNumber").val();
    const salary = $("#Salary").val();
    const userId = $("#UserId").val();
    const dutyhrs = $("#DutyHours").val();
    const address = $("#Address").val();
    ajaxPost("/api/Employee/SaveEmployee/",
        {
            Name: name,
            UserId: userId,
            PhoneNumber: number,
            Salary: salary,
            DutyHours: dutyhrs,
            Address: address
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Employee Added", NotificationEnum.Success);
                ajaxPost("/api/Template/GetEmployeeSingleRow/",
                    {
                        Id: result.result
                    },
                    function (result2) {
                        $("#employeeTable").prepend(result2.result);
                    },
                    function (result2) {
                        showNotification("Error Occured", NotificationEnum.Warning);
                    });
                $("#close").click();
            }
        },
        function (result) {
            showNotification("Error Occured", NotificationEnum.Warning);
        });
}
function editEmployee() {
    const id = $("#EmployeeId").val();
    const name = $("#Name").val();
    const number = $("#PhoneNumber").val();
    const salary = $("#Salary").val();
    const userId = $("#UserId").val();
    const dutyhrs = $("#DutyHours").val();
    const address = $("#Address").val();
    ajaxPost("/api/Employee/UpdateEmployee/",
        {
            Id: id,
            Name: name,
            UserId: userId,
            PhoneNumber: number,
            Salary: salary,
            DutyHours: dutyhrs,
            Address: address
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Employee Updated", NotificationEnum.Success);
            }
        },
        function (result) {
            showNotification("Error Updating Employee", NotificationEnum.Warning);
        });
}


function assignPharmacy() {
    const id = $("#EmployeeId").val();
    const userId = $("#UserId").val();
    const pharmacyId = $("#PharmacyId").val();
    ajaxPost("/api/Employee/AssignEmployee/",
        {
            Id: id,
            UserId: userId,
            PharmacyId: pharmacyId,
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Employee Assigned to Pharmacy", NotificationEnum.Success);
                closeDialogBox();
                location.reload();
            }
        },
        function (result) {
            showNotification("Error Assigning Employee", NotificationEnum.Warning);
        });
}
