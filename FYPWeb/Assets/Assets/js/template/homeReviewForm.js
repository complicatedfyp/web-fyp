﻿
function saveReview() {
    const employeeForm = $("#step-1");
    employeeForm.validator('validate');
    const elmErr = employeeForm.children('.has-error');
    if (elmErr && elmErr.length > 0) {
        return;
    }

    const id = $("#ReviewId").val();
    const medicineId = $("#MedicineId").val();
    const userId = $("#UserId").val();
    const review = $("#Review").val();
    const rating = $("#ratingReview").starRating('getRating');
    ajaxPost("/api/Review/SaveReview/",
        {
            Id: id,
            UserId: userId,
            MedicineId: medicineId,
            Review: review,
            Rating: rating
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Review Added", NotificationEnum.Success);
                $("#close").click();
                location.reload();
            }
        },
        function (result) {
            showNotification("Error Occured", NotificationEnum.Warning);
        });
}
function editReview() {
    const id = $("#ReviewId").val();
    const medicineId = $("#MedicineId").val();
    const userId = $("#UserId").val();
    const review = $("#Review").val();
    const rating = $("#ratingReview").starRating('getRating');
    ajaxPost("/api/Review/UpdateReview/",
        {
            Id: id,
            UserId: userId,
            MedicineId: medicineId,
            Review: review,
            Rating: rating
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Review Updated", NotificationEnum.Success);
            }
        },
        function (result) {
            showNotification("Error Updating Review", NotificationEnum.Warning);
        });
}

