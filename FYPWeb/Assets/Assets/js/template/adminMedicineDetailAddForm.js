﻿function initFunctionMedicineDetailAddForm(diseaseInt, diseaseCont) {
    $("#DiseaseInteraction").select2({
        multiple: true,
        ajax: {
            url: "/api/Disease/SearchDisease",
            type: "POST",
            delay: 250,
            data: function (params) {
                return {
                    Parameter: params.term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data.result,
                        function (val, i) {
                            return {
                                id: val.Id,
                                text: val.Name
                            }
                        })
                };
            }
        }
    });
    $("#DiseaseContradiction").select2({
        multiple: true,
        ajax: {
            url: "/api/Disease/SearchDisease",
            type: "POST",
            delay: 250,
            data: function (params) {
                return {
                    Parameter: params.term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data.result,
                        function (val, i) {
                            return {
                                id: val.Id,
                                text: val.Name
                            }
                        })
                };
            }
        },
    });
    if (diseaseInt.length > 0) {
        var d = JSON.parse(diseaseInt);
        var box = $("#DiseaseInteraction");
        d.forEach(function (data) {
            var option = new Option(data.text, data.id, true, true);
            box.append(option);
        });
        box.trigger('change');
    }
    if (diseaseCont.length > 0) {
        var d = JSON.parse(diseaseCont);
        var box = $("#DiseaseContradiction");
        d.forEach(function (data) {
            var option = new Option(data.text, data.id, true, true);
            box.append(option);
        });
        box.trigger('change');
    }
}


function saveMedicineDetail() {
    const elmForm = $("#step-1");
    elmForm.validator('validate');
    const elmErr = elmForm.children('.has-error');
    if (elmErr && elmErr.length > 0) {
        return;
    }

    const id = $("#MedicineDetailId").val();
    const name = $("#Name").val();
    const overviwe = $("#Overview").val();
    const diseaseInt = $("#DiseaseInteraction").val();
    const diseaseCont = $("#DiseaseContradiction").val();
    ajaxPost("/api/MedicineDetail/SaveMedicineDetail/",
        {
            Name: name,
            Overview: overviwe,
            DiseaseInteractions: diseaseInt,
            DiseaseContradictions: diseaseCont
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("MedicineDetail Added", NotificationEnum.Success);
                ajaxPost("/api/Template/GetMedicineDetailSingleRow",
                    {
                        Id: result.result.Id
                    },
                    function (result2) {
                        $("#medicineDetailTable").prepend(result2.result);
                    },
                    function (result2) {
                        showNotification("Error Occured", NotificationEnum.Warning);
                    });
                $("#close").click();
            }
        },
        function (result) {
            showNotification("Error Occured", NotificationEnum.Warning);
        });
}
function editMedicineDetail() {
    const id = $("#MedicineDetailId").val();
    const name = $("#Name").val();
    const diseaseInt = $("#DiseaseInteraction").val();
    const overviwe = $("#Overview").val();
    const diseaseCont = $("#DiseaseContradiction").val();
    ajaxPost("/api/MedicineDetail/UpdateMedicineDetail/",
        {
            Id: id,
            Name: name,
            DiseaseInteractions: diseaseInt,
            Overview: overviwe,
            DiseaseContradictions: diseaseCont

        },
        function (result) {
            if (result.message === "Success") {
                showNotification("MedicineDetail Updated", NotificationEnum.Success);
            }
        },
        function (result) {
            showNotification("Error Updating MedicineDetail", NotificationEnum.Warning);
        });
}