﻿function increment() {
    try {
        var count = parseInt($("#qtyCount").val());
        if (count < 100) {
            count++;
        }
        $("#qtyCount").val(count);
    } catch (e) {
        $("#qtyCount").val(1);
    }
}

function decrement() {
    try {
        var count = parseInt($("#qtyCount").val());
        if (count > 1) {
            count--;
        }
        $("#qtyCount").val(count);
    } catch (e) {
        $("#qtyCount").val(1);
    }
}

function showReviewDialog(medicineId, userId) {
    showDialogBox("/api/Template/GetReviewForm/",
        {
            MedicineId: medicineId,
            UserId: userId
        },
        "Add Review Information");
}

function setRating(r) {
    $("#rating").starRating({
        totalStars: 5,
        initialRating: r,
        starShape: "rounded",
        readOnly: true,
        starSize: 30,
        emptyColor: "lightgray",
        hoverColor: "salmon",
        activeColor: "green",
        useGradient: false
    });
}

function showRecommendationDialog(userId, medicineId) {
    showDialogBox("/api/Template/GetRecommendationForm/",
        {
            UserId: userId,
            MedicineId: medicineId,
        },
        "Recommend this Medicine");
}


function addToCartShow(medicineId, userId, sessionId = "") {
    const qty = $("#qtyCount").val();
    showDialogBox("/api/Template/GetAddToCartForm",
        {
            MedicineId: medicineId,
            UserId: userId,
            SessionId: sessionId,
            Quantity: qty
        },
        "Add To Cart Form");
}

function changeTotalPrice() {
    var item = parseInt($("#medicineItem").html());
    var qty = parseInt($("#medicineQuantity").html());
    var price = parseInt($("#medicinePrice").html());
    var total = price * (item * qty);
    $("#medicneTotal").html(total);
}

function changeItem(q) {
    $("#medicineItem").html(q);
    changeTotalPrice();
}
function changeQuantity(q) {
    $("#medicineQuantity").html($(q).val());
    changeTotalPrice();
}

function changePrice(e) {
    var price = $(e).find(":selected").attr('data-price');
    $("#medicinePrice").html(price);
    changeTotalPrice();
}

function addToCartFromInventory() {
    var qty = parseInt($("#Quantity").val());
    qty = parseInt($("#medicineItem").html()) * qty;
    const userId = $("#UserId").val();
    const inventoryId = $("#InventoryId").val();
    const sessionId = $("#SessionId").val();
    if (inventoryId == 0) {
        showNotification("Please Select Pharmacy", NotificationEnum.Info);
        return;
    }
    ajaxPost("/api/Medicine/AddToCart/",
        {
            MedicineId: inventoryId,
            UserId: userId,
            Quantity: qty,
            SessionId: sessionId,
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Medicine Added to Cart", NotificationEnum.Success);
                if (result.result > 0) {
                    $("#cartCount").html(result.result);
                }
                closeDialogBox();
            } else {
                showNotification("Low Pharamcy Stock", NotificationEnum.Info);
            }
        },
        function (result) {
            showNotification("Error Addeding to Cart", NotificationEnum.Warning);
        });
}

function addToCart(medicineId, userId, sessionId = "") {
    const qty = $("#qtyCount").val();
    ajaxPost("/api/Medicine/AddToCart/",
        {
            MedicineId: medicineId,
            UserId: userId,
            Quantity: qty,
            SessionId: sessionId,
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Medicine Added to Cart", NotificationEnum.Success);
                if (result.result > 0) {
                    $("#cartCount").html(result.result);
                }
            }
        },
        function (result) {
            showNotification("Error Addeding to Cart", NotificationEnum.Warning);
        });
}
function addToInventory(medicineId, userId) {
    const qty = $("#qtyCount").val();
    showDialogBox("/api/Template/GetAddToInventoryForm/",
        {
            UserId: userId,
            MedicineId: medicineId,
            Quantity: qty,
        },
        "Add To Inventory");

}


function showLoginRegsiterForm() {
    $("#myLoginModel").modal("show");
}
