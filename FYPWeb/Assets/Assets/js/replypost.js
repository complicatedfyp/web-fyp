﻿function submitForm() {
    for (var instance in CKEDITOR.instances)
        if (CKEDITOR.instances.hasOwnProperty(instance))
            CKEDITOR.instances[instance].updateElement();
    const postForm = $("#step-1");
    postForm.validator('validate');
    const elmErr = postForm.children('.has-error');
    if (elmErr && elmErr.length > 0) {
        return false;
    }
    $("#postForm").submit();
}
