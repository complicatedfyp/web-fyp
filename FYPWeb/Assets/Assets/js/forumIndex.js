﻿function searchPost(e, event) {
    if (e !== null && event.which !== 13) {
        return;
    }
    var p = e === null ? "" : $(e).val();
    ajaxPost("/api/Template/GetForumPostThreadRow/",
        {
            Parameter: p,
            Page: 1,
            Size: 10
        },
        function (result) {
            $("#postBody").html("");
            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#postBody").append(d);
                });
            } else {
                $("#postBody").html("<p>No Data Found</p>");
            }

        },
        function (result) {
            $("#postBody").html("");
            $("#postBody").html("<p>Error Occured</p>");
        });
}

function searchCategory(e, event) {
    if (e !== null && event.which !== 13) {
        return;
    }
    var p = e === null ? "" : $(e).val();
    ajaxPost("/api/Template/GetCategoryThreadRow/",
        {
            Parameter: p,
            Page: 1,
            Size: 10
        },
        function (result) {
            $("#categoryBody").html("");
            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#categoryBody").append(d);
                });
            } else {
                $("#categoryBody").html("<p>No Data Found</p>");
            }

        },
        function (result) {
            $("#categoryBody").html("");
            $("#categoryBody").html("<p>Error Occured</p>");
        });
}
