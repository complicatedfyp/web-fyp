﻿function changePage(e, index, parameter) {
    const p = parameter;

    ajaxPost("/api/Template/GetPharmacies/",
        {
            Parameter: p,
            Page: index,
            Size: 10,
            IsAdmin: true,
            Type: type
        },
        function (result) {
            $("#pharmacyTable").html("");

            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#pharmacyTable").append(d);
                });
            } else {
                $("#pharmacyTable").html("<p>No Data Found</p>");
            }
        },
        function (result) {
            $("#pharmacyTable").html("");
            $("#pharmacyTable").html("<p>Error Occured</p>");
            Log(result);
        });
}

function searchPharmacy(e, event) {
    if (e !== null && event.which !== 13) {
        return;
    }
    var p = e === null ? "" : $(e).val();
    showLoading("overlay");
    ajaxPost("/api/Template/GetPharmacies/",
        {
            Parameter: p,
            Size: 10,
            IsAdmin: true,
            Type: type
        },
        function (result) {
            $("#pharmacyTable").html("");
            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#pharmacyTable").append(d);
                });
            } else {
                $("#pharmacyTable").html("<p>No Data Found</p>");
            }
            hideLoading("overlay");

        },
        function (result) {
            $("#pharmacyTable").html("");
            $("#pharmacyTable").html("<p>Error Occured</p>");
            hideLoading("overlay");
            Log(result);
        });

    ajaxPost("/api/Template/GetPharmaciesCount/",
        {
            Parameter: p,
            Page: 1,
            Size: 10,
            IsAdmin: true,
            Type: type

        },
        function (result) {
            $("#pharmacyPagination").html("");
            $("#pharmacyPagination").pagination({
                items: result.count,
                itemsOnPage: 1,
                cssStyle: 'light-theme',
                onPageClick: function (page, e) {
                    changePage(e, page, p);
                }
            });
        },
        function (result) {
            Log(result);
        });
}





function showeditDialogPharmacy(id,userId) {
    showDialogBox("/api/Template/GetPharmacyForm/",
        { PharmacyId: id,UserId: userId },
        "Edit Pharmacy Information");
}

function showdeleteDialogPharmacy(id,userId) {
    showConfirmDialogBox("Delete Pharmacy",
        "Do you want to delete this Pharmacy?<br>Deleting this Pharmacy will also delete its pharmacys and its Medicines",
        "Delete Pharmacy",
        "No",
        function (e) {
            ajaxPost("/api/Pharmacy/DeletePharmacy",
                {
                    Id: id,
                    UserId: userId,
                },
                function (result) {
                    if (result.message === "Success") {
                        showNotification("Pharmacy Deleted", NotificationEnum.Success);
                        $("#row_pharmacy_" + id).animate({ opacity: "0.0" }, "slow", function () {
                            $(this).remove();
                            $("#myConfirmModal").modal("hide");
                        });
                    }
                },
                function (result) {

                });
        });
}

function showdisapproveDialogPharmacy(id) {
    showConfirmDialogBox("Approve Pharmacy",
        "",
        "Disapprove Pharmacy",
        "No",
        function (e) {
            ajaxPost("/api/Pharmacy/DisApprovePharmacy",
                {
                    Id: id,
                    IsAdmin: true,
                },
                function (result) {
                    if (result.message === "Success") {
                        showNotification("Pharmacy Disapproved", NotificationEnum.Success);
                        $("#row_pharmacy_" + id).animate({ opacity: "0.0" }, "slow", function () {
                            $(this).remove();
                            $("#myConfirmModal").modal("hide");
                        });
                    }
                },
                function (result) {
                    showNotification("Error Occured", NotificationEnum.Warning);
                });
        });
}

function showapproveDialogPharmacy(id) {
    showConfirmDialogBox("Approve Pharmacy",
        "",
        "Approve Pharmacy",
        "No",
        function (e) {
            ajaxPost("/api/Pharmacy/ApprovePharmacy",
                {
                    Id: id,
                    IsAdmin: true,
                },
                function (result) {
                    if (result.message === "Success") {
                        showNotification("Pharmacy Approved", NotificationEnum.Success);
                        $("#row_pharmacy_" + id).animate({ opacity: "0.0" }, "slow", function () {
                            $(this).remove();
                            $("#myConfirmModal").modal("hide");
                        });
                    }
                },
                function (result) {
                    showNotification("Error Occured", NotificationEnum.Warning);
                });
        });
}

function showaddDialogPharmacy(userId) {
    showDialogBox("/api/Template/GetPharmacyForm/",
        {
            UserId: userId,
            IsAdmin: true
        },
        "Add Pharmacy Information");
}


function showImageDialogPharmacy(id) {
    showDialogBox("/api/Template/GetUploadForm/",
        {
            ActionUrl: "/File/SavePharmacyProfile",
            Id: id,
            Limit: 1
        },
        "Change Pharmacy Picture");
}