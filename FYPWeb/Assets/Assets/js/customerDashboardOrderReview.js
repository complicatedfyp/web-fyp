﻿function showorderDetailDialog(id) {
    showDialogBox("/api/Template/GetOrderDetailForm/",
        {
            OrderId: id,

        },
        "Order Details");
}
function showreviewOrderDialog(id) {
    showDialogBox("/api/Template/GetReviewOrderForm/",
        {
            OrderId: id,
        },
        "Review Order");
}
function initMapPharmacy(x, y) {
    var uluru = { lat: x, lng: y };
    if (x !== "" && y !== "") {
        uluru = { lat: parseFloat(x), lng: parseFloat(y) };
    }
    var map = new google.maps.Map(document.getElementById('map'),
        {
            zoom: 17,
            center: uluru
        });
    var marker = new google.maps.Marker({
        position: uluru,
        map: map,
        icon: "/Assets/Assets/img/logo-32.png"
    });
}
