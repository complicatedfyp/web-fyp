﻿function changePage(e, index, parameter) {
    const p = parameter;

    ajaxPost("/api/Template/GetCategories/",
        {
            Parameter: p,
            Page: index,
            Size: 10
        },
        function (result) {
            $("#categoryTable").html("");

            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#categoryTable").append(d);
                });
            } else {
                $("#categoryTable").html("<p>No Data Found</p>");
            }
        },
        function (result) {
            $("#categoryTable").html("");
            $("#categoryTable").html("<p>Error Occured</p>");
        });
}

function searchCategory(e, event) {
    if (e !== null && event.which !== 13) {
        return;
    }
    var p = e === null ? "" : $(e).val();
    showLoading("overlay");
    ajaxPost("/api/Template/GetCategories/",
        {
            Parameter: p,
            Page: 1,
            Size: 10
        },
        function (result) {
            $("#categoryTable").html("");
            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#categoryTable").append(d);
                });
            } else {
                $("#categoryTable").html("<p>No Data Found</p>");
            }
            hideLoading("overlay");

        },
        function (result) {
            $("#categoryTable").html("");
            $("#categoryTable").html("<p>Error Occured</p>");
            hideLoading("overlay");
            Log(result);
        });

    ajaxPost("/api/Template/GetCategoriesCount/",
        {
            Parameter: p,
            Page: 1,
            Size: 10
        },
        function (result) {
            $("#categoryPagination").html("");
            $("#categoryPagination").pagination({
                items: result.count,
                itemsOnPage: 1,
                cssStyle: 'light-theme',
                onPageClick: function (page, e) {
                    changePage(e, page, p);
                }
            });
        },
        function (result) {
            Log(result);
        });
}





function showeditDialogCategory(id) {
    showDialogBox("/api/Template/GetCategoryForm/",
        { CategoryId: id },
        "Edit Category Information");
}

function showdeleteDialogCategory(id) {
    showConfirmDialogBox("Delete Category",
        "Do you want to delete this Category?<br>Deleting this Category will also delete its categories and its Medicines",
        "Delete Category",
        "No",
        function (e) {
            ajaxPost("/api/Category/DeleteCategory",
                {
                    Id: id
                },
                function (result) {
                    if (result.message === "Success") {
                        showNotification("Category Deleted", NotificationEnum.Success);
                        $("#row_category_" + id).animate({ opacity: "0.0" }, "slow", function () {
                            $(this).remove();
                            $("#myConfirmModal").modal("hide");
                        });
                    }
                },
                function (result) {

                });
        });
}

function showaddDialogCategory() {
    showDialogBox("/api/Template/GetCategoryForm/",
        null,
        "Add Category Information");
}


function showImageDialogCategory(id) {
    showDialogBox("/api/Template/GetUploadForm/",
        {
            ActionUrl: "/File/SaveCategoryProfile",
            Id: id,
            Limit: 1
        },
        "Change Category Picture");
}