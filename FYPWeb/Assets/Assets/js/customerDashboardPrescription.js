﻿function showImageDialogPrescription(id) {
    showDialogBox("/api/Template/GetUploadForm/",
        {
            ActionUrl: "/File/SavePrescription",
            Id: id,
            Limit: 10
        },
        "Upload Prescription");
}

function showProcessImageDialogPrescription(id) {
    showDialogBox("/api/Template/GetProcessImageForm/",
        {
            Id: id,
        },
        "Upload Prescription");
}

var listOfPrescriptionGuesses = [];
function addToList(data, e) {
    listOfPrescriptionGuesses.push(data);
    $(e).remove();
    $("#selectedWords").append(getWordsTemplate(data, true));

}
function removeFromList(data, e) {
    var index = listOfPrescriptionGuesses.indexOf(data);
    if (index > -1) {
        array.splice(index, 1);
    }
    $(e).remove();
    $("#notSelectedWords").append(getWordsTemplate(data, false));
}

function getWordsTemplate(word, isSelected) {
    if (!isSelected) {
        return '<span onclick="addToList(' + word + ',this)" class="label label-primary col-xs-2">' + word + '</span>';
    }
    return '<span onclick="removeFromList(' + word + ',this)" class="label label-info col-xs-2">' + word + '</span>';
}

function processWords() {
    ajaxPost("/api/Medicine/SearchMedicine",
        {
            ListOfParameter: listOfPrescriptionGuesses
        },
        function (result) {

        },
        function (result) {

        });
}

function savePrescriptionDrugs() {
    const brandForm = $("#step-1");
    brandForm.validator('validate');
    const elmErr = brandForm.children('.has-error');
    if (elmErr && elmErr.length > 0) {
        return;
    }
    var id = $("#OrderId").val();
    var comments = $("#EmployeeId").val();
    var time = $("#AcceptedTime").val();
    ajaxPost("/api/Order/AcceptOrder",
        {
            OrderId: id,
            EmployeeId: comments,
            AcceptedTime: time,
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Order is Accepted", NotificationEnum.Success);
                $("#row_order_" + id).animate({ opacity: "0.0" }, "slow", function () {
                    $(this).remove();
                    closeDialogBox();
                });
            }
        },
        function (result) {

        });

}


function showdeleteDialogPrescription(id) {
    showConfirmDialogBox("Delete Prescription",
        "Do you want to delete this Prescription?",
        "Delete Prescription",
        "No",
        function (e) {
            ajaxPost("/api/Prescription/DeletePrescription",
                {
                    Id: id
                },
                function (result) {
                    if (result.message === "Success") {
                        showNotification("Prescription Deleted", NotificationEnum.Success);
                        $("#row_" + id).animate({ opacity: "0.0" }, "slow", function () {
                            $(this).remove();
                            $("#myConfirmModal").modal("hide");
                        });
                    }
                },
                function (result) {

                });
        });
}