﻿var userType;
function changePage(e, index, parameter) {
    var p = parameter;

    ajaxPost("/api/Template/GetUsers/",
        {
            Parameter: p,
            Page: index,
            Size: 10,
            Type: userType
        },
        function (result) {
            $("#userTable").html("");

            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#userTable").append(d);
                });
            } else {
                $("#userTable").html("<p>No Data Found</p>");
            }
        },
        function (result) {
            $("#userTable").html("");
            $("#userTable").html("<p>Error Occured</p>");
        });
}

function searchUser(e,event) {
    if (e !== null && event.which !== 13) {
        return;
    }
    var p = e === null ? "" : $(e).val();
    showLoading("overlay");
    ajaxPost("/api/Template/GetUsers/",
        {
            Parameter: p,
            Page: 1,
            Size: 10,
            Type: userType
        },
        function (result) {
            $("#userTable").html("");
            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#userTable").append(d);
                });
            } else {
                $("#userTable").html("<p>No Data Found</p>");
            }
            hideLoading("overlay");

        },
        function (result) {
            $("#userTable").html("");
            $("#userTable").html("<p>Error Occured</p>");
            hideLoading("overlay");
        });

    ajaxPost("/api/Template/GetUsersCount/",
        {
            Parameter: p,
            Page: 1,
            Size: 10,
            Type: userType
        },
        function (result) {
            $("#userPagination").html("");
            $("#userPagination").pagination({
                items: result.count,
                itemsOnPage: 1,
                cssStyle: 'light-theme',
                onPageClick: function (page, e) {
                    changePage(e, page, p);
                }
            });
        },
        function (result) {
        });
}



function enableUser(id,name) {
    ajaxPost("/api/User/SetUserEnable/",
        {
            UserId: id
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("" + name + " has been Enabled",NotificationEnum.Success);
                $("#row_user_" + id).animate({ opacity: "0.0" }, "slow", function () {
                    $(this).remove();
                });
            } else {
                showNotification("Error Occured", NotificationEnum.Error);
            }
        },
        function (result) {
            showNotification("Error Occured", NotificationEnum.Error);

        });
}

function verifyUser(id, name) {
    ajaxPost("/api/User/SetUserVerify/",
        {
            UserId: id
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("" + name + " has been Verified", NotificationEnum.Success);
            } else {
                showNotification("Error Occured", NotificationEnum.Error);
            }
        },
        function (result) {
            showNotification("Error Occured", NotificationEnum.Error);

        });
}

function disableUser(id, name) {
    ajaxPost("/api/User/SetUserDisable/",
        {
            UserId: id
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("" + name + " has been Disabled", NotificationEnum.Success);
                $("#row_user_" + id).animate({ opacity: "0.0" }, "slow", function () {
                    $(this).remove();
                });
            } else {
                showNotification("Error Occured", NotificationEnum.Error);
            }
        },
        function (result) {
            showNotification("Error Occured", NotificationEnum.Error);
        });
}



function editUser(id) {
    showDialogBox("/api/Template/GetUserForm/",
        { UserId: id },
        "Edit User Information");
}