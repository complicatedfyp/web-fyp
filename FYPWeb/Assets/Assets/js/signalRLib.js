﻿function startSignalR(id) {
    $.connection.hub.qs = { 'Id': id };
    $.connection.hub.start()
        .done(function () {
            $('[data-toggle="tooltip"]').tooltip();
            ajaxPost("/api/Notification/GetNotificationUnSeenCount",
                {
                    UserId: id.split('_')[2],
                },
                function (result) {
                    if (result.message === "Success") {
                        $("#notificationCount").html(result.result);
                    } else {
                        $("#notificationCount").html("0");
                    }
                },
                function (result) {
                    showNotification("Error Occured", NotificationEnum.Warning);
                });
        })
        .fail(function () {
            Log("Not Connected");
        });
}
$.connection.notificationHub.client.sendNotification = function (body, url, date, priority) {
    showNotification(body, priority, url);
    $("#notificationCount").html(parseInt($("#notificationCount").html()) + 1);
    first = false;
}

function scrollDownBox() {
    $('#messageBody').animate({ scrollTop: $('#messageBody').prop("scrollHeight") }, 1000);

}

function sendMessage() {

    var mycontent = $("#ContentBody").val();
    if (mycontent.length <= 0) {
        return;
    }
    $.connection.notificationHub.server.sendMessage(sId, rId, mycontent, orderId,myname,mypicture).done(function () {
        $("#ContentBody").val("");
        $("#messageBody").append(myMessageTemplate(myname, moment().format('MMMM Do YYYY, h:mm:ss a'), mypicture, mycontent));
        scrollDownBox();
    }).fail(function (error) {
        showNotification("Error Occured", NotificationEnum.Warning);
    });
}

$.connection.notificationHub.client.recieveMessage = function (name, date, picture, message) {
    $("#messageBody").append(otherMessageTemplate(name, date, picture, message));
    scrollDownBox();
}
var first = false;
function getNotification(e, id) {
    //$(e).dropdown("toggle");
    if (!first) {
        first = true;
        ajaxPost("/api/Template/GetNotification",
            {
                UserId: id,
            },
            function (result) {
                if (result.message === "Success") {
                    $("#notificationList").html(result.result);
                    $("#notificationCount").html(result.count);
                }
            },
            function (result) {
                showNotification("Error Occured", NotificationEnum.Warning);
            });

    }

}