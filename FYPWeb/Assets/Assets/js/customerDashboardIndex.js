﻿function barChartGraph(id, label, values, title = "Sales By Month") {
    var barChart = new Chart(document.getElementById(id),
        {
            "type": "bar",
            "data": {
                "labels": label,
                "datasets": [
                    {
                        "label": title,
                        "data": values,
                        "fill": false,
                        "backgroundColor": [
                            "rgba(255, 99, 132, 0.2)", "rgba(255, 159, 64, 0.2)", "rgba(255, 205, 86, 0.2)",
                            "rgba(75, 192, 192, 0.2)", "rgba(54, 162, 235, 0.2)", "rgba(153, 102, 255, 0.2)",
                            "rgba(201, 203, 207, 0.2)",
                            "rgba(255, 99, 132, 0.2)", "rgba(255, 159, 64, 0.2)", "rgba(255, 205, 86, 0.2)",
                            "rgba(75, 192, 192, 0.2)", "rgba(54, 162, 235, 0.2)", "rgba(153, 102, 255, 0.2)",
                            "rgba(201, 203, 207, 0.2)",
                            "rgba(255, 99, 132, 0.2)", "rgba(255, 159, 64, 0.2)", "rgba(255, 205, 86, 0.2)",
                            "rgba(75, 192, 192, 0.2)", "rgba(54, 162, 235, 0.2)", "rgba(153, 102, 255, 0.2)",
                            "rgba(201, 203, 207, 0.2)",


                        ],
                        "borderColor": [
                            "rgb(255, 99, 132)", "rgb(255, 159, 64)", "rgb(255, 205, 86)", "rgb(75, 192, 192)",
                            "rgb(54, 162, 235)", "rgb(153, 102, 255)", "rgb(201, 203, 207)"
                            , "rgb(255, 99, 132)", "rgb(255, 159, 64)", "rgb(255, 205, 86)", "rgb(75, 192, 192)",
                            "rgb(54, 162, 235)", "rgb(153, 102, 255)", "rgb(201, 203, 207)"
                            , "rgb(255, 99, 132)", "rgb(255, 159, 64)", "rgb(255, 205, 86)", "rgb(75, 192, 192)",
                            "rgb(54, 162, 235)", "rgb(153, 102, 255)", "rgb(201, 203, 207)"

                        ],
                        "borderWidth": 1
                    }
                ]
            },
            "options": { "scales": { "yAxes": [{ "ticks": { "beginAtZero": true } }] } }
        });
}
function showorderDetailDialog(id) {
    showDialogBox("/api/Template/GetOrderDetailForm/",
        {
            OrderId: id,

        },
        "Order Details");
}
function initMapPharmacy(x, y) {
    var uluru = { lat: x, lng: y };
    if (x !== "" && y !== "") {
        uluru = { lat: parseFloat(x), lng: parseFloat(y) };
    }
    var map = new google.maps.Map(document.getElementById('map'),
        {
            zoom: 17,
            center: uluru
        });
    var marker = new google.maps.Marker({
        position: uluru,
        map: map,
        icon: "/Assets/Assets/img/logo-32.png"
    });
}

function showcancelOrderDialog(id) {
    showDialogBox("/api/Template/GetCancelOrderForm/",
        {
            OrderId: id,

        },
        "Cancel Order");
}

$(document).ready(function () {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            ajaxPost("/api/Promotion/GetNearestPromotionOffers",
                pos,
                function (result) {
                    if (result.message == "Success") {
                        if (result.result.length == 0) {
                            $("#promotionTable").html("<tr><td colspan='5'>No Promotions Found Near you</td></tr>");
                        } else {
                            $("#promotionTable").html("");
                            result.result.forEach(function (e) {
                                $("#promotionTable").append("<tr  style='cursor:pointer' onclick='goToUrl('" + "/Profile/" + e.Username + "')'>" +
                                    "<td>" + e.ShopName + "</td>" +
                                    "<td>" + e.Name + "</td>" +
                                    "<td>" + e.Code + "</td>" +
                                    "<td>" + e.PercentageOff + "</td>" +
                                    "<td>" + e.EndDate + "</td>" +
                                    "</tr>");
                            });

                        }
                    }
                },
                function (result) {
                    showNotification("Error Occured", NotificationEnum.Warning);
                    $("#promotionTable").html("<tr><td colspan='5'>No Promotions Found Near you</td></tr>");
                });
            ajaxPost("/api/Medicine/GetNearestMedicine",
                pos,
                function (result) {
                    if (result.message == "Success") {
                        if (result.result.length == 0) {
                            $("#medicineTable").html("<tr><td colspan='5'>No Medicines Found Near you</td></tr>");
                        } else {
                            $("#medicineTable").html("");
                            result.result.forEach(function (e) {
                                $("#medicineTable").append("<tr style='cursor:pointer'  onclick='goToUrl('" + "/Medicine/" + e.Id + "')'>" +
                                    "<td><img src='" + e.ImageLink + "' alt='' width='32' class='image'/></td>" +
                                    "<td>" + e.Name + "</td>" +
                                    "<td>" + e.DrugForm + "</td>" +
                                    "<td>" + e.Packing + "</td>" +
                                    "</tr>");
                            });

                        }
                    }
                },
                function (result) {
                    showNotification("Error Occured", NotificationEnum.Warning);
                    $("#medicineTable").html("<tr><td colspan='5'>No Medicines Found Near you</td></tr>");
                });

        }, function () {

        });
    } else {
        showNotification("Geo Location Blocked", NotificationEnum.Warning);
        $("#promotionTable").html("<tr><td colspan='5'>No Promotions Found Near you</td></tr>");
        $("#medicineTable").html("<tr><td colspan='5'>No Medicines Found Near you</td></tr>");
    }

});