﻿$(function () {
    searchPromotion(null);
});

function changePage(e, index, parameter) {
    const p = parameter;

    ajaxPost("/api/Template/GetPromotions/",
        {
            Parameter: p,
            Page: index,
            UserId: userId,
            Type: type,
            Size: 10
        },
        function (result) {
            $("#promotionTable").html("");

            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#promotionTable").append(d);
                });
            } else {
                $("#promotionTable").html("<p>No Data Found</p>");
            }
            $("#promotionPagination").find("a").each(function () {
                $(this).removeClass("active");
            });
            $(e).addClass("active");
        },
        function (result) {
            $("#promotionTable").html("");
            $("#promotionTable").html("<p>Error Occured</p>");
        });
}

function searchPromotion(e, event) {
    if (e !== null && event.which !== 13) {
        return;
    }
    var p = e === null ? "" : $(e).val();
    showLoading("overlay");
    ajaxPost("/api/Template/GetPromotions/",
        {
            Parameter: p,
            Page: 1,
            Size: 10,
            Type: type,
            UserId: userId,
        },
        function (result) {
            $("#promotionTable").html("");
            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#promotionTable").append(d);
                });
            } else {
                $("#promotionTable").html("<p>No Data Found</p>");
            }
            hideLoading("overlay");

        },
        function (result) {
            $("#promotionTable").html("");
            $("#promotionTable").html("<p>Error Occured</p>");
            hideLoading("overlay");
            Log(result);
        });

    ajaxPost("/api/Template/GetPromotionsCount/",
        {
            Parameter: p,
            Page: 1,
            Size: 10,
            Type: type,
            UserId: userId,
        },
        function (result) {
            $("#promotionPagination").html("");
            for (let i = 0; i < result.count; i++) {
                $("#promotionPagination").append("<li><a href='#' onclick='changePage(this," + (i + 1) + ",\"" + p + "\")'>" + (i + 1) + "</a></li>");
            }
        },
        function (result) {
            Log(result);
        });
}





function showeditDialogPromotion(id,userId) {
    showDialogBox("/api/Template/GetPromotionForm/",
        { PromotionId: id,UserId: userId },
        "Edit Promotion Information");
}

function showdeleteDialogPromotion(id) {
    showConfirmDialogBox("Delete Promotion",
        "Do you want to delete this Promotion?<br>Deleting this Promotion will also delete its promotions and its Medicines",
        "Delete Promotion",
        "No",
        function (e) {
            ajaxPost("/api/Promotion/DeletePromotion",
                {
                    Id: id
                },
                function (result) {
                    if (result.message === "Success") {
                        showNotification("Promotion Deleted", NotificationEnum.Success);
                        $("#row_promotion_" + id).animate({ opacity: "0.0" }, "slow", function () {
                            $(this).remove();
                            $("#myConfirmModal").modal("hide");
                        });
                    }
                },
                function (result) {

                });
        });
}

function showstopDialogPromotion(id) {
    showConfirmDialogBox("Stop Promotion",
        "Do you want to Stop this Promotion?",
        "Stop Promotion",
        "No",
        function (e) {
            ajaxPost("/api/Promotion/StopPromotion",
                {
                    PromotionId: id
                },
                function (result) {
                    if (result.message === "Success") {
                        showNotification("Promotion Stopped", NotificationEnum.Success);
                        $("#row_promotion_" + id).animate({ opacity: "0.0" }, "slow", function () {
                            $(this).remove();
                            $("#myConfirmModal").modal("hide");
                        });
                    }
                },
                function (result) {
                    showNotification("Promotion Error", NotificationEnum.Warning);

                });
        });
}


function showaddDialogPromotion() {
    showDialogBox("/api/Template/GetPromotionForm/",
        {
            UserId: userId
        },
        "Add Promotion Information");
}

function showstartDialogPromotion(id) {
    showDialogBox("/api/Template/GetStartPromotionForm/",
        {
            PromotionId: id,
            UserId: userId,
        },
        "Start Promotion");
}


function initPromotionStart() {
    $("#StartDate").daterangepicker({
        locale: {
            format: "MM/DD/YYYY"
        },
        minDate: moment().format("MM/DD/YYYY")
    });
}