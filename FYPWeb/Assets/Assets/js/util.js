﻿var NotificationEnum = { "Info": 1, "Error": 2, "Warning": 3, "Success": 4 };

$(function () {
    Object.freeze(NotificationEnum);
});


function goToUrl(url) {
    window.location.href = url;
}

function searchAll(e) {
    if (e.keyCode === 13) {
        var q = $("#search").val();
        if (q !== "") {
            var type = $("#searchType").val();
            var url = ("/Search?q=" + q + "&type=" + type);
        }
    }
    return false;
}

function Log(data) {
    console.log(data);
}

function ajaxPost(url1, data1, success1, error1) {
    $.ajax({
        type: "POST",
        url: url1,
        data: data1,
        contentType: "application/x-www-form-urlencoded; charset=UTF-8", //this could be left out as it is the default content-type of an AJAX request
        success: success1,
        error: error1
    });
}

function ajaxPGET(url, data, success, error) {
    $.ajax({
        crossDomain: true,
        type: 'GET',
        dataType: 'jsonp',
        jsonp: false,
        url: url,
        data: data,
        success: success,
        error: error
    });
}


function ajaxGET(url, data, success, error) {
    $.ajax({
        type: "GET",
        url: url,
        data: data,
        success: success,
        error: error
    });
}

function showImage(src) {
    $("#modalTestImage2").attr("src", src);
    $("#myImageModal").modal("show");
}
function showNotification(msg, type,url = "") {
    toastr.options.closeButton = true;
    toastr.options.closeHtml = '<button><i class="icon-off"></i></button>';
    toastr.options.positionClass ="toast-bottom-right";
    if (url === "") {
        toastr.options.onclick = function() {

        };
    } else {
        toastr.options.onclick = function () {
            goToUrl(url);
        };        
    }
    if (type === NotificationEnum.Info) {
        toastr.info(msg);
    }
    else if (type === NotificationEnum.Error) {
        toastr.error(msg);
    }
    else if (type === NotificationEnum.Warning) {
        toastr.warning(msg);
    }
    else if (type === NotificationEnum.Success) {
        toastr.success(msg);
    }
}

function isEmpty(str) {
    return (!str || 0 === str.length);
}

function showConfirmDialogBox(title, description, yestext, notext, yesfunction, nofunction) {
    $("#myConfirmModal .modal-body").html(description);
    $("#myConfirmModal .modal-title").html(title);
    if (!isEmpty(yestext)) {
        $("#myConfirmModal .yesbtn").html(yestext);
    }
    if (!isEmpty(notext)) {
        $("#myConfirmModal .nobtn").html(notext);
    }
    if (yesfunction != undefined) {
        $("#myConfirmModal .yesbtn").click(yesfunction);
    }
    if (nofunction != undefined) {
        $("#myConfirmModal .nobtn").click(nofunction);
    }
    $("#myConfirmModal").modal("show");
}



function showDialogBox(url, data, title) {
    $("#myModal .modal-body").html("Processing...");
    ajaxPost(url,
        data,
        function (result) {
            if (result.message === "Success") {
                $("#myModal .modal-title").html(title);
                $("#myModal .modal-body").html(result.result);
                $("#myModal").modal("show");
            }
        },
        function (result) {
            showNotification("Error Occured Initializing Dialog", NotificationEnum.Warning);
            $("#myModal").modal("close");
        });
}

function closeDialogBox() {
    $("#myModal").modal("hide");
}

function showLoading(id) {
    $(`#${id}`).html('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
}
function hideLoading(id) {
    $(`#${id}`).html("");
}


function changeType(e, type) {
    $("#typeCustomer").removeClass("btn-success btn-default").addClass("btn-default");
    $("#typePharmacist").removeClass("btn-success btn-default").addClass("btn-default");
    $(e).removeClass('btn-default').addClass('btn-success');
    $("#Type").val(type);
}


