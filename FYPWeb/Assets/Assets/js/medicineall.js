﻿function initMedicineAll() {
    $("#pharmacySearch").select2({
        placeholder: "Select a Pharmacy",
        ajax: {
            url: "/api/Pharmacy/SearchPharmacy",
            type: "POST",
            delay: 250,
            data: function (params) {
                return {
                    Parameter: params.term,
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data.result, function (val, i) {
                        return {
                            id: val.Id,
                            text: val.Name
                        }
                    })
                };
            }
        }
    });
    $("#drugFormSearch").select2({
        placeholder: "Select a Drug Form",
        ajax: {
            url: "/api/Medicine/SearchDrugForm",
            type: "POST",
            delay: 250,
            data: function (params) {
                return {
                    Parameter: params.term,
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(data.result, function (val, i) {
                        return {
                            id: val.Id,
                            text: val.Name
                        }
                    })
                };
            }
        }
    });

}

var rating = 0;
var brandId = 0;
var pharmacyId = 0;
var form = "";
var min = 0;
var max = 0;
var parameter = "";

function changeRating(e) {
    if ($(e).hasClass("active")) {
        $(e).removeClass("active");
        rating = 0;
        return;
    }
    $(".list-group-item").removeClass("active");
    $(e).addClass("active");
    rating = $(e).attr("data-rating");
}

function addToPharmacyInventory() {
    var medicineId = $("#MedicineId").val();
    var pharmacyId = $("#PharmacyId").val();
    var quantity = $("#Quantity").val();
    const radioValue = $("input[name='AddType']:checked").val();

    if (pharmacyId > 0) {
        ajaxPost("/api/Inventory/SaveInventory/",
            {
                DrugId: medicineId,
                PharmacyId: pharmacyId,
                AddType: radioValue,
                Quantity: quantity,
            },
            function (result) {
                if (result.message === "Success") {
                    showNotification("Medicine Added To Inventory", NotificationEnum.Success);
                    closeDialogBox();
                }
            },
            function (result) {
                showNotification("Error Occured", NotificationEnum.Warning);
            });
    }
}

function applyFilter() {

    brandId = $("#brandSearch").val();
    pharmacyId = $("#pharmacySearch").val();
    form = $("#drugFormSearch").val();
    goToUrl("/Home/AllMedicine?rating=" +
        rating +
        "&pharmacyId=" +
        pharmacyId +
        "&form=" +
        form +
        "&min=" +
        min +
        "&max=" +
        max +
        "&parameter=" +
        parameter +
        "&sortBy=" +
        $("#sortType").val()
    );
}
function addToCart(medicineId, userId, sessionId = "") {
    ajaxPost("/api/Medicine/AddToCart/",
        {
            MedicineId: medicineId,
            UserId: userId,
            Quantity: 1,
            SessionId: sessionId,
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Medicine Added to Cart", NotificationEnum.Success);
                if (result.result > 0) {
                    $("#cartCount").html(result.result);
                }
            } else {
                showNotification("Low Pharamcy Stock", NotificationEnum.Info);
            }
        },
        function (result) {
            showNotification("Error Addeding to Cart", NotificationEnum.Warning);
        });
}
function addToInventory(medicineId, userId) {
    const qty = 0;
    showDialogBox("/api/Template/GetAddToInventoryForm/",
        {
            UserId: userId,
            MedicineId: medicineId,
            Quantity: qty,
        },
        "Add To Inventory");

}