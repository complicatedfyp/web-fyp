function changeType(e,type) {
    $("#typeCustomer").removeClass("btn-success btn-default").addClass("btn-default");
    $("#typePharmacist").removeClass("btn-success btn-default").addClass("btn-default");
    $(e).removeClass('btn-default').addClass('btn-success');
    $("#Type").val(type);
}
$(function () {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });

    $('#smartwizard').smartWizard({
        selected: 0,  // Initial selected step, 0 = first step
        keyNavigation:false, // Enable/Disable keyboard navigation(left and right keys are used if enabled)
        autoAdjustHeight:false, // Automatically adjust content height
        cycleSteps: false, // Allows to cycle the navigation of steps
        backButtonSupport: false, // Enable the back button support
        useURLhash: true, // Enable selection of the step based on url hash
        lang: {  // Language variables
            next: 'Next',
            previous: 'Previous'
        },
        toolbarSettings: {
            toolbarPosition: 'bottom', // none, top, bottom, both
            toolbarButtonPosition: 'right', // left, right
            showNextButton: true, // show/hide a Next button
            showPreviousButton: true, // show/hide a Previous button
            toolbarExtraButtons: [
                $('<button></button>').text('Finish')
                    .addClass('btn btn-success')
                    .on('click', function(){

                    })
            ]
        },
        anchorSettings: {
            anchorClickable: true, // Enable/Disable anchor navigation
            enableAllAnchors: false, // Activates all anchors clickable all times
            markDoneStep: true, // add done css
            enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
        },
        contentURL: null, // content url, Enables Ajax content loading. can set as data data-content-url on anchor
        disabledSteps: [],    // Array Steps disabled
        errorSteps: [],    // Highlight step with errors
        theme: 'arrows',
        transitionEffect: 'fade', // Effect on navigation, none/slide/fade
        transitionSpeed: '400'
    });
    $("#smartwizard").on("leaveStep", function (e, anchorObject, stepNumber, stepDirection) {
        var elmForm = $("#step-" + (stepNumber + 1));
        // stepDirection === 'forward' :- this condition allows to do the form validation 
        // only on forward navigation, that makes easy navigation on backwards still do the validation when going next

        if (stepDirection === 'forward' && elmForm) {
            elmForm.validator('validate');
            var elmErr = elmForm.children('.has-error');
            if (elmErr && elmErr.length > 0) {
                // Form validation failed
                return false;
            }
        }
        return true;
    });
    $("#smartwizard").on("showStep", function (e, anchorObject, stepNumber) {
        var number = ((stepNumber + 1));
        if (number === 3) {
            console.log(number);
            $("#FirstNameLabel").html($("#FirstName").val());
            $("#LastNameLabel").html($("#LastName").val());
            $("#EmailLabel").html($("#Email").val());
            $("#AddressLabel").html($("#Address").val());
            $("#UserNameLabel").html($("#Username").val());
            $("#PhoneLabel").html($("#PhoneNumber").val());
            $("#DOBLabel").html($("#DOB").val());
            $("#GenderLabel").html($("input[name='gender']:checked").val());
            $("#TypeLabel").html($("#Type").val() === "2" ? "Customer" : "Pharmacist");
        }
        return true;
    });
    $("#Email").inputmask({ alias: "email" });
    $("#DOB").inputmask("m/d/y");

    $("#PhoneNumber").inputmask("9999-9999999");
});