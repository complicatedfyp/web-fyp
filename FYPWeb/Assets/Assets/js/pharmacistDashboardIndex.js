﻿

function barChartGraph(id, label, values, title = "Sales By Month") {
    var barChart = new Chart(document.getElementById(id),
    {
        "type": "bar",
        "data": {
            "labels": label,
            "datasets": [
                {
                    "label": title,
                    "data": values,
                    "fill": false,
                    "backgroundColor": [
                        "rgba(255, 99, 132, 0.2)", "rgba(255, 159, 64, 0.2)", "rgba(255, 205, 86, 0.2)",
                        "rgba(75, 192, 192, 0.2)", "rgba(54, 162, 235, 0.2)", "rgba(153, 102, 255, 0.2)",
                        "rgba(201, 203, 207, 0.2)"
                    ],
                    "borderColor": [
                        "rgb(255, 99, 132)", "rgb(255, 159, 64)", "rgb(255, 205, 86)", "rgb(75, 192, 192)",
                        "rgb(54, 162, 235)", "rgb(153, 102, 255)", "rgb(201, 203, 207)"
                    ],
                    "borderWidth": 1
                }
            ]
        },
        "options": { "scales": { "yAxes": [{ "ticks": { "beginAtZero": true } }] } }
    });
}





function showcancelOrderDialog(id) {
    showDialogBox("/api/Template/GetCancelOrderForm/",
        {
            OrderId: id,

        },
        "Cancel Order");
}


function showcompleteOrderDialog(id) {
    showDialogBox("/api/Template/GetCompleteOrderForm/",
        {
            OrderId: id,
        },
        "Complete Order");
}
function showorderDetailDialog(id) {
    showDialogBox("/api/Template/GetOrderDetailForm/",
        {
            OrderId: id,

        },
        "Order Details");
}

function initMapPharmacy(x, y) {
    var uluru = { lat: x, lng: y };
    if (x !== "" && y !== "") {
        uluru = { lat: parseFloat(x), lng: parseFloat(y) };
    }
    var map = new google.maps.Map(document.getElementById('map'),
        {
            zoom: 17,
            center: uluru
        });
    var marker = new google.maps.Marker({
        position: uluru,
        map: map,
        icon: "/Assets/Assets/img/logo-32.png"
    });
}

function showrejectOrderDialog(id) {
    showDialogBox("/api/Template/GetRejectOrderForm/",
        {
            OrderId: id,

        },
        "Reject Order");
}
function showacceptOrderDialog(id, userId) {
    showDialogBox("/api/Template/GetAcceptOrderForm/",
        {
            OrderId: id,
            UserId: userId,
        },
        "Accept Order");
}

function rejectOrder() {
    const brandForm = $("#step-1");
    brandForm.validator('validate');
    const elmErr = brandForm.children('.has-error');
    if (elmErr && elmErr.length > 0) {
        return;
    }
    var id = $("#OrderId").val();
    var comments = $("#Comment").val();
    ajaxPost("/api/Order/RejectOrder",
        {
            OrderId: id,
            Comment: comments
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Order is Cancelled", NotificationEnum.Success);
                $("#row_order_" + id).animate({ opacity: "0.0" }, "slow", function () {
                    $(this).remove();
                    $("#myConfirmModal").modal("hide");
                });
            }
        },
        function (result) {

        });

}

function setPrescription(id) {
    ajaxPost("/api/Order/SetPrescription",
        {
            OrderId: id
        },
        function(result) {
            if (result.message == "Success") {
                showNotification("Prescription Set", NotificationEnum.Success);
                location.reload();
            }
        },
        function(result) {
            showNotification("Error Occured", NotificationEnum.Warning);
        });
}

function removeFromOrder(id, id2) {
    ajaxPost("/api/Template/RemoveOrderDetail/",
        {
            Id: id,
            OrderId: id2
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Removed Successfully", NotificationEnum.Success);
                $("#detailBody").html(result.result);
                $("#totalPrice").html(result.TotalPrice);
                $("#totalQuantity").html(result.TotalQuantity);

            }
        },
        function (result) {
            showNotification("Error Occured", NotificationEnum.Warning);
        });
}
function editAmountOrderDetail(id, value) {
    $("#amount_new_" + id).val(value);
    $("#amount_new_" + id).show();
    $("#edit_" + id).hide();
    $("#remove_" + id).hide();
    $("#cancel_" + id).show();
    $("#save_" + id).show();
    $("#current_" + id).hide();
}
function saveAmountOrderDetail(id) {
    var qty = $("#amount_new_" + id).val();
    ajaxPost("/api/Template/UpdateOrderDetail",
        {
            Id: id,
            Quantity: qty
        },
        function (result) {
            if (result.message === "Success") {
                $("#detailBody").html(result.result);
                $("#totalPrice").html(result.TotalPrice);
                $("#totalQuantity").html(result.TotalQuantity);
            }
        },
        function (result) {
            showNotification("Error Occured", NotificationEnum.Warning);
        });
}




function cancelEditOrder(id) {
    $("#amount_new_" + id).hide();
    $("#current_" + id).show();
    $("#remove_" + id).show();
    $("#edit_" + id).show();
    $("#cancel_" + id).hide();
    $("#save_" + id).hide();
}

function addToOrder() {
    var id = $("#MedicineId").val();
    var amount = $("#Amount").val();
    var orderId = $("#OrderId").val();
    ajaxPost("/api/Template/AddToOrder/",
        {
            InventoryId: id,
            Amount: amount,
            OrderId: orderId
        },
        function (result) {
            if (result.message === "Success") {
                $("#detailBody").html(result.result);
                $("#totalPrice").html(result.TotalPrice);
                $("#totalQuantity").html(result.TotalQuantity);
                $("#Amount").val("0");
            } else {
                showNotification("Cannot Add More than Stock", NotificationEnum.Info);
            }
        },
        function (result) {
            showNotification("Error Occured", NotificationEnum.Warning);
        });
}

function showPrescriptionSetMedicineDialog(id) {
    showDialogBox("/api/Template/GetPrescriptionSetMedicineForm",
        {
            OrderId: id
        },
        "Set Medicine Prescription");
}

function acceptOrder() {
    const brandForm = $("#step-1");
    brandForm.validator('validate');
    const elmErr = brandForm.children('.has-error');
    if (elmErr && elmErr.length > 0) {
        return;
    }
    var id = $("#OrderId").val();
    var comments = $("#EmployeeId").val();
    var time = $("#AcceptedTime").val();
    ajaxPost("/api/Order/AcceptOrder",
        {
            OrderId: id,
            EmployeeId: comments,
            AcceptedTime: time,
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Order is Accepted", NotificationEnum.Success);
                $("#row_order_" + id).animate({ opacity: "0.0" },
                    "slow",
                    function() {
                        $(this).remove();
                        closeDialogBox();
                    });
            } else {
                showNotification("Unable to Accept Order due to Stock Shortage", NotificationEnum.Error);

            }
        },
        function (result) {

        });

}

function cancelOrder() {
    const brandForm = $("#step-1");
    brandForm.validator('validate');
    const elmErr = brandForm.children('.has-error');
    if (elmErr && elmErr.length > 0) {
        return;
    }
    var id = $("#OrderId").val();
    var comments = $("#Comment").val();
    ajaxPost("/api/Order/CancelOrder",
        {
            OrderId: id,
            Comment: comments
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Order is Cancelled", NotificationEnum.Success);
                $("#row_order_" + id).animate({ opacity: "0.0" }, "slow", function () {
                    $(this).remove();
                    closeDialogBox();
                });
            }
        },
        function (result) {

        });

}

function completeOrder() {
    var id = $("#OrderId").val();
    ajaxPost("/api/Order/CompleteOrder",
        {
            OrderId: id,
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Order is Completed", NotificationEnum.Success);
                $("#row_order_" + id).animate({ opacity: "0.0" }, "slow", function () {
                    $(this).remove();
                    closeDialogBox();
                });
            }
        },
        function (result) {

        });

}
