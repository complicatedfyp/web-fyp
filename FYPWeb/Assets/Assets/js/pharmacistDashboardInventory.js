﻿
function changePage(e, index, parameter) {
    const p = parameter;
    var pId = $("#PharmacyInventoryId").val();
    if (pId === "0") {
        return;
    }
    ajaxPost("/api/Template/GetInventories/",
        {
            Parameter: p,
            Page: index,
            Size: 10,
            PharmacyId: pId
        },
        function (result) {
            $("#inventoryTable").html("");

            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#inventoryTable").append(d);
                });
            } else {
                $("#inventoryTable").html("<p>No Data Found</p>");
            }
            $("#inventoryPagination").find("a").each(function () {
                $(this).removeClass("active");
            });
            $(e).addClass("active");
        },
        function (result) {
            $("#inventoryTable").html("");
            $("#inventoryTable").html("<p>Error Occured</p>");
            Log(result);
        });
}

function searchInventory(e, event) {
    if (e !== null && event.which !== 13) {
        return;
    }
    var p = e === null ? "" : $(e).val();
    showLoading("overlay");
    var pId = $("#PharmacyInventoryId").val();
    if (pId === "0") {
        return;
    }
    ajaxPost("/api/Inventory/GetTotalCount/",
        {
            Parameter: p,
            PharmacyId: pId
        },
        function (result) {
            if (result.message === "Success") {
                $("#stockCount").html(result.result);
            }
        },
        function (result) {
            Log(result);
        });

    ajaxPost("/api/Template/GetInventories/",
        {
            Parameter: p,
            Page: 1,
            Size: 10,
            PharmacyId: pId
        },
        function (result) {
            $("#inventoryTable").html("");
            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#inventoryTable").append(d);
                });
            } else {
                $("#inventoryTable").html("<p>No Data Found</p>");
            }
            hideLoading("overlay");

        },
        function (result) {
            $("#inventoryTable").html("");
            $("#inventoryTable").html("<p>Error Occured</p>");
            hideLoading("overlay");
            Log(result);
        });

    ajaxPost("/api/Template/GetInventoriesCount/",
        {
            Parameter: p,
            Page: 1,
            Size: 10,
            PharmacyId: pId
        },
        function (result) {
            $("#inventoryPagination").pagination({
                items: result.count,
                itemsOnPage: 1,
                cssStyle: 'light-theme',
                onPageClick: function (page, e) {
                    alert(page);
                    changePage(this, page, p);
                }
            });
        },
        function (result) {
            Log(result);
        });
}

function showaddDialogInventory(userId) {
    showDialogBox("/api/Template/GetInventoryForm/",
        {
            UserId: userId
        },
        "Add To Stock");
}

function showremoveDialogInventory(inventoryId) {
    showDialogBox("/api/Template/GetRemoveInventoryForm/",
        {
            InventoryId: inventoryId,
        },
        "Remove From Stock");
}

function savePrice() {
    const brandForm = $("#step-1");
    brandForm.validator('validate');
    const elmErr = brandForm.children('.has-error');
    if (elmErr && elmErr.length > 0) {
        return;
    }

    const name = $("#InventoryId").val();
    const companyId = $("#Price").val();
    ajaxPost("/api/Inventory/SavePrice/",
        {
            Id: name,
            Price: companyId,
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Price Updated", NotificationEnum.Success);
                closeDialogBox();
            }
        },
        function (result) {
            showNotification("Error Occured", NotificationEnum.Warning);
        });
}


function removeFromPharmacyInventory() {
    var id = $("#Id").val();
    var quantity = $("#Quantity").val();
    const radioValue = $("input[name='AddType']:checked").val();
    ajaxPost("/api/Inventory/RemoveInventory/",
        {
            Id: id,
            AddType: radioValue,
            Quantity: quantity,
        },
        function (result) {
            if (result.message === "Success") {
                showNotification("Medicine Removed From Inventory", NotificationEnum.Success);
                closeDialogBox();
            } else {
                showNotification("Removing More than Quantity", NotificationEnum.Error);
            }
        },
        function (result) {
            showNotification("Error Occured", NotificationEnum.Warning);
        });
}

function showSetPriceDialog(userId) {
    showDialogBox("/api/Template/GetSetPriceForm/",
        {
            Id: userId
        },
        "Set Price");
}