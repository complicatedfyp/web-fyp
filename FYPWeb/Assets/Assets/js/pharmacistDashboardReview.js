﻿$(function () {
    searchReview(null);
});

function changePage(e, index, parameter) {
    const p = parameter;

    ajaxPost("/api/Template/GetReviews/",
        {
            Parameter: p,
            UserId: userId,
            Page: index,
            Size: 10
        },
        function (result) {
            $("#reviewTable").html("");

            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#reviewTable").append(d);
                });
            } else {
                $("#reviewTable").html("<p>No Data Found</p>");
            }
            $("#reviewPagination").find("a").each(function () {
                $(this).removeClass("active");
            });
            $(e).addClass("active");
        },
        function (result) {
            $("#reviewTable").html("");
            $("#reviewTable").html("<p>Error Occured</p>");
            Log(result);
        });
}

function searchReview(e, event) {
    if (e !== null && event.which !== 13) {
        return;
    }
    var p = e === null ? "" : $(e).val();
    showLoading("overlay");
    ajaxPost("/api/Template/GetReviews/",
        {
            Parameter: p,
            UserId: userId,
            Page: 1,
            Size: 10
        },
        function (result) {
            $("#reviewTable").html("");
            if (result.result.length > 0) {
                result.result.forEach(function (d) {
                    $("#reviewTable").append(d);
                });
            } else {
                $("#reviewTable").html("<p>No Data Found</p>");
            }
            hideLoading("overlay");

        },
        function (result) {
            $("#reviewTable").html("");
            $("#reviewTable").html("<p>Error Occured</p>");
            hideLoading("overlay");
            Log(result);
        });

    ajaxPost("/api/Template/GetReviewsCount/",
        {
            Parameter: p,
            UserId: userId,
            Page: 1,
            Size: 10
        },
        function (result) {
            $("#reviewPagination").html("");
            for (let i = 0; i < result.count; i++) {
                $("#reviewPagination").append("<li><a href='#' onclick='changePage(this," + (i + 1) + ",\"" + p + "\")'>" + (i + 1) + "</a></li>");
            }
        },
        function (result) {
            Log(result);
        });
}





function showeditDialogReview(id) {
    showDialogBox("/api/Template/GetReviewForm/",
        { Id: id },
        "Edit Review Information");
}

function showdeleteDialogReview(id) {
    showConfirmDialogBox("Delete Review",
        "Do you want to delete this Review?",
        "Delete Review",
        "No",
        function (e) {
            ajaxPost("/api/Review/DeleteReview",
                {
                    Id: id
                },
                function (result) {
                    if (result.message === "Success") {
                        showNotification("Review Deleted", NotificationEnum.Success);
                        $("#row_review_" + id).animate({ opacity: "0.0" }, "slow", function () {
                            $(this).remove();
                            $("#myConfirmModal").modal("hide");
                        });
                    }
                },
                function (result) {

                });
        });
}

