$(function() {
    $(".slimScrollDiv").slimScroll({
        height: '600px'
    });
});

function getTimer() {
    var id = $("#OrderId").val();
    if (id > 0) {
        var date = $("#OrderId").find(':selected').attr('data-endDate');
        $("#myTimer").html("<div id=\"counter\" data-date=\"" + date + "\"></div>");
        $('#counter').TimeCircles({
            start: true, // determines whether or not TimeCircles should start immediately.
            animation: "smooth",
            // smooth or ticks. The way the circles animate can be either a constant gradual rotating, slowly moving from one second to the other. 
            count_past_zero: true,
            // This option is only really useful for when counting down. What it does is either give you the option to stop the timer, or start counting up after you've hit the predefined date (or your stopwatch hits zero).
            circle_bg_color: "#60686F", // determines the color of the background circle.
            use_background: true, // sets whether any background circle should be drawn at all. 
            fg_width: 0.1, //  sets the width of the foreground circle. 
            bg_width: 1.2, // sets the width of the backgroundground circle. 
            text_size: 0.07, // This option sets the font size of the text in the circles. 
            total_duration: "Auto",
            // This option can be set to change how much time will fill the largest visible circle.
            direction: "Clockwise", // "Clockwise", "Counter-clockwise" or "Both".
            use_top_frame: false,
            start_angle: 0,
            // This option can be set to change the starting point from which the circles will fill up. 
            time: { //  a group of options that allows you to control the options of each time unit independently.
                Days: {
                    show: false,
                },Hours: {
                    show: true,
                    text: "Hours",
                    color: "#27aae1"
                },
                Minutes: {
                    show: true,
                    text: "Minutes",
                    color: "#27aae1"
                },
                Seconds: {
                    show: true,
                    text: "Sec",
                    color: "#27aae1"
                }
            }
        }).addListener(countdownComplete);
        function countdownComplete(unit, value, total) {
            if (total <= 0) {
                $("#myTimer").html("<h2>Time has Expired</h2>");
            }
        }
    }
}