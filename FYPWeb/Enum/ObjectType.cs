﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FYPWeb.Enum
{
    public enum ObjectType
    {
        All,
        Rejected,
        Pending,
        Approved
    }

    public enum SearchType
    {
        SortByNone = 0,
        SortByName = 1,
        SortByNameDesc = 2,
        SortByPrice = 3,
        SortByPriceDesc = 4,
        SortByRating = 5,
        SortByRatingDesc = 6,
    }

    public enum NotificationType
    {
        Info = 1,
        Error = 2,
        Warning = 3,
        Success = 4,
    }
}