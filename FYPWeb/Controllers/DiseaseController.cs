﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FYPWeb.Repository.Repository;
using FYPWeb.Service;
using FYPWeb.ViewModels;

namespace FYPWeb.Controllers
{
    public class DiseaseController : Controller
    {
        private readonly DrugCartRepository _drugCartRepository;
        private readonly DiseaseRepository _diseaseRepository;
        private readonly DrugRepository _drugRepository;
        public DiseaseController(DiseaseRepository diseaseRepository, DrugCartRepository drugCartRepository, DrugRepository drugRepository)
        {
            _diseaseRepository = diseaseRepository;
            _drugCartRepository = drugCartRepository;
            _drugRepository = drugRepository;
        }

        public ActionResult Index(int id,int indPage = 1,int conPage = 1)
        {
            var d = _diseaseRepository.GetById(id);
            if (d == null) return RedirectToAction("Index", "Home");
            var i = 0;
            if (indPage < 1)
            {
                indPage = 1;
            }
            if (conPage < 1)
            {
                conPage = 1;
            }

            if (SessionService.UserId != "")
            {
                i = _drugCartRepository.GetUniqueMedicineCountByUserId(int.Parse(SessionService.UserId));
            }
            d.PageVisit++;
            _diseaseRepository.Update(d);
            var model = new DiseaseProfileViewModel()
            {
                Disease = d,
                Id = id,
                HeaderViewModel = new HomeHeaderViewModel()
                {
                    Header = HomeHeader.None,
                    CartCount = i,
                    NotificationCount = 0,
                },
                IndicationDrugs = _drugRepository.GetIndicationDrugsByDiseaseId(id,indPage,9),
                NonIndicationDrugs = _drugRepository.GetNonIndicationDrugsByDiseaseId(id,conPage, 9),
                IndPage = indPage,
                ConPage = conPage,
                ConPageCount = _drugRepository.GetNonIndicationDrugsByDiseaseIdCount(id, 9),
                IndPageCount = _drugRepository.GetIndicationDrugsByDiseaseIdCount(id, 9),
            };
            return View("Index", model);

        }
    }
}