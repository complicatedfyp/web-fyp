﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using FYPWeb.Repository.Repository;
using FYPWeb.Service;
using FYPWeb.ViewModels;

namespace FYPWeb.Controllers
{
    public class ProfileController : Controller
    {
        private readonly UserRepository _userRepository;
        private readonly PharmacyRepository _pharmacyRepository;
        private readonly DrugCartRepository _drugCartRepository;
        private readonly PharmacistReviewRepository _pharmacistReviewRepository;
        private readonly InventoryTrackRepository _inventoryRepository;
        public ProfileController(UserRepository userRepository, PharmacyRepository pharmacyRepository, DrugCartRepository drugCartRepository, PharmacistReviewRepository pharmacistReviewRepository, InventoryTrackRepository inventoryRepository)
        {
            _userRepository = userRepository;
            _pharmacyRepository = pharmacyRepository;
            _drugCartRepository = drugCartRepository;
            _pharmacistReviewRepository = pharmacistReviewRepository;
            _inventoryRepository = inventoryRepository;
        }

        public ActionResult Index(string username,int page = 1)
        {
            try
            {
                int c = 0;
                if (SessionService.UserId != "")
                {
                    c = _drugCartRepository.GetUniqueMedicineCountByUserId(int.Parse(SessionService.UserId));
                }
                var user = _userRepository.GetByUsername(username);
                if (user != null)
                {
                    if (user.UserRoleId == 2)
                    {
                        var m = new HomeProfileViewModel()
                        {
                            User = user,
                            HeaderViewModel = new HomeHeaderViewModel()
                            {
                                Header = HomeHeader.None,
                                CartCount = c,
                            },
                        };
                        return View("CustomerProfile", m);
                    }
                    else if(user.UserRoleId == 3)
                    {
                        var m = new HomeProfileViewModel()
                        {
                            User = user,
                            HeaderViewModel = new HomeHeaderViewModel()
                            {
                                Header = HomeHeader.None,
                                CartCount = c,
                            },
                            Pharmacy = _pharmacyRepository.GetApprovedPharmaciesByUserId(user.Id).FirstOrDefault(),
                        };
                        if (m.Pharmacy != null)
                        {
                            m.InventoryTracks = _inventoryRepository.GetDrugsByPharmacyId(m.Pharmacy.Id,page,12);
                            m.Page = page;
                            m.PageCount = _inventoryRepository.GetDrugsByPharmacyIdCount(m.Pharmacy.Id, 12);
                        }
                        return View("PharmacistProfile", m);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return RedirectToAction("Index", "Home");
        }
    }
}