﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FYPWeb.Repository.Repository;
using FYPWeb.Service;
using FYPWeb.ViewModels;
using Model.Entity;

namespace FYPWeb.Controllers
{
    public class CartController : Controller
    {
        private readonly DrugCartRepository _drugCartRepository;
        private readonly PharmacyRepository _pharmacyRepository;
        private readonly SessionCartRepository _sessionCartRepository;
        private readonly InventoryTrackRepository _inventoryTrackRepository;
        public CartController(DrugCartRepository drugCartRepository, PharmacyRepository pharmacyRepository, SessionCartRepository sessionCartRepository, PharmacyRepository pharmacyRepository1, InventoryTrackRepository inventoryTrackRepository)
        {
            _drugCartRepository = drugCartRepository;
            _sessionCartRepository = sessionCartRepository;
            _pharmacyRepository = pharmacyRepository1;
            _inventoryTrackRepository = inventoryTrackRepository;
        }

        public ActionResult Index()
        {
            if (SessionService.UserId == "Pharmacist")
                return RedirectToAction("Index", "Home");

            if (SessionService.UserId != "")
            {
                var id = int.Parse(SessionService.UserId);

                var d = new HomeCartViewModel()
                {
                    HeaderViewModel = new HomeHeaderViewModel()
                    {
                        Header = HomeHeader.None,
                        CartCount = _drugCartRepository.GetUniqueMedicineCountByUserId(id),
                        NotificationCount = 0,
                    },
                    DrugCarts = _drugCartRepository.GetByUserId(id),
                };
                if (d.DrugCarts.Any())
                {
                    var listOfSelected =
                        d.DrugCarts.GroupBy(p => p.Inventory.PharmacyId)
                            .Select(p => p.FirstOrDefault().Inventory.Pharmacy);
                    d.Pharmacies = listOfSelected;
                }
                return View("Index", d);
            }
            else
            {
                var d = new HomeCartViewModel()
                {
                    HeaderViewModel = new HomeHeaderViewModel()
                    {
                        Header = HomeHeader.None,
                        CartCount = _sessionCartRepository.GetUniqueMedicineCountBySessionId(SessionService.SessionId),
                        NotificationCount = 0,
                    },
                    DrugCarts = _sessionCartRepository.GetBySessionId(SessionService.SessionId).Select(p => new DrugCart()
                    {
                        InventoryId = p.InventoryId,
                        Inventory = p.Inventory,
                        Quantity = p.Quantity,
                    }),
                };
                if (d.DrugCarts.Any())
                {
                    var listOfSelected =
                        d.DrugCarts.GroupBy(p => p.Inventory.PharmacyId)
                            .Select(p => p.FirstOrDefault().Inventory.Pharmacy);
                    d.Pharmacies = listOfSelected;
                }
                return View("Index", d);

            }
        }
    }
}