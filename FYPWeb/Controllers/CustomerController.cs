﻿using System;
using System.Diagnostics;
using System.Web.Mvc;
using FYPWeb.Repository.Repository;
using FYPWeb.Service;
using FYPWeb.ViewModels;

namespace FYPWeb.Controllers
{
    public class CustomerController : Controller
    {
        private readonly UserRepository _userRepository;
        private readonly PrescriptionRepository _prescriptionRepository;
        private readonly RecommendationRepository _recommendationRepository;
        private readonly OrderRepository _orderRepository;
        private readonly PromotionRepository _promotionRepository;
        private readonly CustomerReviewRepository _customerReviewRepository;
        private readonly NotificationRepository _notificationRepository;
        private readonly DiseaseRepository _diseaseRepository;
        public CustomerController(UserRepository userRepository, PrescriptionRepository prescriptionRepository, RecommendationRepository recommendationRepository, OrderRepository orderRepository, PromotionRepository promotionRepository, CustomerReviewRepository customerReviewRepository, NotificationRepository notificationRepository, DiseaseRepository diseaseRepository)
        {
            _userRepository = userRepository;
            _prescriptionRepository = prescriptionRepository;
            _recommendationRepository = recommendationRepository;
            _orderRepository = orderRepository;
            _promotionRepository = promotionRepository;
            _customerReviewRepository = customerReviewRepository;
            _notificationRepository = notificationRepository;
            _diseaseRepository = diseaseRepository;
        }

        public ActionResult Dashboard()
        {
            if (SessionService.UserId == "" && SessionService.UserRole != "Customer")
            {
                return RedirectToAction("Index", "Home");
            }
            int id = int.Parse(SessionService.UserId);
            var model = new CustomerDashboardViewModel()
            {
                HeaderViewModel = new CustomerHeaderViewModel()
                {
                    SiderBar = CustomerSiderBar.Dashboard
                },
                Promotions = _promotionRepository.GetTopActivePromotion(),
                ActiveOrders = _orderRepository.GetActiveOrdersByUserId(id),
                Diseases = _diseaseRepository.PerformAnalysisOnOrders(id),
                StringValues = _orderRepository.GetLastOrdersData(id,15)
            };
            return View("Dashboard", model);
        }
        public ActionResult Order(string type, int page = 1)
        {
            try
            {
                if (SessionService.UserId == "" || SessionService.UserRole != "Customer")
                {
                    return RedirectToAction("Index", "Home");
                }
                var m = new CustomerDashboardOrderViewModel()
                {
                    HeaderViewModel = new CustomerHeaderViewModel()
                    {
                        SiderBar =
                            type.ToLower().Contains("pending")
                                ? CustomerSiderBar.OrderPending
                                : type.ToLower().Contains("rejected")
                                ? CustomerSiderBar.OrderRejected
                                : type.ToLower().Contains("completed")
                                ? CustomerSiderBar.OrderCompleted
                                : type.ToLower().Contains("cancelled")
                                ? CustomerSiderBar.OrderCancelled
                                : CustomerSiderBar.OrderActive
                    },
                    Type = type,
                    Parameter = "",
                    Page = page,
                };
                var id = int.Parse(SessionService.UserId);
                if (type.ToLower().Contains("pending"))
                {
                    m.PageCount = _orderRepository.GetPendingOrdersByUserIdCount(id);
                    m.Orders = _orderRepository.GetPendingOrdersByUserId(id, m.Page);
                }
                else if (type.ToLower().Contains("rejected"))
                {
                    m.PageCount = _orderRepository.GetRejectedOrdersByUserIdCount(id);
                    m.Orders = _orderRepository.GetRejectedOrdersByUserId(id, m.Page);
                }
                else if (type.ToLower().Contains("completed"))
                {
                    m.PageCount = _orderRepository.GetCompletedOrdersByUserIdCount(id);
                    m.Orders = _orderRepository.GetCompletedOrdersByUserId(id, m.Page);
                }
                else if (type.ToLower().Contains("cancelled"))
                {
                    m.PageCount = _orderRepository.GetCancelledOrdersByUserIdCount(id);
                    m.Orders = _orderRepository.GetCancelledOrdersByUserId(id, m.Page);
                }
                else 
                {
                    m.PageCount = _orderRepository.GetActiveOrdersByUserIdCount(id);
                    m.Orders = _orderRepository.GetActiveOrdersByUserId(id, m.Page);
                }
                return View("Order", m);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return RedirectToAction("Dashboard", "Customer");
        }
        public ActionResult Notification(int page = 1)
        {
            if (SessionService.UserId == "" || SessionService.UserRole != "Customer")
            {
                return RedirectToAction("Index", "Home");
            }
            if (page < 1) page = 1;
            var model = new CustomerDashboardNotificationViewModel()
            {
                HeaderViewModel = new CustomerHeaderViewModel()
                {
                    SiderBar = CustomerSiderBar.Dashboard
                },
                Parameter = "",
                Page = page,
                PageCount = _notificationRepository.GetPageCountByUserId(int.Parse(SessionService.UserId)),
                Notifications = _notificationRepository.GetNotificationByUserId(int.Parse(SessionService.UserId)),
            };
            return View("Notification", model);
        }
        public ActionResult Recommendation(string type, int page = 1)
        {
            try
            {
                if (SessionService.UserId == "" || SessionService.UserRole != "Customer")
                {
                    return RedirectToAction("Index", "Home");
                }
                var m = new CustomerDashboardRecommendationViewModel()
                {
                    HeaderViewModel = new CustomerHeaderViewModel()
                    {
                        SiderBar = 
                            type.ToLower().Contains("recieved")
                                ? CustomerSiderBar.RecommendationRecieved
                                : CustomerSiderBar.RecommendationSent
                    },
                    Type = type,
                    Parameter = "",
                    Page = page,
                };
                var id = int.Parse(SessionService.UserId);
                if (type.ToLower().Contains("recieved"))
                {
                    m.PageCount = _recommendationRepository.GetPageCountByRecievedUserId(id);
                    m.Recommendations = _recommendationRepository.GetByRecievedUserId(id, m.Page);
                }
                else
                {
                    m.PageCount = _recommendationRepository.GetPageCountByFromUserId(id);
                    m.Recommendations = _recommendationRepository.GetByFromUserId(id, m.Page);

                }
                return View("Recommendation", m);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return RedirectToAction("Dashboard", "Customer");
        }
        public ActionResult MyOrderReview(int page = 1)
        {
            if (SessionService.UserId == "" || SessionService.UserRole != "Customer")
            {
                return RedirectToAction("Index", "Home");
            }
            int id = int.Parse(SessionService.UserId);
            if (page < 1) page = 1;
            var model = new CustomerDashboardOrderReviewViewModel()
            {
                HeaderViewModel = new CustomerHeaderViewModel()
                {
                    SiderBar = CustomerSiderBar.OrderReview
                },
                Page = page,
                PageCount = _customerReviewRepository.GetReviewPageCountByUser(id),
                CustomerReviews = _customerReviewRepository.GetReviewsByUserId(id,page)
            };
            return View("OrderReview", model);
        }
        public ActionResult MyMedicineReview()
        {
            if (SessionService.UserId == "" || SessionService.UserRole != "Customer")
            {
                return RedirectToAction("Index", "Home");
            }
            var model = new CustomerDashboardReviewViewModel()
            {
                HeaderViewModel = new CustomerHeaderViewModel()
                {
                    SiderBar = CustomerSiderBar.MedicineReview
                }
            };
            return View("Review", model);
        }
        public ActionResult MyProfile()
        {
            if (SessionService.UserId == "" || SessionService.UserRole != "Customer")
            {
                return RedirectToAction("Index", "Home");
            }
            var model = new CustomerDashboardProfileViewModel()
            {
                HeaderViewModel = new CustomerHeaderViewModel()
                {
                    SiderBar = CustomerSiderBar.Profile
                },
                User = _userRepository.GetById(int.Parse(SessionService.UserId))
            };
            return View("Profile", model);
        }
        public ActionResult MyPosts()
        {
            if (SessionService.UserId == "" || SessionService.UserRole != "Customer")
            {
                return RedirectToAction("Index", "Home");
            }
            var model = new CustomerDashboardPostViewModel()
            {
                HeaderViewModel = new CustomerHeaderViewModel()
                {
                    SiderBar = CustomerSiderBar.Posts
                }
            };
            return View("PostThread", model);

        }
        public ActionResult MyReplies()
        {
            if (SessionService.UserId == "" || SessionService.UserRole != "Customer")
            {
                return RedirectToAction("Index", "Home");
            }
            var model = new CustomerDashboardReplyViewModel()
            {
                HeaderViewModel = new CustomerHeaderViewModel()
                {
                    SiderBar = CustomerSiderBar.PostReplies
                }
            };
            return View("PostReply", model);
        }

        public ActionResult Prescription()
        {
            if (SessionService.UserId == "" || SessionService.UserRole != "Customer")
            {
                return RedirectToAction("Index", "Home");
            }
            var model = new CustomerDashboardPrescriptionViewModel()
            {
                HeaderViewModel = new CustomerHeaderViewModel()
                {
                    SiderBar = CustomerSiderBar.Prescription
                },
                Prescriptions = _prescriptionRepository.GetByUserId(int.Parse(SessionService.UserId))
            };
            return View("Prescription", model);
        }

    }
}
