﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FYPWeb.Repository.Repository;
using FYPWeb.Service;
using FYPWeb.ViewModels;

namespace FYPWeb.Controllers
{
    public class BrandController : Controller
    {
        private readonly DrugCartRepository _drugCartRepository;
        private readonly BrandRepository _brandRepository;

        public BrandController(DrugCartRepository drugCartRepository, BrandRepository brandRepository)
        {
            _drugCartRepository = drugCartRepository;
            _brandRepository = brandRepository;
        }

        public ActionResult Index(int id)
        {
            var b = _brandRepository.GetById(id);
            if (b == null) return RedirectToAction("Index", "Home");
            var i = 0;
            if (SessionService.UserId != "")
            {
                i = _drugCartRepository.GetUniqueMedicineCountByUserId(int.Parse(SessionService.UserId));
            }
            var model = new BrandProfileViewModel()
            {
                Brand = b,
                HeaderViewModel = new HomeHeaderViewModel()
                {
                    Header = HomeHeader.None,
                    CartCount = i,
                    NotificationCount = 0,
                }
            };
            return View("Index", model);
        }
    }
}