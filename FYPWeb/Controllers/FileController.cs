﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FYPWeb.Repository.Repository;
using FYPWeb.Service;
using Model.Entity;

namespace FYPWeb.Controllers
{
    public class FileController : Controller
    {
        private readonly PharmacyRepository _pharmacyRepository;
        private readonly DrugDetailRepository _drugDetailRepository;
        private readonly CompanyRepository _companyRepository;
        private readonly BrandRepository _brandRepository;
        private readonly PrescriptionRepository _prescriptionRepository;
        private readonly UserRepository _userRepository;
        public FileController(CompanyRepository companyRepository, BrandRepository brandRepository, DrugDetailRepository drugDetailRepository, PharmacyRepository pharmacyRepository, PrescriptionRepository prescriptionRepository, UserRepository userRepository)
        {
            _companyRepository = companyRepository;
            _brandRepository = brandRepository;
            _drugDetailRepository = drugDetailRepository;
            _pharmacyRepository = pharmacyRepository;
            _prescriptionRepository = prescriptionRepository;
            _userRepository = userRepository;
        }
        [HttpPost]
        public string SaveCompanyProfile(AddPictureParameter parameter)
        {
            var fileName2 = "";
            if (parameter.Id > 0 && parameter.File != null && parameter.File.ContentLength > 0)
            {
                // extract only the filename
                var user = _companyRepository.GetById(parameter.Id);
                if (user != null)
                {
                    var fileName = Path.GetExtension(parameter.File.FileName);
                    // store the file inside ~/App_Data/uploads folder
                    var path = Path.Combine(Server.MapPath("~/Content/CompanyProfileImage"), parameter.Id + fileName);
                    parameter.File.SaveAs(path);
                    fileName2 = user.ImagePath = "/Content/CompanyProfileImage/" + parameter.Id + fileName;
                    _companyRepository.Update(user);   
                }
            }
            return fileName2;
        }

        [HttpPost]
        public string SaveBrandProfile(AddPictureParameter parameter)
        {
            var fileName2 = "";
            if (parameter.Id > 0 && parameter.File != null && parameter.File.ContentLength > 0)
            {
                // extract only the filename
                var user = _brandRepository.GetById(parameter.Id);
                if (user != null)
                {
                    var fileName = Path.GetExtension(parameter.File.FileName);
                    // store the file inside ~/App_Data/uploads folder
                    var path = Path.Combine(Server.MapPath("~/Content/BrandProfileImage"), parameter.Id + fileName);
                    parameter.File.SaveAs(path);
                    fileName2 = user.ImagePath = "/Content/BrandProfileImage/" + parameter.Id + fileName;
                    _brandRepository.Update(user);
                }
            }
            return fileName2;
        }

        [HttpPost]
        public string SavePharmacyProfile(AddPictureParameter parameter)
        {
            var fileName2 = "";
            if (parameter.Id > 0 && parameter.File != null && parameter.File.ContentLength > 0)
            {
                // extract only the filename
                var user = _pharmacyRepository.GetById(parameter.Id);
                if (user != null)
                {
                    var fileName = Path.GetExtension(parameter.File.FileName);
                    // store the file inside ~/App_Data/uploads folder
                    var path = Path.Combine(Server.MapPath("~/Content/PharmacyProfileImage"), parameter.Id + fileName);
                    parameter.File.SaveAs(path);
                    fileName2 = user.ImagePath = "/Content/PharmacyProfileImage/" + parameter.Id + fileName;
                    _pharmacyRepository.Update(user);
                }
            }
            return fileName2;
        }

        [HttpPost]
        public string SaveMedicineProfile(AddPictureParameter parameter)
        {
            var fileName2 = "";
            if (parameter.Id > 0 && parameter.File != null && parameter.File.ContentLength > 0)
            {
                // extract only the filename
                var user = _drugDetailRepository.GetById(parameter.Id);
                if (user != null)
                {
                    var fileName = Path.GetExtension(parameter.File.FileName);
                    // store the file inside ~/App_Data/uploads folder
                    var path = Path.Combine(Server.MapPath("~/Content/DrugProfileImage"), parameter.Id + fileName);
                    parameter.File.SaveAs(path);
                    fileName2 = user.ImageLink = "/Content/DrugProfileImage/" + parameter.Id + fileName;
                    _drugDetailRepository.Update(user);
                }
            }
            return fileName2;
        }

        [HttpPost]
        public string SaveEmployeeProfile(AddPictureParameter parameter)
        {
            var fileName2 = "";
            if (parameter.Id > 0 && parameter.File != null && parameter.File.ContentLength > 0)
            {
                // extract only the filename
                var user = _drugDetailRepository.GetById(parameter.Id);
                if (user != null)
                {
                    var fileName = Path.GetExtension(parameter.File.FileName);
                    // store the file inside ~/App_Data/uploads folder
                    var path = Path.Combine(Server.MapPath("~/Content/EmployeeProfileImage"), parameter.Id + fileName);
                    parameter.File.SaveAs(path);
                    fileName2 = user.ImageLink = "/Content/EmployeeProfileImage/" + parameter.Id + fileName;
                    _drugDetailRepository.Update(user);
                }
            }
            return fileName2;
        }

        [HttpPost]
        public string SavePrescription(AddPictureParameter parameter)
        {
            var fileName2 = "";
            if (parameter.Id > 0 && parameter.File != null && parameter.File.ContentLength > 0)
            {
                // extract only the filename
                var user = _userRepository.IsExist(parameter.Id);
                if (user)
                {
                    var fileName = Path.GetExtension(parameter.File.FileName);
                    // store the file inside ~/App_Data/uploads folder
                    var newFileName = Guid.NewGuid() + fileName;
                    var path = Path.Combine(Server.MapPath("~/Content/PrescriptionImage"), newFileName);
                    parameter.File.SaveAs(path);
                    newFileName = "/Content/PrescriptionImage/" + newFileName;
                    var pres = new Prescription()
                    {
                        UserId = parameter.Id,
                        PrescriptionPath = newFileName
                    };
                    _prescriptionRepository.Add(pres);
                }
            }
            return fileName2;
        }

        [HttpPost]
        public string SaveUserProfile(AddPictureParameter parameter)
        {
            var fileName2 = "";
            if (parameter.Id > 0 && parameter.File != null && parameter.File.ContentLength > 0)
            {
                // extract only the filename
                var user = _userRepository.GetById(parameter.Id);
                if (user != null)
                {
                    var fileName = Path.GetExtension(parameter.File.FileName);
                    // store the file inside ~/App_Data/uploads folder
                    var newFileName = Guid.NewGuid() + fileName;
                    var path = Path.Combine(Server.MapPath("~/Content/UserProfile"), newFileName);
                    parameter.File.SaveAs(path);
                    newFileName = "/Content/UserProfile/" + newFileName;
                    user.UserImage = newFileName;
                    _userRepository.Update(user);
                    if (SessionService.UserId != "")
                    {
                        SessionService.UserImage = user.UserImage;
                    }
                }
            }
            return fileName2;
        }

    }

    public class AddPictureParameter
    {
        public HttpPostedFileBase File { get; set; }
        public int Id { get; set; }

    }
}