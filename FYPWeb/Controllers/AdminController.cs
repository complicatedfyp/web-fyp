﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FYPWeb.Repository.Repository;
using FYPWeb.Service;
using FYPWeb.Services.Util;
using FYPWeb.ViewModels;
using Model.Entity;

namespace FYPWeb.Controllers
{
    public class AdminController : Controller
    {
        private readonly UserRepository _userRepository;
        public AdminController(UserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public ActionResult Login()
        {
            var model = new LoginViewModel()
            {
                Message = TempDataService<string>.GetData(this, "Message") + ""
            };
            return View("Login", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LoginUser(LoginParameter parameter)
        {
            var data = _userRepository.LoginIn(parameter.Email, UtilService.EncryptPassword(parameter.Password).ToLower(), 1);
            if (data != null)
            {
                SessionService.UserId = data.Id + "";
                SessionService.UserRole = data.UserRole.UserRoleName;
                SessionService.UserImage = data.UserImage;
                SessionService.UserName = data.Username;
                SessionService.FullName = data.FirstName + " " + data.LastName;
                return RedirectToAction("Index", "Admin");
            }
            TempDataService<string>.SetData(this, "Message", "Incorrect Username/Password");
            return RedirectToAction("Login", "Admin");
        }

        public ActionResult Index()
        {
            if (SessionService.UserRole != "Admin" || SessionService.UserId == "")
            {
                return RedirectToAction("Index", "Home");
            }
            var model = new AdminIndexViewModel()
            {
                AdminHeaderViewModel = new AdminHeaderViewModel()
                {
                    SiderBar = AdminSideBar.Dashboard,
                },

            };
            return View("Index", model);
        }
        public ActionResult Post()
        {
            if (SessionService.UserRole != "Admin" || SessionService.UserId == "")
            {
                return RedirectToAction("Index", "Home");
            }
            var model = new AdminDashboardPostViewModel()
            {
                HeaderViewModel = new AdminHeaderViewModel()
                {
                    SiderBar = AdminSideBar.Post,
                }
            };
            return View("Post", model);
        }

        public ActionResult Users(string type)
        {
            if (SessionService.UserRole != "Admin" || SessionService.UserId == "")
            {
                return RedirectToAction("Index", "Home");
            }
            var model = new AdminUsersViewModel()
            {
                AdminHeaderViewModel = new AdminHeaderViewModel()
                {
                    SiderBar = type.ToLower().Contains("inactive") ? AdminSideBar.UserInActive : AdminSideBar.UserActive,
                }
            };
            return View("Users", model);
        }

        [HttpGet]
        public ActionResult Category()
        {
            if (SessionService.UserRole != "Admin" || SessionService.UserId == "")
            {
                return RedirectToAction("Index", "Home");
            }
            var model = new AdminForumCateogryViewModel()
            {
                AdminHeaderViewModel = new AdminHeaderViewModel()
                {
                    SiderBar = AdminSideBar.ForumCategory
                }
            };
            return View("ForumCategory", model);
        }
        [HttpGet]
        public ActionResult Company()
        {
            if (SessionService.UserRole != "Admin" || SessionService.UserId == "")
            {
                return RedirectToAction("Index", "Home");
            }
            var model = new AdminCompaniesViewModel()
            {
                AdminHeaderViewModel = new AdminHeaderViewModel()
                {
                    SiderBar = AdminSideBar.Company
                }
            };
            return View("Company", model);
        }

        [HttpGet]
        public ActionResult Brand()
        {
            if (SessionService.UserRole != "Admin" || SessionService.UserId == "")
            {
                return RedirectToAction("Index", "Home");
            }
            var model = new AdminBrandViewModel()
            {
                AdminHeaderViewModel = new AdminHeaderViewModel()
                {
                    SiderBar = AdminSideBar.Brands
                }
            };
            return View("Brand", model);
        }

        [HttpGet]
        public ActionResult Medicine()
        {
            if (SessionService.UserRole != "Admin" || SessionService.UserId == "")
            {
                return RedirectToAction("Index", "Home");
            }
            var model = new AdminMedicineViewModel()
            {
                AdminHeaderViewModel = new AdminHeaderViewModel()
                {
                    SiderBar = AdminSideBar.Medicines
                }
            };
            return View("Medicine", model);
        }

        [HttpGet]
        public ActionResult Disease()
        {
            if (SessionService.UserRole != "Admin" || SessionService.UserId == "")
            {
                return RedirectToAction("Index", "Home");
            }
            var model = new AdminDiseaseViewModel()
            {
                AdminHeaderViewModel = new AdminHeaderViewModel()
                {
                    SiderBar = AdminSideBar.Diseases
                }
            };
            return View("Disease", model);
        }

        [HttpGet]
        public ActionResult MedicineDetail()
        {
            if (SessionService.UserRole != "Admin" || SessionService.UserId == "")
            {
                return RedirectToAction("Index", "Home");
            }
            var model = new AdminMedicineDetailViewModel()
            {
                AdminHeaderViewModel = new AdminHeaderViewModel()
                {
                    SiderBar = AdminSideBar.MedicineDetails
                }
            };
            return View("MedicineDetail", model);
        }

        [HttpGet]
        public ActionResult Pharmacy(string type)
        {
            if (SessionService.UserRole != "Admin" || SessionService.UserId == "")
            {
                return RedirectToAction("Index", "Home");
            }
            var t = type.ToLower();
            var model = new AdminPharmacyViewModel()
            {
                AdminHeaderViewModel = new AdminHeaderViewModel()
                {
                    SiderBar = t.Contains("approved") ? AdminSideBar.PharmacyApproved : t.Contains("rejected") ? AdminSideBar.PharmacyRejected : AdminSideBar.PharmacyPending
                },
                Type = t.Contains("approved") ? "Approved" : t.Contains("rejected") ? "Rejected" : "Pending"
            };
            return View("Pharmacy", model);
        }

    }

    
}