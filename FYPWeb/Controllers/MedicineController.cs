﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FYPWeb.Repository.Repository;
using FYPWeb.Service;
using FYPWeb.ViewModels;

namespace FYPWeb.Controllers
{
    public class MedicineController : Controller
    {
        private readonly DrugCartRepository _drugCartRepository;
        private readonly DrugReviewRepository _drugReviewRepository;
        private readonly DrugRepository _drugRepository;
        private readonly PharmacyRepository _pharmacyRepository;
        private readonly PromotionRepository _promotionRepository;
        private readonly RecommendationRepository _recommendationRepository;
        private readonly InventoryTrackRepository _inventoryTrackRepository;
        public MedicineController(DrugCartRepository drugCartRepository, DrugReviewRepository drugReviewRepository, DrugRepository drugRepository, PharmacyRepository pharmacyRepository, PromotionRepository promotionRepository, RecommendationRepository recommendationRepository, InventoryTrackRepository inventoryTrackRepository)
        {
            _drugCartRepository = drugCartRepository;
            _drugReviewRepository = drugReviewRepository;
            _drugRepository = drugRepository;

            _pharmacyRepository = pharmacyRepository;
            _promotionRepository = promotionRepository;
            _recommendationRepository = recommendationRepository;
            _inventoryTrackRepository = inventoryTrackRepository;
        }

        public ActionResult Index(int id)
        {
            var isReview = false;
            var i = 0;
            if (SessionService.UserId != "")
            {
                i = _drugCartRepository.GetUniqueMedicineCountByUserId(int.Parse(SessionService.UserId));
                isReview = _drugReviewRepository.IsReviewed(id, int.Parse(SessionService.UserId));
            }

            var m = _drugRepository.GetById(id);
            if (m != null)
            {
                m.PageVisit++;
                _drugRepository.Update(m);
                var d = new HomeMedicineViewModel()
                {
                    Drug = m,
                    HeaderViewModel = new HomeHeaderViewModel()
                    {
                        Header = HomeHeader.None,
                        CartCount = i,
                        NotificationCount = 0,
                    },
                    IsReviewed = isReview,
                    Rating = _drugReviewRepository.GetAverageReview(id),
                    ReviewPageCount = _drugReviewRepository.GetPaginationCountByMedicineId(id),
                    TotalReview = _drugReviewRepository.GetTotalCountByMedicineId(id),
                    DrugReviews = _drugReviewRepository.GetByMedicineId(id),
                    Pharmacies = _pharmacyRepository.GetPharmaciesByStockMedicineId(id),
                    RecommendationCount = _recommendationRepository.GetUniqueRecommendationByMedicineId(m.Id),
                    Prices = _inventoryTrackRepository.GetByDrugId(id)
                };
                return View("Index", d);
            }
            return RedirectToAction("Index", "Error");
        }
    }
}