﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using FYPWeb.Enum;
using FYPWeb.Hub;
using FYPWeb.Repository.Repository;
using FYPWeb.Service;
using FYPWeb.ViewModels;
using Model.Entity;

namespace FYPWeb.Controllers
{
    public class ForumController : Controller
    {
        private readonly PostThreadRepository _postThreadRepository;
        private readonly TagPostThreadRepository _tagPostThreadRepository;
        private readonly PostCategoryRepository _postCategoryRepository;
        private readonly PostReplyRepository _postReplyRepository;
        private readonly TagRepository _tagRepository;
        private readonly NotificationRepository _notificationRepository;
        public ForumController(DrugCartRepository drugCartRepository, PostCategoryRepository postCategoryRepository, PostThreadRepository postThreadRepository, PostReplyRepository postReplyRepository, TagPostThreadRepository tagPostThreadRepository, TagRepository tagRepository, NotificationRepository notificationRepository)
        {
            _postCategoryRepository = postCategoryRepository;
            _postThreadRepository = postThreadRepository;
            _postReplyRepository = postReplyRepository;
            _tagPostThreadRepository = tagPostThreadRepository;
            _tagRepository = tagRepository;
            _notificationRepository = notificationRepository;
        }

        public ActionResult Index()
        {
            var m = new ForumIndexViewModel()
            {
                Header = new ForumHeaderViewModel()
                {
                    Header = HomeHeader.Forum
                },
                PostThreads = _postThreadRepository.GetLatestThreads(1,5),
                Categories = _postCategoryRepository.GetTopCategories(1,5),
                HotPostThreads = _postThreadRepository.GetHotThreads(5)

            };
            return View("Index", m);
        }

        [HttpGet]
        public ActionResult AllThreadPost(string parameter = "", int page = 1)
        {
            try
            {
                if (page < 1)
                {
                    page = 1;
                }
                var m = new ForumAllThreadPostViewModel()
                {
                    Header = new ForumHeaderViewModel()
                    {
                        Header = HomeHeader.Forum
                    },
                    Page = page,
                    Parameter = parameter,
                };
                if (string.IsNullOrEmpty(parameter))
                {
                    m.PostThreads = _postThreadRepository.GetLatestThreads(page);
                    m.PageCount = _postThreadRepository.GetPaginationCount(10);
                }
                else
                {
                    m.PostThreads = _postThreadRepository.GetLatestThreads(parameter, page);
                    m.PageCount = _postThreadRepository.GetPaginationSearchCount(parameter, 10);
                }
                return View("AllThreadPost", m);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return RedirectToAction("Index", "Forum");
        }


        [HttpGet]
        public ActionResult AllTags(string parameter = "", int page = 1)
        {
            try
            {
                if (page < 1)
                {
                    page = 1;
                }
                var m = new ForumAllTagsViewModel()
                {
                    Header = new ForumHeaderViewModel()
                    {
                        Header = HomeHeader.Forum
                    },
                    Page = page,
                    Parameter = parameter,
                };
                if (string.IsNullOrEmpty(parameter))
                {
                    m.Tags = _tagRepository.GetAllTags(page);
                    m.PageCount = _tagRepository.GetPaginationCount();
                }
                else
                {
                    m.Tags = _tagRepository.GetAllTagsSearch(parameter,page);
                    m.PageCount = _tagRepository.GetPaginationSearchCount(parameter);
                }
                return View("AllTag", m);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return RedirectToAction("Index", "Forum");
        }

        [HttpGet]
        public ActionResult AllCategories(string parameter = "", int page = 1)
        {
            try
            {
                if (page < 1)
                {
                    page = 1;
                }
                var m = new ForumAllCategoriesViewModel()
                {
                    Header = new ForumHeaderViewModel()
                    {
                        Header = HomeHeader.Forum
                    },
                    Page = page,
                    Parameter = parameter,
                };
                if (string.IsNullOrEmpty(parameter))
                {
                    m.PostCategories = _postCategoryRepository.GetCategories(page);
                    m.PageCount = _postCategoryRepository.GetPaginationCount();
                }
                else
                {
                    m.PostCategories = _postCategoryRepository.GetCategoriesSearch(parameter,page);
                    m.PageCount = _postCategoryRepository.GetPaginationSearchCount(parameter);
                }
                return View("AllCategories", m);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return RedirectToAction("Index", "Forum");
        }

        [HttpGet]
        public ActionResult AllThreadPostByTag(string tag = "", int page = 1)
        {
            try
            {
                if (page < 1)
                {
                    page = 1;
                }
                var m = new ForumAllThreadPostViewModel()
                {
                    Header = new ForumHeaderViewModel()
                    {
                        Header = HomeHeader.Forum
                    },
                    Page = page,
                    Tag = tag,
                };
                if (!string.IsNullOrEmpty(tag))
                {
                    m.PostThreads = _postThreadRepository.GetLatestThreadsByTag(tag,page);
                    m.PageCount = _postThreadRepository.GetPaginationCountByTag(tag,10);
                    return View("AllThreadPostByTag", m);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return RedirectToAction("Index", "Forum");
        }
        [HttpGet]
        public ActionResult AllThreadPostByCategory(int catId = 0, int page = 1)
        {
            try
            {
                if (page < 1)
                {
                    page = 1;
                }
                var m = new ForumAllThreadPostViewModel()
                {
                    Header = new ForumHeaderViewModel()
                    {
                        Header = HomeHeader.Forum
                    },
                    Page = page,
                };
                if (catId > 0)
                {
                    m.Category = _postCategoryRepository.GetById(catId)?.CategoryName;
                    m.PostThreads = _postThreadRepository.GetLatestThreadsByCategory(catId, page);
                    m.PageCount = _postThreadRepository.GetPaginationCountByCategory(catId, 10);
                    return View("AllThreadPostByCategory", m);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return RedirectToAction("Index", "Forum");
        }

        [HttpGet]
        public ActionResult PostThread(int id, int page = 1)
        {
            try
            {
                if (id > 0)
                {
                    var p = _postThreadRepository.GetById(id);
                    if (p != null)
                    {
                        var pCount = _postReplyRepository.GetPaginationCountByPostId(p.Id);
                        var m = new ForumPostThreadViewModel()
                        {
                            Header = new ForumHeaderViewModel()
                            {
                                Header = HomeHeader.Forum
                            },
                            PostThread = p,
                            CurrentPage = page,
                            Page = pCount,
                            Tags = _tagPostThreadRepository.GetTagsByPostId(p.Id)
                        };
                        return View("PostThread", m);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return RedirectToAction("Index", "Forum");
        }

        public ActionResult EditPost(int id)
        {
            try
            {
                if (SessionService.UserId != "")
                {
                    var p = _postThreadRepository.GetById(id);
                    if (p != null && p.UserId + "" == SessionService.UserId)
                    {
                        var m = new ForumEditPostViewModel()
                        {
                            Header = new ForumHeaderViewModel()
                            {
                                Header = HomeHeader.Forum
                            },
                            PostCategories = _postCategoryRepository.GetAll(),
                            PostThread = p
                        };
                        return View("EditPost", m);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return RedirectToAction("Login", "Home");
        }
        public ActionResult EditReply(int id)
        {
            try
            {
                if (SessionService.UserId != "")
                {
                    var p = _postReplyRepository.GetById(id);
                    if (p != null && p.UserId + "" == SessionService.UserId)
                    {
                        var m = new ForumEditReplyPostFormViewModel()
                        {
                            Header = new ForumHeaderViewModel()
                            {
                                Header = HomeHeader.None
                            },
                            PostReply = p,
                            PostThread = p.PostThread
                        };
                        return View("EditReply", m);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return RedirectToAction("Login", "Home");
        }

        public ActionResult CreatePost()
        {
            try
            {
                if (SessionService.UserId != "")
                {
                    var m = new ForumCreatePostViewModel()
                    {
                        Header = new ForumHeaderViewModel()
                        {
                            Header = HomeHeader.None
                        },
                        PostCategories = _postCategoryRepository.GetAll()
                    };
                    return View("CreatePost", m);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return RedirectToAction("Login", "Home");
        }
        public ActionResult CreateReply(int id)
        {
            try
            {
                if (SessionService.UserId != "")
                {
                    var p = _postThreadRepository.GetById(id);
                    if (p != null)
                    {
                        var m = new ForumReplyPostFormViewModel()
                        {
                            Header = new ForumHeaderViewModel()
                            {
                                Header = HomeHeader.None
                            },
                            PostThread = p
                        };
                        return View("PostReplyForm", m);
                    }
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return RedirectToAction("Login", "Home");
        }

        [HttpPost, ValidateInput(false), ValidateAntiForgeryToken]
        public ActionResult SubmitPost(SubmitPostParamter paramter)
        {
            try
            {
                if (SessionService.UserId != "")
                {
                    if (paramter != null && paramter.CategoryId > 0)
                    {
                        var post = new PostThread()
                        {
                            UserId = int.Parse(SessionService.UserId),
                            Body = paramter.Body,
                            PostCategoryId = paramter.CategoryId,
                            Subject = paramter.Subject,
                            PostedAt = DateTime.Now,
                        };
                        _postThreadRepository.Add(post);
                        var list = paramter.Tags.Select(p => p.ToLower());
                        var tagIds = _tagRepository.GetIdsOfTags(list);
                        foreach (var tagId in tagIds)
                        {
                            var t = new TagPostThread()
                            {
                                PostThreadId = post.Id,
                                TagId = tagId
                            };
                            _tagPostThreadRepository.Add(t);
                        }
                        return RedirectToAction("PostThread", new { id = post.Id });
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return RedirectToAction("Index");
        }
        [HttpPost, ValidateInput(false), ValidateAntiForgeryToken]
        public ActionResult UpdatePost(SubmitPostParamter paramter)
        {
            try
            {
                if (SessionService.UserId != "")
                {
                    if (paramter != null && paramter.CategoryId > 0 && paramter.PostThreadId > 0)
                    {
                        var post = _postThreadRepository.GetById(paramter.PostThreadId);
                        post.Body = paramter.Body;
                        post.Subject = paramter.Subject;
                        post.PostCategoryId = paramter.CategoryId;
                        _postThreadRepository.Update(post);
                        var list = paramter.Tags.Select(p => p.ToLower()).ToArray();
                        var tagIds = _tagPostThreadRepository.GetTagsByPostId(paramter.PostThreadId).ToList();
                        var addList = list.Except(tagIds.Select(p => p.Name)).ToList();
                        var addTagList = _tagRepository.GetIdsOfTags(addList);
                        foreach (var tagId in tagIds)
                        {
                            if (list.All(p => p != tagId.Name))
                            {
                                _tagPostThreadRepository.DeleteByPostIdTagId(paramter.PostThreadId, tagId.Id);
                            }
                        }
                        foreach (var i in addTagList)
                        {
                            var td = new TagPostThread()
                            {
                                PostThreadId = paramter.PostThreadId,
                                TagId = i
                            };
                            _tagPostThreadRepository.Add(td);
                        }
                        return RedirectToAction("PostThread", new { id = post.Id });
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return RedirectToAction("Index");
        }

        [HttpPost, ValidateInput(false), ValidateAntiForgeryToken]
        public ActionResult SubmitReply(SubmitPostParamter paramter)
        {
            try
            {
                if (SessionService.UserId != "")
                {
                    if (paramter != null && paramter.PostThreadId > 0)
                    {
                        var p = _postThreadRepository.IsExist(paramter.PostThreadId);
                        if (p)
                        {
                            var rep = new PostReply()
                            {
                                Body = paramter.Body,
                                UserId = int.Parse(SessionService.UserId),
                                PostThreadId = paramter.PostThreadId,
                                RepliedAt = DateTime.Now,
                            };
                            _postReplyRepository.Add(rep);
                            var post = _postThreadRepository.GetById(paramter.PostThreadId);
                            var notification = new Notification()
                            {
                                Url = "/Forum/PostThread/" + paramter.PostThreadId,
                                SenderUserId = int.Parse(SessionService.UserId),
                                Body = " has Replied to your Post '" + post.Subject +"'",
                                CreatedAt = DateTime.Now,
                                IsPharmacy = false,
                                IsPost = false,
                                IsSeen = false,
                                IsUser = true,
                                ReceiverUserId = post.UserId.GetValueOrDefault(),
                                NotificationType = (int)NotificationType.Info,
                            };
                            _notificationRepository.Add(notification);
                            NotificationHub.StaticSendNotification(notification, notification.ReceiverUserId, notification.NotificationType);
                            return RedirectToAction("PostThread", new { id = paramter.PostThreadId, page = _postReplyRepository.GetPaginationCountByPostId(paramter.PostThreadId) });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return RedirectToAction("Index");
        }
        [HttpPost, ValidateInput(false), ValidateAntiForgeryToken]
        public ActionResult UpdateReply(SubmitPostParamter paramter)
        {
            try
            {
                if (SessionService.UserId != "")
                {
                    if (paramter != null && paramter.ReplyId > 0)
                    {
                        var p = _postReplyRepository.GetById(paramter.ReplyId);
                        if (p != null)
                        {
                            p.Body = paramter.Body;
                            _postReplyRepository.Update(p);
                            return RedirectToAction("PostThread", new { id = paramter.PostThreadId });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return RedirectToAction("Index");
        }

    }

    public class SubmitPostParamter
    {
        public int ReplyId { get; set; }
        public int PostThreadId { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }
        public string[] Tags { get; set; }
        public int CategoryId { get; set; }

    }
}