﻿using System.Web.Mvc;
using FYPWeb.Repository.Repository;
using FYPWeb.Service;
using FYPWeb.ViewModels;

namespace FYPWeb.Controllers
{
    public class CompanyController : Controller
    {
        private readonly DrugCartRepository _drugCartRepository;
        private readonly CompanyRepository _companyRepository;
        public CompanyController(DrugCartRepository drugCartRepository, CompanyRepository companyRepository)
        {
            _drugCartRepository = drugCartRepository;
            _companyRepository = companyRepository;
        }

        public ActionResult Index(int id)
        {
            var c = _companyRepository.GetById(id);
            if (c == null) return RedirectToAction("Index", "Home");
            var i = 0;
            if (SessionService.UserId != "")
            {
                i = _drugCartRepository.GetUniqueMedicineCountByUserId(int.Parse(SessionService.UserId));
            }
            var model = new CompanyProfileViewModel()
            {
                Company = c,
                HeaderViewModel = new HomeHeaderViewModel()
                {
                    Header = HomeHeader.None,
                    CartCount = i,
                    NotificationCount = 0,
                }
            };
            return View("Index", model);

        }
    }
}