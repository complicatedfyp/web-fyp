﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using FYPWeb.Repository.Repository;
using FYPWeb.Service;
using FYPWeb.ViewModels;
using Model.Entity;

namespace FYPWeb.Controllers
{
    public class PharmacistController : Controller
    {
        private readonly InventoryTrackRepository _inventoryTrackRepository;
        private readonly PharmacyRepository _pharmacyRepository;
        private readonly RecommendationRepository _recommendationRepository;
        private readonly UserRepository _userRepository;
        private readonly NotificationRepository _notificationRepository;
        private readonly OrderRepository _orderRepository;
        private readonly PharmacistReviewRepository _pharmacistReviewRepository;
        public PharmacistController(PharmacyRepository pharmacyRepository, UserRepository userRepository, RecommendationRepository recommendationRepository, OrderRepository orderRepository, PharmacistReviewRepository pharmacistReviewRepository, NotificationRepository notificationRepository, InventoryTrackRepository inventoryTrackRepository)
        {
            _pharmacyRepository = pharmacyRepository;
            _userRepository = userRepository;
            _recommendationRepository = recommendationRepository;
            _orderRepository = orderRepository;
            _pharmacistReviewRepository = pharmacistReviewRepository;
            _notificationRepository = notificationRepository;
            _inventoryTrackRepository = inventoryTrackRepository;
        }

        public ActionResult MyOrderReview(int page = 1)
        {
            if (SessionService.UserId == "" || SessionService.UserRole != "Pharmacist")
            {
                return RedirectToAction("Index", "Home");
            }
            int uid = int.Parse(SessionService.UserId);
            var id = _userRepository.GetById(uid).Pharmacies.First(p => p.IsApproved && !p.IsRejected).Id;
            if (page < 1) page = 1;
            var model = new PharmacistDashboardOrderReviewViewModel()
            {
                PharmacistHeaderViewModel = new PharmacistHeaderViewModel()
                {
                    Header = PharmacistHeader.OrderReviews
                },
                Page = page,
                PageCount = _pharmacistReviewRepository.GetReviewPageCountByPharmacy(id),
                PharmacyReviews = _pharmacistReviewRepository.GetReviewsByPharmacy(id, page)
            };
            return View("OrderReview", model);
        }

        public ActionResult Notification(int page = 1)
        {
            if (SessionService.UserId == "" || SessionService.UserRole != "Pharmacist")
            {
                return RedirectToAction("Index", "Home");
            }
            if (page < 1) page = 1;
            var model = new PharmacistDashboardNotificationViewModel()
            {
                PharmacistHeaderViewModel = new PharmacistHeaderViewModel()
                {
                    Header = PharmacistHeader.Dashboard
                },
                Parameter = "",
                Page = page,
                PageCount = _notificationRepository.GetPageCountByUserId(int.Parse(SessionService.UserId)),
                Notifications = _notificationRepository.GetNotificationByUserId(int.Parse(SessionService.UserId)),
            };
            return View("Notification", model);
        }

        public ActionResult RelocateSuggest()
        {
            if (SessionService.UserId == "" || SessionService.UserRole != "Pharmacist")
            {
                return RedirectToAction("Index", "Home");
            }
            var model = new PharmacistDashboardRelocationSuggestionViewModel()
            {
                PharmacistHeaderViewModel = new PharmacistHeaderViewModel()
                {
                    Header = PharmacistHeader.ChangeLocation
                },
            };
            return View(model);
        }

        public ActionResult Dashboard()
        {
            if (SessionService.UserId == "" || SessionService.UserRole != "Pharmacist")
            {
                return RedirectToAction("Index", "Home");
            }
            var u = _userRepository.GetById(int.Parse(SessionService.UserId));
            var id = u.Pharmacies.Any(p => p.IsApproved && !p.IsRejected)
                ? u.Pharmacies.First(p => p.IsApproved && !p.IsRejected).Id
                : 0;
            var model = new PharmacistDashboardViewModel()
            {
                PharmacistHeaderViewModel = new PharmacistHeaderViewModel()
                {
                    Header = PharmacistHeader.Dashboard
                },
                ActiveOrders = _orderRepository.GetActiveOrdersByPharmacyId(id),
                PendingOrders = _orderRepository.GetPendingOrdersByPharmacyId(id),
                StringValues = _orderRepository.GetMonthlySaleByPharmacy(id),
                EmployeeStringValues = _orderRepository.GetSaleByEmployee(id),
            };
            foreach (var modelStringValue in model.StringValues)
            {
                modelStringValue.Title =
                    DateTimeFormatInfo.CurrentInfo?.GetAbbreviatedMonthName(int.Parse(modelStringValue.Title));
            }
            return View("Dashboard",model);
        }

        public ActionResult Employee()
        {
            if (SessionService.UserId == "" || SessionService.UserRole != "Pharmacist")
            {
                return RedirectToAction("Index", "Home");
            }
            var model = new PharmacistDashboardEmployeeViewModel()
            {
                PharmacistHeaderViewModel = new PharmacistHeaderViewModel()
                {
                    Header = PharmacistHeader.Employee
                }
            };
            return View("Employee", model);
        }
        public ActionResult Order(string type, int page = 1)
        {
            try
            {
                if (SessionService.UserId == "" || SessionService.UserRole != "Pharmacist")
                {
                    return RedirectToAction("Index", "Home");
                }
                var m = new PharmacistDashboardOrderViewModel()
                {
                    PharmacistHeaderViewModel = new PharmacistHeaderViewModel()
                    {
                        Header = 
                            type.ToLower().Contains("pending")
                                ? PharmacistHeader.OrderPending
                                : type.ToLower().Contains("rejected")
                                ? PharmacistHeader.OrderRejected
                                : type.ToLower().Contains("completed")
                                ? PharmacistHeader.OrderCompleted
                                : type.ToLower().Contains("cancelled")
                                ? PharmacistHeader.OrderCancelled
                                : PharmacistHeader.OrderActive
                    },
                    Type = type,
                    Parameter = "",
                    Page = page,
                };
                var uid = int.Parse(SessionService.UserId);
                var list = _pharmacyRepository.GetApprovedPharmaciesByUserId(uid);
                var pharmacies = list as Pharmacy[] ?? list.ToArray();
                var id = pharmacies.Any() ? pharmacies.First().Id : 0;
                if (id == 0)
                {
                    m.PageCount = 0;
                    m.Orders = new List<Order>();
                }
                else if (type.ToLower().Contains("pending"))
                {
                    m.PageCount = _orderRepository.GetPendingOrdersByPharmacyIdCount(id);
                    m.Orders = _orderRepository.GetPendingOrdersByPharmacyId(id, m.Page);
                }
                else if (type.ToLower().Contains("rejected"))
                {
                    m.PageCount = _orderRepository.GetRejectedOrdersByPharmacyIdCount(id);
                    m.Orders = _orderRepository.GetRejectedOrdersByPharmacyId(id, m.Page);
                }
                else if (type.ToLower().Contains("completed"))
                {
                    m.PageCount = _orderRepository.GetCompletedOrdersByPharmacyIdCount(id);
                    m.Orders = _orderRepository.GetCompletedOrdersByPharmacyId(id, m.Page);
                }
                else if (type.ToLower().Contains("cancelled"))
                {
                    m.PageCount = _orderRepository.GetCancelledOrdersByPharmacyIdCount(id);
                    m.Orders = _orderRepository.GetCancelledOrdersByPharmacyId(id, m.Page);
                }
                else
                {
                    m.PageCount = _orderRepository.GetActiveOrdersByPharmacyIdCount(id);
                    m.Orders = _orderRepository.GetActiveOrdersByPharmacyId(id, m.Page);
                }
                return View("Order", m);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return RedirectToAction("Dashboard", "Pharmacist");
        }

        public ActionResult MyMedicineReview()
        {
            if (SessionService.UserId == "" || SessionService.UserRole != "Pharmacist")
            {
                return RedirectToAction("Index", "Home");
            }
            var model = new PharmacistDashboardReviewViewModel()
            {
                PharmacistHeaderViewModel = new PharmacistHeaderViewModel()
                {
                    Header = PharmacistHeader.MedicineReviews
                },
            };
            return View("Review", model);
        }

        public ActionResult Stock()
        {
            if (SessionService.UserId == "" || SessionService.UserRole != "Pharmacist")
            {
                return RedirectToAction("Index", "Home");
            }
            var model = new PharmacistDashboardInventoryViewModel()
            {
                PharmacistHeaderViewModel = new PharmacistHeaderViewModel()
                {
                    Header = PharmacistHeader.Stock
                },
                Pharmacies = _pharmacyRepository.GetApprovedPharmaciesByUserId(int.Parse(SessionService.UserId))
            };
            return View("Stock", model);
        }
        public ActionResult MyPosts()
        {
            if (SessionService.UserId == "" || SessionService.UserRole != "Pharmacist")
            {
                return RedirectToAction("Index", "Home");
            }
            var model = new PharmacistDashboardForumViewModel()
            {
                PharmacistHeaderViewModel = new PharmacistHeaderViewModel()
                {
                    Header = PharmacistHeader.Posts
                },
            };
            return View("PostThread", model);

        }
        public ActionResult MyReplies()
        {
            if (SessionService.UserId == "" || SessionService.UserRole != "Pharmacist")
            {
                return RedirectToAction("Index", "Home");
            }
            var model = new PharmacistDashboardReplyViewModel()
            {
                PharmacistHeaderViewModel = new PharmacistHeaderViewModel()
                {
                    Header = PharmacistHeader.PostReplies
                },
            };
            return View("PostReply", model);
        }

        public ActionResult MyReport()
        {
            if (SessionService.UserId == "" || SessionService.UserRole != "Pharmacist")
            {
                return RedirectToAction("Index", "Home");
            }
            var id = int.Parse(SessionService.UserId);
            var u = _userRepository.GetById(id);
            var model = new PharmacistDashboardReportViewModel()
            {
                PharmacistHeaderViewModel = new PharmacistHeaderViewModel()
                {
                    Header = PharmacistHeader.Report
                },
                HasPharmacies = u.Pharmacies.Any(p => p.IsApproved && !p.IsRejected)
            };
            return View("Report", model);
        }


        public ActionResult Recommendation(string type,int page = 1)
        {
            try
            {
                if (SessionService.UserId == "" || SessionService.UserRole != "Pharmacist")
                {
                    return RedirectToAction("Index", "Home");
                }
                var m = new PharmacistDashboardRecommendationViewModel()
                {
                    PharmacistHeaderViewModel = new PharmacistHeaderViewModel()
                    {
                        Header =
                            type.ToLower().Contains("recieved")
                                ? PharmacistHeader.RecommendationRecieved
                                : PharmacistHeader.RecommendationSent
                    },
                    Type = type,
                    Parameter = "",
                    Page = page,
                };
                var id = int.Parse(SessionService.UserId);
                if (type.ToLower().Contains("recieved"))
                {
                    m.PageCount = _recommendationRepository.GetPageCountByRecievedUserId(id);
                    m.Recommendations = _recommendationRepository.GetByRecievedUserId(id, m.Page);
                }
                else
                {
                    m.PageCount = _recommendationRepository.GetPageCountByFromUserId(id);
                    m.Recommendations = _recommendationRepository.GetByFromUserId(id, m.Page);

                }
                return View("Recommendation", m);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return RedirectToAction("Dashboard", "Pharmacist");
        }

        public ActionResult Promotion(string type)
        {
            try
            {
                if (SessionService.UserId == "" || SessionService.UserRole != "Pharmacist")
                {
                    return RedirectToAction("Index", "Home");
                }
                var m = new PharmacistDashboardPromotionViewModel()
                {
                    PharmacistHeaderViewModel = new PharmacistHeaderViewModel()
                    {
                        Header =
                            type.ToLower().Contains("inactive")
                                ? PharmacistHeader.PromotionExpired
                                : PharmacistHeader.PromotionActive
                    }
                };
                return View("Promotion", m);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return RedirectToAction("Dashboard", "Pharmacist");
        }
        public ActionResult MyProfile()
        {
            if (SessionService.UserId == "" || SessionService.UserRole != "Pharmacist")
            {
                return RedirectToAction("Index", "Home");
            }
            var model = new PharmacistDashboardProfileViewModel()
            {
                PharmacistHeaderViewModel = new PharmacistHeaderViewModel()
                {
                    Header = PharmacistHeader.Profile
                },
                User = _userRepository.GetById(int.Parse(SessionService.UserId))
            };
            return View("Profile", model);

        }

        public ActionResult TrackStock()
        {
            if (SessionService.UserId == "" || SessionService.UserRole != "Pharmacist")
            {
                return RedirectToAction("Index", "Home");
            }
            var model = new PharmacistDashboardInventoryViewModel()
            {
                PharmacistHeaderViewModel = new PharmacistHeaderViewModel()
                {
                    Header = PharmacistHeader.TrackStock
                },
                Pharmacies = _pharmacyRepository.GetApprovedPharmaciesByUserId(int.Parse(SessionService.UserId))
            };
            return View("TrackStock", model);
        }

        public ActionResult Pharmacy()
        {
            if (SessionService.UserId == "" || SessionService.UserRole != "Pharmacist")
            {
                return RedirectToAction("Index", "Home");
            }
            var model = new PharmacistDashboardPharmacyViewModel()
            {
                PharmacistHeaderViewModel = new PharmacistHeaderViewModel()
                {
                    Header = PharmacistHeader.Pharmacy
                }
            };
            return View("Pharmacy", model);
        }
    }
}