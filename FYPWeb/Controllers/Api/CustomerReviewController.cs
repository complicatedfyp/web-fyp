﻿using System;
using System.Diagnostics;
using System.Web.Http;
using FYPWeb.Enum;
using FYPWeb.Hub;
using FYPWeb.Repository.Repository;
using Model.Entity;

namespace FYPWeb.Controllers.Api
{
    public class CustomerReviewController : ApiController
    {
        private readonly CustomerReviewRepository _customerReviewRepository;
        private readonly PharmacyRepository _pharmacyRepository;
        private readonly NotificationRepository _notificationRepository;
        private readonly OrderRepository _orderRepository;
        public CustomerReviewController(CustomerReviewRepository customerReviewRepository, PharmacyRepository pharmacyRepository, NotificationRepository notificationRepository, OrderRepository orderRepository)
        {
            _customerReviewRepository = customerReviewRepository;
            _pharmacyRepository = pharmacyRepository;
            _notificationRepository = notificationRepository;
            _orderRepository = orderRepository;
        }


        [HttpPost]
        public IHttpActionResult GetCustomerReviewById([FromBody] GetCustomerReviewParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.ReviewId > 0)
                {
                    var c = _customerReviewRepository.GetById(parameter.ReviewId);
                    return Ok(new
                    {
                        message = "Success",
                        result = c.ForJson(),
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetCustomerReviewByUserId([FromBody] GetCustomerReviewParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.UserId > 0)
                {
                    var page = parameter.Page > 0 ? parameter.Page : 1;
                    var list = _customerReviewRepository.GetReviewsByUserId(parameter.UserId,page);
                    var pageCount = _customerReviewRepository.GetReviewPageCountByUser(parameter.UserId);
                    return Ok(new
                    {
                        message = "Success",
                        pageCount,
                        result = list
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult SaveReview([FromBody] SavePharmacyReviewParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.PharmacyId > 0 && parameter.UserId > 0 && parameter.OrderId > 0)
                {
                    if (_customerReviewRepository.IsCheck(p => p.OrderId == parameter.OrderId))
                    {
                        return Ok(new
                        {
                            message = "Error"
                        });
                    }
                    var r = new CustomerReview()
                    {
                        Rating = parameter.Rating,
                        Review = parameter.Review,
                        UserId = parameter.UserId,
                        PostedAt = DateTime.Now,
                        PharmacyId = parameter.PharmacyId,
                        OrderId = parameter.OrderId,
                    };
                    _customerReviewRepository.Add(r);
                    var order = _orderRepository.GetById(parameter.OrderId);
                    var notification = new Notification()
                    {
                        Url = "/Customer/MyOrderReview/",
                        SenderUserId = order.Pharmacy.UserId,
                        Body = " has given you a Review on Order# " + order.Id,
                        CreatedAt = DateTime.Now,
                        IsPharmacy = true,
                        IsPost = false,
                        IsSeen = false,
                        ReceiverUserId = parameter.UserId,
                        NotificationType = (int)NotificationType.Info,
                    };
                    _notificationRepository.Add(notification);
                    NotificationHub.StaticSendNotification(notification, notification.ReceiverUserId, notification.NotificationType);
                    return Ok(new
                    {
                        result = new { Id = r.Id },
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }


        [HttpPost]
        public IHttpActionResult UpdateReview([FromBody] SavePharmacyReviewParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.Id > 0)
                {
                    var r = _customerReviewRepository.GetById(parameter.Id);
                    r.Rating = parameter.Rating;
                    r.Review = parameter.Review;
                    _customerReviewRepository.Update(r);
                    return Ok(new
                    {
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

    }

    public class GetCustomerReviewParameter
    {
        public int ReviewId { get; set; }
        public int Page { get; set; }
        public string Search { get; set; }
        public int UserId { get; set; }
    }
}