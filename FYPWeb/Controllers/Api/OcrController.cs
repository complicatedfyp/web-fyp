﻿
using System.Diagnostics;
using System.Drawing;
using System.Web.Http;
using FYPWeb.Repository.Repository;
using Tesseract;

namespace FYPWeb.Controllers.Api
{
    public class OcrController : ApiController
    {
        private readonly PrescriptionRepository _prescriptionRepository;

        public OcrController(PrescriptionRepository prescriptionRepository)
        {
            _prescriptionRepository = prescriptionRepository;
        }

        [HttpPost]
        public IHttpActionResult GetResults([FromBody] GetResultsParameter parameter)
        {
            if (parameter != null && parameter.PrescriptionId > 0)
            {
                var p = _prescriptionRepository.GetById(parameter.PrescriptionId);
                if (p != null)
                {
                    using (var engine = new TesseractEngine(System.Web.Hosting.HostingEnvironment.MapPath(@"~/tessdata"), "eng", EngineMode.Default))
                    {
                        // have to load Pix via a bitmap since Pix doesn't support loading a stream.
                        using (var image = new Bitmap(p.PrescriptionPath))
                        {
                            using (var pix = PixConverter.ToPix(image))
                            {

                                using (var page = engine.Process(pix))
                                {
                                    Debug.WriteLine("Here : " + page.GetText());
                                }
                            }
                        }
                    }
                }
            }
            return InternalServerError();
        }
    }

    public class GetResultsParameter
    {
        public int PrescriptionId { get; set; }
    }
}