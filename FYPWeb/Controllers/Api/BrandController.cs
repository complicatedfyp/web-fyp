﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Web.Http;
using FYPWeb.Repository.Repository;
using FYPWeb.Services.Util;
using Model.Entity;

namespace FYPWeb.Controllers.Api
{
    public class BrandController : ApiController
    {
        private readonly BrandRepository _brandRepository;

        public BrandController(BrandRepository brandRepository)
        {
            _brandRepository = brandRepository;
        }


        [HttpPost]
        public IHttpActionResult GetBrandById([FromBody] GetBrandParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.BrandId > 0)
                {
                    var brand = _brandRepository.GetById(parameter.BrandId);
                    return Ok(new
                    {
                        result = brand.ForJson(),
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetAllBrands([FromBody] GetBrandParameter parameter)
        {
            try
            {
                var page = parameter?.Page > 0 ? parameter.Page : 1;
                if (string.IsNullOrEmpty(parameter?.Search))
                {
                    var list = _brandRepository.GetBrands(page).Select(p => p.ForJson());
                    var pageCount = _brandRepository.GetPaginationCount();
                    return Ok(new
                    {
                        result = list,
                        pageCount = pageCount,
                        message = "Success",
                    });
                }
                else
                {
                    var list = _brandRepository.GetBrandsSearch(parameter.Search,page).Select(p => p.ForJson());
                    var pageCount = _brandRepository.GetBrandsSearchCount(parameter.Search);
                    return Ok(new
                    {
                        result = list,
                        pageCount = pageCount,
                        message = "Success",
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult SaveBrand([FromBody] SaveBrandParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.CompanyId > 0)
                {
                    var brand = new Brand()
                    {
                        BrandName = parameter.Name,
                        CompanyId = parameter.CompanyId,
                        ImagePath = UtilService.DefaultImagePath
                    };
                    _brandRepository.Add(brand);
                    return Ok(new
                    {
                        result = brand.ForJson(),
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }


        [HttpPost]
        public IHttpActionResult UpdateBrand([FromBody] SaveBrandParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.Id > 0)
                {
                    var brand = _brandRepository.GetById(parameter.Id);
                    brand.BrandName = parameter.Name;
                    brand.CompanyId = parameter.CompanyId > 0 ? parameter.CompanyId : brand.CompanyId;
                    _brandRepository.Update(brand);
                    return Ok(new
                    {
                        result = brand.ForJson(),
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult DeleteBrand([FromBody] SaveBrandParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.Id > 0)
                {
                    var brand = _brandRepository.GetById(parameter.Id);
                    if (brand.Drugs.Any())
                    {
                        return Ok(new
                        {
                            message = "Error"
                        });
                    }
                    _brandRepository.Delete(brand);

                    return Ok(new
                    {
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult SearchBrand([FromBody] SearchParameter paramter)
        {
            try
            {
                if (!string.IsNullOrEmpty(paramter?.Parameter))
                {
                    var d = _brandRepository.GetBrandsSearch(paramter.Parameter);
                    var listOfCompany = d.Select(p => new { Id = p.Id, Name = p.BrandName }).ToList();
                    return Ok(new
                    {
                        result = listOfCompany,
                        message = "Success"
                    });
                }
                return Ok(new
                {
                    result = "",
                    message = "Success"
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

    }

    public class GetBrandParameter
    {
        public int BrandId { get; set; }
        public int Page { get; set; }
        public string Search { get; set; }
    }

    public class SaveBrandParameter
    {
        public int CompanyId { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public int Id { get; set; }
    }
}