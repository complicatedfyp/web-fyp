﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Web.Http;
using FYPWeb.Repository.Repository;
using Model.Entity;

namespace FYPWeb.Controllers.Api
{
    public class MedicineController : ApiController
    {
        private readonly DrugRepository _drugRepository;
        private readonly DrugCartRepository _drugCartRepository;
        private readonly SessionCartRepository _sessionCartRepository;
        private readonly InventoryTrackRepository _inventoryTrackRepository;

        public MedicineController(DrugRepository drugRepository, DrugCartRepository drugCartRepository, SessionCartRepository sessionCartRepository, InventoryTrackRepository inventoryTrackRepository)
        {
            _drugRepository = drugRepository;
            _drugCartRepository = drugCartRepository;
            _sessionCartRepository = sessionCartRepository;
            _inventoryTrackRepository = inventoryTrackRepository;
        }


        [HttpPost]
        public IHttpActionResult GetNearestMedicine([FromBody]GetNearestPromotionOffersParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.lat > 0 && parameter.lng > 0)
                {
                    var list = _drugRepository.GetDrugSoldFromNear(parameter.lat, parameter.lng);
                    return Ok(new
                    {
                        result = list.Select(p => new
                        {
                            p.Id,
                            Name = p.Brand.BrandName + " " + p.DrugName,
                            p.DrugForm,
                            p.Packing,
                            p.DrugDetail.ImageLink
                        }),
                        message = "Success",
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }
        [HttpPost]
        public IHttpActionResult GetMedicineById([FromBody] GetMedicineParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.MedicineId > 0)
                {
                    var medicine = _drugRepository.GetById(parameter.MedicineId);
                    return Ok(new
                    {
                        result = medicine.ForJson(),
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetAllMedicines([FromBody] GetMedicineParameter parameter)
        {
            try
            {
                var page = parameter?.Page > 0 ? parameter.Page : 1;
                if (string.IsNullOrEmpty(parameter?.Search))
                {
                    var list = _drugRepository.GetDrugs(page).ToList().Select(p => p.ForJson());
                    var pageCount = _drugRepository.GetPaginationCount();
                    return Ok(new
                    {
                        result = list,
                        pageCount = pageCount,
                        message = "Success",
                    });
                }
                else
                {
                    var list = _drugRepository.GetDrugsSearch(parameter.Search, page).ToList().Select(p => p.ForJson());
                    var pageCount = _drugRepository.GetDrugsSearchCount(parameter.Search);
                    return Ok(new
                    {
                        result = list,
                        pageCount = pageCount,
                        message = "Success",
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }


        [HttpPost]
        public IHttpActionResult SearchMedicine([FromBody] SearchParameter paramter)
        {
            try
            {
                if (!string.IsNullOrEmpty(paramter?.Parameter))
                {
                    var d = _drugRepository.GetDrugsSearch(paramter.Parameter);
                    var listOfCompany = d.Select(p => new { Id = p.Id, Name = p.Brand + " " + p.DrugName + "-" + p.Packing }).ToList();
                    return Ok(new
                    {
                        result = listOfCompany,
                        message = "Success"
                    });
                }
                return Ok(new
                {
                    result = "",
                    message = "Success"
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult RemoveFromCart([FromBody] AddToCartParameter paramter)
        {
            try
            {
                if (paramter != null && paramter.MedicineId > 0 && paramter.UserId > 0)
                {
                    _drugCartRepository.DeleteByMedicineIdAndUserId(paramter.MedicineId, paramter.UserId);
                    return Ok(new
                    {
                        result = "",
                        message = "Success"
                    });
                }
                else if (paramter != null && paramter.MedicineId > 0 && !string.IsNullOrEmpty(paramter.SessionId))
                {
                    _sessionCartRepository.DeleteByMedicineIdAndSessionId(paramter.MedicineId, paramter.SessionId);
                    return Ok(new
                    {
                        result = "",
                        message = "Success"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }




        [HttpPost]
        public IHttpActionResult AddToCart([FromBody] AddToCartParameter paramter)
        {
            try
            {
                if (paramter != null && paramter.MedicineId > 0 && paramter.UserId > 0 && paramter.Quantity > 0)
                {
                    var inventory = _inventoryTrackRepository.GetById(paramter.MedicineId);
                    var d = _drugCartRepository.GetByMedicineIdAndUserId(paramter.MedicineId, paramter.UserId);
                    var count = 0;
                    if (d != null)
                    {
                        d.Quantity += paramter.Quantity;
                        if (d.Quantity > inventory.TotalCount)
                        {
                            return Ok(new
                            {
                                message = "Error"
                            });
                        }
                        _drugCartRepository.Update(d);
                    }
                    else
                    {
                        var drugCart = new DrugCart()
                        {
                            InventoryId = paramter.MedicineId,
                            Quantity = paramter.Quantity,
                            UserId = paramter.UserId
                        };
                        if (drugCart.Quantity > inventory.TotalCount)
                        {
                            return Ok(new
                            {
                                message = "Error"
                            });
                        }
                        _drugCartRepository.Add(drugCart);
                        count = _drugCartRepository.GetUniqueMedicineCountByUserId(paramter.UserId);
                    }
                    return Ok(new
                    {
                        result = count,
                        message = "Success"
                    });
                }
                else if(paramter != null && paramter.MedicineId > 0 && !string.IsNullOrEmpty(paramter.SessionId))
                {
                    var inventory = _inventoryTrackRepository.GetById(paramter.MedicineId);
                    var d = _sessionCartRepository.GetByMedicineIdAndSessionId(paramter.MedicineId, paramter.SessionId);
                    var count = 0;
                    if (paramter.Quantity == 0) paramter.Quantity = 1;
                    if (d != null)
                    {
                        d.Quantity += paramter.Quantity;
                        if (d.Quantity > inventory.TotalCount)
                        {
                            return Ok(new
                            {
                                message = "Error"
                            });
                        }
                        _sessionCartRepository.Update(d);
                    }
                    else
                    {
                        var drugCart = new SessionCart()
                        {
                            InventoryId = paramter.MedicineId,
                            Quantity = paramter.Quantity,
                            SessionId = paramter.SessionId
                        };
                        if (drugCart.Quantity > inventory.TotalCount)
                        {
                            return Ok(new
                            {
                                message = "Error"
                            });
                        }
                        _sessionCartRepository.Add(drugCart);
                        count = _sessionCartRepository.GetUniqueMedicineCountBySessionId(paramter.SessionId);
                    }
                    return Ok(new
                    {
                        result = count,
                        message = "Success"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }
        [HttpPost]
        public IHttpActionResult GetCartByUserId([FromBody] AddToCartParameter paramter)
        {
            try
            {
                if (paramter != null && paramter.UserId > 0)
                {
                    var list = _drugCartRepository.GetByUserId(paramter.UserId).ToList().Select(p => p.ForJson());
                    return Ok(new
                    {
                        message = "Success",
                        result = list
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult UpdateCart([FromBody] AddToCartParameter paramter)
        {
            try
            {
                if (paramter != null && paramter.MedicineId > 0 && paramter.UserId > 0 && paramter.Quantity > 0)
                {

                    var d = _drugCartRepository.GetByMedicineIdAndUserId(paramter.MedicineId, paramter.UserId);
                    if (d != null)
                    {
                        d.Quantity = paramter.Quantity;
                        _drugCartRepository.Update(d);

                        return Ok(new
                        {
                            result = "",
                            message = "Success"
                        });
                    }

                }
                else if(paramter != null && paramter.MedicineId > 0 && !string.IsNullOrEmpty(paramter.SessionId) && paramter.Quantity > 0)
                {
                    var d = _sessionCartRepository.GetByMedicineIdAndSessionId(paramter.MedicineId, paramter.SessionId);
                    if (d != null)
                    {
                        d.Quantity = paramter.Quantity;
                        _sessionCartRepository.Update(d);
                        return Ok(new
                        {
                            result = "",
                            message = "Success"
                        });
                    }

                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult SaveMedicine([FromBody] SaveMedicineParameter paramter)
        {
            try
            {
                if (paramter != null)
                {
                    var d = new Drug()
                    {
                        BrandId   = paramter.BrandId,
                        DrugDetailId = paramter.DrugDetailId,
                        DrugForm = paramter.Form,
                        DrugName = paramter.Name,
                        Packing = paramter.Packing,
                        ItemsInPack = paramter.ItemsInPack
                    };
                    _drugRepository.Add(d);
                    return Ok(new
                    {
                        result = new { Id = d.Id},
                        message = "Success"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult UpdateMedicine([FromBody] SaveMedicineParameter paramter)
        {
            try
            {
                if (paramter != null && paramter.Id > 0)
                {
                    var d = _drugRepository.GetById(paramter.Id);
                    if (d != null)
                    {
                        d.DrugName = paramter.Name;
                        d.ItemsInPack = paramter.ItemsInPack;
                        d.DrugForm = paramter.Form;
                        d.Packing = paramter.Packing;
                        d.BrandId = paramter.BrandId > 0 ? paramter.BrandId : d.BrandId;
                        d.DrugDetailId = paramter.DrugDetailId > 0 ? paramter.DrugDetailId : d.DrugDetailId;
                        _drugRepository.Update(d);
                        return Ok(new
                        {
                            result = "",
                            message = "Success"
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult DeleteMedicine([FromBody] DeleteMedicineDetailParameter paramter)
        {
            try
            {
                if (paramter != null && paramter.Id > 0)
                {
                    var d = _drugRepository.GetById(paramter.Id);
                    if (d != null)
                    {
                        if (d.OrderDetails.Any() || d.DrugReviews.Any() || d.Recommendations.Any())
                        {
                            return Ok(new
                            {
                                result = "",
                                message = "Error"
                            });
                        }
                        _drugRepository.Delete(d);
                        return Ok(new
                        {
                            result = "",
                            message = "Success"
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult SearchDrugForm([FromBody] SearchParameter paramter)
        {
            try
            {
                if (!string.IsNullOrEmpty(paramter?.Parameter))
                {
                    var d = _drugRepository.GetDrugsFromSearch(paramter.Parameter);
                    var listOfCompany = d.Select(p => new { Id = p.Id, Name = p.Data }).ToList();
                    return Ok(new
                    {
                        result = listOfCompany,
                        message = "Success"
                    });
                }
                return Ok(new
                {
                    result = "",
                    message = "Success"
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }
    }

    public class GetMedicineParameter
    {
        public int MedicineId { get; set; }
        public int Page { get; set; }
        public string Search { get; set; }
    }

    public class AddToCartParameter
    {
        public int MedicineId { get; set; }
        public int UserId { get; set; }
        public int Quantity { get; set; }
        public string SessionId { get; set; }
        public int IsPack { get; set; }
    }

    public class SaveMedicineParameter
    {
        public string Name { get; set; }
        public int ItemsInPack { get; set; }
        public int Id { get; set; }
        public float CostPrice { get; set; }
        public float SalePrice { get; set; }
        public int BrandId { get; set; }
        public int DrugDetailId { get; set; }
        public string Form { get; set; }
        public string Packing { get; set; }
    }
}
