﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Web.Http;
using FYPWeb.Enum;
using FYPWeb.Hub;
using FYPWeb.Repository.Repository;
using FYPWeb.Service;
using FYPWeb.Services.Util;
using Model.Entity;

namespace FYPWeb.Controllers.Api
{
    public class UserController : ApiController
    {
        private readonly UserRepository _userRepository;
        private readonly NotificationRepository _notificationRepository;
        public UserController(UserRepository userRepository, NotificationRepository notificationRepository)
        {
            _userRepository = userRepository;
            _notificationRepository = notificationRepository;
        }

        [System.Web.Http.HttpPost]
        public IHttpActionResult SearchUserDropDown([FromBody] SearchUserParameter parameter)
        {
            try
            {
                if (!string.IsNullOrEmpty(parameter?.Parameter))
                {
                    var d = _userRepository.GetActiveUsersSearch(parameter.Parameter);
                    var listOfCompany = d.Select(p => new { Id = p.Id, Name = p.ToString() }).ToList();
                    return Ok(new
                    {
                        result = listOfCompany,
                        message = "Success"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
            return InternalServerError();
        }
        [System.Web.Http.HttpPost]
        public IHttpActionResult Login([FromBody] LoginParameter parameter)
        {
            try
            {
                if (parameter != null && !string.IsNullOrEmpty(parameter.Email) &&
                    !string.IsNullOrEmpty(parameter.Password) && parameter.Type > 0)
                {
                    var u = _userRepository.GetByEmail(parameter.Email);
                    if (u.Password.ToLower() == UtilService.EncryptPassword(parameter.Password).ToLower() && u.IsVerified && u.UserRoleId == parameter.Type)
                    {
                        return Ok(new
                        {
                            result = u.ForJson(),
                            message = "Success",
                        });
                    }
                    return Ok(new
                    {
                        message = "Error",
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
            return InternalServerError();
        }

        [System.Web.Http.HttpPost]
        public IHttpActionResult GetUserById([FromBody]SetUserParameter parameter)
        {
            if (parameter != null && parameter.UserId > 0)
            {
                var u = _userRepository.GetById(parameter.UserId);
                return Ok(new
                {
                    message = "Success",
                    result = u.ForJson(),
                });
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult RegisterUser(RegisterUserParameter parameter)
        {
            try
            {
                if (!string.IsNullOrEmpty(parameter?.Email) && !string.IsNullOrEmpty(parameter.UserName))
                {
                    if (_userRepository.UserEmailIsAvailable(parameter.Email.ToLower()))
                    {
                        return Ok(new
                        {
                            message = "Error"
                        });
                    }
                    if (_userRepository.UserNameIsAvailable(parameter.UserName.ToLower()))
                    {
                        return Ok(new
                        {
                            message = "Error"
                        });
                    }
                    var user = new User()
                    {
                        Address = parameter.Address,
                        ContactNumber = parameter.PhoneNumber,
                        DateOfBrith = parameter.DOB,
                        UserRoleId = parameter.Type,
                        Email = parameter.Email.ToLower(),
                        FirstName = parameter.FirstName,
                        IsActive = true,
                        LastName = parameter.LastName,
                        Password = UtilService.EncryptPassword(parameter.Password).ToLower(),
                        Gender = parameter.Gender,
                        Signature = "",
                        Username = parameter.UserName.ToLower(),
                        AuthCode = UtilService.RandomString(),
                        RegisteredAt = DateTime.Now,
                        UserImage = parameter.Type == 1 ? UtilService.DefaultUserImagePath : UtilService.DefaultPharmacistImagePath,
                    };
                    _userRepository.Add(user);
                    var notification = new Notification()
                    {
                        Url = "/Admin/Users?type=active",
                        Body = "A New User has been Registered",
                        CreatedAt = DateTime.Now,
                        IsPharmacy = false,
                        IsPost = false,
                        IsSeen = false,
                        ReceiverUserId = SessionService.AdminId,
                        NotificationType = (int)NotificationType.Info,
                    };
                    _notificationRepository.Add(notification);
                    NotificationHub.StaticSendNotification(notification, notification.ReceiverUserId, notification.NotificationType);
                    //EmailService.SendEmail(parameter.Email, "Verification Code for OMDM",
                    //    Url.Action("Verify", "Home",
                    //        new { email = parameter.Email, code = UtilService.EncryptPassword(user.AuthCode) }));
                    return Ok(new
                    {
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }


        [System.Web.Http.HttpPost]
        public IHttpActionResult SetUserDisable([FromBody]SetUserParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.UserId > 0)
                {

                    if (_userRepository.SetStatus(parameter.UserId, false))
                        return Ok(new
                        {
                            message = "Success"
                        });

                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
            return InternalServerError();
        }

        [System.Web.Http.HttpPost]
        public IHttpActionResult SetUserVerify([FromBody]SetUserParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.UserId > 0)
                {

                    if (_userRepository.SetVerifyStatus(parameter.UserId, true))
                        return Ok(new
                        {
                            message = "Success"
                        });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
            return InternalServerError();
        }

        [System.Web.Http.HttpPost]
        public IHttpActionResult GetUser([FromBody]RegisterUserParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.UserId > 0)
                {
                    var d = _userRepository.GetById(parameter.UserId);
                    if (d != null)
                    {
                        return Ok(new
                        {
                            result = d.ForJson(),
                            message = "Success"
                        });
                    }
                    return Ok(new
                    {
                        message = "Error"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
            return InternalServerError();
        }

        [System.Web.Http.HttpPost]
        public IHttpActionResult UpdateUser([FromBody]RegisterUserParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.UserId > 0)
                {
                    var d = _userRepository.GetById(parameter.UserId);
                    if (d != null)
                    {
                        d.Address = parameter.Address;
                        d.ContactNumber = parameter.PhoneNumber;
                        d.FirstName = parameter.FirstName;
                        d.LastName = parameter.LastName;
                        d.DateOfBrith = parameter.DOB;
                        d.Gender = parameter.Gender.ToLower().Contains("female") ? "Female" : "Male";
                        d.Signature = string.IsNullOrEmpty(parameter.Signature) ? "" : parameter.Signature;
                        d.AboutMe = string.IsNullOrEmpty(parameter.AboutMe) ? "" : parameter.AboutMe;
                        _userRepository.Update(d);
                        return Ok(new
                        {
                            message = "Success"
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
            return InternalServerError();
        }

        [System.Web.Http.HttpPost]
        public IHttpActionResult SetUserEnable([FromBody]SetUserParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.UserId > 0)
                {

                    if (_userRepository.SetStatus(parameter.UserId, true))
                        return Ok(new
                        {
                            message = "Success"
                        });

                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
            return InternalServerError();
        }
    }
    public class SearchUserParameter
    {
        public string Parameter { get; set; }
        public int MedicineId { get; set; }
        public int UserId { get; set; }
    }

    public class LoginParameter
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public int Type { get; set; }
    }

    public class SetUserParameter
    {
        public int UserId { get; set; }
    }
}
