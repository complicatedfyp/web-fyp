﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Http;
using FYPWeb.Repository.Repository;
using FYPWeb.Services.Util;
using Model.Entity;

namespace FYPWeb.Controllers.Api
{
    public class CategoryController : ApiController
    {
        private readonly PostCategoryRepository _postCategoryRepository;

        public CategoryController(PostCategoryRepository postCategoryRepository)
        {
            _postCategoryRepository = postCategoryRepository;
        }

        [HttpPost]
        public IHttpActionResult GetCategoryById([FromBody] GetCategoryParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.CategoryId > 0)
                {
                    var c = _postCategoryRepository.GetById(parameter.CategoryId);
                    return Ok(new
                    {
                        message = "Success",
                        result = c.ForJson(),
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetAllCategory([FromBody] GetCategoryParameter parameter)
        {
            try
            {
                if (parameter != null)
                {
                    var page = parameter.Page > 0 ? parameter.Page : 1;
                    if (string.IsNullOrEmpty(parameter.Search))
                    {
                        var list = _postCategoryRepository.GetCategories(page);
                        var pageCount = _postCategoryRepository.GetPaginationCount();
                        return Ok(new
                        {
                            message = "Success",
                            pageCount,
                            result = list
                        });
                    }
                    else
                    {
                        var list = _postCategoryRepository.GetCategoriesSearch(parameter.Search,page);
                        var pageCount = _postCategoryRepository.GetCategoriesSearchCount(parameter.Search);
                        return Ok(new
                        {
                            message = "Success",
                            pageCount,
                            result = list
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }


        [HttpPost]
        public IHttpActionResult SaveCategory([FromBody] SaveCategoryParameter parameter)
        {
            try
            {
                if (!string.IsNullOrEmpty(parameter?.Name))
                {
                    var cat = new PostCategory()
                    {
                        CategoryName = parameter.Name
                    };
                    _postCategoryRepository.Add(cat);
                    return Ok(new
                    {
                        result = cat.ForJson(),
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }


        [HttpPost]
        public IHttpActionResult UpdateCategory([FromBody] SaveCategoryParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.Id > 0)
                {
                    var cat = _postCategoryRepository.GetById(parameter.Id);
                    cat.CategoryName = parameter.Name;
                    _postCategoryRepository.Update(cat);
                    return Ok(new
                    {
                        result = cat.ForJson(),
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult DeleteCategory([FromBody] SaveBrandParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.Id > 0)
                {
                    var cat = _postCategoryRepository.GetById(parameter.Id);
                    if (cat.PostThreads.Any())
                    {
                        return Ok(new
                        {
                            message = "Error"
                        });
                    }
                    _postCategoryRepository.Delete(cat);
                    return Ok(new
                    {
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult SearchCategory([FromBody] SearchParameter paramter)
        {
            try
            {
                if (!string.IsNullOrEmpty(paramter?.Parameter))
                {
                    var d = _postCategoryRepository.GetCategoriesSearch(paramter.Parameter);
                    var listOfCompany = d.Select(p => new { Id = p.Id, Name = p.CategoryName }).ToList();
                    return Ok(new
                    {
                        result = listOfCompany,
                        message = "Success"
                    });
                }
                return Ok(new
                {
                    result = "",
                    message = "Success"
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

    }

    public class GetCategoryParameter
    {
        public int CategoryId { get; set; }
        public string Search { get; set; }
        public int Page { get; set; }
    }

    public class SaveCategoryParameter
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}