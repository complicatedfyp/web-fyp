﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Results;
using FYPWeb.Repository.Repository;
using FYPWeb.Repository.RepositoryViewModel;
using FYPWeb.Service;
using FYPWeb.Services.Util;
using FYPWeb.ViewModels;
using Model.Entity;
using RazorEngine.Compilation.ImpromptuInterface;

namespace FYPWeb.Controllers.Api
{
    public class TemplateController : ApiController
    {
        private readonly OrderRepository _orderRepository;
        private readonly OrderDetailRepository _orderDetailRepository;
        private readonly InventoryTrackRepository _inventoryTrackRepository;
        private readonly SessionCartRepository _sessionCartRepository;
        private readonly PostReplyRepository _postReplyRepository;
        private readonly PostCategoryRepository _postCategoryRepository;
        private readonly DrugCartRepository _drugCartRepository;
        private readonly UserRepository _userRepository;
        private readonly CompanyRepository _companyRepository;
        private readonly BrandRepository _brandRepository;
        private readonly CityRepository _cityRepository;
        private readonly DrugRepository _drugRepository;
        private readonly DrugDetailRepository _drugDetailRepository;
        private readonly PharmacyRepository _pharmacyRepository;
        private readonly DiseaseRepository _diseaseRepository;
        private readonly DrugDetailDiseaseRepository _drugDetailDiseaseRepository;
        private readonly EmployeeRepository _employeeRepository;
        private readonly DrugReviewRepository _drugReviewRepository;
        private readonly PostThreadRepository _postThreadRepository;
        private readonly PromotionRepository _promotionRepository;
        private readonly CustomerReviewRepository _customerReviewRepository;
        private readonly NotificationRepository _notificationRepository;
        private readonly PrescriptionRepository _prescriptionRepository;

        public TemplateController(UserRepository userRepository, CompanyRepository companyRepository,
            BrandRepository brandRepository, CityRepository cityRepository, DrugRepository drugRepository,
            DiseaseRepository diseaseRepository, DrugDetailRepository drugDetailRepository,
            DrugDetailDiseaseRepository drugDetailDiseaseRepository, PharmacyRepository pharmacyRepository,
            EmployeeRepository employeeRepository, DrugReviewRepository drugReviewRepository,
            DrugCartRepository drugCartRepository, PostCategoryRepository postCategoryRepository,
            PostReplyRepository postReplyRepository, PostThreadRepository postThreadRepository,
            PromotionRepository promotionRepository, OrderDetailRepository orderDetailRepository,
            OrderRepository orderRepository, CustomerReviewRepository customerReviewRepository,
            NotificationRepository notificationRepository, SessionCartRepository sessionCartRepository,
            InventoryTrackRepository inventoryTrackRepository, PrescriptionRepository prescriptionRepository)
        {
            _userRepository = userRepository;
            _companyRepository = companyRepository;
            _brandRepository = brandRepository;
            _cityRepository = cityRepository;
            _drugRepository = drugRepository;
            _diseaseRepository = diseaseRepository;
            _drugDetailRepository = drugDetailRepository;
            _drugDetailDiseaseRepository = drugDetailDiseaseRepository;
            _pharmacyRepository = pharmacyRepository;
            _employeeRepository = employeeRepository;
            _drugReviewRepository = drugReviewRepository;
            _drugCartRepository = drugCartRepository;
            _postCategoryRepository = postCategoryRepository;
            _postReplyRepository = postReplyRepository;
            _postThreadRepository = postThreadRepository;
            _promotionRepository = promotionRepository;
            _orderDetailRepository = orderDetailRepository;
            _orderRepository = orderRepository;
            _customerReviewRepository = customerReviewRepository;
            _notificationRepository = notificationRepository;
            _sessionCartRepository = sessionCartRepository;
            _inventoryTrackRepository = inventoryTrackRepository;
            _prescriptionRepository = prescriptionRepository;
        }

        // GET: Template
        [HttpPost]
        public IHttpActionResult GetUsers([FromBody] GetSearhParameter parameter)
        {
            try
            {
                RenderService.Compile(RenderService.AdminDashboardUserTableRow);
                IEnumerable<User> users;
                if (string.IsNullOrEmpty(parameter.Parameter))
                    users = parameter.Type == "active"
                        ? _userRepository.GetActiveUsers(parameter.Page, parameter.Size)
                        : _userRepository.GetInActiveUsers(parameter.Page, parameter.Size);
                else
                {
                    users = parameter.Type == "active"
                        ? _userRepository.GetActiveUsersSearch(parameter.Parameter, parameter.Page, parameter.Size)
                        : _userRepository.GetInActiveUsersSearch(parameter.Parameter, parameter.Page, parameter.Size);
                }
                var list = users.Select(user => RenderService.Run(RenderService.AdminDashboardUserTableRow,
                    new AdminDashboardUserTableRowViewModel()
                    {
                        Email = user.Email,
                        UserName = user.Username,
                        FullName = user.FirstName + " " + user.LastName,
                        Phone = user.ContactNumber,
                        Id = user.Id,
                        IsActive = user.IsActive,
                        IsVerified = user.IsVerified,
                        UserType = user.UserRole.UserRoleName
                    })).ToList();
                return Ok(new
                {
                    result = list,
                    message = "Sucess",
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return new InternalServerErrorResult(this);
        }

        [HttpPost]
        public IHttpActionResult GetDiseaseShowForm([FromBody] GetDiseaseShowFormParameter parameter)
        {
            try
            {
                if (parameter.Id > 0)
                {
                    RenderService.Compile(RenderService.DiseaseShowForm);
                    var list =
                        _drugDetailDiseaseRepository.GetMany(
                                p => p.IsIndication == parameter.IsInidication && p.DrugDetailId == parameter.Id)
                            .Select(p => p.Disease);
                    var d = RenderService.Run(RenderService.DiseaseShowForm, new AdminDiseaseShowFormViewModel()
                    {
                        Diseases = list
                    });
                    return Ok(new
                    {
                        result = d,
                        message = "Success"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetUserForm([FromBody] GetUserFormParameter parameter)
        {
            try
            {
                RenderService.Compile(RenderService.UserEditForm);
                var d = _userRepository.GetById(parameter.UserId);
                if (d != null)
                {
                    var list = RenderService.Run(RenderService.UserEditForm, new UserFormViewModel()
                    {
                        UserId = d.Id,
                        Email = d.Email,
                        FirstName = d.FirstName,
                        LastName = d.LastName,
                        DOB = d.DateOfBrith,
                        Address = d.Address,
                        Gender = d.Gender,
                        PhoneNumber = d.ContactNumber,
                        Type = d.UserRoleId,
                        Signature = d.Signature
                    });
                    return Ok(new
                    {
                        result = list,
                        message = "Success",
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetUsersCount([FromBody] GetSearhParameter parameter)
        {
            try
            {
                int users;
                if (string.IsNullOrEmpty(parameter.Parameter))
                    users = parameter.Type == "active"
                        ? _userRepository.GetActivePaginationCount(parameter.Size)
                        : _userRepository.GetInActivePaginationCount(parameter.Size);
                else
                {
                    users = parameter.Type == "active"
                        ? _userRepository.GetActiveUsersSearchCount(parameter.Parameter, parameter.Size)
                        : _userRepository.GetInActiveUsersSearchCount(parameter.Parameter, parameter.Size);
                }
                return Ok(new
                {
                    message = "Sucess",
                    count = users
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();

        }



        [HttpPost]
        public IHttpActionResult GetCompanies([FromBody] GetSearhParameter parameter)
        {
            try
            {
                RenderService.Compile(RenderService.AdminDashboardCompanyTableRow);
                IEnumerable<Company> companies;
                if (string.IsNullOrEmpty(parameter.Parameter))
                    companies = _companyRepository.GetCompanies(parameter.Page, parameter.Size);
                else
                {
                    companies = _companyRepository.GetCompaniesSearch(parameter.Parameter, parameter.Page,
                        parameter.Size);
                }
                var list = companies.Select(user => RenderService.Run(RenderService.AdminDashboardCompanyTableRow,
                    new AdminDashboardCompanyTableRowViewModel()
                    {
                        Name = user.CompanyName,
                        Phone = user.Phone,
                        Id = user.Id,
                        Fax = user.Fax,
                        Address = user.Address,
                        BrandCount = _brandRepository.GetBrandCountByCompany(user.Id),
                        Link = user.WebLink,
                        City = user.City.CityName,
                        ImageLink = user.ImagePath,
                    })).ToList();
                return Ok(new
                {
                    result = list,
                    message = "Sucess",
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();

        }


        [HttpPost]
        public IHttpActionResult GetCompanySingleRow([FromBody] GetSingleRowParameter parameter)
        {
            try
            {
                RenderService.Compile(RenderService.AdminDashboardCompanyTableRow);
                var user = _companyRepository.GetById(parameter.Id);
                var d = RenderService.Run(RenderService.AdminDashboardCompanyTableRow,
                    new AdminDashboardCompanyTableRowViewModel()
                    {
                        Name = user.CompanyName,
                        Phone = user.Phone,
                        Id = user.Id,
                        Fax = user.Fax,
                        Address = user.Address,
                        BrandCount = _brandRepository.GetBrandCountByCompany(user.Id),
                        Link = user.WebLink,
                        City = user.City.CityName,
                        ImageLink = user.ImagePath,
                    });
                return Ok(new
                {
                    result = d,
                    message = "Sucess",
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();


        }

        [HttpPost]
        public IHttpActionResult GetCompanyForm([FromBody] GetAddCompanyParameter parameter)
        {
            try
            {
                string data;
                if (parameter != null && parameter.CompanyId > 0)
                {
                    var c = _companyRepository.GetById(parameter.CompanyId);
                    RenderService.Compile(RenderService.CompanyAddForm);
                    data = RenderService.Run(RenderService.CompanyAddForm, new AdminCompanyAddFormViewModel()
                    {
                        Address = c.Address,
                        Fax = c.Fax,
                        Phone = c.Phone,
                        Id = c.Id,
                        Name = c.CompanyName,
                        Link = c.WebLink,
                        CityId = c.CityId,
                        Cities = _cityRepository.GetAll()
                    });
                }
                else
                {
                    RenderService.Compile(RenderService.CompanyAddForm);
                    data = RenderService.Run(RenderService.CompanyAddForm, new AdminCompanyAddFormViewModel()
                    {
                        Cities = _cityRepository.GetAll()
                    });
                }
                return Ok(new
                {
                    result = data,
                    message = "Success",
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetCompaniesCount([FromBody] GetSearhParameter parameter)
        {
            try
            {
                var users = string.IsNullOrEmpty(parameter.Parameter)
                    ? _companyRepository.GetPaginationCount(parameter.Size)
                    : _companyRepository.GetCompaniesSearchCount(parameter.Parameter, parameter.Size);
                return Ok(new
                {
                    message = "Sucess",
                    count = users
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetUploadForm([FromBody] GetUploadFormParameter parameter)
        {
            try
            {
                if (!string.IsNullOrEmpty(parameter?.ActionUrl))
                {
                    RenderService.Compile(RenderService.FileUploadForm);
                    var d = RenderService.Run(RenderService.FileUploadForm, new FileUploadViewModel()
                    {
                        Id = parameter.Id,
                        ActionUrl = parameter.ActionUrl,
                        Limit = parameter.Limit
                    });
                    return Ok(new
                    {
                        result = d,
                        message = "Success",
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return new InternalServerErrorResult(this);
        }


        [HttpPost]
        public IHttpActionResult GetBrands([FromBody] GetSearhParameter parameter)
        {
            try
            {
                RenderService.Compile(RenderService.AdminDashboardBrandTableRow);
                IEnumerable<Brand> brands;
                if (string.IsNullOrEmpty(parameter.Parameter))
                    brands = _brandRepository.GetBrands(parameter.Page, parameter.Size);
                else
                {
                    brands = _brandRepository.GetBrandsSearch(parameter.Parameter, parameter.Page,
                        parameter.Size);
                }
                var list = brands.Select(user => RenderService.Run(RenderService.AdminDashboardBrandTableRow,
                    new AdminDashboardBrandTableRowViewModel()
                    {
                        Name = user.BrandName,
                        Id = user.Id,
                        CompanyName = user.Company.CompanyName,
                        DrugCount = _drugRepository.GetCountByBrand(user.Id),
                        ImageLink = user.ImagePath,
                    })).ToList();
                return Ok(new
                {
                    result = list,
                    message = "Sucess",
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return new InternalServerErrorResult(this);

        }

        [HttpPost]
        public IHttpActionResult GetBrandSingleRow([FromBody] GetSingleRowParameter parameter)
        {
            try
            {
                RenderService.Compile(RenderService.AdminDashboardBrandTableRow);
                var list = _brandRepository.GetById(parameter.Id);
                var d = RenderService.Run(RenderService.AdminDashboardBrandTableRow,
                    new AdminDashboardBrandTableRowViewModel()
                    {
                        CompanyName = list.Company.CompanyName,
                        DrugCount = _drugRepository.GetCountByBrand(list.Id),
                        ImageLink = list.ImagePath,
                        Name = list.BrandName,
                        Id = list.Id
                    });
                return Ok(new
                {
                    result = d,
                    message = "Success",
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();


        }

        [HttpPost]
        public IHttpActionResult GetBrandForm([FromBody] GetAddBrandParameter parameter)
        {
            try
            {
                string data;
                if (parameter != null && parameter.BrandId > 0)
                {
                    var c = _brandRepository.GetById(parameter.BrandId);
                    RenderService.Compile(RenderService.BrandAddForm);
                    data = RenderService.Run(RenderService.BrandAddForm, new AdminBrandAddFormViewModel()
                    {
                        Id = c.Id,
                        Name = c.BrandName,
                    });
                }
                else
                {
                    RenderService.Compile(RenderService.BrandAddForm);
                    data = RenderService.Run(RenderService.BrandAddForm, new AdminBrandAddFormViewModel());
                }
                return Ok(new
                {
                    result = data,
                    message = "Success",
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetBrandsCount([FromBody] GetSearhParameter parameter)
        {
            try
            {
                var users = string.IsNullOrEmpty(parameter.Parameter)
                    ? _brandRepository.GetPaginationCount(parameter.Size)
                    : _brandRepository.GetBrandsSearchCount(parameter.Parameter, parameter.Size);
                return Ok(new
                {
                    message = "Sucess",
                    count = users
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }


        [HttpPost]
        public IHttpActionResult GetDiseases([FromBody] GetSearhParameter parameter)
        {
            try
            {
                RenderService.Compile(RenderService.AdminDashboardDiseaseTableRow);
                IEnumerable<Disease> dieases;
                if (string.IsNullOrEmpty(parameter.Parameter))
                    dieases = _diseaseRepository.GetDiseases(parameter.Page, parameter.Size);
                else
                {
                    dieases = _diseaseRepository.GetDiseasesSearch(parameter.Parameter, parameter.Page,
                        parameter.Size);
                }
                var list = dieases.Select(user => RenderService.Run(RenderService.AdminDashboardDiseaseTableRow,
                    new AdminDashboardDiseaseTableRowViewModel()
                    {
                        Name = user.DiseaseName,
                        Id = user.Id,
                        Description = user.Description
                    })).ToList();
                return Ok(new
                {
                    result = list,
                    message = "Sucess",
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return new InternalServerErrorResult(this);

        }

        [HttpPost]
        public IHttpActionResult GetDiseaseSingleRow([FromBody] GetSingleRowParameter parameter)
        {
            try
            {
                RenderService.Compile(RenderService.AdminDashboardDiseaseTableRow);
                var list = _diseaseRepository.GetById(parameter.Id);
                var d = RenderService.Run(RenderService.AdminDashboardDiseaseTableRow,
                    new AdminDashboardDiseaseTableRowViewModel()
                    {
                        Name = list.DiseaseName,
                        Id = list.Id,
                        Description = list.Description
                    });
                return Ok(new
                {
                    result = d,
                    message = "Success",
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();


        }

        [HttpPost]
        public IHttpActionResult GetDiseaseForm([FromBody] GetAddDiseaseParameter parameter)
        {
            try
            {
                string data;
                if (parameter != null && parameter.DiseaseId > 0)
                {
                    var c = _diseaseRepository.GetById(parameter.DiseaseId);
                    RenderService.Compile(RenderService.DiseaseAddForm);
                    data = RenderService.Run(RenderService.DiseaseAddForm, new AdminDiseaseAddFormViewModel()
                    {
                        Id = c.Id,
                        Name = c.DiseaseName,
                        Description = c.Description
                    });
                }
                else
                {
                    RenderService.Compile(RenderService.DiseaseAddForm);
                    data = RenderService.Run(RenderService.DiseaseAddForm, new AdminDiseaseAddFormViewModel());
                }
                return Ok(new
                {
                    result = data,
                    message = "Success",
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetDiseasesCount([FromBody] GetSearhParameter parameter)
        {
            try
            {
                var users = string.IsNullOrEmpty(parameter.Parameter)
                    ? _diseaseRepository.GetPaginationCount(parameter.Size)
                    : _diseaseRepository.GetDiseasesSearchCount(parameter.Parameter, parameter.Size);
                return Ok(new
                {
                    message = "Sucess",
                    count = users
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }


        [HttpPost]
        public IHttpActionResult GetMedicines([FromBody] GetSearhParameter parameter)
        {
            try
            {
                RenderService.Compile(RenderService.AdminDashboardMedicineTableRow);
                IEnumerable<Drug> drugs;
                if (string.IsNullOrEmpty(parameter.Parameter))
                    drugs = _drugRepository.GetDrugs(parameter.Page, parameter.Size);
                else
                {
                    drugs = _drugRepository.GetDrugsSearch(parameter.Parameter, parameter.Page,
                        parameter.Size);
                }
                var list = drugs.Select(user => RenderService.Run(RenderService.AdminDashboardMedicineTableRow,
                    new AdminDashboardMedicineTableRowViewModel()
                    {
                        Name = user.DrugName,
                        Id = user.Id,
                        Brand = user.Brand.BrandName,
                        BrandId = user.BrandId,
                        ImageLink = user.DrugDetail.ImageLink,
                        Form = user.DrugForm,
                        Packing = user.Packing,
                    })).ToList();
                return Ok(new
                {
                    result = list,
                    message = "Sucess",
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return new InternalServerErrorResult(this);

        }

        [HttpPost]
        public IHttpActionResult GetMedicineSingleRow([FromBody] GetSingleRowParameter parameter)
        {
            try
            {
                RenderService.Compile(RenderService.AdminDashboardMedicineTableRow);
                var user = _drugRepository.GetById(parameter.Id);
                var d = RenderService.Run(RenderService.AdminDashboardMedicineTableRow,
                    new AdminDashboardMedicineTableRowViewModel()
                    {
                        Name = user.DrugName,
                        Id = user.Id,
                        Brand = user.Brand.BrandName,
                        ImageLink = user.DrugDetail.ImageLink,
                        Form = user.DrugForm,
                        Packing = user.Packing,
                        BrandId = user.BrandId
                    });
                return Ok(new
                {
                    result = d,
                    message = "Success",
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();


        }

        [HttpPost]
        public IHttpActionResult GetMedicineForm([FromBody] GetAddMedicineParameter parameter)
        {
            try
            {
                string data;
                if (parameter != null && parameter.MedicineId > 0)
                {
                    var c = _drugRepository.GetById(parameter.MedicineId);
                    RenderService.Compile(RenderService.MedicineAddForm);
                    data = RenderService.Run(RenderService.MedicineAddForm, new AdminMedicineAddFormViewModel()
                    {
                        Id = c.Id,
                        Name = c.DrugName,
                        Form = c.DrugForm,
                        Packing = c.Packing,
                        ItemsInPack = c.ItemsInPack
                    });
                }
                else
                {
                    RenderService.Compile(RenderService.MedicineAddForm);
                    data = RenderService.Run(RenderService.MedicineAddForm, new AdminMedicineAddFormViewModel());
                }
                return Ok(new
                {
                    result = data,
                    message = "Success",
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetMedicinesCount([FromBody] GetSearhParameter parameter)
        {
            try
            {
                var users = string.IsNullOrEmpty(parameter.Parameter)
                    ? _drugRepository.GetPaginationCount(parameter.Size)
                    : _drugRepository.GetDrugsSearchCount(parameter.Parameter, parameter.Size);
                return Ok(new
                {
                    message = "Sucess",
                    count = users
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetMedicineDetails([FromBody] GetSearhParameter parameter)
        {
            try
            {
                RenderService.Compile(RenderService.AdminDashboardMedicineDetailTableRow);
                IEnumerable<DrugDetail> drugs;
                if (string.IsNullOrEmpty(parameter.Parameter))
                    drugs = _drugDetailRepository.GetDrugDetails(parameter.Page, parameter.Size);
                else
                {
                    drugs = _drugDetailRepository.GetDrugDetailsSearch(parameter.Parameter, parameter.Page,
                        parameter.Size);
                }
                var list = drugs.Select(user => RenderService.Run(RenderService.AdminDashboardMedicineDetailTableRow,
                    new AdminDashboardMedicineDetailTableRowViewModel()
                    {
                        Name = user.Name,
                        Id = user.Id,
                        ImageLink = user.ImageLink,
                        Overview = user.OverView,
                        CureDiseases = user.DrugDetailDiseases.Where(p => p.IsIndication).Select(p => p.Disease),
                        NonCureDiseases = user.DrugDetailDiseases.Where(p => !p.IsIndication).Select(p => p.Disease),
                    })).ToList();
                return Ok(new
                {
                    result = list,
                    message = "Sucess",
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return new InternalServerErrorResult(this);

        }

        [HttpPost]
        public IHttpActionResult GetMedicineDetailSingleRow([FromBody] GetSingleRowParameter parameter)
        {
            try
            {
                RenderService.Compile(RenderService.AdminDashboardMedicineDetailTableRow);
                var user = _drugDetailRepository.GetById(parameter.Id);
                var d = RenderService.Run(RenderService.AdminDashboardMedicineDetailTableRow,
                    new AdminDashboardMedicineDetailTableRowViewModel()
                    {
                        Name = user.Name,
                        Id = user.Id,
                        ImageLink = user.ImageLink,
                        Overview = user.OverView,
                        CureDiseases = user.DrugDetailDiseases.Where(p => p.IsIndication).Select(p => p.Disease),
                        NonCureDiseases = user.DrugDetailDiseases.Where(p => !p.IsIndication).Select(p => p.Disease),
                    });
                return Ok(new
                {
                    result = d,
                    message = "Success",
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();


        }

        [HttpPost]
        public IHttpActionResult GetMedicineDetailForm([FromBody] GetAddMedicineDetailParameter parameter)
        {
            try
            {
                string data;
                if (parameter != null && parameter.MedicineDetailId > 0)
                {
                    var c = _drugDetailRepository.GetById(parameter.MedicineDetailId);
                    RenderService.Compile(RenderService.MedicineDetailAddForm);
                    var diseaseInt = c.DrugDetailDiseases.Where(p => p.IsIndication).Select(p => p.Disease);
                    var diseaseCont = c.DrugDetailDiseases.Where(p => !p.IsIndication).Select(p => p.Disease);
                    var diseaseIntstring = diseaseInt
                        .Aggregate("",
                            (current, disease) => current + ("{\"id\": \"" + disease.Id) + "\",\"text\": \"" +
                                                  disease.DiseaseName + "\"},").TrimEnd(',');
                    var diseaseContstring = diseaseCont
                        .Aggregate("",
                            (current, disease) => current + ("{\"id\": \"" + disease.Id) + "\",\"text\": \"" +
                                                  disease.DiseaseName + "\"},").TrimEnd(',');
                    data = RenderService.Run(RenderService.MedicineDetailAddForm,
                        new AdminMedicineDetailAddFormViewModel
                        {
                            Id = c.Id,
                            Name = c.Name,
                            Overview = c.OverView,
                            DrugDiseasesContradiction = diseaseContstring,
                            DrugDiseasesIndication = diseaseIntstring
                        });
                }
                else
                {
                    RenderService.Compile(RenderService.MedicineDetailAddForm);
                    data = RenderService.Run(RenderService.MedicineDetailAddForm,
                        new AdminMedicineDetailAddFormViewModel()
                        {
                            DrugDiseasesIndication = string.Empty,
                            DrugDiseasesContradiction = string.Empty
                        });
                }
                return Ok(new
                {
                    result = data,
                    message = "Success",
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetMedicineDetailsCount([FromBody] GetSearhParameter parameter)
        {
            try
            {
                var users = string.IsNullOrEmpty(parameter.Parameter)
                    ? _drugDetailRepository.GetPaginationCount(parameter.Size)
                    : _drugDetailRepository.GetDrugDetailsSearchCount(parameter.Parameter, parameter.Size);
                return Ok(new
                {
                    message = "Sucess",
                    count = users
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetPharmacies([FromBody] GetSearhParameter parameter)
        {
            try
            {

                RenderService.Compile(RenderService.PharmacistDashboardPharmacyTableRow);
                if (parameter.IsAdmin)
                {
                    IEnumerable<Pharmacy> pharmacies;
                    if (string.IsNullOrEmpty(parameter.Parameter))
                        pharmacies = _pharmacyRepository.GetPharmacies(parameter.Page, parameter.Size, parameter.Type);
                    else
                    {
                        pharmacies = _pharmacyRepository.GetPharmaciesSearch(parameter.Parameter, parameter.Page,
                            parameter.Size);
                    }
                    var list =
                        pharmacies.Select(
                            user =>
                                RenderService.Run(RenderService.PharmacistDashboardPharmacyTableRow,
                                    new PharmacistDashboardPharmacyTableRowViewModel()
                                    {
                                        Name = user.ShopName,
                                        Id = user.Id,
                                        Address = user.ShopAddress,
                                        ImageLink = user.ImagePath,
                                        ContactPerson = user.ContactPersonName,
                                        UserId = user.UserId.GetValueOrDefault(0),
                                        Number = user.ContactNumber,
                                        IsApproved = user.IsApproved,
                                        IsAdmin = true,
                                        Username = user.User.Username,
                                    })).ToList();
                    return Ok(new
                    {
                        result = list,
                        message = "Sucess",
                    });
                }
                else if (parameter.UserId > 0)
                {
                    IEnumerable<Pharmacy> pharmacies;
                    if (string.IsNullOrEmpty(parameter.Parameter))
                        pharmacies =
                            _pharmacyRepository.GetPharmacies(parameter.UserId, parameter.Page, parameter.Size);
                    else
                    {
                        pharmacies = _pharmacyRepository.GetPharmaciesSearch(parameter.Parameter, parameter.UserId,
                            parameter.Page,
                            parameter.Size);
                    }
                    var list =
                        pharmacies.Select(
                            user =>
                                RenderService.Run(RenderService.PharmacistDashboardPharmacyTableRow,
                                    new PharmacistDashboardPharmacyTableRowViewModel()
                                    {
                                        Name = user.ShopName,
                                        IsApproved = user.IsApproved,
                                        Id = user.Id,
                                        Address = user.ShopAddress,
                                        ImageLink = user.ImagePath,
                                        ContactPerson = user.ContactPersonName,
                                        UserId = user.UserId.GetValueOrDefault(0),
                                        Username = user.User.Username,
                                        Number = user.ContactNumber
                                    })).ToList();
                    return Ok(new
                    {
                        result = list,
                        message = "Sucess",
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();

        }

        [HttpPost]
        public IHttpActionResult GetPharmacySingleRow([FromBody] GetSingleRowParameter parameter)
        {
            try
            {
                RenderService.Compile(RenderService.PharmacistDashboardPharmacyTableRow);
                var user = _pharmacyRepository.GetById(parameter.Id);
                var d = RenderService.Run(RenderService.PharmacistDashboardPharmacyTableRow,
                    new PharmacistDashboardPharmacyTableRowViewModel()
                    {
                        Name = user.ShopName,
                        Id = user.Id,
                        Address = user.ShopAddress,
                        ImageLink = user.ImagePath,
                        ContactPerson = user.ContactPersonName,
                        UserId = user.UserId.GetValueOrDefault(0),
                        IsApproved = user.IsApproved,
                        Username = user.User.Username,
                        Number = user.ContactNumber
                    });
                return Ok(new
                {
                    result = d,
                    message = "Success",
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();


        }

        [HttpPost]
        public IHttpActionResult GetPharmacyForm([FromBody] GetAddPharmacyParameter parameter)
        {
            try
            {
                var data = "";
                if (parameter != null && parameter.PharmacyId > 0)
                {
                    var c = _pharmacyRepository.GetByIdAndUser(parameter.PharmacyId, parameter.UserId);
                    RenderService.Compile(RenderService.PharmacyAddForm);
                    data = RenderService.Run(RenderService.PharmacyAddForm, new PharmacistPharmacyAddFormViewModel()
                    {
                        Id = c.Id,
                        Address = c.ShopAddress,
                        Name = c.ShopName,
                        PhoneNumber = c.ContactNumber,
                        X = c.ShopLocationX + "",
                        Y = c.ShopLocationY + "",
                        ContactPerson = c.ContactPersonName,
                        UserId = c.UserId.GetValueOrDefault(0),
                        Charges = c.DeliveryCharges
                    });
                }
                else if (parameter != null && parameter.UserId > 0)
                {
                    RenderService.Compile(RenderService.PharmacyAddForm);
                    data = RenderService.Run(RenderService.PharmacyAddForm, new PharmacistPharmacyAddFormViewModel()
                    {
                        UserId = parameter.UserId
                    });
                }
                return Ok(new
                {
                    result = data,
                    message = "Success",
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetPharmaciesCount([FromBody] GetSearhParameter parameter)
        {
            try
            {
                if (parameter.IsAdmin)
                {
                    var users = string.IsNullOrEmpty(parameter.Parameter)
                        ? _pharmacyRepository.GetPaginationCount(parameter.Size)
                        : _pharmacyRepository.GetPharmaciesSearchCount(parameter.Parameter, parameter.Size);
                    return Ok(new
                    {
                        message = "Sucess",
                        count = users
                    });
                }
                else if (parameter.UserId > 0)
                {
                    var users = string.IsNullOrEmpty(parameter.Parameter)
                        ? _pharmacyRepository.GetPaginationCount(parameter.UserId, parameter.Size)
                        : _pharmacyRepository.GetPharmaciesSearchCount(parameter.Parameter, parameter.UserId,
                            parameter.Size);
                    return Ok(new
                    {
                        message = "Sucess",
                        count = users
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }


        [HttpPost]
        public IHttpActionResult GetEmployees([FromBody] GetSearhParameter parameter)
        {
            try
            {

                RenderService.Compile(RenderService.PharmacistDashboardEmployeeTableRow);
                if (parameter.IsAdmin)
                {
                    IEnumerable<Employee> employees;
                    if (string.IsNullOrEmpty(parameter.Parameter))
                        employees = _employeeRepository.GetEmployees(parameter.Page, parameter.Size);
                    else
                    {
                        employees = _employeeRepository.GetEmployeesSearch(parameter.Parameter, parameter.Page,
                            parameter.Size);
                    }
                    var list =
                        employees.Select(
                            user =>
                                RenderService.Run(RenderService.PharmacistDashboardEmployeeTableRow,
                                    new PharmacistDashboardEmployeeTableRowViewModel()
                                    {
                                        Name = user.Name,
                                        Id = user.Id,
                                        Address = user.Address,
                                        ImageLink = user.ImageLink,
                                        UserId = user.UserId,
                                        Number = user.ContactNumber,
                                        Pharmacy = user.Pharmacy?.ShopName + "",
                                        IsAdmin = true
                                    })).ToList();
                    return Ok(new
                    {
                        result = list,
                        message = "Sucess",
                    });
                }
                else if (parameter.UserId > 0)
                {
                    IEnumerable<Employee> pharmacies;
                    if (string.IsNullOrEmpty(parameter.Parameter))
                        pharmacies = _employeeRepository.GetEmployees(parameter.UserId, parameter.Page, parameter.Size);
                    else
                    {
                        pharmacies = _employeeRepository.GetEmployeesSearch(parameter.Parameter, parameter.UserId,
                            parameter.Page,
                            parameter.Size);
                    }
                    var list =
                        pharmacies.Select(
                            user =>
                                RenderService.Run(RenderService.PharmacistDashboardEmployeeTableRow,
                                    new PharmacistDashboardEmployeeTableRowViewModel()
                                    {
                                        Name = user.Name,
                                        Id = user.Id,
                                        Address = user.Address,
                                        ImageLink = user.ImageLink,
                                        UserId = user.UserId,
                                        Number = user.ContactNumber,
                                        Pharmacy = user.Pharmacy?.ShopName + "",
                                        IsAdmin = true
                                    })).ToList();
                    return Ok(new
                    {
                        result = list,
                        message = "Sucess",
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();

        }

        [HttpPost]
        public IHttpActionResult GetEmployeeSingleRow([FromBody] GetSingleRowParameter parameter)
        {
            try
            {
                RenderService.Compile(RenderService.PharmacistDashboardEmployeeTableRow);
                var user = _employeeRepository.GetById(parameter.Id);
                var d = RenderService.Run(RenderService.PharmacistDashboardEmployeeTableRow,
                    new PharmacistDashboardEmployeeTableRowViewModel()
                    {
                        Name = user.Name,
                        Id = user.Id,
                        Address = user.Address,
                        ImageLink = user.ImageLink,
                        UserId = user.UserId,
                        Number = user.ContactNumber,
                        Pharmacy = user.Pharmacy?.ShopName,
                        IsAdmin = false
                    });
                return Ok(new
                {
                    result = d,
                    message = "Success",
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();


        }

        [HttpPost]
        public IHttpActionResult GetEmployeeForm([FromBody] GetAddEmployeeParameter parameter)
        {
            try
            {
                var data = "";
                if (parameter != null && parameter.EmployeeId > 0)
                {
                    var c = _employeeRepository.GetByIdAndUser(parameter.EmployeeId, parameter.UserId);
                    RenderService.Compile(RenderService.EmployeeAddForm);
                    data = RenderService.Run(RenderService.EmployeeAddForm, new PharmacistEmployeeAddFormViewModel()
                    {
                        Id = c.Id,
                        Address = c.Address,
                        Name = c.Name,
                        UserId = c.UserId,
                        ContactNumber = c.ContactNumber,
                        Salary = c.Salary,
                        DutyHours = c.DutyHours
                    });
                }
                else if (parameter != null && parameter.UserId > 0)
                {
                    RenderService.Compile(RenderService.EmployeeAddForm);
                    data = RenderService.Run(RenderService.EmployeeAddForm, new PharmacistEmployeeAddFormViewModel()
                    {
                        UserId = parameter.UserId
                    });
                }
                return Ok(new
                {
                    result = data,
                    message = "Success",
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetEmployeeAssignForm([FromBody] GetAddEmployeeParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.EmployeeId > 0 && parameter.UserId > 0)
                {
                    var c = _employeeRepository.GetByIdAndUser(parameter.EmployeeId, parameter.UserId);
                    RenderService.Compile(RenderService.EmployeeAssignForm);
                    var data = RenderService.Run(RenderService.EmployeeAssignForm,
                        new PharmacistEmployeeAssignFormViewModel()
                        {
                            UserId = c.UserId,
                            PharamcyId = c.PharmacyId.GetValueOrDefault(0),
                            Pharmacies = _pharmacyRepository.GetApprovedPharmaciesByUserId(parameter.UserId),
                            EmployeeId = c.Id
                        });
                    return Ok(new
                    {
                        result = data,
                        message = "Success",
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetEmployeesCount([FromBody] GetSearhParameter parameter)
        {
            try
            {
                if (parameter.IsAdmin)
                {
                    var users = string.IsNullOrEmpty(parameter.Parameter)
                        ? _employeeRepository.GetPaginationCount(parameter.Size)
                        : _employeeRepository.GetEmployeesSearchCount(parameter.Parameter, parameter.Size);
                    return Ok(new
                    {
                        message = "Sucess",
                        count = users
                    });
                }
                else if (parameter.UserId > 0)
                {
                    var users = string.IsNullOrEmpty(parameter.Parameter)
                        ? _employeeRepository.GetPaginationCount(parameter.UserId, parameter.Size)
                        : _employeeRepository.GetEmployeesSearchCount(parameter.Parameter, parameter.UserId,
                            parameter.Size);
                    return Ok(new
                    {
                        message = "Sucess",
                        count = users
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetReviewForm([FromBody] GetReviewParameter parameter)
        {
            try
            {
                string data;
                if (parameter != null && parameter.Id > 0)
                {
                    var c = _drugReviewRepository.GetById(parameter.Id);
                    RenderService.Compile(RenderService.ReviewForm);
                    data = RenderService.Run(RenderService.ReviewForm, new HomeMedicineReviewFormViewModel()
                    {
                        UserId = c.UserId,
                        Id = c.Id,
                        MedicineId = c.DrugId,
                        Review = c.Review,
                        Rating = c.Rating + "",
                    });
                    return Ok(new
                    {
                        result = data,
                        message = "Success",
                    });
                }
                if (parameter != null && parameter.MedicineId > 0 && parameter.UserId > 0)
                {
                    RenderService.Compile(RenderService.ReviewForm);
                    data = RenderService.Run(RenderService.ReviewForm, new HomeMedicineReviewFormViewModel()
                    {
                        UserId = parameter.UserId,
                        MedicineId = parameter.MedicineId,
                    });
                    return Ok(new
                    {
                        result = data,
                        message = "Success",
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetCartForm([FromBody] GetCartParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.MedicineId > 0 && parameter.UserId > 0)
                {
                    var c = _drugCartRepository.GetByMedicineIdAndUserId(parameter.MedicineId, parameter.UserId);
                    RenderService.Compile(RenderService.CartForm);
                    var data = RenderService.Run(RenderService.CartForm, new HomeMedicineCartFormViewModel()
                    {
                        UserId = c.UserId,
                        Id = c.Id,
                        InventoryId = c.InventoryId,
                        Quantity = c.Quantity
                    });
                    return Ok(new
                    {
                        result = data,
                        message = "Success",
                    });
                }
                else if (parameter != null && parameter.MedicineId > 0 && !string.IsNullOrEmpty(parameter.SessionId))
                {
                    var c = _sessionCartRepository.GetByMedicineIdAndSessionId(parameter.MedicineId,
                        parameter.SessionId);
                    RenderService.Compile(RenderService.CartForm);
                    var data = RenderService.Run(RenderService.CartForm, new HomeMedicineCartFormViewModel()
                    {
                        UserId = 0,
                        Id = c.Id,
                        SessionId = parameter.SessionId,
                        InventoryId = c.InventoryId,
                        Quantity = c.Quantity
                    });
                    return Ok(new
                    {
                        result = data,
                        message = "Success",
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetCategories([FromBody] GetSearhParameter parameter)
        {
            try
            {
                RenderService.Compile(RenderService.AdminDashboardCategoryTableRow);
                IEnumerable<PostCategory> categories;
                if (string.IsNullOrEmpty(parameter.Parameter))
                    categories = _postCategoryRepository.GetCategories(parameter.Page, parameter.Size);
                else
                {
                    categories = _postCategoryRepository.GetCategoriesSearch(parameter.Parameter, parameter.Page,
                        parameter.Size);
                }
                var list = categories.Select(user => RenderService.Run(RenderService.AdminDashboardCategoryTableRow,
                    new AdminDashboardCategoryTableRowViewModel()
                    {
                        Name = user.CategoryName,
                        Id = user.Id,
                        PostCount = user.PostThreads.Count
                    })).ToList();
                return Ok(new
                {
                    result = list,
                    message = "Sucess",
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return new InternalServerErrorResult(this);

        }

        [HttpPost]
        public IHttpActionResult GetCategorySingleRow([FromBody] GetSingleRowParameter parameter)
        {
            try
            {
                RenderService.Compile(RenderService.AdminDashboardCategoryTableRow);
                var list = _postCategoryRepository.GetById(parameter.Id);
                var d = RenderService.Run(RenderService.AdminDashboardCategoryTableRow,
                    new AdminDashboardCategoryTableRowViewModel()
                    {
                        PostCount = 0,
                        Name = list.CategoryName,
                        Id = list.Id
                    });
                return Ok(new
                {
                    result = d,
                    message = "Success",
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();


        }

        [HttpPost]
        public IHttpActionResult GetCategoryForm([FromBody] GetAddCategoryParameter parameter)
        {
            try
            {
                string data;
                if (parameter != null && parameter.CategoryId > 0)
                {
                    var c = _postCategoryRepository.GetById(parameter.CategoryId);
                    RenderService.Compile(RenderService.CategoryAddForm);
                    data = RenderService.Run(RenderService.CategoryAddForm, new AdminCategoryAddFormViewModel()
                    {
                        Id = c.Id,
                        Name = c.CategoryName,
                    });
                }
                else
                {
                    RenderService.Compile(RenderService.CategoryAddForm);
                    data = RenderService.Run(RenderService.CategoryAddForm, new AdminCategoryAddFormViewModel());
                }
                return Ok(new
                {
                    result = data,
                    message = "Success",
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetCategoriesCount([FromBody] GetSearhParameter parameter)
        {
            try
            {
                var users = string.IsNullOrEmpty(parameter.Parameter)
                    ? _postCategoryRepository.GetPaginationCount(parameter.Size)
                    : _postCategoryRepository.GetCategoriesSearchCount(parameter.Parameter, parameter.Size);
                return Ok(new
                {
                    message = "Sucess",
                    count = users
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetInventories([FromBody] GetSearhParameter parameter)
        {
            try
            {
                RenderService.Compile(RenderService.PharmacistDashboardInventoryTableRow);
                IEnumerable<InventoryTrack> repository;
                if (string.IsNullOrEmpty(parameter.Parameter))
                    repository =
                        _inventoryTrackRepository.GetMedicineInventoryByPharmacy(parameter.PharmacyId, parameter.Page);
                else
                {
                    repository = _inventoryTrackRepository.GetMedicineInventoryByPharmacySearch(parameter.PharmacyId,
                        parameter.Parameter, parameter.Page,
                        parameter.Size);
                }
                var list = repository.Select(user => RenderService.Run(
                    RenderService.PharmacistDashboardInventoryTableRow,
                    new PharmacistDashboardInventoryTableRowViewModel()
                    {
                        Name = user.Drug.Brand.BrandName + "-" + user.Drug.DrugName,
                        DrugId = user.DrugId,
                        Form = user.Drug.DrugForm,
                        TotalCount = user.TotalCount,
                        Price = user.Price,
                        Id = user.Id
                    })).ToList();
                return Ok(new
                {
                    result = list,
                    message = "Sucess",
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }


        [HttpPost]
        public IHttpActionResult GetInventoryForm([FromBody] GetAddInventoryParamter paramter)
        {
            try
            {
                if (paramter != null && paramter.UserId > 0)
                {
                    RenderService.Compile(RenderService.InventoryAddForm);
                    var d = RenderService.Run(RenderService.InventoryAddForm, new PharmacistAddInventoryViewModel()
                    {
                        Pharmacies = _pharmacyRepository.GetApprovedPharmaciesByUserId(paramter.UserId)
                    });
                    return Ok(new
                    {
                        result = d,
                        message = "Success"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetInventoriesCount([FromBody] GetSearhParameter parameter)
        {
            try
            {
                var users = string.IsNullOrEmpty(parameter.Parameter)
                    ? _inventoryTrackRepository.GetMedicineInventoryByPharmacyCount(parameter.PharmacyId,
                        parameter.Size)
                    : _inventoryTrackRepository.GetMedicineInventoryByPharmacySearchCount(parameter.PharmacyId,
                        parameter.Parameter, parameter.Size);
                return Ok(new
                {
                    message = "Sucess",
                    count = users
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }


        [HttpPost]
        public IHttpActionResult GetPosts([FromBody] GetSearhParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.PostId > 0 && parameter.Page > 0)
                {
                    RenderService.Compile(RenderService.PostThreadReply);
                    var list =
                        _postReplyRepository.GetRepliesByPostId(parameter.PostId, parameter.Page)
                            .Select(p => RenderService.Run(RenderService.PostThreadReply, new ForumReplyViewModel()
                            {
                                Id = p.Id,
                                Name = p.User.ToString(),
                                ImagePath = p.User.UserImage,
                                PostedAt = p.RepliedAt,
                                Signature = p.User.Signature,
                                Gender = p.User.Gender,
                                JoinedDate = p.User.RegisteredAt,
                                Reply = p.Body,
                                Age = UtilService.GetAge(DateTime.Today, p.User.DateOfBrith),
                                IsMine = parameter.UserId == p.UserId
                            }));
                    return Ok(new
                    {
                        message = "Sucess",
                        result = list
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetPostsCount([FromBody] GetSearhParameter parameter)
        {
            try
            {
                if (parameter != null)
                {
                    if (parameter.IsAdmin)
                    {
                        var count = string.IsNullOrEmpty(parameter.Parameter)
                            ? _postThreadRepository.GetPaginationCount(parameter.Size)
                            : _postThreadRepository.GetPaginationSearchCount(parameter.Parameter, parameter.Size);
                        return Ok(new
                        {
                            count = count,
                            message = "Success"
                        });
                    }
                    if (parameter.UserId > 0)
                    {
                        var count = string.IsNullOrEmpty(parameter.Parameter)
                            ? _postThreadRepository.GetPaginationCountByUserId(parameter.UserId, parameter.Size)
                            : _postThreadRepository.GetPaginationSearchCountByUserId(parameter.UserId,
                                parameter.Parameter, parameter.Size);
                        return Ok(new
                        {
                            count = count,
                            message = "Success"
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetPostTableRow([FromBody] GetSearhParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.Page > 0)
                {
                    RenderService.Compile(RenderService.PostThreadTableRow);
                    if (parameter.IsAdmin)
                    {
                        IEnumerable<string> list;
                        if (string.IsNullOrEmpty(parameter.Parameter))
                        {
                            list = _postThreadRepository.GetThreads(parameter.Page, parameter.Size).Select(
                                p => RenderService.Run(RenderService.PostThreadTableRow,
                                    new DashboardPostTableRowViewModel
                                    {
                                        Id = p.Id,
                                        Subject = p.Subject,
                                        Tags = p.TagPostThreads.Select(pr => pr.Tag).ToArray(),
                                        Category = p.PostCategory.CategoryName,
                                        OrignalPoster = p.User.ToString(),
                                        UserName = p.User.Username,
                                        ReplyCount = p.PostReplies.Count,
                                        PostedAt = p.PostedAt
                                    }));
                        }
                        else
                        {
                            list = _postThreadRepository
                                .GetThreadSearch(parameter.Parameter, parameter.Page, parameter.Size).Select(
                                    p => RenderService.Run(RenderService.PostThreadTableRow,
                                        new DashboardPostTableRowViewModel
                                        {
                                            Id = p.Id,
                                            Subject = p.Subject,
                                            Tags = p.TagPostThreads.Select(pr => pr.Tag).ToArray(),
                                            Category = p.PostCategory.CategoryName,
                                            OrignalPoster = p.User.ToString(),
                                            UserName = p.User.Username,
                                            ReplyCount = p.PostReplies.Count,
                                            PostedAt = p.PostedAt
                                        }));
                        }
                        return Ok(new
                        {
                            message = "Success",
                            result = list
                        });
                    }
                    if (parameter.UserId > 0)
                    {
                        IEnumerable<string> list;
                        if (string.IsNullOrEmpty(parameter.Parameter))
                        {
                            list = _postThreadRepository
                                .GetThreadsByUserId(parameter.UserId, parameter.Page, parameter.Size).Select(
                                    p => RenderService.Run(RenderService.PostThreadTableRow,
                                        new DashboardPostTableRowViewModel
                                        {
                                            Id = p.Id,
                                            Subject = p.Subject,
                                            Tags = p.TagPostThreads.Select(pr => pr.Tag).ToArray(),
                                            Category = p.PostCategory.CategoryName,
                                            OrignalPoster = p.User.ToString(),
                                            UserName = p.User.Username,
                                            ReplyCount = _postReplyRepository.GetCountByPostId(p.Id),
                                        }));
                        }
                        else
                        {
                            list = _postThreadRepository
                                .GetThreadSearchByUserId(parameter.UserId, parameter.Parameter, parameter.Page,
                                    parameter.Size).Select(p => RenderService.Run(RenderService.PostThreadTableRow,
                                    new DashboardPostTableRowViewModel
                                    {
                                        Id = p.Id,
                                        Subject = p.Subject,
                                        Tags = p.TagPostThreads.Select(pr => pr.Tag).ToArray(),
                                        Category = p.PostCategory.CategoryName,
                                        OrignalPoster = p.User.ToString(),
                                        UserName = p.User.Username,
                                        ReplyCount = _postReplyRepository.GetCountByPostId(p.Id),
                                    }));
                        }
                        return Ok(new
                        {
                            message = "Success",
                            result = list
                        });
                    }
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetReplies([FromBody] GetSearhParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.Page > 0 && parameter.UserId > 0)
                {
                    RenderService.Compile(RenderService.PostThreadReplyTableRow);
                    IEnumerable<string> list;
                    if (string.IsNullOrEmpty(parameter.Parameter))
                    {
                        list =
                            _postReplyRepository.GetRepliesByUserId(parameter.UserId, parameter.Page, parameter.Size)
                                .Select(
                                    p =>
                                        RenderService.Run(RenderService.PostThreadReplyTableRow,
                                            new DashboardPostReplyTableRowViewModel()
                                            {
                                                Id = p.Id,
                                                Subject = p.PostThread.Subject,
                                                UserName = p.User.Username,
                                                RepliedAt = p.RepliedAt,
                                                OrignalPoster = p.User.ToString(),
                                                ThreadId = p.PostThreadId
                                            }));
                    }
                    else
                    {
                        list =
                            _postReplyRepository.GetRepliesSearchByUserId(parameter.UserId, parameter.Parameter,
                                    parameter.Page, parameter.Size)
                                .Select(
                                    p =>
                                        RenderService.Run(RenderService.PostThreadReplyTableRow,
                                            new DashboardPostReplyTableRowViewModel()
                                            {
                                                Id = p.Id,
                                                Subject = p.PostThread.Subject,
                                                UserName = p.User.Username,
                                                RepliedAt = p.RepliedAt,
                                                ThreadId = p.PostThreadId,
                                                OrignalPoster = p.User.ToString(),
                                            }));

                    }
                    return Ok(new
                    {
                        result = list,
                        mesage = "Success"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetRepliesCount([FromBody] GetSearhParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.UserId > 0)
                {
                    var list = string.IsNullOrEmpty(parameter.Parameter)
                        ? _postReplyRepository.GetPaginationCountByUserId(parameter.UserId)
                        : _postReplyRepository.GetPaginationSearchCountByUserId(parameter.UserId, parameter.Parameter);
                    return Ok(new
                    {
                        count = list,
                        mesage = "Success"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetReviews([FromBody] GetSearhParameter parameter)
        {
            try
            {
                if (parameter != null)
                {
                    RenderService.Compile(RenderService.MedicineReviewTableRow);
                    if (parameter.IsAdmin)
                    {
                        var list = _drugReviewRepository.GetAll(parameter.Page, parameter.Size).Select(
                            p => RenderService.Run(RenderService.MedicineReviewTableRow,
                                new AdminDashboardReviewTableRowViewModel()
                                {
                                    Id = p.Id,
                                    FullName = p.User.ToString(),
                                    Username = p.User.Username,
                                    MedicineId = p.DrugId,
                                    PostedAt = p.PostedAt,
                                    Review = p.Review,
                                    Rating = p.Rating,
                                    MedicineName = p.Drug.ToString(),
                                }));
                        return Ok(new
                        {
                            result = list,
                            message = "Success"
                        });
                    }
                    if (parameter.UserId > 0)
                    {
                        var list = _drugReviewRepository.GetByUserId(parameter.UserId, parameter.Page, parameter.Size)
                            .Select(p => RenderService.Run(RenderService.MedicineReviewTableRow,
                                new AdminDashboardReviewTableRowViewModel()
                                {
                                    Id = p.Id,
                                    FullName = p.User.ToString(),
                                    Username = p.User.Username,
                                    MedicineId = p.DrugId,
                                    PostedAt = p.PostedAt,
                                    Review = p.Review,
                                    Rating = p.Rating,
                                    MedicineName = p.Drug.ToString(),
                                }));
                        return Ok(new
                        {
                            result = list,
                            message = "Success"
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetPromotions([FromBody] GetSearhParameter parameter)
        {
            try
            {
                if (parameter != null)
                {
                    RenderService.Compile(RenderService.PharmacistDashboardPromotionTableRow);
                    if (parameter.IsAdmin)
                    {
                        if (!parameter.Type.Contains("inactive"))
                        {

                            if (string.IsNullOrEmpty(parameter.Parameter))
                            {
                                var list =
                                    _promotionRepository.GetActivePromotions(parameter.Page, parameter.Size)
                                        .Select(
                                            p =>
                                                RenderService.Run(RenderService.PharmacistDashboardPromotionTableRow,
                                                    new PharmacistDashboardPromotionTableRow()
                                                    {
                                                        Id = p.Id,
                                                        UserId = p.Pharmacy.UserId.GetValueOrDefault(),
                                                        FullName = p.Pharmacy.User.ToString(),
                                                        Name = p.Name,
                                                        Code = p.Code,
                                                        Description = p.Description,
                                                        IsAdmin = parameter.IsAdmin,
                                                        Username = p.Pharmacy.User.Username,
                                                        EndDate = p.EndDate,
                                                        StartDate = p.StartDate,
                                                        IsActive = p.EndDate > DateTime.Now,
                                                        PercentOff = p.PercentageOff,
                                                    }));
                                return Ok(new
                                {
                                    result = list,
                                    message = "Success"
                                });
                            }
                            else
                            {
                                var list =
                                    _promotionRepository
                                        .GetActivePromotionsSearch(parameter.Parameter, parameter.Page, parameter.Size)
                                        .Select(
                                            p =>
                                                RenderService.Run(RenderService.PharmacistDashboardPromotionTableRow,
                                                    new PharmacistDashboardPromotionTableRow()
                                                    {
                                                        Id = p.Id,
                                                        UserId = p.Pharmacy.UserId.GetValueOrDefault(),
                                                        FullName = p.Pharmacy.User.ToString(),
                                                        Name = p.Name,
                                                        Description = p.Description,
                                                        IsAdmin = parameter.IsAdmin,
                                                        Username = p.Pharmacy.User.Username,
                                                        Code = p.Code,
                                                        EndDate = p.EndDate,
                                                        StartDate = p.StartDate,
                                                        PercentOff = p.PercentageOff,
                                                        IsActive = p.EndDate > DateTime.Now
                                                    }));
                                return Ok(new
                                {
                                    result = list,
                                    message = "Success"
                                });
                            }
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(parameter.Parameter))
                            {
                                var list =
                                    _promotionRepository.GetInActivePromotions(parameter.Page, parameter.Size)
                                        .Select(
                                            p =>
                                                RenderService.Run(RenderService.PharmacistDashboardPromotionTableRow,
                                                    new PharmacistDashboardPromotionTableRow()
                                                    {
                                                        Id = p.Id,
                                                        UserId = p.Pharmacy.UserId.GetValueOrDefault(),
                                                        FullName = p.Pharmacy.User.ToString(),
                                                        Name = p.Name,
                                                        Description = p.Description,
                                                        IsAdmin = parameter.IsAdmin,
                                                        Username = p.Pharmacy.User.Username,
                                                        EndDate = p.EndDate,
                                                        Code = p.Code,
                                                        StartDate = p.StartDate,
                                                        PercentOff = p.PercentageOff,
                                                        IsActive = p.EndDate > DateTime.Now
                                                    }));
                                return Ok(new
                                {
                                    result = list,
                                    message = "Success"
                                });
                            }
                            else
                            {
                                var list =
                                    _promotionRepository
                                        .GetInActivePromotionsSearch(parameter.Parameter, parameter.Page,
                                            parameter.Size)
                                        .Select(
                                            p =>
                                                RenderService.Run(RenderService.PharmacistDashboardPromotionTableRow,
                                                    new PharmacistDashboardPromotionTableRow()
                                                    {
                                                        Id = p.Id,
                                                        UserId = p.Pharmacy.UserId.GetValueOrDefault(),
                                                        FullName = p.Pharmacy.User.ToString(),
                                                        Name = p.Name,
                                                        Description = p.Description,
                                                        IsAdmin = parameter.IsAdmin,
                                                        Username = p.Pharmacy.User.Username,
                                                        Code = p.Code,
                                                        EndDate = p.EndDate,
                                                        StartDate = p.StartDate,
                                                        PercentOff = p.PercentageOff,
                                                        IsActive = p.EndDate > DateTime.Now
                                                    }));
                                return Ok(new
                                {
                                    result = list,
                                    message = "Success"
                                });
                            }
                        }
                    }
                    if (parameter.UserId > 0)
                    {

                        if (!parameter.Type.Contains("inactive"))
                        {
                            if (string.IsNullOrEmpty(parameter.Parameter))
                            {
                                var list =
                                    _promotionRepository.GetActivePromotionsByUserId(parameter.UserId, parameter.Page,
                                            parameter.Size)
                                        .Select(
                                            p =>
                                                RenderService.Run(RenderService.PharmacistDashboardPromotionTableRow,
                                                    new PharmacistDashboardPromotionTableRow()
                                                    {
                                                        Id = p.Id,
                                                        UserId = p.Pharmacy.UserId.GetValueOrDefault(),
                                                        FullName = p.Pharmacy.User.ToString(),
                                                        Code = p.Code,
                                                        Name = p.Name,
                                                        Description = p.Description,
                                                        IsAdmin = parameter.IsAdmin,
                                                        Username = p.Pharmacy.User.Username,
                                                        EndDate = p.EndDate,
                                                        StartDate = p.StartDate,
                                                        PercentOff = p.PercentageOff,
                                                        IsActive = p.EndDate > DateTime.Now
                                                    }));
                                return Ok(new
                                {
                                    result = list,
                                    message = "Success"
                                });
                            }
                            else
                            {
                                var list =
                                    _promotionRepository.GetActivePromotionsSearchByUserId(parameter.Parameter,
                                            parameter.UserId, parameter.Page,
                                            parameter.Size)
                                        .Select(
                                            p =>
                                                RenderService.Run(RenderService.PharmacistDashboardPromotionTableRow,
                                                    new PharmacistDashboardPromotionTableRow()
                                                    {
                                                        Id = p.Id,
                                                        UserId = p.Pharmacy.UserId.GetValueOrDefault(),
                                                        FullName = p.Pharmacy.User.ToString(),
                                                        Name = p.Name,
                                                        Description = p.Description,
                                                        IsAdmin = parameter.IsAdmin,
                                                        Code = p.Code,
                                                        Username = p.Pharmacy.User.Username,
                                                        EndDate = p.EndDate,
                                                        StartDate = p.StartDate,
                                                        PercentOff = p.PercentageOff,
                                                        IsActive = p.EndDate > DateTime.Now
                                                    }));
                                return Ok(new
                                {
                                    result = list,
                                    message = "Success"
                                });

                            }
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(parameter.Parameter))
                            {
                                var list =
                                    _promotionRepository.GetInActivePromotionsByUserId(parameter.UserId, parameter.Page,
                                            parameter.Size)
                                        .Select(
                                            p =>
                                                RenderService.Run(RenderService.PharmacistDashboardPromotionTableRow,
                                                    new PharmacistDashboardPromotionTableRow()
                                                    {
                                                        Id = p.Id,
                                                        UserId = p.Pharmacy.UserId.GetValueOrDefault(),
                                                        FullName = p.Pharmacy.User.ToString(),
                                                        Name = p.Name,
                                                        Description = p.Description,
                                                        IsAdmin = parameter.IsAdmin,
                                                        Username = p.Pharmacy.User.Username,
                                                        EndDate = p.EndDate,
                                                        StartDate = p.StartDate,
                                                        PercentOff = p.PercentageOff,
                                                        Code = p.Code,
                                                        IsActive = p.EndDate > DateTime.Now
                                                    }));
                                return Ok(new
                                {
                                    result = list,
                                    message = "Success"
                                });
                            }
                            else
                            {
                                var list =
                                    _promotionRepository.GetInActivePromotionsSearchByUserId(parameter.Parameter,
                                            parameter.UserId, parameter.Page,
                                            parameter.Size)
                                        .Select(
                                            p =>
                                                RenderService.Run(RenderService.PharmacistDashboardPromotionTableRow,
                                                    new PharmacistDashboardPromotionTableRow()
                                                    {
                                                        Id = p.Id,
                                                        UserId = p.Pharmacy.UserId.GetValueOrDefault(),
                                                        FullName = p.Pharmacy.User.ToString(),
                                                        Code = p.Code,
                                                        Name = p.Name,
                                                        Description = p.Description,
                                                        IsAdmin = parameter.IsAdmin,
                                                        Username = p.Pharmacy.User.Username,
                                                        EndDate = p.EndDate,
                                                        StartDate = p.StartDate,
                                                        PercentOff = p.PercentageOff,
                                                        IsActive = p.EndDate > DateTime.Now
                                                    }));
                                return Ok(new
                                {
                                    result = list,
                                    message = "Success"
                                });
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetPromotionsCount([FromBody] GetSearhParameter parameter)
        {
            try
            {
                if (parameter != null)
                {
                    if (parameter.IsAdmin)
                    {
                        int count;
                        if (parameter.Type.Contains("inactive"))
                        {
                            count = string.IsNullOrEmpty(parameter.Parameter)
                                ? _promotionRepository.GetInActivePromotionsPaginationCount(parameter.Size)
                                : _promotionRepository.GetInActivePromotionsSearchCount(parameter.Parameter,
                                    parameter.Size);
                        }
                        else
                        {
                            count = string.IsNullOrEmpty(parameter.Parameter)
                                ? _promotionRepository.GetActivePromotionsPaginationCount(parameter.Size)
                                : _promotionRepository.GetActivePromotionsSearchCount(parameter.Parameter,
                                    parameter.Size);
                        }
                        return Ok(new
                        {
                            result = count,
                            message = "Success"
                        });
                    }
                    if (parameter.UserId > 0)
                    {
                        int count;
                        if (parameter.Type.Contains("inactive"))
                        {
                            count = string.IsNullOrEmpty(parameter.Parameter)
                                ? _promotionRepository.GetInActivePromotionsByUserIdPaginationCount(parameter.UserId,
                                    parameter.Size)
                                : _promotionRepository.GetInActivePromotionsSearchByUserIdPaginationCount(
                                    parameter.Parameter, parameter.UserId, parameter.Size);
                        }
                        else
                        {
                            count = string.IsNullOrEmpty(parameter.Parameter)
                                ? _promotionRepository.GetActivePromotionsByUserIdPaginationCount(parameter.UserId,
                                    parameter.Size)
                                : _promotionRepository.GetActivePromotionsSearchByUserIdPaginationCount(
                                    parameter.Parameter, parameter.UserId, parameter.Size);
                        }
                        return Ok(new
                        {
                            result = count,
                            message = "Success"
                        });

                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetPromotionForm([FromBody] GetPromotionFormParameter parameter)
        {
            try
            {
                if (parameter != null)
                {
                    RenderService.Compile(RenderService.PromotionAddForm);
                    if (parameter.PromotionId > 0 && parameter.UserId > 0)
                    {
                        var p = _promotionRepository.GetById(parameter.PromotionId);
                        if (p != null)
                        {
                            var list = RenderService.Run(RenderService.PromotionAddForm,
                                new PharmacistPromotionFormViewModel()
                                {
                                    Id = p.Id,
                                    Pharmacies = _pharmacyRepository.GetApprovedPharmaciesByUserId(parameter.UserId),
                                    Description = p.Description,
                                    Name = p.Name,
                                    IsAdmin = parameter.IsAdmin,
                                    PharmacyId = p.PharmacyId,
                                    PercentOff = p.PercentageOff
                                });
                            return Ok(new
                            {
                                result = list,
                                message = "Success"
                            });
                        }
                    }
                    else if (parameter.UserId > 0)
                    {
                        var list = RenderService.Run(RenderService.PromotionAddForm,
                            new PharmacistPromotionFormViewModel()
                            {
                                Pharmacies = _pharmacyRepository.GetApprovedPharmaciesByUserId(parameter.UserId),
                                IsAdmin = parameter.IsAdmin
                            });
                        return Ok(new
                        {
                            result = list,
                            message = "Success"
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetStartPromotionForm([FromBody] GetPromotionFormParameter parameter)
        {
            try
            {
                if (parameter != null)
                {
                    RenderService.Compile(RenderService.PromotionStartForm);
                    if (parameter.PromotionId > 0 && parameter.UserId > 0)
                    {
                        var p = _promotionRepository.GetById(parameter.PromotionId);
                        if (p != null)
                        {
                            var list = RenderService.Run(RenderService.PromotionStartForm,
                                new PharmacistPromotionStartFormViewModel()
                                {
                                    Id = p.Id,
                                    IsAdmin = parameter.IsAdmin,
                                    PharmacyId = p.PharmacyId,
                                });
                            return Ok(new
                            {
                                result = list,
                                message = "Success"
                            });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetMapPharmacy([FromBody] GetMapPharmacy parameter)
        {
            try
            {
                if (parameter != null)
                {
                    RenderService.Compile(RenderService.MapForm);
                    if (parameter.PharmacyId > 0)
                    {
                        var p = _pharmacyRepository.GetById(parameter.PharmacyId);
                        if (p != null)
                        {
                            var list = RenderService.Run(RenderService.MapForm,
                                new MapFormViewModel()
                                {
                                    Id = p.Id,
                                    Address = p.ShopAddress,
                                    X = p.ShopLocationX + "",
                                    Y = p.ShopLocationY + "",
                                });
                            return Ok(new
                            {
                                result = list,
                                message = "Success"
                            });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetSelectLocationForm()
        {
            try
            {
                RenderService.Compile(RenderService.SelectLocationForm);
                var list = RenderService.Run(RenderService.SelectLocationForm,
                    new SelectLocationFormViewModel()
                    {

                    });
                return Ok(new
                {
                    result = list,
                    message = "Success"
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }


        [HttpPost]
        public IHttpActionResult GetAddToInventoryForm([FromBody] GetAddToInventoryFormParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.UserId > 0 && parameter.MedicineId > 0)
                {
                    RenderService.Compile(RenderService.AddToInventoryForm);
                    var m = _drugRepository.GetById(parameter.MedicineId);
                    var list = RenderService.Run(RenderService.AddToInventoryForm,
                        new AddToInventoryFormViewModel()
                        {
                            UserId = parameter.UserId,
                            MedicineId = parameter.MedicineId,
                            MedicineName = m.Brand + " - " + m.DrugName + "-" + m.Packing,
                            Pharmacies = _pharmacyRepository.GetApprovedPharmaciesByUserId(parameter.UserId),
                            Quantity = parameter.Quantity,
                        });
                    return Ok(new
                    {
                        result = list,
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }


        [HttpPost]
        public IHttpActionResult GetRecommendationForm([FromBody] GetRecommendationFormParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.UserId > 0 && parameter.MedicineId > 0)
                {
                    RenderService.Compile(RenderService.RecommendationForm);
                    var m = _drugRepository.GetById(parameter.MedicineId);
                    var list = RenderService.Run(RenderService.RecommendationForm,
                        new RecommendationFormViewModel
                        {
                            UserId = parameter.UserId,
                            MedicineId = parameter.MedicineId,
                            MedicineName = m.DrugName
                        });
                    return Ok(new
                    {
                        result = list,
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }


        [HttpPost]
        public IHttpActionResult GetCancelOrderForm([FromBody] GetCancelOrderFormParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.OrderId > 0)
                {
                    RenderService.Compile(RenderService.CancelOrderForm);
                    var list = RenderService.Run(RenderService.CancelOrderForm,
                        new CancelOrderFormViewModel()
                        {
                            OrderId = parameter.OrderId
                        });
                    return Ok(new
                    {
                        result = list,
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetRejectOrderForm([FromBody] GetCancelOrderFormParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.OrderId > 0)
                {
                    RenderService.Compile(RenderService.RejectOrderForm);
                    var list = RenderService.Run(RenderService.RejectOrderForm,
                        new RejectOrderFormViewModel()
                        {
                            OrderId = parameter.OrderId
                        });
                    return Ok(new
                    {
                        result = list,
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetCompleteOrderForm([FromBody] GetCancelOrderFormParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.OrderId > 0)
                {
                    RenderService.Compile(RenderService.CompleteOrderForm);
                    var order = _orderRepository.GetById(parameter.OrderId);
                    var list = RenderService.Run(RenderService.CompleteOrderForm,
                        new CompleteOrderFormViewModel()
                        {
                            OrderId = parameter.OrderId,
                            Order = order
                        });
                    return Ok(new
                    {
                        result = list,
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetAcceptOrderForm([FromBody] GetCancelOrderFormParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.OrderId > 0 && parameter.UserId > 0)
                {
                    RenderService.Compile(RenderService.AcceptOrderForm);
                    var list = RenderService.Run(RenderService.AcceptOrderForm,
                        new AcceptOrderFormViewModel()
                        {
                            OrderId = parameter.OrderId,
                            Employees = _employeeRepository.GetEmployees(parameter.UserId, 1, 100),
                        });
                    return Ok(new
                    {
                        result = list,
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();


        }

        [HttpPost]
        public IHttpActionResult GetCustomerReviewOrderForm([FromBody] GetCustomerReviewOrderFormParameter parameter)
        {
            try
            {
                RenderService.Compile(RenderService.CustomerReviewOrderForm);
                if (parameter != null && parameter.Id > 0)
                {
                    var review = _customerReviewRepository.GetById(parameter.Id);
                    if (review != null)
                    {
                        var list = RenderService.Run(RenderService.CustomerReviewOrderForm,
                            new PharmacistCustomerReviewFormViewModel()
                            {
                                UserId = review.UserId,
                                Id = review.Id,
                                PharmacyId = review.PharmacyId,
                                OrderId = review.OrderId,
                                Rating = review.Rating + "",
                                Review = review.Review
                            });
                        return Ok(new
                        {
                            result = list,
                            message = "Success"
                        });
                    }
                }
                else if (parameter != null && parameter.OrderId > 0)
                {
                    var order = _orderRepository.GetById(parameter.OrderId);
                    var list = RenderService.Run(RenderService.CustomerReviewOrderForm,
                        new PharmacistCustomerReviewFormViewModel()
                        {
                            UserId = order.UserId.GetValueOrDefault(),
                            OrderId = order.Id,
                            PharmacyId = order.PharmacyId.GetValueOrDefault(),
                            Rating = "0",
                            Review = ""
                        });
                    return Ok(new
                    {
                        result = list,
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetPharmacyReviewOrderForm([FromBody] GetCustomerReviewOrderFormParameter parameter)
        {
            try
            {
                RenderService.Compile(RenderService.PharmacyReviewOrderForm);
                if (parameter != null && parameter.Id > 0)
                {
                    var review = _customerReviewRepository.GetById(parameter.Id);
                    if (review != null)
                    {
                        var list = RenderService.Run(RenderService.PharmacyReviewOrderForm,
                            new PharmacistCustomerReviewFormViewModel()
                            {
                                UserId = review.UserId,
                                Id = review.Id,
                                PharmacyId = review.PharmacyId,
                                OrderId = review.OrderId,
                                Rating = review.Rating + "",
                                Review = review.Review
                            });
                        return Ok(new
                        {
                            result = list,
                            message = "Success"
                        });
                    }
                }
                else if (parameter != null && parameter.OrderId > 0)
                {
                    var order = _orderRepository.GetById(parameter.OrderId);
                    var list = RenderService.Run(RenderService.PharmacyReviewOrderForm,
                        new CustomerPharmacyReviewFormViewModel()
                        {
                            UserId = order.UserId.GetValueOrDefault(),
                            OrderId = order.Id,
                            PharmacyId = order.PharmacyId.GetValueOrDefault()
                        });
                    return Ok(new
                    {
                        result = list,
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetOrderDetailForm([FromBody] GetCancelOrderFormParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.OrderId > 0)
                {
                    var m = _orderRepository.GetById(parameter.OrderId);
                    if (m != null)
                    {
                        RenderService.Compile(RenderService.OrderDetailForm);
                        var list = RenderService.Run(RenderService.OrderDetailForm,
                            new OrderDetailFormViewModel()
                            {
                                OrderId = parameter.OrderId,
                                Order = m
                            });
                        return Ok(new
                        {
                            result = list,
                            message = "Success"
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetReviewOrderForm([FromBody] GetCustomerReviewOrderFormParameter parameter)
        {
            try
            {
                string data;
                if (parameter != null && parameter.OrderId > 0)
                {
                    var c = _orderRepository.GetById(parameter.OrderId);
                    RenderService.Compile(RenderService.ReviewOrderForm);
                    data = RenderService.Run(RenderService.ReviewOrderForm, new ReviewOrderFormViewModel()
                    {
                        Id = c.Id,
                        Order = c
                    });
                    return Ok(new
                    {
                        result = data,
                        message = "Success",
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GenerateReport([FromBody] GenerateReportParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.PharmacyId > 0)
                {
                    var c = _pharmacyRepository.GetById(parameter.PharmacyId);
                    RenderService.Compile(RenderService.GeneratedReportTable);
                    if (c == null)
                        return InternalServerError();
                    var orders = _orderRepository.GenerateReport(parameter.PharmacyId, parameter.EmployeeId,
                        parameter.StartDate, parameter.EndDate);

                    var enumerable = orders as Order[] ?? orders.ToArray();
                    var data = RenderService.Run(RenderService.GeneratedReportTable, new GeneratedReportTableViewModel()
                    {
                        Orders = enumerable,
                        IsAll = parameter.EmployeeId.Contains(0),
                        End = parameter.EndDate.AddHours(23).AddMinutes(59),
                        Start = parameter.StartDate,
                        EmployeeNames = enumerable.GroupBy(p => p.Employee.Name).Select(p => p.Key).ToArray()
                    });
                    return Ok(new
                    {
                        result = data,
                        message = "Success",
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetReportForm([FromBody] GetReportFormParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.UserId > 0)
                {
                    var u = _userRepository.GetById(parameter.UserId);
                    if (u == null)
                        return InternalServerError();
                    RenderService.Compile(RenderService.ReportForm);
                    if (!u.Pharmacies.Any())
                    {
                        return InternalServerError();
                    }
                    var data = RenderService.Run(RenderService.ReportForm, new ReportFormViewModel()
                    {
                        UserId = u.Id,
                        PharmacyId = u.Pharmacies.First(p => p.IsApproved && !p.IsRejected).Id,
                        Employees = u.Pharmacies.First(p => p.IsApproved && !p.IsRejected).Employees
                    });
                    return Ok(new
                    {
                        result = data,
                        message = "Success",
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetNotification([FromBody] GetNotificationParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.UserId > 0)
                {

                    RenderService.Compile(RenderService.NotificationItem);
                    var notification = _notificationRepository.GetNotificationByUserId(parameter.UserId);
                    var notifications = notification as Notification[] ?? notification.ToArray();
                    _notificationRepository.SetUnSeen(notifications.Select(p => p.Id).ToArray());
                    var data =
                        notifications.Select(
                            p => RenderService.Run(RenderService.NotificationItem, new NotificationItemViewModel()
                            {
                                Url = p.Url,
                                Body = p.ToString(),
                                Date = p.CreatedAt,
                                Id = p.Id,
                                Priority = p.NotificationType
                            }));
                    return Ok(new
                    {
                        result = data,
                        count = _notificationRepository.GetUnSeenCountByUserId(parameter.UserId),
                        message = "Success",
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetForumPostThreadRow([FromBody] GetSearhParameter parameter)
        {
            try
            {
                RenderService.Compile(RenderService.ForumPostThreadRow);
                if (string.IsNullOrEmpty(parameter?.Parameter))
                {

                    var list =
                        _postThreadRepository.GetLatestThreads()
                            .Select(
                                p =>
                                    RenderService.Run(RenderService.ForumPostThreadRow,
                                        new ForumPostThreadRowViewModel()
                                        {
                                            PostThread = p
                                        }));
                    return Ok(new
                    {
                        result = list,
                        message = "Success",
                    });
                }
                else
                {
                    var list =
                        _postThreadRepository.GetLatestThreadsBySearchAndTag(parameter.Parameter)
                            .Select(
                                p =>
                                    RenderService.Run(RenderService.ForumPostThreadRow,
                                        new ForumPostThreadRowViewModel()
                                        {
                                            PostThread = p
                                        }));
                    return Ok(new
                    {
                        result = list,
                        message = "Success",
                    });

                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetCategoryThreadRow([FromBody] GetSearhParameter parameter)
        {
            try
            {
                RenderService.Compile(RenderService.ForumCategoryRow);
                if (string.IsNullOrEmpty(parameter?.Parameter))
                {

                    var list =
                        _postCategoryRepository.GetTopCategories()
                            .Select(
                                p =>
                                    RenderService.Run(RenderService.ForumCategoryRow,
                                        new ForumCategoryRowViewModel()
                                        {
                                            PostCategory = p
                                        }));
                    return Ok(new
                    {
                        result = list,
                        message = "Success",
                    });
                }
                else
                {
                    var list =
                        _postCategoryRepository.GetCategoriesSearch(parameter.Parameter)
                            .Select(
                                p =>
                                    RenderService.Run(RenderService.ForumCategoryRow,
                                        new ForumCategoryRowViewModel()
                                        {
                                            PostCategory = p
                                        }));
                    return Ok(new
                    {
                        result = list,
                        message = "Success",
                    });

                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetProcessImageForm([FromBody] GetProcessImageFormParameter parameter)
        {
            if (parameter != null && parameter.Id > 0)
            {
                RenderService.Compile(RenderService.PrescriptionImageProcessForm);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetSetPriceForm([FromBody] GetSetPriceFormParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.Id > 0)
                {
                    RenderService.Compile(RenderService.SetPriceForm);
                    var m = _inventoryTrackRepository.GetById(parameter.Id);
                    var list = RenderService.Run(RenderService.SetPriceForm,
                        new SetPriceViewModel()
                        {
                            Id = m.Id,
                            Name = m.Drug.Brand + " " + m.Drug.DrugName,
                            ShopName = m.Pharmacy.ShopName,
                            Price = m.Price
                        });
                    return Ok(new
                    {
                        result = list,
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetRemoveInventoryForm([FromBody] GetRemoveInventoryFormParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.InventoryId > 0)
                {
                    RenderService.Compile(RenderService.RemoveFromInventoryForm);
                    var m = _inventoryTrackRepository.GetById(parameter.InventoryId);
                    var list = RenderService.Run(RenderService.RemoveFromInventoryForm,
                        new RemoveFromInventoryFormViewModel()
                        {
                            MedicineId = m.DrugId,
                            MedicineName = m.Drug.Brand + " - " + m.Drug.DrugName + "-" + m.Drug.Packing,
                            PharmacyName = m.Pharmacy.ShopName,
                            Max = m.TotalCount,
                            Id = m.Id
                        });
                    return Ok(new
                    {
                        result = list,
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetPrescriptionSetMedicineForm(
            [FromBody] GetPrescriptionSetMedicineParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.OrderId > 0)
                {
                    RenderService.Compile(RenderService.PrescriptionSetMedicineForm);
                    var order = _orderRepository.GetById(parameter.OrderId);
                    var list = RenderService.Run(RenderService.PrescriptionSetMedicineForm,
                        new PrescriptionSetMedicineFormViewModel()
                        {
                            OrderDetails = order.OrderDetails,
                            OrderId = order.Id,
                            OrderPrescriptions = order.OrderPrescriptions,
                            PharmacyId = order.PharmacyId.GetValueOrDefault()
                        });
                    return Ok(new
                    {
                        message = "Success",
                        result = list
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetOrderPrescriptionRow([FromBody] GetOrderPrescriptionRowParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.Id > 0)
                {
                    RenderService.Compile(RenderService.PrescriptionOrderSetMedicineTableRow);
                    var order = _orderRepository.GetById(parameter.Id);
                    var list = RenderService.Run(RenderService.PrescriptionOrderSetMedicineTableRow,
                        new PrescriptionOrderSetMedicineTableRowViewModel()
                        {
                            OrderDetails = order.OrderDetails
                        });
                    return Ok(new
                    {
                        result = list,
                        message = "Success",
                        TotalPrice = order.OrderDetails.Sum(p => p.Price * p.Quantity),
                        TotalQuantity = order.OrderDetails.Sum(p => p.Quantity),
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult AddToOrder([FromBody] AddToOrderParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.OrderId > 0 && parameter.InventoryId > 0 && parameter.Amount > 0)
                {
                    RenderService.Compile(RenderService.PrescriptionOrderSetMedicineTableRow);
                    var inventory = _inventoryTrackRepository.GetById(parameter.InventoryId);
                    var detail = _orderDetailRepository.GetByOrderIdDrugId(parameter.OrderId, inventory.DrugId);
                    if (detail != null)
                    {
                        detail.Quantity += parameter.Amount;
                        if (inventory.TotalCount >= detail.Quantity)
                        {
                            _orderDetailRepository.Update(detail);
                        }
                        else
                        {
                            return Ok(new
                            {
                                message = "Error"
                            });
                        }
                    }
                    else
                    {
                        var d = new OrderDetail()
                        {
                            OrderId = parameter.OrderId,
                            DrugId = inventory.DrugId,
                            Quantity = parameter.Amount,
                            Price = inventory.Price,
                        };
                        if (inventory.TotalCount >= d.Quantity)
                        {
                            _orderDetailRepository.Add(d);
                        }
                        else
                        {
                            return Ok(new
                            {
                                message = "Error"
                            });
                        }
                    }
                    var order = _orderRepository.GetById(parameter.OrderId);
                    var list = RenderService.Run(RenderService.PrescriptionOrderSetMedicineTableRow,
                        new PrescriptionOrderSetMedicineTableRowViewModel()
                        {
                            OrderDetails = order.OrderDetails
                        });
                    return Ok(new
                    {
                        message = "Success",
                        result = list,
                        TotalPrice = order.OrderDetails.Sum(p => p.Price * p.Quantity),
                        TotalQuantity = order.OrderDetails.Sum(p => p.Quantity),
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult RemoveOrderDetail(
            [FromBody] RemoveOrderDetailParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.Id > 0)
                {
                    _orderDetailRepository.Delete(p => p.Id == parameter.Id);
                    var order = _orderRepository.GetById(parameter.OrderId);
                    var list = RenderService.Run(RenderService.PrescriptionOrderSetMedicineTableRow,
                        new PrescriptionOrderSetMedicineTableRowViewModel()
                        {
                            OrderDetails = order.OrderDetails
                        });
                    return Ok(new
                    {
                        message = "Success",
                        result = list,
                        TotalPrice = order.OrderDetails.Sum(p => p.Price * p.Quantity),
                        TotalQuantity = order.OrderDetails.Sum(p => p.Quantity),
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult UpdateOrderDetail(
            [FromBody] RemoveOrderDetailParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.Id > 0 && parameter.Quantity > 0)
                {
                    var orderDetail = _orderDetailRepository.GetById(parameter.Id);
                    orderDetail.Quantity = parameter.Quantity;
                    _orderDetailRepository.Update(orderDetail);
                    var order = _orderRepository.GetById(orderDetail.OrderId);
                    var list = RenderService.Run(RenderService.PrescriptionOrderSetMedicineTableRow,
                        new PrescriptionOrderSetMedicineTableRowViewModel()
                        {
                            OrderDetails = order.OrderDetails
                        });
                    return Ok(new
                    {
                        message = "Success",
                        result = list,
                        TotalPrice = order.OrderDetails.Sum(p => p.Price * p.Quantity),
                        TotalQuantity = order.OrderDetails.Sum(p => p.Quantity),
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetProcessLocationDialogBox([FromBody] GetProcessLocationParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.UserId > 0)
                {
                    RenderService.Compile(RenderService.ProcessLocationForm);
                    var list = _pharmacyRepository.GetApprovedPharmaciesByUserId(parameter.UserId);
                    var data = RenderService.Run(RenderService.ProcessLocationForm, new ProcessLocationFormViewModel()
                    {
                        Pharmacies = list
                    });
                    return Ok(new
                    {
                        result = data,
                        message = "Success"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetAddToCartForm([FromBody] GetAddToCartFormParameter parameter)
        {
            try
            {
                if (parameter != null && (parameter.UserId > 0 || !string.IsNullOrEmpty(parameter.SessionId)) && parameter.MedicineId > 0 && parameter.Quantity > 0)
                {
                    RenderService.Compile(RenderService.AddToCartForm);
                    var list = _inventoryTrackRepository.GetByDrugId(parameter.MedicineId);
                    var data = RenderService.Run(RenderService.AddToCartForm, new AddToCartFormViewModel()
                    {
                        InventoryTracks = list,
                        UserId = parameter.UserId,
                        Count = parameter.Quantity,
                        SessionId = parameter.SessionId
                    });
                    return Ok(new
                    {
                        result = data,
                        message = "Success"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }
        [HttpPost]
        public IHttpActionResult GetOrderPlaceForm([FromBody] GetOrderPlaceFormParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.UserId > 0)
                {
                    if (parameter.Prescriptions != null && parameter.Prescriptions.Any())
                    {
                        RenderService.Compile(RenderService.OrderPlacePrescriptionForm);
                        var data = RenderService.Run(RenderService.OrderPlacePrescriptionForm, new OrderPlaceFormPrescriptionViewModel()
                        {
                            UserId = parameter.UserId,
                            Pharmacy = _pharmacyRepository.GetById(parameter.PharmacyId),
                            Prescriptions = _prescriptionRepository.GetMany(p => p.UserId == parameter.UserId)
                        });
                        return Ok(new
                        {
                            result = data,
                            message = "Success"
                        });
                    }
                    if (parameter.PharmacyId > 0)
                    {
                        RenderService.Compile(RenderService.OrderPlaceForm);

                        var data = RenderService.Run(RenderService.OrderPlaceForm, new OrderPlaceFormViewModel()
                        {
                            UserId = parameter.UserId,
                            InventoryTracks = _drugCartRepository.GetMany(p => p.Inventory.PharmacyId == parameter.PharmacyId).Select(p => p.Inventory),
                            Pharmacies = _pharmacyRepository.GetMany(p => p.Id == parameter.PharmacyId)
                        });
                        return Ok(new
                        {
                            result = data,
                            message = "Success"
                        });

                    }
                    else
                    {
                        RenderService.Compile(RenderService.OrderPlaceForm);
                        var m = new OrderPlaceFormViewModel()
                        {
                            UserId = parameter.UserId,
                            InventoryTracks = _drugCartRepository.GetMany(p => p.UserId == parameter.UserId)
                                .Select(p => p.Inventory),
                        };
                        m.Pharmacies = m.InventoryTracks.GroupBy(p => p.PharmacyId)
                            .Select(p => p.FirstOrDefault().Pharmacy);
                        var data = RenderService.Run(RenderService.OrderPlaceForm, m);

                        return Ok(new
                        {
                            result = data,
                            message = "Success"
                        });

                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }
    }

    public class GetOrderPlaceFormParameter
    {
        public int UserId { get; set; }
        public int PharmacyId { get; set; }
        public int[] Prescriptions { get; set; }
    }

    public class GetAddToCartFormParameter
    {
        public int UserId { get; set; }
        public int MedicineId { get; set; }
        public int Quantity { get; set; }
        public string SessionId { get; set; }
    }

    public class GetProcessLocationParameter
    {
        public int UserId { get; set; }
    }

    public class GetOrderPrescriptionRowParameter
    {
        public int Id { get; set; }
    }

    public class RemoveOrderDetailParameter
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
        public int OrderId { get; set; }
    }

    public class GetPrescriptionSetMedicineParameter
    {
        public int OrderId { get; set; }
    }

    public class GetRemoveInventoryFormParameter
    {
        public int InventoryId { get; set; }
    }

    public class GetSetPriceFormParameter
    {
        public int Id { get; set; }
    }

    public class GetProcessImageFormParameter
    {
        public int Id { get; set; }
    }

    public class GetReportFormParameter
    {
        public int UserId { get; set; }
    }

    public class GenerateReportParameter
    {
        public int PharmacyId { get; set; }
        public int[] EmployeeId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

    public class GetCustomerReviewOrderFormParameter
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
    }

    public class GetCancelOrderFormParameter
    {
        public int OrderId { get; set; }
        public int UserId { get; set; }
    }

    public class GetAddToInventoryFormParameter
    {
        public int UserId { get; set; }
        public int MedicineId { get; set; }
        public int Quantity { get; set; }
    }

    public class GetRecommendationFormParameter
    {
        public int UserId { get; set; }
        public int MedicineId { get; set; }
    }

    public class GetMapPharmacy
    {
        public int PharmacyId { get; set; }
    }
    public class GetPromotionFormParameter
    {
        public int PromotionId { get; set; }
        public int UserId { get; set; }
        public bool IsAdmin { get; set; }
    }

    public class GetAddInventoryParamter
    {
        public int UserId { get; set; }

    }

    public class GetCartParameter
    {
        public int UserId { get; set; }
        public int MedicineId { get; set; }
        public string SessionId { get; set; }
    }

    public class GetReviewParameter
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int MedicineId { get; set; }
        public string Review { get; set; }
        public int Rating { get; set; }
    }

    public class GetAddEmployeeParameter
    {
        public string Address { get; set; }
        public int UserId { get; set; }
        public int PharmacyId { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public float Salary { get; set; }
        public float DutyHours { get; set; }
        public int EmployeeId { get; set; }
    }

    public class GetDiseaseShowFormParameter
    {
        public int Id { get; set; }
        public bool IsInidication { get; set; }
    }

    public class GetAddMedicineDetailParameter
    {
        public int MedicineDetailId { get; set; }
    }

    public class GetAddMedicineParameter
    {
        public int MedicineId { get; set; }
    }

    public class GetAddPharmacyParameter
    {
        public int PharmacyId { get; set; }
        public int UserId { get; set; }
    }

    public class GetAddCategoryParameter
    {
        public int CategoryId { get; set; }
    }

    public class GetAddBrandParameter
    {
        public int BrandId { get; set; }
    }

    public class GetAddDiseaseParameter
    {
        public int DiseaseId { get; set; }
    }

    public class GetUploadFormParameter
    {
        public string ActionUrl { get; set; }
        public int Id { get; set; }
        public int Limit { get; set; }
    }

    public class GetAddCompanyParameter
    {
        public int CompanyId { get; set; }
    }

    public class GetSearhParameter
    {
        public int PostId { get; set; }
        public int TagId { get; set; }
        public int UserId { get; set; }
        public string Parameter { get; set; }
        public int Page { get; set; }
        public int Size { get; set; }
        public string Type { get; set; }
        public bool IsAdmin { get; set; }
        public int PharmacyId { get; set; }
    }
    public class GetUserFormParameter
    {
        public int UserId { get; set; }
    }

    public class GetSingleRowParameter
    {
        public int Id { get; set; }
        public int UserId { get; set; }
    }
}