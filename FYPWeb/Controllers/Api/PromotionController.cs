﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Web.Http;
using FYPWeb.Repository.Repository;
using Model.Entity;

namespace FYPWeb.Controllers.Api
{
    public class PromotionController : ApiController
    {
        private readonly PromotionRepository _promotionRepository;

        public PromotionController(PromotionRepository promotionRepository)
        {
            _promotionRepository = promotionRepository;
        }

        [HttpPost]
        public IHttpActionResult GetNearestPromotionOffers([FromBody]GetNearestPromotionOffersParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.lat > 0 && parameter.lng > 0)
                {
                    var list = _promotionRepository.GetPromotionsFromNear(parameter.lat, parameter.lng);
                    return Ok(new
                    {
                        result = list.Select(p => new
                        {
                            EndDate = p.EndDate.ToShortDateString(),
                            p.Code,
                            p.Pharmacy.ShopName,
                            p.PercentageOff,
                            p.Name,
                            p.Pharmacy.User.Username
                        }),
                        message = "Success",
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }
        [HttpPost]
        public IHttpActionResult StartPromotion([FromBody] SavePromotionParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.PromotionId > 0)
                {
                    var p = _promotionRepository.GetById(parameter.PromotionId);
                    if (p != null)
                    {
                        p.StartDate = parameter.StartDate;
                        p.EndDate = parameter.EndDate.AddDays(1).AddSeconds(-1);
                        _promotionRepository.Update(p);
                        return Ok(new
                        {
                            p.Id,
                            message = "Success"

                        });
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult StopPromotion([FromBody] SavePromotionParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.PromotionId > 0)
                {
                    var p = _promotionRepository.GetById(parameter.PromotionId);
                    if (p != null)
                    {
                        p.StartDate = DateTime.UtcNow;
                        p.EndDate = DateTime.UtcNow;
                        _promotionRepository.Update(p);
                        return Ok(new
                        {
                            p.Id,
                            message = "Success"

                        });
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult SavePromotion([FromBody] SavePromotionParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.PharmacyId > 0)
                {
                    if (_promotionRepository.IsCheck(pr => pr.Code == parameter.Code))
                    {
                        return Ok(new
                        {
                            message = "Error"

                        });
                    }
                    var p = new Promotion()
                    {
                        Description = parameter.Description,
                        EndDate = DateTime.UtcNow,
                        StartDate = DateTime.UtcNow,
                        PercentageOff = parameter.PercentOff,
                        Name = parameter.Name,
                        PharmacyId = parameter.PharmacyId,
                        Code = parameter.Code
                    };
                    _promotionRepository.Add(p);
                    return Ok(new
                    {
                        p.Id,
                        message = "Success"

                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult UpdatePromotion([FromBody] SavePromotionParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.PharmacyId > 0)
                {
                    var p = _promotionRepository.GetById(parameter.PromotionId);
                    
                    if (p != null)
                    {
                        if (_promotionRepository.IsCheck(pr => pr.Code == parameter.Code && pr.Id != parameter.PromotionId))
                        {
                            return Ok(new
                            {
                                message = "Error"

                            });
                        }
                        p.Description = parameter.Description;
                        p.PharmacyId = parameter.PharmacyId;
                        p.PercentageOff = parameter.PercentOff;
                        p.Name = parameter.Name;
                        _promotionRepository.Update(p);
                        return Ok(new
                        {
                            p.Id,
                            message = "Success"

                        });
                    }
                    
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult RedeemPromotion([FromBody] SavePromotionParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.PharmacyId > 0 && !string.IsNullOrEmpty(parameter.Code))
                {
                    var pro = _promotionRepository.Get(p =>
                        p.PharmacyId == parameter.PharmacyId && p.Code == parameter.Code && p.EndDate > DateTime.Now);
                    if (pro != null)
                    {
                        return Ok(new
                        {
                            Id= pro.Id,
                            Promotion = pro.Name + " - %" + pro.PercentageOff.ToString("F"),
                            message = "Success"
                        });
                    }
                    return Ok(new
                    {
                        message = "Error"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

    }

    public class GetNearestPromotionOffersParameter
    {
        public float lat { get; set; }
        public float lng { get; set; }
    }

    public class SavePromotionParameter
    {
        public int PromotionId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public float PercentOff { get; set; }
        public int PharmacyId { get; set; }
        public bool IsStart { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}