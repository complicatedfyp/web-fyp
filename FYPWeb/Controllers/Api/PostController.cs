﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Http;
using FYPWeb.Repository.Repository;

namespace FYPWeb.Controllers.Api
{
    public class PostController : ApiController
    {
        private readonly PostThreadRepository _postThreadRepository;

        public PostController(PostThreadRepository postThreadRepository)
        {
            _postThreadRepository = postThreadRepository;
        }

        [HttpPost]
        public IHttpActionResult DeletePost([FromBody] DeletePostParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.Id > 0)
                {
                    var p = _postThreadRepository.GetById(parameter.Id);
                    if (p != null)
                    {
                        _postThreadRepository.Delete(p);
                        return Ok(new
                        {
                            message = "Success"
                        });
                    }
                    
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }
    }

    public class DeletePostParameter
    {
        public int Id { get; set; }
    }
}