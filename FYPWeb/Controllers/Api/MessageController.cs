﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Http;
using FYPWeb.Repository.Repository;

namespace FYPWeb.Controllers.Api
{
    public class MessageController : ApiController
    {
        private readonly MessageRepository _messageRepository;

        public MessageController(MessageRepository messageRepository)
        {
            _messageRepository = messageRepository;
        }

        [HttpPost]
        public IHttpActionResult GetMessagesByOrderId([FromBody] GetMessagesByOrderIdParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.OrderId > 0 && parameter.Page > 0)
                {
                    var list = _messageRepository.GetMany(p => p.OrderId == parameter.OrderId, parameter.Page, 10,
                        p => p.CreatedOn, true).ToList().Select(p => new
                    {
                        name = p.SenderUser.ToString(),
                        picture = p.SenderUser.UserImage,
                        content = p.Content,
                        date = p.CreatedOn,
                        sId = p.SenderUserId,
                        rId = p.RecieverUserId
                    });

                    return Ok(new
                    {
                        result = list,
                        message = "Success"
                    });
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            return InternalServerError();
        }
    }

    public class GetMessagesByOrderIdParameter
    {
        public int OrderId { get; set; }
        public int Page { get; set; }
    }
}