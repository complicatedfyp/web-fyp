﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Http;
using FYPWeb.Repository.Repository;
using FYPWeb.Services.Util;
using Model.Entity;

namespace FYPWeb.Controllers.Api
{
    public class InventoryController : ApiController
    {
        private readonly InventoryTrackRepository _inventoryTrackRepository;
        private readonly DrugRepository _drugRepository;
        public InventoryController(InventoryTrackRepository inventoryTrackRepository, DrugRepository drugRepository)
        {
            _inventoryTrackRepository = inventoryTrackRepository;
            _drugRepository = drugRepository;
        }
        [HttpPost]
        public IHttpActionResult GetAllMedicines([FromBody] GetMedicineParameter parameter)
        {
            try
            {
                var page = parameter?.Page > 0 ? parameter.Page : 1;
                if (string.IsNullOrEmpty(parameter?.Search))
                {
                    var list = _inventoryTrackRepository.GetDrugsByInventory("", 0, 0, 0, 0, 0, "", 0, page).ToList().Select(p => p.ForJson());
                    var pageCount = _inventoryTrackRepository.GetDrugsByInventoryCount("",0,0,00,0,0,"");
                    return Ok(new
                    {
                        result = list,
                        pageCount = pageCount,
                        message = "Success",
                    });
                }
                else
                {
                    var list = _inventoryTrackRepository.GetDrugsByInventory(parameter.Search,0,0,0,0,0,"",0,page).ToList().Select(p => p.ForJson());
                    var pageCount = _inventoryTrackRepository.GetDrugsByInventoryCount(parameter.Search, 0, 0, 00, 0, 0, "");
                    return Ok(new
                    {
                        result = list,
                        pageCount = pageCount,
                        message = "Success",
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult SearchMedicine([FromBody] SearchParameter parameter)
        {
            try
            {
                if (!string.IsNullOrEmpty(parameter?.Parameter) && parameter.PharmacyId > 0)
                {
                    var list = _inventoryTrackRepository.GetDrugsByPharmacyIdSearch(parameter.PharmacyId,parameter.Parameter).Select(p => new
                    {
                        Name = p.Drug.Brand.BrandName + " " + p.Drug.DrugName + " - Rs. " + p.Price.ToString("F"),
                        Id = p.Id
                    });
                    return Ok(new
                    {
                        result = list,
                        message = "Success"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetInventory([FromBody] SaveInventoryParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.PharmacyId > 0)
                {
                    if (!string.IsNullOrEmpty(parameter.Search))
                    {
                        var list = _inventoryTrackRepository.GetDrugsByPharmacyIdSearch(parameter.PharmacyId,parameter.Search, parameter.Page)
                            .Select(p => new
                            {
                                Name = p.Drug.Brand.BrandName + " " + p.Drug.DrugName,
                                Id = p.Id,
                                DrugId = p.DrugId,
                                Drug = p.Drug.ForJson(),
                                Pharmacy = p.Pharmacy.ForJson(),
                                PharmacyId = p.PharmacyId,
                                Price = p.Price,
                                Quantity = p.TotalCount
                            });
                        var count = _inventoryTrackRepository.GetDrugsByPharmacyIdSearchCount(parameter.PharmacyId,parameter.Search);
                        return Ok(new
                        {
                            message = "Success",
                            result = list,
                            pageCount = count
                        });
                    }
                    else
                    {
                        var list = _inventoryTrackRepository.GetDrugsByPharmacyId(parameter.PharmacyId, parameter.Page).ToList()
                            .Select(p => new
                            {
                                Name = p.Drug.Brand.BrandName + " " + p.Drug.DrugName,
                                Id = p.Id,
                                DrugId = p.DrugId,
                                Drug = p.Drug.ForJson(),
                                Pharmacy = p.Pharmacy.ForJson(),
                                PharmacyId = p.PharmacyId,
                                Price = p.Price,
                                Quantity = p.TotalCount
                            });
                        var count = _inventoryTrackRepository.GetDrugsByPharmacyIdCount(parameter.PharmacyId);
                        return Ok(new
                        {
                            message = "Success",
                            result = list,
                            pageCount = count
                        });

                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }


        [HttpPost]
        public IHttpActionResult SaveInventory([FromBody] SaveInventoryParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.DrugId > 0 && parameter.PharmacyId > 0)
                {

                    var d = _drugRepository.GetById(parameter.DrugId);
                    var inventory = new InventoryTrack()
                    {
                        DrugId = parameter.DrugId,
                        PharmacyId = parameter.PharmacyId,
                        TotalCount = parameter.AddType == 0 ? parameter.Quantity * d.ItemsInPack : parameter.Quantity,
                    };
                    if (_inventoryTrackRepository.UpdateTrack(inventory.PharmacyId, inventory.DrugId,inventory.TotalCount))
                    {
                        return Ok(new
                        {
                            result = new { inventory.Id },
                            message = "Success"
                        });
                    }
                    return Ok(new
                    {
                        message = "Error"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }


        [HttpPost]
        public IHttpActionResult SavePrice([FromBody] SaveInventoryParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.Id > 0)
                {

                    var d = _inventoryTrackRepository.GetById(parameter.Id);
                    d.Price = parameter.Price;
                    _inventoryTrackRepository.Update(d);
                    return Ok(new
                    {
                        message = "Success"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult RemoveInventory([FromBody] SaveInventoryParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.Id > 0)
                {
                    var invent = _inventoryTrackRepository.GetById(parameter.Id);
                    var d = _drugRepository.GetById(invent.DrugId);
                    var inventory = new InventoryTrack()
                    {
                        DrugId = invent.DrugId,
                        PharmacyId = invent.PharmacyId,
                        TotalCount = parameter.AddType == 0 ? parameter.Quantity * d.ItemsInPack : parameter.Quantity,
                    };
                    if (_inventoryTrackRepository.UpdateTrack(inventory.PharmacyId, inventory.DrugId, -inventory.TotalCount))
                    {
                        return Ok(new
                        {
                            result = new { inventory.Id },
                            message = "Success"
                        });
                    }
                    return Ok(new
                    {
                        message = "Error"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetTotalCount([FromBody] GetTotalCountParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.PharmacyId > 0)
                {
                    var count = string.IsNullOrEmpty(parameter.Parameter)
                        ? _inventoryTrackRepository.GetTotalMedicineCountByPharmacy(parameter.PharmacyId)
                        : _inventoryTrackRepository.GetTotalMedicineCountByPharmacy(parameter.PharmacyId, parameter.Parameter);
                    return Ok(new
                    {
                        result = count,
                        message = "Success"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

    }

    public class GetTotalCountParameter
    {
        public int PharmacyId { get; set; }
        public string Parameter { get; set; }
    }

    public class SaveInventoryParameter
    {
        public int Page { get; set; }
        public string Search { get; set; }
        public int Id { get; set; }
        public int DrugId { get; set; }
        public float Price { get; set; }
        public int PharmacyId { get; set; }
        public int Quantity { get; set; }
        public int AddType { get; set; }
    }
}