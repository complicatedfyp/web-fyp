﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Http;
using FYPWeb.Enum;
using FYPWeb.Hub;
using FYPWeb.Repository.Repository;
using FYPWeb.Repository.RepositoryViewModel;
using FYPWeb.Service;
using FYPWeb.ViewModels;
using Model.Entity;

namespace FYPWeb.Controllers.Api
{
    public class OrderController : ApiController
    {
        private readonly UserRepository _userRepository;
        private readonly PharmacyRepository _pharmacyRepository;
        private readonly OrderRepository _orderRepository;
        private readonly OrderDetailRepository _orderDetailRepository;
        private readonly InventoryTrackRepository _inventoryTrackRepository;
        private readonly DrugCartRepository _drugCartRepository;
        private readonly NotificationRepository _notificationRepository;
        private readonly OrderPrescriptionRepository _orderPrescriptionRepository;
        public OrderController(OrderRepository orderRepository, OrderDetailRepository orderDetailRepository, UserRepository userRepository, PharmacyRepository pharmacyRepository, DrugCartRepository drugCartRepository, NotificationRepository notificationRepository, InventoryTrackRepository inventoryTrackRepository, OrderPrescriptionRepository orderPrescriptionRepository)
        {
            _orderRepository = orderRepository;
            _orderDetailRepository = orderDetailRepository;
            _userRepository = userRepository;
            _pharmacyRepository = pharmacyRepository;
            _drugCartRepository = drugCartRepository;
            _notificationRepository = notificationRepository;
            _inventoryTrackRepository = inventoryTrackRepository;
            _orderPrescriptionRepository = orderPrescriptionRepository;
        }

        [HttpPost]
        public IHttpActionResult SetPrescription([FromBody] AddToOrderParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.OrderId > 0)
                {
                    var order = _orderRepository.GetById(parameter.OrderId);
                    order.IsPrescriptionSet = true;
                    _orderRepository.Update(order);
                    return Ok(new
                    {
                        message = "Success"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult AddToOrder([FromBody] AddToOrderParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.OrderId > 0 && parameter.PharmacyId > 0 && parameter.InventoryId > 0 && parameter.Amount > 0)
                {
                    RenderService.Compile(RenderService.PrescriptionOrderSetMedicineTableRow);
                    var inventory = _inventoryTrackRepository.GetById(parameter.InventoryId);
                    var orderDetail =
                        _orderDetailRepository.Get(p => p.OrderId == parameter.OrderId && p.DrugId == inventory.DrugId);
                    if (orderDetail != null)
                    {
                        orderDetail.Quantity += parameter.Amount;
                        _orderDetailRepository.Update(orderDetail);
                    }
                    else
                    {
                        var d = new OrderDetail()
                        {
                            OrderId = parameter.OrderId,
                            DrugId = inventory.DrugId,
                            Quantity = parameter.Amount,
                            Price = inventory.Price,
                        };
                        _orderDetailRepository.Add(d);
                    }
                    var order = _orderRepository.GetById(parameter.OrderId);
                    var list = RenderService.Run(RenderService.PrescriptionOrderSetMedicineTableRow,
                        new PrescriptionOrderSetMedicineTableRowViewModel()
                        {
                            OrderDetails = order.OrderDetails
                        });
                    return Ok(new
                    {
                        message = "Success",
                        result = list,
                        TotalPrice = order.OrderDetails.Sum(p => p.Price * p.Quantity),
                        TotalQuantity = order.OrderDetails.Sum(p => p.Quantity),
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult CompleteOrder([FromBody] CancelOrderParameter parameter)
        {
            if (parameter != null && parameter.OrderId > 0)
            {
                var order = _orderRepository.GetById(parameter.OrderId);
                if (order != null)
                {
                    if (order.IsAccepted && !order.IsCanceled && !order.IsRejected)
                    {
                        order.IsCompleted = true;
                        order.CompletedAt = DateTime.Now;
                        _orderRepository.Update(order);

                        var notification = new Notification()
                        {
                            Url = "/Customer/Order?type=Completed",
                            SenderUserId = order.Pharmacy.UserId,
                            Body = " has Completed your Order# " + order.Id,
                            CreatedAt = DateTime.Now,
                            IsPharmacy = true,
                            IsPost = false,
                            IsSeen = false,
                            ReceiverUserId = order.UserId.GetValueOrDefault(),
                            NotificationType = (int)NotificationType.Info,
                        };
                        _notificationRepository.Add(notification);

                        NotificationHub.StaticSendNotification(notification, notification.ReceiverUserId, notification.NotificationType);

                        return Ok(new
                        {
                            message = "Success"
                        });
                    }
                }
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult RejectOrder([FromBody] CancelOrderParameter parameter)
        {
            if (parameter != null && parameter.OrderId > 0)
            {
                var order = _orderRepository.GetById(parameter.OrderId);
                if (order != null)
                {
                    if (!order.IsAccepted && !order.IsCanceled)
                    {
                        order.IsRejected = true;
                        order.RejectedAt = DateTime.Now;
                        order.CommentsRejected = parameter.Comment;
                        _orderRepository.Update(order);
                        var notification = new Notification()
                        {
                            Url = "/Customer/Order?type=Rejected",
                            SenderUserId = order.Pharmacy.UserId,
                            Body = " has Rejected your Order# " + order.Id,
                            CreatedAt = DateTime.Now,
                            IsPharmacy = true,
                            IsPost = false,
                            IsSeen = false,
                            ReceiverUserId = order.UserId.GetValueOrDefault(),
                            NotificationType = (int)NotificationType.Error,
                        };
                        _notificationRepository.Add(notification);
                        NotificationHub.StaticSendNotification(notification, notification.ReceiverUserId, notification.NotificationType);

                        return Ok(new
                        {
                            message = "Success"
                        });
                    }
                }
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult AcceptOrder([FromBody] CancelOrderParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.OrderId > 0 && parameter.EmployeeId > 0)
                {
                    var order = _orderRepository.GetById(parameter.OrderId);
                    if (order != null)
                    {
                        if (!order.IsAccepted)
                        {
                            var list = order.OrderDetails;
                            var listOfInventory =
                                _inventoryTrackRepository.GetMedicineInventoryByPharmacy(order.PharmacyId.GetValueOrDefault(),list.Select(p => p.DrugId.GetValueOrDefault()).ToArray(),1,100);
                            var inventoryRepositoryViewModels = listOfInventory.ToArray();
                            var isHaving = list.Aggregate(true, (current, orderDetail) => current && inventoryRepositoryViewModels.Any(p => p.DrugId == orderDetail.DrugId && p.TotalCount >= orderDetail.Quantity));
                            if (!isHaving)
                                return Ok(new
                                {
                                    message = "Error"
                                });
                            order.IsAccepted = true;
                            order.AcceptedAt = DateTime.Now;
                            order.EmployeeId = parameter.EmployeeId;
                            order.ExpectedDeliveryTime = DateTime.Now.AddMinutes(parameter.AcceptedTime);
                            _orderRepository.Update(order);
                            foreach (var orderDetail in list)
                            {
                                var inv = new InventoryTrack()
                                {
                                    PharmacyId = order.PharmacyId.GetValueOrDefault(),
                                    DrugId = orderDetail.DrugId.GetValueOrDefault(),
                                    TotalCount = -orderDetail.Quantity,
                                };
                                _inventoryTrackRepository.UpdateTrack(inv.PharmacyId, inv.DrugId, inv.TotalCount);
                            }
                            var notification = new Notification()
                            {
                                Url = "/Customer/Order?type=Active",
                                SenderUserId = order.Pharmacy.UserId,
                                Body = " has Accepted your Order# " + order.Id,
                                CreatedAt = DateTime.Now,
                                IsPharmacy = true,
                                IsPost = false,
                                IsSeen = false,
                                ReceiverUserId = order.UserId.GetValueOrDefault(),
                                NotificationType = (int)NotificationType.Info,
                            };
                            _notificationRepository.Add(notification);
                            NotificationHub.StaticSendNotification(notification, notification.ReceiverUserId, notification.NotificationType);

                            return Ok(new
                            {
                                message = "Success"
                            });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult CancelOrder([FromBody] CancelOrderParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.OrderId > 0)
                {
                    var order = _orderRepository.GetById(parameter.OrderId);
                    if (order != null)
                    {
                        if (order.IsAccepted)
                        {
                            if (order.OrderedAt.AddMinutes(5) <= DateTime.Now)
                            {
                                order.IsCanceled = true;
                                order.CancelledAt = DateTime.Now;
                                order.CommentsCancelled = parameter.Comment;
                                foreach (var orderDetail in order.OrderDetails)
                                {
                                    var inv = new InventoryTrack()
                                    {
                                        PharmacyId = order.PharmacyId.GetValueOrDefault(),
                                        DrugId = orderDetail.DrugId.GetValueOrDefault(),
                                        TotalCount = orderDetail.Quantity,
                                    };
                                    _inventoryTrackRepository.UpdateTrack(inv.PharmacyId, inv.DrugId, inv.TotalCount);
                                }
                                _orderRepository.Update(order);
                                var notification = new Notification()
                                {
                                    Url = "/Pharmacist/Order?type=Cancelled",
                                    SenderUserId = order.UserId,
                                    Body = " has Cancelled the Order# " + order.Id,
                                    CreatedAt = DateTime.Now,
                                    IsPharmacy = false,
                                    IsPost = false,
                                    IsSeen = false,
                                    IsUser = true,
                                    ReceiverUserId = order.Pharmacy.UserId.GetValueOrDefault(),
                                    NotificationType = (int)NotificationType.Warning,
                                };
                                _notificationRepository.Add(notification);
                                NotificationHub.StaticSendNotification(notification, notification.ReceiverUserId, notification.NotificationType);
                                return Ok(new
                                {
                                    message = "Success"
                                });
                            }
                            return Ok(new
                            {
                                message = "Error"
                            });
                        }
                        order.IsCanceled = true;
                        order.CancelledAt = DateTime.Now;
                        order.CommentsCancelled = parameter.Comment;
                        foreach (var orderDetail in order.OrderDetails)
                        {
                            var inv = new InventoryTrack()
                            {
                                PharmacyId = order.PharmacyId.GetValueOrDefault(),
                                DrugId = orderDetail.DrugId.GetValueOrDefault(),
                                TotalCount = orderDetail.Quantity,
                            };
                            _inventoryTrackRepository.UpdateTrack(inv.PharmacyId, inv.DrugId, inv.TotalCount);
                        }
                        _orderRepository.Update(order);
                        var notification2 = new Notification()
                        {
                            Url = "/Pharmacist/Order?type=Cancelled",
                            SenderUserId = order.UserId,
                            Body = " has Cancelled the Order# " + order.Id,
                            CreatedAt = DateTime.Now,
                            IsPharmacy = false,
                            IsPost = false,
                            IsSeen = false,
                            IsUser = true,
                            ReceiverUserId = order.Pharmacy.UserId.GetValueOrDefault(),
                            NotificationType = (int)NotificationType.Warning,
                        };
                        _notificationRepository.Add(notification2);
                        NotificationHub.StaticSendNotification(notification2, notification2.ReceiverUserId, notification2.NotificationType);
                        return Ok(new
                        {
                            message = "Success"
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult PlaceOrder([FromBody] PlaceOrderParameter parameter)
        {
            try
            {
                if (parameter?.UserId > 0 && parameter.PharmacyId.Any() && !string.IsNullOrEmpty(parameter.X) && !string.IsNullOrEmpty(parameter.Y) && !string.IsNullOrEmpty(parameter.Address))
                {
                    var user = _userRepository.GetById(parameter.UserId);
                    if (user == null)
                        return InternalServerError();
                    for (int i = 0; i < parameter.PharmacyId.Length; i++)
                    {
                        var pharmacy = _pharmacyRepository.GetById(parameter.PharmacyId[i]);
                        if (pharmacy == null)
                            return InternalServerError();
                        var order = new Order
                        {
                            UserId = parameter.UserId,
                            DeliveryLocation = parameter.Address,
                            DeliveryLocationX = parameter.X,
                            DeliveryLocationY = parameter.Y,
                            DeliveryCharges = pharmacy.DeliveryCharges,
                            PromotionId = null,
                            IsAccepted = false,
                            IsCanceled = false,
                            PharmacyId = parameter.PharmacyId[i],
                            AcceptedAt = null,
                            CancelledAt = null,
                            FromLocation = pharmacy.ShopAddress,
                            FromLocationX = pharmacy.ShopLocationX + "",
                            FromLocationY = pharmacy.ShopLocationY + "",
                            OrderedAt = DateTime.Now,
                            IsPrescription = false
                        };
                        if (parameter.PromotionId[i] > 0)
                            order.PromotionId = parameter.PromotionId[i];
                        _orderRepository.Add(order);
                        var list = new List<OrderDetail>();
                        var cart = _drugCartRepository.GetByUserIdAndPharmacyId(parameter.UserId,parameter.PharmacyId[i]);
                        foreach (var drugCart in cart)
                        {
                            var d = new OrderDetail
                            {
                                DrugId = drugCart.Inventory.DrugId,
                                OrderId = order.Id,
                                Price = drugCart.Inventory.Price,
                                Quantity = drugCart.Quantity
                            };
                            list.Add(d);
                        }
                        _drugCartRepository.DeleteByUserIdAndPharmacyId(parameter.UserId, parameter.PharmacyId[i]);
                        _orderDetailRepository.AddRange(list);
                        var notification2 = new Notification()
                        {
                            Url = "/Pharmacist/Order?type=Pending",
                            SenderUserId = parameter.UserId,
                            Body = " has placed an Order with you",
                            CreatedAt = DateTime.Now,
                            IsPharmacy = false,
                            IsPost = false,
                            IsSeen = false,
                            IsUser = true,
                            ReceiverUserId = pharmacy.UserId.GetValueOrDefault(),
                            NotificationType = (int)NotificationType.Info,
                        };
                        _notificationRepository.Add(notification2);
                        NotificationHub.StaticSendNotification(notification2, notification2.ReceiverUserId, notification2.NotificationType);
                        _drugCartRepository.DeleteByUserIdAndPharmacyId(parameter.UserId, parameter.PharmacyId[i]);
                    }
                    return Ok(new
                    {
                        message = "Success"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }
        [HttpPost]
        public IHttpActionResult PlaceOrderPrescription([FromBody] PlaceOrderParameter parameter)
        {
            try
            {
                if (parameter?.UserId > 0 && parameter.Prescriptions.Any() && parameter.PharmacyId.Any() && !string.IsNullOrEmpty(parameter.X) && !string.IsNullOrEmpty(parameter.Y) && !string.IsNullOrEmpty(parameter.Address))
                {
                    var user = _userRepository.GetById(parameter.UserId);
                    if (user == null)
                        return InternalServerError();
                    for (int i = 0; i < parameter.PharmacyId.Length; i++)
                    {
                        var pharmacy = _pharmacyRepository.GetById(parameter.PharmacyId[i]);
                        if (pharmacy == null)
                            return InternalServerError();
                        var order = new Order
                        {
                            UserId = parameter.UserId,
                            DeliveryLocation = parameter.Address,
                            DeliveryLocationX = parameter.X,
                            DeliveryLocationY = parameter.Y,
                            DeliveryCharges = pharmacy.DeliveryCharges,
                            PromotionId = null,
                            IsAccepted = false,
                            IsCanceled = false,
                            PharmacyId = parameter.PharmacyId[i],
                            AcceptedAt = null,
                            CancelledAt = null,
                            FromLocation = pharmacy.ShopAddress,
                            FromLocationX = pharmacy.ShopLocationX + "",
                            FromLocationY = pharmacy.ShopLocationY + "",
                            OrderedAt = DateTime.Now,
                            IsPrescription = true
                        };
                        if (parameter.PromotionId[i] > 0)
                            order.PromotionId = parameter.PromotionId[i];
                        _orderRepository.Add(order);
                        var list = parameter.Prescriptions.Select(prescription => new OrderPrescription()
                            {
                                OrderId = order.Id,
                                PrescriptionId = prescription
                            })
                            .ToList();
                        _orderPrescriptionRepository.AddRange(list);
                        var notification2 = new Notification()
                        {
                            Url = "/Pharmacist/Order?type=Pending",
                            SenderUserId = parameter.UserId,
                            Body = " has placed an Order with you",
                            CreatedAt = DateTime.Now,
                            IsPharmacy = false,
                            IsPost = false,
                            IsSeen = false,
                            IsUser = true,
                            ReceiverUserId = pharmacy.UserId.GetValueOrDefault(),
                            NotificationType = (int)NotificationType.Info,
                        };
                        _notificationRepository.Add(notification2);
                        NotificationHub.StaticSendNotification(notification2, notification2.ReceiverUserId, notification2.NotificationType);
                        _drugCartRepository.DeleteByUserIdAndPharmacyId(parameter.UserId, parameter.PharmacyId[i]);
                    }
                    return Ok(new
                    {
                        message = "Success"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetAllActiveOrderByPharmacy([FromBody] GetAllOrderParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.PharmacyId > 0)
                {
                    if (parameter.Page <= 0) parameter.Page = 1;
                    var list = _orderRepository.GetActiveOrdersByPharmacyId(parameter.PharmacyId, parameter.Page).ToList().Select(p => p.ForJson());
                    var count = _orderRepository.GetActiveOrdersByPharmacyIdCount(parameter.PharmacyId);
                    return Ok(new
                    {
                        message = "Success",
                        result = list,
                        pageCount = count
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }
        [HttpPost]
        public IHttpActionResult GetAllRejectedOrderByPharmacy([FromBody] GetAllOrderParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.PharmacyId > 0)
                {
                    if (parameter.Page <= 0) parameter.Page = 1;
                    var list = _orderRepository.GetRejectedOrdersByPharmacyId(parameter.PharmacyId, parameter.Page).ToList().Select(p => p.ForJson());
                    var count = _orderRepository.GetRejectedOrdersByPharmacyIdCount(parameter.PharmacyId);
                    return Ok(new
                    {
                        message = "Success",
                        result = list,
                        pageCount = count
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }
        [HttpPost]
        public IHttpActionResult GetAllCompletedOrderByPharmacy([FromBody] GetAllOrderParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.PharmacyId > 0)
                {
                    if (parameter.Page <= 0) parameter.Page = 1;
                    var list = _orderRepository.GetCompletedOrdersByPharmacyId(parameter.PharmacyId, parameter.Page).ToList().Select(p => p.ForJson());
                    var count = _orderRepository.GetCompletedOrdersByPharmacyIdCount(parameter.PharmacyId);
                    return Ok(new
                    {
                        message = "Success",
                        result = list,
                        pageCount = count
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }
        [HttpPost]
        public IHttpActionResult GetAllPendingOrderByPharmacy([FromBody] GetAllOrderParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.PharmacyId > 0)
                {
                    if (parameter.Page <= 0) parameter.Page = 1;
                    var list = _orderRepository.GetPendingOrdersByPharmacyId(parameter.PharmacyId, parameter.Page).ToList().Select(p => p.ForJson());
                    var count = _orderRepository.GetPendingOrdersByPharmacyIdCount(parameter.PharmacyId);
                    return Ok(new
                    {
                        message = "Success",
                        result = list,
                        pageCount = count
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }
        [HttpPost]
        public IHttpActionResult GetAllCancelledOrderByPharmacy([FromBody] GetAllOrderParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.PharmacyId > 0)
                {
                    if (parameter.Page <= 0) parameter.Page = 1;
                    var list = _orderRepository.GetCancelledOrdersByPharmacyId(parameter.PharmacyId, parameter.Page).ToList().Select(p => p.ForJson());
                    var count = _orderRepository.GetCancelledOrdersByPharmacyIdCount(parameter.PharmacyId);
                    return Ok(new
                    {
                        message = "Success",
                        result = list,
                        pageCount = count
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetAllActiveOrderByUser([FromBody] GetAllOrderParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.UserId > 0)
                {
                    if (parameter.Page <= 0) parameter.Page = 1;
                    var list = _orderRepository.GetActiveOrdersByUserId(parameter.UserId, parameter.Page).ToList().Select(p => p.ForJson());
                    var count = _orderRepository.GetActiveOrdersByUserIdCount(parameter.UserId);
                    return Ok(new
                    {
                        message = "Success",
                        result = list,
                        pageCount = count
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }
        [HttpPost]
        public IHttpActionResult GetAllRejectedOrderByUser([FromBody] GetAllOrderParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.UserId > 0)
                {
                    if (parameter.Page <= 0) parameter.Page = 1;
                    var list = _orderRepository.GetRejectedOrdersByUserId(parameter.UserId, parameter.Page).ToList().Select(p => p.ForJson());
                    var count = _orderRepository.GetRejectedOrdersByUserIdCount(parameter.UserId);
                    return Ok(new
                    {
                        message = "Success",
                        result = list,
                        pageCount = count
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }
        [HttpPost]
        public IHttpActionResult GetAllCompletedOrderByUser([FromBody] GetAllOrderParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.UserId > 0)
                {
                    if (parameter.Page <= 0) parameter.Page = 1;
                    var list = _orderRepository.GetCompletedOrdersByUserId(parameter.UserId, parameter.Page).ToList().Select(p => p.ForJson());
                    var count = _orderRepository.GetCompletedOrdersByUserIdCount(parameter.UserId);
                    return Ok(new
                    {
                        message = "Success",
                        result = list,
                        pageCount = count
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }
        [HttpPost]
        public IHttpActionResult GetAllPendingOrderByUser([FromBody] GetAllOrderParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.UserId > 0)
                {
                    if (parameter.Page <= 0) parameter.Page = 1;
                    var list = _orderRepository.GetPendingOrdersByUserId(parameter.UserId, parameter.Page).ToList().Select(p => p.ForJson());
                    var count = _orderRepository.GetPendingOrdersByUserIdCount(parameter.UserId);
                    return Ok(new
                    {
                        message = "Success",
                        result = list,
                        pageCount = count
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }
        [HttpPost]
        public IHttpActionResult GetAllCancelledOrderByUser([FromBody] GetAllOrderParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.UserId > 0)
                {
                    if (parameter.Page <= 0) parameter.Page = 1;
                    var list = _orderRepository.GetCancelledOrdersByUserId(parameter.UserId, parameter.Page).ToList().Select(p => p.ForJson());
                    var count = _orderRepository.GetCancelledOrdersByUserIdCount(parameter.UserId);
                    return Ok(new
                    {
                        message = "Success",
                        result = list,
                        pageCount = count
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }
    }

    public class GetAllOrderParameter
    {
        public int Page { get; set; }
        public int UserId { get; set; }
        public int PharmacyId { get; set; }
    }

    public class AddToOrderParameter
    {
        public int PharmacyId { get; set; }
        public int InventoryId { get; set; }
        public int Amount { get; set; }
        public int OrderId { get; set; }
    }

    public class CancelOrderParameter
    {
        public int OrderId { get; set; }
        public string Comment { get; set; }
        public int EmployeeId { get; set; }
        public float AcceptedTime { get; set; }
    }

    public class PlaceOrderParameter
    {
        public int UserId { get; set; }
        public int[] PharmacyId { get; set; }
        public int[] PromotionId { get; set; }
        public int[] Prescriptions { get; set; }
        public string Address { get; set; }
        public string X { get; set; }
        public string Y { get; set; }
    }
}