﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using FYPWeb.Repository.Repository;

namespace FYPWeb.Controllers.Api
{
    public class NotificationController : ApiController
    {
        private readonly NotificationRepository _notificationRepository;
        public NotificationController(NotificationRepository notificationRepository)
        {
            _notificationRepository = notificationRepository;
        }
        public IHttpActionResult GetNotificationAdmin([FromBody] GetNotificationParameter parameter)
        {
            if (parameter.Id > 0)
            {
                   
            }
            return Ok(new
            {
                result = "",
                message = "Error : Id incorrect"
            });
        }
        [HttpPost]
        public IHttpActionResult GetNotificationUnSeenCount([FromBody] GetNotificationParameter parameter)
        {
            var i = 0;
            if (parameter.UserId > 0)
            {
                i = _notificationRepository.GetUnSeenCountByUserId(parameter.UserId);
            }
            return Ok(new
            {
                result = i,
                message = "Success"
            });
        }
    }

    public class GetNotificationParameter
    {
        public int Id { get; set; }
        public int UserId { get; set; }
    }
}