﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using FYPWeb.Repository.Repository;
using FYPWeb.Services.Util;
using Model.Entity;

namespace FYPWeb.Controllers.Api
{
    public class MedicineDetailController : ApiController
    {
        private readonly DrugDetailRepository _drugDetailRepository;
        private readonly DrugDetailDiseaseRepository _drugDetailDiseaseRepository;
        public MedicineDetailController(DrugDetailRepository drugDetailRepository, DrugDetailDiseaseRepository drugDetailDiseaseRepository)
        {
            _drugDetailRepository = drugDetailRepository;
            _drugDetailDiseaseRepository = drugDetailDiseaseRepository;
        }


        [HttpPost]
        public IHttpActionResult SearchMedicineDetail([FromBody] SearchParameter paramter)
        {
            try
            {
                if (!string.IsNullOrEmpty(paramter?.Parameter))
                {
                    var d = _drugDetailRepository.GetDrugDetailsSearch(paramter.Parameter);
                    var listOfCompany = d.Select(p => new { Id = p.Id, Name = p.Name }).ToList();
                    return Ok(new
                    {
                        result = listOfCompany,
                        message = "Success"
                    });
                }
                return Ok(new
                {
                    result = "",
                    message = "Success"
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult SaveMedicineDetail([FromBody] SaveDetailParameter paramter)
        {
            try
            {
                if (paramter != null)
                {
                    var d = new DrugDetail()
                    {
                        ImageLink = UtilService.DefaultImagePath,
                        Name = paramter.Name,
                        OverView = paramter.Overview,
                    };
                    var listInt =
                        paramter.DiseaseInteractions?.Select(
                            p => new DrugDetailDisease() { DiseaseId = p,IsIndication = true}).ToList() ??
                        new List<DrugDetailDisease>();
                    var listCont =
                        paramter.DiseaseContradictions?.Select(
                            p => new DrugDetailDisease() { DiseaseId = p, IsIndication = false }).ToList() ??
                        new List<DrugDetailDisease>();
                    listInt.AddRange(listCont);
                    d.DrugDetailDiseases = listInt;
                    _drugDetailRepository.Add(d);
                    return Ok(new
                    {
                        result = new {Id = d.Id},
                        message = "Success"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return new InternalServerErrorResult(this);
        }

        [HttpPost]
        public IHttpActionResult UpdateMedicineDetail([FromBody] SaveDetailParameter paramter)
        {
            try
            {
                if (paramter != null && paramter.Id > 0)
                {
                    var d = _drugDetailRepository.GetById(paramter.Id);
                    if (d != null)
                    {
                        d.Name = paramter.Name;
                        d.OverView = paramter.Overview;
                        _drugDetailRepository.Update(d);

                        var listInt =
                            paramter.DiseaseInteractions?.Select(
                                p => new DrugDetailDisease() {DrugDetailId = d.Id, DiseaseId = p, IsIndication = true }).ToList();
                        var listCont =
                            paramter.DiseaseContradictions?.Select(
                                p => new DrugDetailDisease() { DrugDetailId = d.Id, DiseaseId = p, IsIndication = false }).ToList();
                        if(listInt == null)
                            listInt = new List<DrugDetailDisease>();
                        if (listCont == null)
                            listCont = new List<DrugDetailDisease>();
                        listInt.AddRange(listCont);
                        _drugDetailDiseaseRepository.UpdateAll(listInt,d.Id);
                        return Ok(new
                        {
                            result = "",
                            message = "Success"
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return new InternalServerErrorResult(this);
        }

        [HttpPost]
        public IHttpActionResult DeleteMedicineDetail([FromBody] DeleteMedicineDetailParameter paramter)
        {
            try
            {
                if (paramter != null && paramter.Id > 0)
                {
                    var d = _drugDetailRepository.GetById(paramter.Id);
                    if (d != null)
                    {
                        _drugDetailRepository.Delete(d);
                        return Ok(new
                        {
                            result = "",
                            message = "Success"
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

    }

    public class DeleteMedicineDetailParameter
    {
        public int Id { get; set; }
    }

    public class SaveDetailParameter
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Overview { get; set; }
        public int[] DiseaseInteractions { get; set; }
        public int[] DiseaseContradictions { get; set; }

    }
}