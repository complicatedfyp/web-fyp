﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using FYPWeb.Repository.Repository;

namespace FYPWeb.Controllers.Api
{
    public class PrescriptionController : ApiController
    {
        private readonly PrescriptionRepository _prescriptionRepository;

        public PrescriptionController(PrescriptionRepository prescriptionRepository)
        {
            _prescriptionRepository = prescriptionRepository;
        }

        [HttpPost]
        public IHttpActionResult DeletePrescription([FromBody] DeletePrescriptionParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.Id > 0)
                {
                    var p = _prescriptionRepository.GetById(parameter.Id);
                    if (p != null)
                    {
                        var path = p.PrescriptionPath.Split('/');
                        File.Delete(HostingEnvironment.MapPath("~/Content/PrescriptionImage") +"/"+ path.LastOrDefault());
                        _prescriptionRepository.Delete(p);
                        return Ok(new
                        {
                            message = "Success",
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }
    }

    public class DeletePrescriptionParameter
    {
        public int Id { get; set; }
    }
}