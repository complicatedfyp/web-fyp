﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Http;
using FYPWeb.Repository.Repository;
using Model.Entity;

namespace FYPWeb.Controllers.Api
{
    public class DiseaseController : ApiController
    {
        private readonly DiseaseRepository _diseaseRepository;
        private readonly DrugRepository _drugRepository;

        public DiseaseController(DiseaseRepository diseaseRepository, DrugRepository drugRepository)
        {
            _diseaseRepository = diseaseRepository;
            _drugRepository = drugRepository;
        }


        [HttpPost]
        public IHttpActionResult GetDiseaseById([FromBody] GetDiseaseParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.DiseaseId > 0)
                {
                    var disease = _diseaseRepository.GetById(parameter.DiseaseId);
                    return Ok(new
                    {
                        result = disease.ForJson(),
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetIDiseaseByDrugId([FromBody] GetDiseaseParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.Id > 0)
                {
                    var disease = _drugRepository.GetById(parameter.Id).DrugDetail.DrugDetailDiseases.Where(p => p.IsIndication).Select(p => p.Disease.ForJson());
                    return Ok(new
                    {
                        result = disease,
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }
        [HttpPost]
        public IHttpActionResult GetCDiseaseByDrugId([FromBody] GetDiseaseParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.Id > 0)
                {
                    var disease = _drugRepository.GetById(parameter.Id).DrugDetail.DrugDetailDiseases.Where(p => !p.IsIndication).Select(p => p.Disease.ForJson());
                    return Ok(new
                    {
                        result = disease,
                        message = "Success"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetAllDiseases([FromBody] GetDiseaseParameter parameter)
        {
            try
            {
                var page = parameter?.Page > 0 ? parameter.Page : 1;
                if (string.IsNullOrEmpty(parameter?.Search))
                {
                    var list = _diseaseRepository.GetDiseases(page).Select(p => p.ForJson());
                    var pageCount = _diseaseRepository.GetPaginationCount();
                    return Ok(new
                    {
                        result = list,
                        pageCount = pageCount,
                        message = "Success",
                    });
                }
                else
                {
                    var list = _diseaseRepository.GetDiseasesSearch(parameter.Search, page).Select(p => p.ForJson());
                    var pageCount = _diseaseRepository.GetDiseasesSearchCount(parameter.Search);
                    return Ok(new
                    {
                        result = list,
                        pageCount = pageCount,
                        message = "Success",
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }


        [HttpPost]
        public IHttpActionResult SearchDisease([FromBody] SearchParameter paramter)
        {
            try
            {
                if (!string.IsNullOrEmpty(paramter?.Parameter))
                {
                    var d = _diseaseRepository.GetDiseasesSearch(paramter.Parameter);
                    var listOfCompany = d.Select(p => new { Id = p.Id, Name = p.DiseaseName }).ToList();
                    return Ok(new
                    {
                        result = listOfCompany,
                        message = "Success"
                    });
                }
                return Ok(new
                {
                    result = "",
                    message = "Success"
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult SaveDisease([FromBody] SaveDiseaseParameter parameter)
        {
            try
            {
                if (parameter != null)
                {
                    var d = new Disease()
                    {
                        Description = parameter.Description,
                        DiseaseName = parameter.Name
                    };
                    _diseaseRepository.Add(d);
                    return Ok(new
                    {
                        result = d.ForJson(),
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }


        [HttpPost]
        public IHttpActionResult UpdateDisease([FromBody] SaveDiseaseParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.Id > 0)
                {
                    var diease = _diseaseRepository.GetById(parameter.Id);
                    diease.DiseaseName = parameter.Name;
                    diease.Description = parameter.Description;
                    _diseaseRepository.Update(diease);
                    return Ok(new
                    {
                        result = diease.ForJson(),
                        message = "Success"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult DeleteDisease([FromBody] SaveDiseaseParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.Id > 0)
                {
                    var d = _diseaseRepository.GetById(parameter.Id);
                    if (d.DrugDetailDiseases.Any())
                    {
                        return Ok(new
                        {
                            message = "Error"
                        });
                    }
                    _diseaseRepository.Delete(d);
                    return Ok(new
                    {
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

    }

    public class GetDiseaseParameter
    {
        public int Id { get; set; }
        public int Page { get; set; }
        public string Search { get; set; }
        public int DiseaseId { get; set; }
    }

    public class SaveDiseaseParameter
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Id { get; set; }
    }
}