﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Http;
using FYPWeb.Enum;
using FYPWeb.Hub;
using FYPWeb.Repository.Repository;
using FYPWeb.Services.Util;
using Model.Entity;

namespace FYPWeb.Controllers.Api
{
    public class EmployeeController : ApiController
    {
        private readonly EmployeeRepository _employeeRepository;
        private readonly NotificationRepository _notificationRepository;
        public EmployeeController(EmployeeRepository employeeRepository, NotificationRepository notificationRepository)
        {
            _employeeRepository = employeeRepository;
            _notificationRepository = notificationRepository;
        }

        [HttpPost]
        public IHttpActionResult GetEmployeeById([FromBody] GetEmployeeParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.EmployeeId > 0)
                {
                    var employee = _employeeRepository.GetById(parameter.EmployeeId);
                    return Ok(new
                    {
                        result = employee.ForJson(),
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetAllEmployees([FromBody] GetEmployeeParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.UserId > 0)
                {
                    var page = parameter?.Page > 0 ? parameter.Page : 1;
                    if (string.IsNullOrEmpty(parameter?.Search))
                    {
                        var list = _employeeRepository.GetEmployeesByUserId(parameter.UserId,page).Select(p => p.ForJson());
                        var pageCount = _employeeRepository.GetCountEmployeesByUserId(parameter.UserId);
                        return Ok(new
                        {
                            result = list,
                            pageCount = pageCount,
                            message = "Success",
                        });
                    }
                    else
                    {
                        var list =
                            _employeeRepository.GetEmployeesSearchByUserId(parameter.UserId,parameter.Search, page).Select(p => p.ForJson());
                        var pageCount = _employeeRepository.GetCountEmployeesSearchByUserId(parameter.UserId,parameter.Search);
                        return Ok(new
                        {
                            result = list,
                            pageCount = pageCount,
                            message = "Success",
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }
        [HttpPost]
        public IHttpActionResult SaveEmployee([FromBody] SaveEmployeeParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.UserId > 0)
                {
                    var e = new Employee()
                    {
                        Address = parameter.Address,
                        ContactNumber = parameter.PhoneNumber,
                        DutyHours = parameter.DutyHours,
                        ImageLink = UtilService.DefaultImagePath,
                        Name = parameter.Name,
                        Salary = parameter.Salary,
                        UserId = parameter.UserId,
                    };
                    _employeeRepository.Add(e);
                    
                    var notification = new Notification()
                    {
                        Url = "/Pharmacy/Employee/",
                        Body = "You have created an Employee. Assign it a Pharmacy",
                        CreatedAt = DateTime.Now,
                        IsPharmacy = false,
                        IsPost = false,
                        IsSeen = false,
                        ReceiverUserId = parameter.UserId,
                        NotificationType = (int)NotificationType.Info,
                    };
                    _notificationRepository.Add(notification);
                    NotificationHub.StaticSendNotification(notification, notification.ReceiverUserId, notification.NotificationType);
                    return Ok(new
                    {
                        result = e.Id,
                        message = "Success"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }
        [HttpPost]
        public IHttpActionResult UpdateEmployee([FromBody] SaveEmployeeParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.UserId > 0 && parameter.Id > 0)
                {

                    var e = _employeeRepository.GetByIdAndUser(parameter.Id, parameter.UserId);
                    e.Address = parameter.Address;
                    e.ContactNumber = parameter.PhoneNumber;
                    e.DutyHours = parameter.DutyHours;
                    e.Name = parameter.Name;
                    e.Salary = parameter.Salary;
                    _employeeRepository.Update(e);
                    return Ok(new
                    {
                        result = "",
                        message = "Success"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }
        [HttpPost]
        public IHttpActionResult AssignEmployee([FromBody] SaveEmployeeParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.UserId > 0 && parameter.Id > 0 && parameter.PharmacyId > 0)
                {

                    var e = _employeeRepository.GetByIdAndUser(parameter.Id, parameter.UserId);
                    e.PharmacyId = parameter.PharmacyId;
                    _employeeRepository.Update(e);
                    return Ok(new
                    {
                        result = e.Id,
                        message = "Success"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }
        [HttpPost]
        public IHttpActionResult DeleteEmployee([FromBody] SaveEmployeeParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.Id > 0)
                {

                    var e = _employeeRepository.GetById(parameter.Id);
                    if (e != null)
                    {
                        if (e.Orders.Any())
                        {
                            return Ok(new
                            {
                                result = 0,
                                message = "Error"
                            });
                        }
                        _employeeRepository.Delete(e);

                        return Ok(new
                        {
                            result = e.Id,
                            message = "Success"
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

    }

    public class GetEmployeeParameter
    {
        public int EmployeeId { get; set; }
        public int Page { get; set; }
        public int PharmacyId { get; set; }
        public string Search { get; set; }
        public int UserId { get; set; }
    }

    public class SaveEmployeeParameter
    {
        public int Id { get; set; }
        public string Address { get; set; }
        public int UserId { get; set; }
        public int PharmacyId { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public float Salary { get; set; }
        public float DutyHours { get; set; }
    }
}