﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Http;
using FYPWeb.Enum;
using FYPWeb.Hub;
using FYPWeb.Repository.Repository;
using FYPWeb.Service;
using FYPWeb.Services.Util;
using Model.Entity;

namespace FYPWeb.Controllers.Api
{
    public class PharmacyController : ApiController
    {
        private readonly PharmacyRepository _pharmacyRepository;
        private readonly NotificationRepository _notificationRepository;

        public PharmacyController(PharmacyRepository pharmacyRepository, NotificationRepository notificationRepository)
        {
            _pharmacyRepository = pharmacyRepository;
            _notificationRepository = notificationRepository;
        }

        [HttpPost]
        public IHttpActionResult ApplyAlgorithm([FromBody] ApplyAlgorithmParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.PharmacyId > 0 && parameter.NumberOfPharmacies > 0)
                {
                    var list = _pharmacyRepository.GetById(parameter.PharmacyId).Orders.Where(p => p.IsCompleted)
                        .Select(p => new[]
                        {
                            double.Parse(p.DeliveryLocationX),
                            double.Parse(p.DeliveryLocationY),
                        }).ToArray();
                    return Ok(new
                    {
                        message = "Success",
                        result = KMean.AppleAlgorithm(list,parameter.NumberOfPharmacies)
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }
        [HttpPost]
        public IHttpActionResult SavePharmacy([FromBody] SavePharmacyParameter parameter)
        {
            try
            {
                if (parameter != null)
                {
                    var p = new Pharmacy()
                    {
                        ContactNumber = parameter.PhoneNumber,
                        ContactPersonName = parameter.ContactPerson,
                        IsApproved = false,
                        IsVacationMode = false,
                        ShopAddress = parameter.Address,
                        ShopLocationX = parameter.X,
                        ShopLocationY = parameter.Y,
                        ShopName = parameter.Name,
                        UserId = parameter.UserId,
                        DeliveryCharges = parameter.Charges,
                        ImagePath = UtilService.DefaultImagePath
                    };
                    _pharmacyRepository.Add(p);
                    var notification = new Notification()
                    {
                        Url = "/Pharmacist/Pharmacy/",
                        Body = "You have added a Pharmacy",
                        CreatedAt = DateTime.Now,
                        IsPharmacy = false,
                        IsPost = false,
                        IsSeen = false,
                        IsUser = true,
                        ReceiverUserId = parameter.UserId,
                        NotificationType = (int)NotificationType.Info,
                    };
                    _notificationRepository.Add(notification);

                    notification = new Notification()
                    {
                        Url = "/Admin/Pharmacy/",
                        Body = "New Pharmacy Has been Added",
                        CreatedAt = DateTime.Now,
                        IsPharmacy = false,
                        IsPost = false,
                        IsSeen = false,
                        IsUser = true,
                        ReceiverUserId = SessionService.AdminId,
                        NotificationType = (int)NotificationType.Info,
                    };
                    _notificationRepository.Add(notification);

                    NotificationHub.StaticSendNotification(notification, notification.ReceiverUserId, notification.NotificationType);

                    return Ok(new
                    {
                        result = new { Id = p.Id },
                        message = "Success"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetNearestPharmacies([FromBody] GetNearestPharmaciesParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.Lat > 0 && parameter.Long > 0)
                {
                    var list = _pharmacyRepository.GetAllApprovedNearestPharmacies(parameter.Lat, parameter.Long).Select(p => new
                    {
                        p.User.Username,
                        Rating = p.PharmacyReviews.Any() ? p.PharmacyReviews.Average(pr => pr.Rating) : 0f,
                        Name = p.ShopName,
                        Address = p.ShopAddress,
                        X = p.ShopLocationX,
                        Y = p.ShopLocationY,
                    });
                    return Ok(new
                    {
                        message = "Success",
                        result = list
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetAllApprovedPharmacyByUser([FromBody] SavePharmacyParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.UserId > 0)
                {
                    var list = _pharmacyRepository.GetApprovedPharmaciesByUserId(parameter.UserId).ToList().Select(p => p.ForJson());
                    return Ok(new
                    {
                        message = "Success",
                        result = list
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult DeletePharmacy([FromBody] SavePharmacyParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.Id > 0)
                {
                    var p = _pharmacyRepository.GetByIdAndUser(parameter.Id, parameter.UserId);
                    if (p.Employees.Any() || p.InventoryTracks.Any() || p.Orders.Any())
                    {
                        return Ok(new
                        {
                            result = "",
                            message = "Error"

                        });
                    }
                    else
                    {
                        _pharmacyRepository.Delete(p);
                        return Ok(new
                        {
                            result = "",
                            message = "Success"

                        });
                    }

                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult ApprovePharmacy([FromBody] SavePharmacyParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.IsAdmin)
                {
                    var p = _pharmacyRepository.GetById(parameter.Id);
                    p.IsApproved = true;
                    _pharmacyRepository.Update(p);
                    var notification = new Notification()
                    {
                        Url = "/Pharmacist/Pharmacy/",
                        Body = "Your Pharmacy '" + p.ShopName + "' has been Approved by Admin",
                        CreatedAt = DateTime.Now,
                        IsPharmacy = false,
                        IsPost = false,
                        IsSeen = false,
                        IsUser = false,
                        ReceiverUserId = p.UserId.GetValueOrDefault(0),
                        NotificationType = (int)NotificationType.Info,
                    };
                    _notificationRepository.Add(notification);

                    NotificationHub.StaticSendNotification(notification, notification.ReceiverUserId, notification.NotificationType);

                    return Ok(new
                    {
                        result = "",
                        message = "Success"

                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult DisapprovePharmacy([FromBody] SavePharmacyParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.IsAdmin)
                {
                    var p = _pharmacyRepository.GetById(parameter.Id);
                    p.IsApproved = false;
                    p.IsRejected = true;
                    _pharmacyRepository.Update(p);
                    var notification = new Notification()
                    {
                        Url = "/Pharmacist/Pharmacy/",
                        Body = "Your Pharmacy '" + p.ShopName + "' has been Disapproved by Admin",
                        CreatedAt = DateTime.Now,
                        IsPharmacy = false,
                        IsPost = false,
                        IsSeen = false,
                        IsUser = false,
                        ReceiverUserId = p.UserId.GetValueOrDefault(0),
                        NotificationType = (int)NotificationType.Error,
                    };
                    _notificationRepository.Add(notification);

                    NotificationHub.StaticSendNotification(notification, notification.ReceiverUserId, notification.NotificationType);

                    return Ok(new
                    {
                        result = "",
                        message = "Success"

                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetAllPharmacy([FromBody] GetAllPharmacyParameter parameter)
        {
            try
            {
                if (string.IsNullOrEmpty(parameter.Search))
                {
                    var pharmacy = _pharmacyRepository.GetAllApprovedPharmacies(parameter.Page,100).ToList().Select(p => p.ForJson());
                    return Ok(new
                    {
                        result = pharmacy,
                        message = "Success"

                    });
                }
                else
                {
                    var pharmacy = _pharmacyRepository.GetAllApprovedPharmaciesSearch(parameter.Search,parameter.Page).ToList().Select(p => p.ForJson());
                    return Ok(new
                    {
                        result = pharmacy,
                        message = "Success"

                    });
                }
                   
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }
        [HttpPost]
        public IHttpActionResult GetByUserId([FromBody] SavePharmacyParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.UserId > 0)
                {
                    var pharmacy = _pharmacyRepository.GetMany(p => p.UserId == parameter.UserId).ToList().Select(p => p.ForJson());
                    return Ok(new
                    {
                        result = pharmacy,
                        message = "Success"

                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }


        [HttpPost]
        public IHttpActionResult UpdatePharmacy([FromBody] SavePharmacyParameter parameter)
        {
            try
            {
                if (parameter != null)
                {
                    var isLocationChanged = false;
                    var p = _pharmacyRepository.GetByIdAndUser(parameter.Id, parameter.UserId);
                    p.ContactNumber = parameter.PhoneNumber;
                    p.ContactPersonName = parameter.ContactPerson;
                    p.ShopAddress = parameter.Address;
                    p.ShopName = parameter.Name;
                    if (p.ShopLocationX.ToString(CultureInfo.InvariantCulture) != parameter.X.ToString(CultureInfo.InvariantCulture) ||
                        p.ShopLocationY.ToString(CultureInfo.InvariantCulture) != parameter.Y.ToString(CultureInfo.InvariantCulture))
                    {
                        p.ShopLocationX = parameter.X;
                        p.ShopLocationY = parameter.Y;
                        p.IsApproved = false;
                        isLocationChanged = true;
                    }
                    p.DeliveryCharges = parameter.Charges;
                    _pharmacyRepository.Update(p);
                    return Ok(new
                    {
                        result = "",
                        isLocationChanged,
                        message = "Success"

                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult SearchPharmacy([FromBody] SearchParameter paramter)
        {
            try
            {
                if (!string.IsNullOrEmpty(paramter?.Parameter))
                {
                    var d = _pharmacyRepository.GetAllApprovedPharmaciesSearch(paramter.Parameter);
                    var listOfCompany = d.Select(p => new { Id = p.Id, Name = p.ShopName + " -- " + p.ShopAddress.Substring(0, 15) }).ToList();
                    return Ok(new
                    {
                        result = listOfCompany,
                        message = "Success"
                    });
                }
                return Ok(new
                {
                    result = "",
                    message = "Success"
                });
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

    }

    public class GetAllPharmacyParameter
    {
        public string Search { get; set; }
        public int Page { get; set; }
    }

    public class ApplyAlgorithmParameter
    {
        public int PharmacyId { get; set; }
        public int NumberOfPharmacies { get; set; }
    }

    public class GetNearestPharmaciesParameter
    {
        public float Long { get; set; }
        public float Lat { get; set; }
    }

    public class SavePharmacyParameter
    {
        public int Id { get; set; }
        public bool IsAdmin { get; set; }
        public string Name { get; set; }
        public string ContactPerson { get; set; }
        public float Charges { get; set; }
        public float X { get; set; }
        public float Y { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public int UserId { get; set; }
    }
}