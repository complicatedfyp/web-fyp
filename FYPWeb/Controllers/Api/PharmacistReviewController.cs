﻿using System;
using System.Diagnostics;
using System.Web.Http;
using FYPWeb.Enum;
using FYPWeb.Hub;
using FYPWeb.Repository.Repository;
using Model.Entity;

namespace FYPWeb.Controllers.Api
{
    public class PharmacistReviewController : ApiController
    {

        private readonly PharmacistReviewRepository _pharmacistReviewRepository;
        private readonly OrderRepository _orderRepository;
        private readonly NotificationRepository _notificationRepository;
        public PharmacistReviewController(PharmacistReviewRepository pharmacistReviewRepository, OrderRepository orderRepository, NotificationRepository notificationRepository)
        {
            _pharmacistReviewRepository = pharmacistReviewRepository;
            _orderRepository = orderRepository;
            _notificationRepository = notificationRepository;
        }

        [HttpPost]
        public IHttpActionResult SaveReview([FromBody] SavePharmacyReviewParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.PharmacyId > 0 && parameter.UserId > 0 && parameter.OrderId > 0)
                {
                    if (_pharmacistReviewRepository.IsCheck(p => p.OrderId == parameter.OrderId))
                    {
                        return Ok(new
                        {
                            message = "Error"
                        });
                    }
                    var r = new PharmacyReview()
                    {
                        Rating = parameter.Rating,
                        Review = parameter.Review,
                        UserId = parameter.UserId,
                        PostedAt = DateTime.Now,
                        PharmacyId = parameter.PharmacyId,
                        OrderId = parameter.OrderId,
                    };
                    _pharmacistReviewRepository.Add(r);
                    var order = _orderRepository.GetById(parameter.OrderId);
                    var notification = new Notification()
                    {
                        Url = "/Pharmacist/MyOrderReview/",
                        SenderUserId = order.UserId,
                        Body = " has given you a Review on Order# " + order.Id,
                        CreatedAt = DateTime.Now,
                        IsPharmacy = false,
                        IsPost = false,
                        IsSeen = false,
                        IsUser = true,
                        ReceiverUserId = order.Pharmacy.UserId.GetValueOrDefault(),
                        NotificationType = (int)NotificationType.Info,
                    };
                    _notificationRepository.Add(notification);
                    NotificationHub.StaticSendNotification(notification, notification.ReceiverUserId, notification.NotificationType);

                    return Ok(new
                    {
                        result = new { Id = r.Id },
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }


        [HttpPost]
        public IHttpActionResult UpdateReview([FromBody] SavePharmacyReviewParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.Id > 0)
                {
                    var r = _pharmacistReviewRepository.GetById(parameter.Id);
                    r.Rating = parameter.Rating;
                    r.Review = parameter.Review;
                    _pharmacistReviewRepository.Update(r);
                    return Ok(new
                    {
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

    }

    public class SavePharmacyReviewParameter
    {
        public float Rating { get; set; }
        public string Review { get; set; }
        public int UserId { get; set; }
        public int PharmacyId { get; set; }
        public int OrderId { get; set; }
        public int Id { get; set; }
    }
}