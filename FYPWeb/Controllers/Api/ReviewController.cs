﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Http;
using FYPWeb.Repository.Repository;
using FYPWeb.Services.Util;
using Model.Entity;

namespace FYPWeb.Controllers.Api
{
    public class ReviewController : ApiController
    {
        private readonly DrugReviewRepository _drugReviewRepository;

        public ReviewController(DrugReviewRepository drugReviewRepository)
        {
            _drugReviewRepository = drugReviewRepository;
        }

        [HttpPost]
        public IHttpActionResult SaveReview([FromBody] SaveReviewParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.MedicineId > 0 && parameter.UserId > 0)
                {
                    var r = new DrugReview()
                    {
                        DrugId = parameter.MedicineId,
                        Rating = parameter.Rating,
                        Review = parameter.Review,
                        UserId = parameter.UserId,
                        PostedAt = DateTime.Now
                    };
                    _drugReviewRepository.Add(r);
                    return Ok(new
                    {
                        result = new { Id = r.Id },
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }


        [HttpPost]
        public IHttpActionResult UpdateReview([FromBody] SaveReviewParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.Id > 0)
                {
                    var r = _drugReviewRepository.GetById(parameter.Id);
                    r.Rating = parameter.Rating;
                    r.Review = parameter.Review;
                    _drugReviewRepository.Update(r);
                    return Ok(new
                    {
                        result = "",
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult DeleteReview([FromBody] SaveReviewParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.Id > 0)
                {
                    var brand = _drugReviewRepository.GetById(parameter.Id);
                    _drugReviewRepository.Delete(brand);
                    return Ok(new
                    {
                        message = "Success"
                    });
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

    }

    public class SaveReviewParameter
    {
        public int Id { get; set; }
        public int MedicineId { get; set; }
        public int UserId { get; set; }
        public string Review { get; set; }
        public float Rating { get; set; }
    }
}