﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Results;
using FYPWeb.Repository.Repository;
using FYPWeb.Services.Util;
using Model.Entity;

namespace FYPWeb.Controllers.Api
{
    public class CompanyController : ApiController
    {
        private readonly CompanyRepository _companyRepository;

        public CompanyController(CompanyRepository companyRepository)
        {
            _companyRepository = companyRepository;
        }


        [HttpPost]
        public IHttpActionResult GetCompanyById([FromBody] GetCompanyParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.CompanyId > 0)
                {
                    var c = _companyRepository.GetById(parameter.CompanyId);
                    return Ok(new
                    {
                        message = "Success",
                        result = c.ForJson(),
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

        [HttpPost]
        public IHttpActionResult GetAllCompany([FromBody] GetCompanyParameter parameter)
        {
            try
            {
                if (parameter != null)
                {
                    var page = parameter.Page > 0 ? parameter.Page : 1;
                    if (string.IsNullOrEmpty(parameter.Search))
                    {
                        var list = _companyRepository.GetCompanies(page);
                        var pageCount = _companyRepository.GetPaginationCount();
                        return Ok(new
                        {
                            message = "Success",
                            pageCount,
                            result = list
                        });
                    }
                    else
                    {
                        var list = _companyRepository.GetCompaniesSearch(parameter.Search, page);
                        var pageCount = _companyRepository.GetCompaniesSearchCount(parameter.Search);
                        return Ok(new
                        {
                            message = "Success",
                            pageCount,
                            result = list
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }


        [HttpPost]
        public IHttpActionResult SaveCompany([FromBody] SaveCompanyParameter paramter)
        {
            try
            {
                if (paramter != null)
                {
                    var company = new Company()
                    {
                        Fax = paramter.Fax,
                        Phone = paramter.PhoneNumber,
                        CompanyName = paramter.Name,
                        Address = paramter.Address,
                        WebLink = paramter.Link,
                        CityId = paramter.CityId,
                        ImagePath = UtilService.DefaultImagePath
                    };
                    _companyRepository.Add(company);
                    return Ok(new
                    {
                        result = company.ForJson(),
                        message = "Success"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return new InternalServerErrorResult(this);
        }

        [HttpPost]
        public IHttpActionResult UpdateCompany([FromBody] SaveCompanyParameter paramter)
        {
            try
            {
                if (paramter != null && paramter.CompanyId > 0)
                {
                    var company = _companyRepository.GetById(paramter.CompanyId);
                    company.Address = paramter.Address;
                    company.CityId = paramter.CityId;
                    company.CompanyName = paramter.Name;
                    company.Fax = paramter.Fax.Replace("_", "");
                    company.WebLink = paramter.Link;
                    company.Phone = paramter.PhoneNumber.Replace("_","");
                    _companyRepository.Update(company);
                    return Ok(new
                    {
                        result = company.ForJson(),
                        message = "Success"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return new InternalServerErrorResult(this);
        }

        [HttpPost]
        public IHttpActionResult DeleteCompany([FromBody] DeleteCompanyParameter paramter)
        {
            try
            {
                if (paramter != null && paramter.CompanyId > 0)
                {
                    var d = _companyRepository.GetById(paramter.CompanyId);
                    if (d.Brands.Any())
                    {
                        return Ok(new
                        {
                            result = "",
                            message = "Error"
                        });
                    }
                    _companyRepository.Delete(d);
                    return Ok(new
                    {
                        result = "",
                        message = "Success"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return new InternalServerErrorResult(this);
        }

        [HttpPost]
        public IHttpActionResult SearchCompany([FromBody] SearchParameter paramter)
        {
            try
            {
                if (!string.IsNullOrEmpty(paramter?.Parameter))
                {
                    var d = _companyRepository.GetCompaniesSearch(paramter.Parameter);
                    var listOfCompany = d.Select(p => new {Id = p.Id, Name = p.CompanyName}).ToList();
                    return Ok(new
                    {
                        result = listOfCompany,
                        message = "Success"
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }

    }

    public class GetCompanyParameter
    {
        public int CompanyId { get; set; }
        public string Search { get; set; }
        public int Page { get; set; }
    }

    public class SearchParameter
    {
        public int PharmacyId { get; set; }
        public string Parameter { get; set; }
        public string[] ListOfParameter { get; set; }
        public int Page { get; set; }
        public int PageCount { get; set; }
    }

    public class DeleteCompanyParameter
    {
        public int CompanyId { get; set; }
    }

    public class SaveCompanyParameter
    {
        public string ImagePath { get; set; }
        public int CompanyId { get; set; }
        public string Name { get; set; }
        public string Fax { get; set; }
        public string PhoneNumber { get; set; }
        public string Link { get; set; }
        public string Address { get; set; }
        public int CityId { get; set; }
    }
}