﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Http;
using FYPWeb.Enum;
using FYPWeb.Hub;
using FYPWeb.Repository.Repository;
using Model.Entity;

namespace FYPWeb.Controllers.Api
{
    public class RecommendationController : ApiController
    {
        private readonly RecommendationRepository _recommendationRepository;
        private readonly NotificationRepository _notificationRepository;
        public RecommendationController(RecommendationRepository recommendationRepository, NotificationRepository notificationRepository)
        {
            _recommendationRepository = recommendationRepository;
            _notificationRepository = notificationRepository;
        }

        [HttpPost]
        public IHttpActionResult SendRecommendation([FromBody] SendRecommendationParameter parameter)
        {
            try
            {
                if (parameter != null && parameter.UserId > 0 && parameter.UserRecieverId > 0 &&
                    parameter.MedicineId > 0)
                {
                    if (parameter.UserId == parameter.UserRecieverId)
                    {
                        return Ok(new
                        {
                            message = "Cannot Send Same User",
                        });
                    }
                    if (_recommendationRepository.IsCheck(
                        p =>
                            p.DrugId == parameter.MedicineId && p.FromUserId == parameter.UserId &&
                            p.ToUserId == parameter.UserRecieverId))
                        return Ok(new
                        {
                            message = "Already Exists",
                        });
                    var recommend = new Recommendation()
                    {
                        DrugId = parameter.MedicineId,
                        FromUserId = parameter.UserId,
                        ToUserId = parameter.UserRecieverId,
                        RecommendedAt = DateTime.Now,
                    };
                    _recommendationRepository.Add(recommend);
                    var notification = new Notification()
                    {
                        Url = "/Medicine/" + parameter.MedicineId,
                        SenderUserId = parameter.UserId,
                        Body = " has Recommended you a Medicine",
                        CreatedAt = DateTime.Now,
                        IsPharmacy = false,
                        IsPost = false,
                        IsSeen = false,
                        IsUser = true,
                        ReceiverUserId = parameter.UserRecieverId,
                        NotificationType = (int)NotificationType.Info,
                    };
                    _notificationRepository.Add(notification);
                    NotificationHub.StaticSendNotification(notification, notification.ReceiverUserId, notification.NotificationType);

                    return Ok(new
                    {
                        message = "Success",
                        result = recommend.Id
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return InternalServerError();
        }
    }

    public class SendRecommendationParameter
    {
        public int UserId { get; set; }
        public int MedicineId { get; set; }
        public int UserRecieverId { get; set; }
    }
}