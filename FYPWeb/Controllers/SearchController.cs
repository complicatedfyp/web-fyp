﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FYPWeb.Repository.Repository;
using FYPWeb.Service;
using FYPWeb.ViewModels;
using Model.Entity;

namespace FYPWeb.Controllers
{
    public class SearchController : Controller
    {
        // GET: Search
        private readonly DrugCartRepository _drugCartRepository;
        private readonly BrandRepository _brandRepository;
        private readonly DrugRepository _drugRepository;
        private readonly CompanyRepository _companyRepository;
        private readonly DiseaseRepository _diseaseRepository;
        public SearchController(DrugCartRepository drugCartRepository, BrandRepository brandRepository, DrugRepository drugRepository, CompanyRepository companyRepository, DiseaseRepository diseaseRepository)
        {
            _drugCartRepository = drugCartRepository;
            _brandRepository = brandRepository;
            _drugRepository = drugRepository;
            _companyRepository = companyRepository;
            _diseaseRepository = diseaseRepository;
        }

        public ActionResult Index(string q,int type,int page = 1)
        {
            
            var m = new SearchIndexViewModel()
            {
                HeaderViewModel = new HomeHeaderViewModel()
                {
                    NotificationCount = 0,
                    Header = HomeHeader.None
                },
                Query = q,
                Page = page,
            };
            var searchType = (SearchType) type;
            m.Type = searchType;
            switch (searchType)
            {
                case SearchType.Brand:
                    m.Brands = _brandRepository.GetBrandsSearch(q, page);
                    m.TotalCount = _brandRepository.GetBrandsSearchCount(q);
                    break;
                case SearchType.Drug:
                    return RedirectToAction("AllMedicine", "Home", new { parameter = q, page });
                    break;
                case SearchType.Disease:
                    m.Diseases = _diseaseRepository.GetDiseasesSearch(q, page);
                    m.TotalCount = _diseaseRepository.GetDiseasesSearchCount(q);
                    break;
                case SearchType.Company:
                    m.Companies = _companyRepository.GetCompaniesSearch(q, page);
                    m.TotalCount = _companyRepository.GetCompaniesSearchCount(q);
                    break;
                case SearchType.Categories:
                    return RedirectToAction("AllCategories", "Forum", new {parameter = q, page});
                case SearchType.Posts:
                    return RedirectToAction("AllThreadPost", "Forum", new {parameter = q, page});
                case SearchType.Tags:
                    return RedirectToAction("AllTags", "Forum", new { parameter = q, page });
                case SearchType.Users:
                    return RedirectToAction("AllUsers", "Home", new { parameter = q, page });
                case SearchType.Pharmacies:
                    return RedirectToAction("AllPharmacies", "Home", new { parameter = q, page });
                default:
                    break;
            }
            int i = 0;
            if (SessionService.UserId != "")
            {
                i = _drugCartRepository.GetUniqueMedicineCountByUserId(int.Parse(SessionService.UserId));
            }
            m.HeaderViewModel.CartCount = i;
            return View("Index",m);
        }
    }
}