﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FYPWeb.Repository.Repository;
using FYPWeb.Service;
using FYPWeb.ViewModels;

namespace FYPWeb.Controllers
{
    public class OrderController : Controller
    {
        private readonly PharmacyRepository _pharmacyRepository;
        private readonly DrugCartRepository _drugCartRepository;
        private readonly PrescriptionRepository _prescriptionRepository;
        private readonly PromotionRepository _promotionRepository;
        public OrderController(PharmacyRepository pharmacyRepository, DrugCartRepository drugCartRepository, PrescriptionRepository prescriptionRepository, PromotionRepository promotionRepository)
        {
            _pharmacyRepository = pharmacyRepository;
            _drugCartRepository = drugCartRepository;
            _prescriptionRepository = prescriptionRepository;
            _promotionRepository = promotionRepository;
        }


        public ActionResult PlaceOrder()
        {
            int i = 0;
            if (SessionService.UserId != "" && SessionService.UserRole == "Customer")
            {
                    i = _drugCartRepository.GetUniqueMedicineCountByUserId(int.Parse(SessionService.UserId));
                    var model = new PlaceOrderViewModel()
                    {
                        HeaderViewModel = new HomeHeaderViewModel()
                        {
                            Header = HomeHeader.None,
                            CartCount = i,
                            NotificationCount = 0,
                        },
                        DrugCarts = _drugCartRepository.GetByUserId(int.Parse(SessionService.UserId)),
                        Prescriptions = _prescriptionRepository.GetByUserId(int.Parse(SessionService.UserId)),
                    };
                    return View("PlaceOrder", model);
            }
            return RedirectToAction("Index", "Home");
        }

        public ActionResult PlaceOrderPrescription()
        {
            int i = 0;
            if (SessionService.UserId != "" && SessionService.UserRole == "Customer")
            {
                i = _drugCartRepository.GetUniqueMedicineCountByUserId(int.Parse(SessionService.UserId));
                var model = new PlaceOrderPrescriptionViewModel()
                {
                    HeaderViewModel = new HomeHeaderViewModel()
                    {
                        Header = HomeHeader.None,
                        CartCount = i,
                        NotificationCount = 0,
                    },
                    Prescriptions = _prescriptionRepository.GetByUserId(int.Parse(SessionService.UserId)),
                    Pharmacies = _pharmacyRepository.GetAllApprovedPharmacies(1,50)
                };
                return View("PlaceOrderPrescription", model);
            }
            return RedirectToAction("Index", "Home");
        }
    }

}