﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FYPWeb.Service;
using FYPWeb.ViewModels;

namespace FYPWeb.Controllers
{
    public class ErrorController : Controller
    {
        public ErrorController()
        {
            
        }

        public ActionResult Index()
        {
            var model = new ErrorViewModel()
            {
                Url = TempDataService<string>.GetData(this, "Url")
            };
            return View("Index",model);
        }
    }
}