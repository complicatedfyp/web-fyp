﻿using FYPWeb.Services.Util;
using Model.Entity;
using System;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using FYPWeb.Repository.Repository;
using FYPWeb.Service;
using FYPWeb.ViewModels;

namespace FYPWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly DrugCartRepository _drugCartRepository;
        private readonly UserRepository _userRepository;
        private readonly PharmacyRepository _pharmacyRepository;
        private readonly DrugRepository _drugRepository;
        private readonly DiseaseRepository _diseaseRepository;
        private readonly BrandRepository _brandRepository;
        private readonly InventoryTrackRepository _inventoryTrackRepository;
        private readonly CompanyRepository _companyRepository;
        private readonly SessionCartRepository _sessionCartRepository;
        private readonly OrderRepository _orderRepository;
        public HomeController(UserRepository userRepository, DrugRepository drugRepository, DrugReviewRepository drugReviewRepository, DrugCartRepository drugCartRepository, DrugRepository drugRepository1, BrandRepository brandRepository, DiseaseRepository diseaseRepository, CompanyRepository companyRepository, PharmacyRepository pharmacyRepository, SessionCartRepository sessionCartRepository, InventoryTrackRepository inventoryTrackRepository, OrderRepository orderRepository)
        {
            _userRepository = userRepository;
            _drugCartRepository = drugCartRepository;
            _drugRepository = drugRepository1;
            _brandRepository = brandRepository;
            _diseaseRepository = diseaseRepository;
            _companyRepository = companyRepository;
            _pharmacyRepository = pharmacyRepository;
            _sessionCartRepository = sessionCartRepository;
            _inventoryTrackRepository = inventoryTrackRepository;
            _orderRepository = orderRepository;
        }
        public ActionResult Login()
        {
            var model = new LoginViewModel()
            {
                Message = TempDataService<string>.GetData(this, "Message") + ""
            };
            return View("Login", model);
        }

        public ActionResult Chat(int id)
        {
            if (SessionService.IsAuth)
            {
                return RedirectToAction("Index", "Home");
            }
            var i = 0;
            if (SessionService.UserId != "")
            {
                i = _drugCartRepository.GetUniqueMedicineCountByUserId(int.Parse(SessionService.UserId));
            }
            var userId = int.Parse(SessionService.UserId);
            var isCorrect = _orderRepository.Get(p => p.Id == id &&
                                                      (p.UserId == userId || p.Pharmacy.UserId == userId));
            if (isCorrect == null)
            {
                return RedirectToAction("Index", "Home");
            }
            var rId = isCorrect.UserId == userId ? isCorrect.Pharmacy.UserId : isCorrect.UserId;
            var model = new HomeChatViewModel()
            {
                HeaderViewModel = new HomeHeaderViewModel()
                {
                    Header = HomeHeader.Home,
                    CartCount = i,
                    NotificationCount = 0,
                },
                OrderId = id,
                RId = rId.GetValueOrDefault(),
            };
            return View("Chat", model);
        }
        public ActionResult Message(int page = 1,int pageSize = 10)
        {
            var i = 0;
            if (SessionService.UserId != "")
            {
                i = _drugCartRepository.GetUniqueMedicineCountByUserId(int.Parse(SessionService.UserId));
            }
            var userId = int.Parse(SessionService.UserId);
            var model = new HomeMessageViewModel()
            {
                HeaderViewModel = new HomeHeaderViewModel()
                {
                    Header = HomeHeader.Home,
                    CartCount = i,
                    NotificationCount = 0,
                },
                Orders = _orderRepository.GetMany(p => (p.IsCompleted || p.IsAccepted) && (p.UserId == userId || p.Pharmacy.UserId == userId),page,pageSize,p => p.OrderedAt,true),
                Page = page,
                PageSize = pageSize,
                PageCount = _orderRepository.GetPageCount(p => p.IsCompleted || p.IsAccepted,pageSize)
            };
            return View("Message", model);
        }
        public ActionResult LogOut()
        {
            try
            {
                SessionService.ClearSession();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return RedirectToAction("Index", "Home");

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LoginUser(LoginParameter parameter)
        {
            try
            {
                var data = _userRepository.LoginIn(parameter.Email, UtilService.EncryptPassword(parameter.Password).ToLower(), parameter.Type);
                if (data != null)
                {
                    SessionService.UserId = data.Id + "";
                    SessionService.UserRole = data.UserRole.UserRoleName;
                    SessionService.UserImage = data.UserImage;
                    SessionService.UserName = data.Username;
                    SessionService.FullName = data.FirstName + " " + data.LastName;
                    
                    var id = int.Parse(SessionService.UserId);
                    var list = _sessionCartRepository.GetBySessionId(SessionService.SessionId);
                    var sessionCarts = list as SessionCart[] ?? list.ToArray();
                    for (var i = 0; i < sessionCarts.Count(); i++)
                    {
                        var dCart = _drugCartRepository.GetByMedicineIdAndUserId(sessionCarts[i].InventoryId, id);
                        if (dCart == null)
                        {
                            var c = new DrugCart()
                            {
                                InventoryId = sessionCarts[i].InventoryId,
                                UserId = id,
                                Quantity = sessionCarts[i].Quantity
                            };
                            _drugCartRepository.Add(c);
                        }
                        else
                        {
                            dCart.Quantity += sessionCarts[i].Quantity;
                            _drugCartRepository.Update(dCart);
                        }
                    }
                    _sessionCartRepository.DeleteBySessionId(SessionService.SessionId);
                    if (SessionService.UserRole == "Customer")
                    {
                        return RedirectToAction("Dashboard", "Customer");
                    }
                    else if (SessionService.UserRole == "Pharmacist")
                    {
                        return RedirectToAction("Dashboard", "Pharmacist");
                    }
                }
                TempDataService<string>.SetData(this, "Message", "Incorrect Username/Password");
                return RedirectToAction("Login", "Home");
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            TempDataService<string>.SetData(this, "Url", Url.Action("Login", "Home"));
            return RedirectToAction("Index", "Error");

        }

        public ActionResult Index()
        {
            var i = 0;
            if (SessionService.UserId != "")
            {
                i = _drugCartRepository.GetUniqueMedicineCountByUserId(int.Parse(SessionService.UserId));
            }
            var model = new HomeIndexViewModel()
            {
                HeaderViewModel = new HomeHeaderViewModel()
                {
                    Header = HomeHeader.Home,
                    CartCount = i,
                    NotificationCount = 0,
                },
                MostSoldDrugs = _drugRepository.GetTopSoldMedicines(),
                MostVisitedDrugs = _drugRepository.GetTopSearchMedicines(),
                RecommendedDrugs = _drugRepository.GetTopRecommendedMedicines(),
            };
            return View("Index", model);
        }


        [HttpGet]
        public ActionResult AllCompanies(string parameter = "", int page = 1)
        {

            var i = 0;
            if (SessionService.UserId != "")
            {
                i = _drugCartRepository.GetUniqueMedicineCountByUserId(int.Parse(SessionService.UserId));
            }
            var model = new AllCompanyViewModal()
            {
                HeaderViewModel = new HomeHeaderViewModel()
                {
                    Header = HomeHeader.Home,
                    CartCount = i,
                    NotificationCount = 0,
                },
                Page = page,
                PageCount = _companyRepository.GetPaginationCount(),
                Companies = _companyRepository.GetCompanies(page)
            };
            return View("AllCompanies", model);
        }

        [HttpGet]
        public ActionResult AllDisease(string parameter = "", int page = 1)
        {

            var i = 0;
            if (SessionService.UserId != "")
            {
                i = _drugCartRepository.GetUniqueMedicineCountByUserId(int.Parse(SessionService.UserId));
            }
            var model = new AllDiseaseViewModel()
            {
                HeaderViewModel = new HomeHeaderViewModel()
                {
                    Header = HomeHeader.Home,
                    CartCount = i,
                    NotificationCount = 0,
                },
                Page = page,
                PageCount = _diseaseRepository.GetPaginationCount(),
                Diseases = _diseaseRepository.GetDiseases(page)
            };
            return View("AllDisease", model);
        }

        [HttpGet]
        public ActionResult AllPharmacies(string parameter = "", int page = 1)
        {

            var i = 0;
            if (SessionService.UserId != "")
            {
                i = _drugCartRepository.GetUniqueMedicineCountByUserId(int.Parse(SessionService.UserId));
            }
            var model = new AllPharmaciesViewModal()
            {
                HeaderViewModel = new HomeHeaderViewModel()
                {
                    Header = HomeHeader.Home,
                    CartCount = i,
                    NotificationCount = 0,
                },
                Parameter = parameter,
                Page = page,
                PageCount = string.IsNullOrEmpty(parameter) ? _pharmacyRepository.GetAllApprovedPharmaciesPageCount() : _pharmacyRepository.GetAllApprovedPharmaciesSearchPageCount(parameter),
                Pharmacies = string.IsNullOrEmpty(parameter) ? _pharmacyRepository.GetAllApprovedPharmacies(page) : _pharmacyRepository.GetAllApprovedPharmaciesSearch(parameter, page)
            };
            return View("AllPharmacies", model);
        }

        [HttpGet]
        public ActionResult AllUsers(string parameter = "", int page = 1)
        {

            var i = 0;
            if (SessionService.UserId != "")
            {
                i = _drugCartRepository.GetUniqueMedicineCountByUserId(int.Parse(SessionService.UserId));
            }
            var model = new AllUsersViewModal()
            {
                HeaderViewModel = new HomeHeaderViewModel()
                {
                    Header = HomeHeader.Home,
                    CartCount = i,
                    NotificationCount = 0,
                },
                Parameter = parameter,
                Page = page,
                PageCount = string.IsNullOrEmpty(parameter) ? _userRepository.GetActivePaginationCount() : _userRepository.GetActivePaginationSearcCount(parameter),
                Users = string.IsNullOrEmpty(parameter) ? _userRepository.GetActiveUsers(page) : _userRepository.GetActiveUsersSearch(parameter, page)
            };
            return View("AllUsers", model);
        }

        [HttpGet]
        public ActionResult AllBrands(string parameter = "", int page = 1)
        {

            var i = 0;
            if (SessionService.UserId != "")
            {
                i = _drugCartRepository.GetUniqueMedicineCountByUserId(int.Parse(SessionService.UserId));
            }
            var model = new AllBrandsViewModal()
            {
                HeaderViewModel = new HomeHeaderViewModel()
                {
                    Header = HomeHeader.Home,
                    CartCount = i,
                    NotificationCount = 0,
                },
                Parameter = parameter,
                Page = page,
                PageCount = _brandRepository.GetPaginationCount(),
                Brands = _brandRepository.GetBrands(page)
            };
            return View("AllBrand", model);
        }

        [HttpGet]
        public ActionResult AllMedicine(string parameter = "", int rating = 0, int min = 0, int max = 0, int brandId = 0, int pharmacyId = 0, string form = "", int page = 1, int sortBy = 0)
        {

            var i = 0;
            if (SessionService.UserId != "")
            {
                i = _drugCartRepository.GetUniqueMedicineCountByUserId(int.Parse(SessionService.UserId));
            }
            var model = new AllMedicinesViewModal()
            {
                HeaderViewModel = new HomeHeaderViewModel()
                {
                    Header = HomeHeader.Home,
                    CartCount = i,
                    NotificationCount = 0,
                },
                Parameter = parameter,
                Page = page,
                BrandId = brandId,
                PharmacyId = pharmacyId,
                Form = form,
                Max = max,
                Min = min,
                Rating = rating,
            };
            if (SessionService.UserRole == "" || SessionService.UserRole == "Customer" || SessionService.UserRole == "Admin")
            {
                model.CustomerDrugs = _inventoryTrackRepository.GetDrugsByInventory(parameter, brandId, pharmacyId, rating, min, max, form, sortBy, page);
                model.PageCount = _inventoryTrackRepository.GetDrugsByInventoryCount(parameter, brandId, pharmacyId, rating, min, max, form);
            }
            else if (SessionService.UserRole == "Pharmacist")
            {
                model.PharmacyDrugs = _drugRepository.GetDrugs(parameter, brandId, rating, min, max, form, sortBy, page);
                model.PageCount = _drugRepository.GetDrugsCount(parameter, brandId, rating, min, max, form);
            }
            return View("AllMedicine", model);
        }


        [HttpGet]
        public ActionResult Register()
        {

            var model = new RegisterViewModel()
            {
                Message = TempDataService<string>.GetData(this, "Message") + "",
                Parameter = TempDataService<RegisterUserParameter>.GetData(this, "Data") ?? new RegisterUserParameter()
            };
            return View("Register", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegisterUser(RegisterUserParameter parameter)
        {
            try
            {
                if (_userRepository.UserEmailIsAvailable(parameter.Email.ToLower()))
                {
                    TempDataService<string>.SetData(this, "Message", "Email is already taken.");
                    TempDataService<RegisterUserParameter>.SetData(this, "Data", parameter);

                    return RedirectToAction("Register", "Home");
                }
                if (_userRepository.UserNameIsAvailable(parameter.UserName.ToLower()))
                {
                    TempDataService<string>.SetData(this, "Message", "Username is already taken");
                    TempDataService<RegisterUserParameter>.SetData(this, "Data", parameter);
                    return RedirectToAction("Register", "Home");
                }
                var user = new User()
                {
                    Address = parameter.Address,
                    ContactNumber = parameter.PhoneNumber,
                    DateOfBrith = parameter.DOB,
                    UserRoleId = parameter.Type,
                    Email = parameter.Email.ToLower(),
                    FirstName = parameter.FirstName,
                    IsActive = true,
                    LastName = parameter.LastName,
                    Password = UtilService.EncryptPassword(parameter.Password).ToLower(),
                    Gender = parameter.Gender,
                    Signature = "",
                    Username = parameter.UserName.ToLower(),
                    AuthCode = UtilService.RandomString(),
                    RegisteredAt = DateTime.Now,
                    UserImage = parameter.Type == 1 ? UtilService.DefaultUserImagePath : UtilService.DefaultPharmacistImagePath,
                };
                _userRepository.Add(user);
                //EmailService.SendEmail(parameter.Email, "Verification Code for OMDM",
                //    Url.Action("Verify", "Home",
                //        new {email = parameter.Email, code = UtilService.EncryptPassword(user.AuthCode)}));
                TempDataService<string>.SetData(this, "Email", parameter.Email);
                return RedirectToAction("SendEmail", "Home");
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            TempDataService<string>.SetData(this, "Url", Url.Action("Register", "Home"));
            return RedirectToAction("Index", "Error");
        }

        public ActionResult ForgetPassword()
        {
            return View("ForgetPassword");
        }

        public ActionResult Test()
        {
            EmailService.SendEmail("h.halai0334work@gmail.com", "Verification Code for OMDM",
                    Url.Action("Verify", "Home",
                        new { email = "h.halai0334work@gmail.com", code = UtilService.EncryptPassword("7B-04-97-8C-4D-7B-9A-C2") }));
            return View("ForgetPassword");
        }

        public ActionResult PharmacyNearYou(int page = 1)
        {
            var i = 0;
            if (SessionService.UserId != "")
            {
                i = _drugCartRepository.GetUniqueMedicineCountByUserId(int.Parse(SessionService.UserId));
            }
            var model = new PharmaciesNearYouViewModal()
            {
                HeaderViewModel = new HomeHeaderViewModel()
                {
                    Header = HomeHeader.None,
                    CartCount = i,
                    NotificationCount = 0
                },
            };
            return View("PharmaciesNearYou", model);
        }
        public ActionResult Verify(string email, string code)
        {
            var user = _userRepository.GetByEmail(email);
            if (user != null)
            {
                var pass = UtilService.EncryptPassword(user.AuthCode);
                if (pass.ToLower() == code.ToLower())
                {
                    user.IsActive = true;
                    user.IsVerified = true;
                    _userRepository.Update(user);
                    return RedirectToAction("Index", "Home");
                }
            }
            return RedirectToAction("Index", "Home");
        }

        public ActionResult SendEmail()
        {
            var model = new SendEmailViewModel()
            {
                Email = TempDataService<string>.GetData(this, "Email"),
            };
            return View("SendEmail", model);
        }

        public ActionResult UserVerified()
        {
            var model = new SendEmailViewModel()
            {
                Email = TempDataService<string>.GetData(this, "Email"),
            };
            if (string.IsNullOrEmpty(model.Email))
                return RedirectToAction("Index", "Home");
            return View("UserVerified", model);
        }

    }



    public class LoginParameter
    {
        public int Type { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }

    public class RegisterUserParameter
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public int Type { get; set; }
        public DateTime DOB { get; set; }
        public string Address { get; set; }
        public string Password { get; set; }
        public string Gender { get; set; }
        public string Signature { get; set; }
        public string AboutMe { get; set; }

    }
}