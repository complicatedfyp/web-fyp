﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FYPWeb
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                "Brand", // Route name
                "Brand/{id}", // URL with parameters*
                new { controller = "Brand", action = "Index" }
            );
            routes.MapRoute(
                "Company", // Route name
                "Company/{id}", // URL with parameters*
                new { controller = "Company", action = "Index" }
            );
            routes.MapRoute(
                "Disease", // Route name
                "Disease/{id}", // URL with parameters*
                new { controller = "Disease", action = "Index" }
            );
            routes.MapRoute(
                "Profile", // Route name
                "Profile/{username}", // URL with parameters*
                new { controller = "Profile", action = "Index" }
            );
            routes.MapRoute(
                "Search", // Route name
                "Search/{id}", // URL with parameters*
                new { controller = "Search", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                "Medicine", // Route name
                "Medicine/{id}", // URL with parameters*
                new {controller = "Medicine", action = "Index", id = UrlParameter.Optional}
            );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new {controller = "Home", action = "Index", id = UrlParameter.Optional}
            );
        }
    }
}
