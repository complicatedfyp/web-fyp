using System.Web.Mvc;
using FYPWeb.Repository.Infrastructure;
using FYPWeb.Repository.Repository;
using Unity.Mvc5;
using Unity;
using Unity.Lifetime;

namespace FYPWeb
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType(typeof(IRepository<>), typeof(BaseRepository<>));
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
            //Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);
        }
    }
}