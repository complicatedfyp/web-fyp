﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using FYPWeb.Repository.Infrastructure;
using FYPWeb.Repository.Repository;
using Unity;

namespace FYPWeb
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();
            //config.Routes.MapHttpRoute(
            //     name: "HubApi",
            //     routeTemplate: "Hubapi/{controller}/{action}/{id}",
            //     defaults: new { id = RouteParameter.Optional }
            // ).Constraints["Namespaces"] = new[] { "APHTOMMDS.Controllers.HubApi" };
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            config.Formatters.JsonFormatter.SupportedMediaTypes
                .Add(new MediaTypeHeaderValue("text/html"));
            var container = new UnityContainer();
            container.RegisterType(typeof(IRepository<>), typeof(BaseRepository<>));
            config.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);
            GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);
        }
    }

}