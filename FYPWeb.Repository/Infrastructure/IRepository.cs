﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Model.Entity;

namespace FYPWeb.Repository.Infrastructure
{
    public interface IRepository<T> where T : Base
    {
        // Marks an entity as new
        bool Add(T entity);

        // Marks an entity to be removed
        void Delete(T entity);

        void Delete(Expression<Func<T, bool>> where);

        // Get an entity using delegate
        T Get(Expression<Func<T, bool>> where);

        // Gets all entities of type T
        IEnumerable<T> GetAll();
        IQueryable<T> GetAll(int page, int pageSize = 10);
        // Get an entity by Guid id
        T GetById(int id);

        // Gets entities using delegate
        IQueryable<T> GetMany(Expression<Func<T, bool>> where);
        IQueryable<T> GetMany(Expression<Func<T, bool>> where, int page, int pageSize = 10,bool isDecending = false);

        // Marks an entity as modified
        void Update(T entity);
    }
}
