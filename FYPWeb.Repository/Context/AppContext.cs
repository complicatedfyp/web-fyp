﻿using System.Data.Entity;
using Model.Entity;

namespace FYPWeb.Repository.Context
{
    public class AppContext : DbContext
    {
        public AppContext() : base("FYPWeb")
        {
            
        }

        public DbSet<Brand> Brands { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<OrderPrescription> OrderPrescriptions { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<SessionCart> SessionCarts { get; set; }
        public DbSet<CustomerReview> CustomerReviews { get; set; }
        public DbSet<Disease> Diseases { get; set; }
        public DbSet<Drug> Drugs { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<DrugCart> DrugCarts { get; set; }
        public DbSet<DrugDetail> DrugDetails { get; set; }
        public DbSet<DrugDetailDisease> DrugDetailDiseases { get; set; }
        public DbSet<DrugReview> DrugReviews { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<Pharmacy> Pharmacies { get; set; }
        public DbSet<PharmacyReview> PharmacyReviews { get; set; }
        public DbSet<PostCategory> PostCategories { get; set; }
        public DbSet<PostReply> PostReplies { get; set; }
        public DbSet<PostThread> PostThreads { get; set; }
        public DbSet<Prescription> Prescriptions { get; set; }
        public DbSet<Promotion> Promotions { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<TagPostThread> TagPostThreads { get; set; }
        public DbSet<Recommendation> Recommendations { get; set; }
        public DbSet<InventoryTrack> InventoryTracks { get; set; }
    }
}
