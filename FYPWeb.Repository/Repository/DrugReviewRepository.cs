﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FYPWeb.Repository.Context;
using Model.Entity;

namespace FYPWeb.Repository.Repository
{
    public class DrugReviewRepository : BaseRepository<DrugReview>
    {
        public DrugReviewRepository(AppContext context) : base(context)
        {
        }

        public bool IsReviewed(int medicineId, int userId)
        {
            return IsCheck(p => p.DrugId == medicineId && p.UserId == userId);
        }

        public float GetAverageReview(int id)
        {
            try
            {
                return GetMany(p => p.DrugId == id).Average(p => p.Rating);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public IEnumerable<DrugReview> GetReviewsByMedicine(int medicineId,int page = 1, int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetMany(p => p.DrugId == medicineId,page, pageCount);
        }

        public IEnumerable<DrugReview> GetReviewsByUser(int userId, int page = 1, int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetMany(p => p.UserId == userId,page, pageCount);
        }
        public int GetPaginationCountByMedicineId(int medicineId,int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetPageCount(p => p.DrugId == medicineId,pageCount);
        }

        public int GetPaginationCountByUserId(int userId, int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetPageCount(p => p.UserId == userId, pageCount);
        }

        public int GetTotalCountByMedicineId(int id)
        {
            return GetCount(p => p.DrugId == id);
        }

        public int GetTotalCountByUserId(int id)
        {
            return GetCount(p => p.UserId == id);
        }
        public IEnumerable<DrugReview> GetByMedicineId(int id,int page = 1,int pageCount = 10)
        {
            if (page < 1)
                page = 1;
            if (pageCount < 10)
                pageCount = 10;
            return GetMany(p => p.DrugId == id, page, pageCount);
        }

        public IEnumerable<DrugReview> GetByUserId(int id, int page = 1, int pageCount = 10)
        {
            if (page < 1)
                page = 1;
            if (pageCount < 10)
                pageCount = 10;
            return GetMany(p => p.UserId == id, page, pageCount);
        }
    }
}
