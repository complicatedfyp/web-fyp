﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FYPWeb.Repository.Context;
using Model.Entity;

namespace FYPWeb.Repository.Repository
{
    public class DrugDetailRepository : BaseRepository<DrugDetail>
    {
        public DrugDetailRepository(AppContext context) : base(context)
        {
        }

        public IEnumerable<DrugDetail> GetDrugDetails(int page = 1, int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetAll(page, pageCount);
        }

        public IEnumerable<DrugDetail> GetDrugDetailsSearch(string parameter, int page = 1, int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetMany(user => ((user.Name).Contains(parameter)), page, pageCount);
        }

        public int GetDrugDetailsSearchCount(string parameter, int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetPageCount(user => ((user.Name).Contains(parameter)), pageCount);
        }
        public int GetPaginationCount(int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetPageCount(pageCount);
        }

    }
}
