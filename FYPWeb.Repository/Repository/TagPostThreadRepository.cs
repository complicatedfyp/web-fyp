﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FYPWeb.Repository.Context;
using Model.Entity;

namespace FYPWeb.Repository.Repository
{
    public class TagPostThreadRepository : BaseRepository<TagPostThread>
    {
        public TagPostThreadRepository(AppContext context) : base(context)
        {
        }
        public IEnumerable<Tag> GetTagsByPostId(int postId)
        {
            return GetMany(p => p.PostThreadId == postId).Select(p => p.Tag);
        }
        public IEnumerable<PostThread> GetPostsByTagId(int tagId,int page = 1,int pageCount = 10)
        {
            if (pageCount < 0) pageCount = 10;
            if (page < 0) page = 10;
            return GetMany(p => p.TagId == tagId,page,pageCount).Select(p => p.PostThread);
        }

        public void DeleteByPostIdTagId(int postId,int tagId)
        {
            Delete(p => p.PostThreadId == postId && p.TagId == tagId);
        }
    }
}
