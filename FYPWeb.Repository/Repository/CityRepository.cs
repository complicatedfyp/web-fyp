﻿using FYPWeb.Repository.Context;
using Model.Entity;

namespace FYPWeb.Repository.Repository
{
    public class CityRepository : BaseRepository<City>
    {
        public CityRepository(AppContext context) : base(context)
        {
        }

        
    }
}
