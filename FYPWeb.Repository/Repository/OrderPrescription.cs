﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FYPWeb.Repository.Context;
using Model.Entity;

namespace FYPWeb.Repository.Repository
{
    public class OrderPrescriptionRepository : BaseRepository<OrderPrescription>
    {
        public OrderPrescriptionRepository(AppContext context) : base(context)
        {
        }

        public IEnumerable<OrderPrescription> GetByOrderId(int orderId)
        {
            return GetMany(p => p.OrderId == orderId);
        }
    }
}
