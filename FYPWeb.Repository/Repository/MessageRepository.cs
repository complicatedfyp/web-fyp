﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FYPWeb.Repository.Context;
using Model.Entity;

namespace FYPWeb.Repository.Repository
{
    public class MessageRepository : BaseRepository<Message>
    {
        public MessageRepository(AppContext context) : base(context)
        {
        }
    }
}
