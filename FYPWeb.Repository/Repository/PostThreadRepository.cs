﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FYPWeb.Repository.Context;
using Model.Entity;

namespace FYPWeb.Repository.Repository
{
    public class PostThreadRepository : BaseRepository<PostThread>
    {
        public PostThreadRepository(AppContext context) : base(context)
        {
        }

        public IEnumerable<PostThread> GetThreadsByUserId(int userId)
        {
            return GetMany(p => p.UserId == userId);
        }
        public IEnumerable<PostThread> GetThreadsByUserId(int userId, int page, int pageCount = 10)
        {
            if (pageCount < 0)
            {
                pageCount = 10;
            }
            if (page < 0)
            {
                page = 1;
            }
            return GetMany(p => p.UserId == userId, page, pageCount);
        }
        public IEnumerable<PostThread> GetThreadSearchByUserId(int userId, string parameter, int page, int pageCount = 10)
        {
            if (pageCount < 0)
            {
                pageCount = 10;
            }
            if (page < 0)
            {
                page = 1;
            }
            return GetMany(p => p.Subject.Contains(parameter) && p.UserId == userId, page, pageCount);
        }
        public IEnumerable<PostThread> GetLatestThreads(int page = 1,int pageSize = 10)
        {
            return Context.Set<PostThread>().OrderByDescending(p => p.PostedAt).Skip((page - 1) * pageSize).Take(pageSize);
        }
        public IEnumerable<PostThread> GetLatestThreads(string parameter,int page = 1, int pageSize = 10)
        {
            return Context.Set<PostThread>().Where(p => p.Subject.Contains(parameter)).OrderByDescending(p => p.PostedAt).Skip((page - 1) * pageSize).Take(pageSize);
        }
        public IEnumerable<PostThread> GetLatestThreadsByTag(string parameter, int page = 1, int pageSize = 10)
        {
            return Context.Set<PostThread>().Where(p => p.TagPostThreads.Any(pr => pr.Tag.Name == parameter)).OrderByDescending(p => p.PostedAt).Skip((page - 1) * pageSize).Take(pageSize);
        }
        public IEnumerable<PostThread> GetLatestThreadsByCategory(int categoryId, int page = 1, int pageSize = 10)
        {
            return Context.Set<PostThread>().Where(p => p.PostCategoryId == categoryId).OrderByDescending(p => p.PostedAt).Skip((page - 1) * pageSize).Take(pageSize);
        }
        public IEnumerable<PostThread> GetThreads(int page, int pageCount = 10)
        {
            if (page < 0) page = 1;
            if (pageCount < 0) pageCount = 10;
            return GetAll(page, pageCount);
        }
        public IEnumerable<PostThread> GetThreadSearch(string parameter, int page, int pageCount = 10)
        {
            if (page < 0) page = 1;
            if (pageCount < 0) pageCount = 10;
            return GetMany(p => p.Subject.Contains(parameter), page, pageCount);
        }
        public int GetPaginationCount(int pageCount)
        {
            if (pageCount < 0) pageCount = 10;
            return GetPageCount(pageCount);
        }

        public int GetPaginationCountByCategory(int categoryId, int pageCount)
        {
            if (pageCount < 0) pageCount = 10;
            return GetPageCount(p => p.PostCategoryId == categoryId, pageCount);
        }
        public int GetPaginationCountByTag(string tag, int pageCount)
        {
            if (pageCount < 0) pageCount = 10;
            return GetPageCount(p => p.TagPostThreads.Any(pr => pr.Tag.Name == tag),pageCount);
        }
        public int GetPaginationSearchCount(string parameter, int pageCount)
        {
            if (pageCount < 0) pageCount = 10;
            return GetPageCount(p => p.Subject.Contains(parameter), pageCount);
        }
        public int GetPaginationCountByUserId(int userId, int pageCount)
        {
            if (pageCount < 0) pageCount = 10;
            return GetPageCount(p => p.UserId == userId, pageCount);
        }
        public int GetPaginationSearchCountByUserId(int userId, string parameter, int pageCount)
        {
            if (pageCount < 0) pageCount = 10;
            return GetPageCount(p => p.UserId == userId && p.Subject.Contains(parameter), pageCount);
        }

        public IEnumerable<PostThread> GetLatestThreadsBySearchAndTag(string parameter,int page = 1,int pageSize = 10)
        {
            return GetMany(p => p.Subject.Contains(parameter) || p.TagPostThreads.Any(pr => pr.Tag.Name == parameter), page, pageSize);
        }
        public IEnumerable<PostThread> GetHotThreads(int page = 1, int pageSize = 10)
        {
            return Context.PostThreads.OrderByDescending(p => p.PostReplies.Count).Take(pageSize);
        }

    }
}
