﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FYPWeb.Repository.Context;
using Model.Entity;

namespace FYPWeb.Repository.Repository
{
    public class PostCategoryRepository : BaseRepository<PostCategory>
    {
        public PostCategoryRepository(AppContext context) : base(context)
        {
        }

        public IEnumerable<PostCategory> GetTopCategories(int page = 1, int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return Context.Set<PostCategory>().OrderByDescending(p => p.PostThreads.Count).Skip((page - 1) * pageCount).Take(pageCount);
        }
        public IEnumerable<PostCategory> GetCategories(int page = 1, int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetAll(page, pageCount);
        }
        public IEnumerable<PostCategory> GetCategoriesSearch(string parameter, int page = 1, int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetMany(user => user.CategoryName.Contains(parameter), page, pageCount);
        }
        public int GetCategoriesSearchCount(string parameter, int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetPageCount(user => user.CategoryName.Contains(parameter), pageCount);
        }


        public int GetPaginationCount(int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetPageCount(pageCount);
        }
        public int GetPaginationSearchCount(string parameter,int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetPageCount(p => p.CategoryName.Contains(parameter), pageCount);
        }

    }
}
