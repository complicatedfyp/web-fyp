﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FYPWeb.Repository.Context;
using Model.Entity;

namespace FYPWeb.Repository.Repository
{
    public class RecommendationRepository : BaseRepository<Recommendation>
    {
        public RecommendationRepository(AppContext context) : base(context)
        {
        }

        public int GetUniqueRecommendationByMedicineId(int medicineId)
        {
            return GetGroupByCount(p => p.DrugId.Value);
        }

        public IEnumerable<Recommendation> GetByRecievedUserId(int id,int page = 1, int pageSize = 10)
        {
            return GetMany(p => p.ToUserId != null && p.ToUserId == id,page, pageSize);
        }
        public int GetPageCountByRecievedUserId(int id,int pageSize = 10)
        {
            return GetPageCount(p => p.ToUserId != null && p.ToUserId == id,pageSize);
        }

        public IEnumerable<Recommendation> GetByFromUserId(int id, int page = 1, int pageSize = 10)
        {
            return GetMany(p => p.FromUserId != null && p.FromUserId == id, page, pageSize);
        }
        public int GetPageCountByFromUserId(int id, int pageSize = 10)
        {
            return GetPageCount(p => p.FromUserId != null && p.FromUserId == id, pageSize);
        }
    }
}
