﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FYPWeb.Repository.Context;
using Model.Entity;

namespace FYPWeb.Repository.Repository
{
    public class PostReplyRepository : BaseRepository<PostReply>
    {
        public PostReplyRepository(AppContext context) : base(context)
        {
        }

        public IEnumerable<PostReply> GetRepliesByUserId(int userId, int page, int pageCount = 10)
        {
            if (pageCount < 0) pageCount = 10;
            if (page < 0) page = 1;
            return GetMany(p => p.UserId == userId, page, pageCount);
        }
        public IEnumerable<PostReply> GetRepliesSearchByUserId(int userId,string parameter, int page, int pageCount = 10)
        {
            if (pageCount < 0) pageCount = 10;
            if (page < 0) page = 1;
            return GetMany(p => p.UserId == userId && p.PostThread.Subject.Contains(parameter), page, pageCount);
        }
        public IEnumerable<PostReply> GetRepliesByPostId(int postId,int page, int pageCount = 10)
        {
            if (pageCount < 0) pageCount = 10;
            if (page < 0) page = 1;
            return GetMany(p => p.PostThreadId == postId, page, pageCount);
        }
        public int GetPaginationCountByPostId(int postId, int pageCount = 10)
        {
            if (pageCount < 0) pageCount = 10;
            return GetPageCount(p => p.PostThreadId == postId, pageCount);
        }
        public int GetCountByPostId(int postId)
        {
            return GetCount(p => p.PostThreadId == postId);
        }
        public int GetPaginationCountByUserId(int userId, int pageCount = 10)
        {
            if (pageCount < 0) pageCount = 10;
            return GetPageCount(p => p.UserId == userId, pageCount);
        }
        public int GetPaginationSearchCountByUserId(int userId,string parameter, int pageCount = 10)
        {
            if (pageCount < 0) pageCount = 10;
            return GetPageCount(p => p.UserId == userId && p.PostThread.Subject.Contains(parameter), pageCount);
        }
    }
}
