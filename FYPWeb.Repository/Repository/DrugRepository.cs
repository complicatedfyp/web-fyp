﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FYPWeb.Repository.Context;
using Model.Entity;

namespace FYPWeb.Repository.Repository
{
    public class DrugRepository : BaseRepository<Drug>
    {
        public DrugRepository(AppContext context) : base(context)
        {

        }

        public int GetCountByBrand(int brandId)
        {
            try
            {
                return GetCount(p => p.BrandId == brandId);
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public IEnumerable<Drug> GetDrugs(int page = 1, int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetAll(page, pageCount);
        }

        public IEnumerable<Drug> GetDrugsSearch(string parameter, int page = 1, int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetMany(user => ((user.Brand.BrandName + " " + user.DrugName).Contains(parameter)), page, pageCount);
        }

        public int GetDrugsSearchCount(string parameter, int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetPageCount(user => ((user.DrugName).Contains(parameter)), pageCount);
        }


        public int GetPaginationCount(int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetPageCount(pageCount);
        }

        public IEnumerable<Drug> GetIndicationDrugsByDiseaseId(int id,int page = 1,int pageSize = 10)
        {
            return GetMany(p => p.DrugDetail.DrugDetailDiseases.Any(pr => pr.DiseaseId == id && pr.IsIndication),page,pageSize);
        }
        public IEnumerable<Drug> GetNonIndicationDrugsByDiseaseId(int id, int page = 1, int pageSize = 10)
        {
            return GetMany(p => p.DrugDetail.DrugDetailDiseases.Any(pr => pr.DiseaseId == id && !pr.IsIndication), page, pageSize);
        }

        public int GetIndicationDrugsByDiseaseIdCount(int id, int pageSize = 10)
        {
            return GetPageCount(p => p.DrugDetail.DrugDetailDiseases.Any(pr => pr.DiseaseId == id && pr.IsIndication), pageSize);
        }
        public int GetNonIndicationDrugsByDiseaseIdCount(int id, int pageSize = 10)
        {
            return GetPageCount(p => p.DrugDetail.DrugDetailDiseases.Any(pr => pr.DiseaseId == id && !pr.IsIndication), pageSize);
        }


        public IEnumerable<Drug> GetTopSoldMedicines()
        {
            return Context.Drugs.Where(p => p.InventoryTracks.Any()).OrderByDescending(p => p.OrderDetails.Count(pr => pr.Order.IsCompleted)).Take(8).AsQueryable();
        }
        public IEnumerable<Drug> GetTopSearchMedicines()
        {
            return Context.Drugs.OrderByDescending(p => p.PageVisit).Take(8).AsQueryable();
        }
        public IEnumerable<Drug> GetTopRecommendedMedicines()
        {
            return Context.Drugs.OrderByDescending(p => p.Recommendations.Count).Take(8).AsQueryable();
        }

        public IEnumerable<IdData> GetDrugsFromSearch(string paramterParameter)
        {
            return Context.Drugs.Where(p => p.DrugForm.Contains(paramterParameter)).GroupBy(p => p.DrugForm).Select(p => new IdData() {Id = p.Key, Data = p.Key});
        }

        public class IdData
        {
            public string Id { get; set; }
            public string Data { get; set; }
        }

        public IEnumerable<Drug> GetDrugs(string parameter = "", int brandId = 0, int rating = 0, int min = 0, int max = 0, string form = "",int sortBy=0, int page = 1,int pageSize = 10)
        {
            var isBrand = brandId > 0;
            var isRating = rating > 0;
            var isMin = min > 0;
            var isMax = max > 0;
            var isForm = !string.IsNullOrEmpty(form);
            var list =
                GetMany(
                    p =>
                        p.DrugName.Contains(parameter) && (!isBrand || p.BrandId == brandId) &&
                        (!isForm || p.DrugForm == form) &&
                        (!isRating || p.DrugReviews.Average(pr => pr.Rating) > rating)
                        );
            DrugRepository.SearchType type = (DrugRepository.SearchType)sortBy;
            IOrderedQueryable<Drug> orderedList;
            switch (type)
            {
                case DrugRepository.SearchType.SortByNone:
                    orderedList = list.OrderBy(p => p.Id);
                    break;
                case DrugRepository.SearchType.SortByName:
                    orderedList = list.OrderBy(p => p.DrugName);
                    break;
                case DrugRepository.SearchType.SortByNameDesc:
                    orderedList = list.OrderByDescending(p => p.DrugName);
                    break;
                case DrugRepository.SearchType.SortByRating:
                    orderedList = list.OrderBy(p => p.DrugReviews.Average(pr => pr.Rating));
                    break;
                case DrugRepository.SearchType.SortByRatingDesc:
                    orderedList = list.OrderByDescending(p => p.DrugReviews.Average(pr => pr.Rating));
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            return orderedList.Skip((page - 1) * pageSize).Take(pageSize).ToList();
        }

        public int GetDrugsCount(string parameter = "", int brandId = 0, int rating = 0, int min = 0, int max = 0, string form = "",int pageSize = 10)
        {
            var isBrand = brandId > 0;
            var isRating = rating > 0;
            var isMin = min > 0;
            var isMax = max > 0;
            var isForm = !string.IsNullOrEmpty(form);
            return
                GetPageCount(
                    p =>
                        p.DrugName.Contains(parameter) && (!isBrand || p.BrandId == brandId) &&
                        (!isForm || p.DrugForm == form) &&
                        (!isRating || p.DrugReviews.Average(pr => pr.Rating) > rating), pageSize);
        }

        public IEnumerable<Drug> GetDrugSoldFromNear(float lat, float lng)
        {
            var coord = DbGeography.FromText(
                $"POINT({lng.ToString(CultureInfo.CurrentCulture).Replace(",", ".")} {lat.ToString(CultureInfo.CurrentCulture).Replace(",", ".")})");
            var nearest = (from h in Context.Orders
                let geo = DbGeography.FromText("POINT(" + h.DeliveryLocationX + " " + h.DeliveryLocationX + ")")
                where h.IsCompleted
                orderby geo.Distance(coord), h.CompletedAt descending
                select h).Take(20).ToList();
            var drugs = nearest.Select(p => p.OrderDetails.Select(pr => pr.Drug).ToList()).ToList();
            var list = drugs.SelectMany(drug => drug).OrderByDescending(p => p.OrderDetails.Sum(pr => pr.Quantity)).GroupBy(p => p.Id).Select(p => p.First()).ToList();
            return list.Take(5);
        }
        public enum SearchType
        {
            SortByNone = 0,
            SortByName = 1,
            SortByNameDesc = 2,
            SortByPrice = 3,
            SortByPriceDesc = 4,
            SortByRating = 5,
            SortByRatingDesc = 6,
        }
    }

}
