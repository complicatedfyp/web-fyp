﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FYPWeb.Repository.Context;
using Model.Entity;

namespace FYPWeb.Repository.Repository
{
    public class BrandRepository : BaseRepository<Brand>
    {
        public BrandRepository(AppContext context) : base(context)
        {
        }

        public int GetBrandCountByCompany(int companyId)
        {
            return GetCount(p => p.CompanyId == companyId);
        }

        public IEnumerable<Brand> GetBrands(int page = 1, int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetAll(page, pageCount);
        }

        public IEnumerable<Brand> GetBrandsSearch(string parameter, int page = 1, int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetMany(user => ((user.BrandName).Contains(parameter) || user.Company.CompanyName.Contains(parameter)), page, pageCount);
        }

        public int GetBrandsSearchCount(string parameter, int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetPageCount(user => ((user.BrandName).Contains(parameter) || user.Company.CompanyName.Contains(parameter)), pageCount);
        }


        public int GetPaginationCount(int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetPageCount(pageCount);
        }

    }
}
