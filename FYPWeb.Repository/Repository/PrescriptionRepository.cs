﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FYPWeb.Repository.Context;
using Model.Entity;

namespace FYPWeb.Repository.Repository
{
    public class PrescriptionRepository : BaseRepository<Prescription>
    {
        public PrescriptionRepository(AppContext context) : base(context)
        {
        }

        public IEnumerable<Prescription> GetByUserId(int userId)
        {
            return GetMany(p => p.UserId == userId);
        }
    }
}
