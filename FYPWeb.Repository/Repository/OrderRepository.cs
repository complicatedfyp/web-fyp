﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using FYPWeb.Repository.Context;
using Model.Entity;

namespace FYPWeb.Repository.Repository
{
    public class OrderRepository : BaseRepository<Order>
    {
        public OrderRepository(AppContext context) : base(context)
        {

        }

        public int GetPendingOrdersByUserIdCount(int id,int pageSize = 10)
        {
            return GetPageCount(p => !p.IsCanceled && !p.IsAccepted && !p.IsRejected && !p.IsCompleted && p.UserId == id, pageSize);
        }
        public int GetActiveOrdersByUserIdCount(int id, int pageSize = 10)
        {
            return GetPageCount(p => !p.IsCanceled && p.IsAccepted && !p.IsRejected && !p.IsCompleted && p.UserId == id, pageSize);
        }
        public int GetCancelledOrdersByUserIdCount(int id, int pageSize = 10)
        {
            return GetPageCount(p => p.IsCanceled && !p.IsRejected && !p.IsCompleted && p.UserId == id, pageSize);
        }
        public int GetRejectedOrdersByUserIdCount(int id, int pageSize = 10)
        {
            return GetPageCount(p => !p.IsCanceled && p.IsRejected && !p.IsCompleted && p.UserId == id, pageSize);
        }
        public IEnumerable<Order> GetCompletedOrdersByUserId(int id, int page = 1,int pageSize = 10)
        {
            return GetMany(p => !p.IsCanceled && !p.IsRejected && p.IsCompleted && p.UserId == id, page,pageSize);
        }

        public IEnumerable<Order> GetPendingOrdersByUserId(int id, int page = 1, int pageSize = 10)
        {
            return GetMany(p => !p.IsCanceled && !p.IsAccepted && !p.IsRejected && !p.IsCompleted && p.UserId == id, page,pageSize);
        }
        public IEnumerable<Order> GetActiveOrdersByUserId(int id, int page = 1,int pageSize = 10)
        {
            return GetMany(p => !p.IsCanceled && p.IsAccepted && !p.IsRejected && !p.IsCompleted && p.UserId == id, page, pageSize);
        }
        public IEnumerable<Order> GetCancelledOrdersByUserId(int id,int page = 1, int pageSize = 10)
        {
            return GetMany(p => p.IsCanceled && !p.IsRejected && !p.IsCompleted && p.UserId == id, page, pageSize);
        }
        public IEnumerable<Order> GetRejectedOrdersByUserId(int id,int page, int pageSize = 10)
        {
            return GetMany(p => !p.IsCanceled && p.IsRejected && !p.IsCompleted && p.UserId == id, page, pageSize);
        }
        public int GetCompletedOrdersByUserIdCount(int id, int pageSize = 10)
        {
            return GetPageCount(p => !p.IsCanceled && !p.IsRejected && p.IsCompleted && p.UserId == id, pageSize);
        }

        public int GetPendingOrdersByPharmacyIdCount(int id, int pageSize = 10)
        {
            return GetPageCount(p => !p.IsCanceled && !p.IsAccepted && !p.IsRejected && !p.IsCompleted && p.PharmacyId == id, pageSize);
        }
        public int GetActiveOrdersByPharmacyIdCount(int id, int pageSize = 10)
        {
            return GetPageCount(p => !p.IsCanceled && p.IsAccepted && !p.IsRejected && !p.IsCompleted && p.PharmacyId == id, pageSize);
        }
        public int GetCancelledOrdersByPharmacyIdCount(int id, int pageSize = 10)
        {
            return GetPageCount(p => p.IsCanceled && !p.IsRejected && !p.IsCompleted && p.PharmacyId == id, pageSize);
        }
        public int GetRejectedOrdersByPharmacyIdCount(int id, int pageSize = 10)
        {
            return GetPageCount(p => !p.IsCanceled && p.IsRejected && !p.IsCompleted && p.PharmacyId == id, pageSize);
        }
        public IEnumerable<Order> GetCompletedOrdersByPharmacyId(int id, int page = 1, int pageSize = 10)
        {
            return GetMany(p => !p.IsCanceled && !p.IsRejected && p.IsCompleted && p.PharmacyId == id, page, pageSize);
        }

        public IEnumerable<Order> GetPendingOrdersByPharmacyId(int id, int page = 1, int pageSize = 10)
        {
            return GetMany(p => !p.IsCanceled && !p.IsAccepted && !p.IsRejected && !p.IsCompleted && p.PharmacyId == id, page, pageSize);
        }
        public IEnumerable<Order> GetActiveOrdersByPharmacyId(int id, int page = 1, int pageSize = 10)
        {
            return GetMany(p => !p.IsCanceled && p.IsAccepted && !p.IsRejected && !p.IsCompleted && p.PharmacyId == id, page, pageSize);
        }
        public IEnumerable<Order> GetCancelledOrdersByPharmacyId(int id, int page = 1, int pageSize = 10)
        {
            return GetMany(p => p.IsCanceled && !p.IsRejected && !p.IsCompleted && p.PharmacyId == id, page, pageSize);
        }
        public IEnumerable<Order> GetRejectedOrdersByPharmacyId(int id, int page, int pageSize = 10)
        {
            return GetMany(p => !p.IsCanceled && p.IsRejected && !p.IsCompleted && p.PharmacyId == id, page, pageSize);
        }
        public int GetCompletedOrdersByPharmacyIdCount(int id, int pageSize = 10)
        {
            return GetPageCount(p => !p.IsCanceled && !p.IsRejected && p.IsCompleted && p.PharmacyId == id, pageSize);
        }

        public IEnumerable<Order> GenerateReport(int pharmacyId, int[] employeeId, DateTime startDate, DateTime endDate)
        {
            endDate = endDate.AddHours(23).AddMinutes(59);
            if (employeeId.Contains(0))
            {
                return
                    GetMany(
                        p =>
                            p.IsCompleted && p.PharmacyId == pharmacyId && p.OrderedAt >= startDate &&
                            p.OrderedAt <= endDate);
            }
            return GetMany(p => p.IsCompleted && p.PharmacyId == pharmacyId && p.EmployeeId != null && employeeId.Contains(p.EmployeeId.Value) && p.OrderedAt >= startDate &&
                                p.OrderedAt <= endDate);
        }

        public IEnumerable<StringValue> GetMonthlySaleByPharmacy(int id)
        {
            return 
                GetMany(p => p.PharmacyId == id && p.IsCompleted)
                    .GroupBy(p => p.OrderedAt.Month)
                    .Select(
                        p =>
                            new StringValue
                            {
                                Title = (p.Key) + "",
                                Value = p.Sum(pr => pr.OrderDetails.Sum(prr => prr.Price * prr.Quantity))
                            }).ToList();

        }
        public class StringValue
        {
            public float Value { get; set; }
            public string Title { get; set; }
        }

        public IEnumerable<StringValue> GetSaleByEmployee(int id)
        {
            return
                GetMany(p => p.PharmacyId == id && p.IsCompleted)
                    .GroupBy(p => p.EmployeeId)
                    .Select(
                        p =>
                            new StringValue
                            {
                                Title = (p.FirstOrDefault().Employee.Name) + "",
                                Value = p.Sum(pr => pr.OrderDetails.Sum(prr => prr.Price * prr.Quantity))
                            }).ToList();


        }

        public IEnumerable<StringValue> GetLastOrdersData(int id,int count)
        {
            return
                Context.Orders.Where(p => p.UserId == id && p.IsCompleted).OrderByDescending(p => p.CompletedAt).Take(count)
                    .ToList().Select(
                        p =>
                            new StringValue
                            {
                                Title = (p.OrderedAt.ToShortDateString()) + "",
                                Value = p.OrderDetails.Sum(prr => prr.Price * prr.Quantity)
                            }).ToList();


        }
    }
}
