﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FYPWeb.Repository.Context;
using Model.Entity;

namespace FYPWeb.Repository.Repository
{
    public class SessionCartRepository : BaseRepository<SessionCart>
    {
        public SessionCartRepository(AppContext context) : base(context)
        {
        }

        public int GetUniqueMedicineCountBySessionId(string sessionId)
        {
            return GetMany(p => p.SessionId == sessionId).GroupBy(p => p.InventoryId).Count();
        }

        public SessionCart GetByMedicineIdAndSessionId(int medicineId, string sessionId)
        {
            return Get(p => p.InventoryId == medicineId && p.SessionId == sessionId);
        }

        public bool DeleteByMedicineIdAndSessionId(int medicineId, string sessionId)
        {
            Delete(p => p.InventoryId == medicineId && p.SessionId == sessionId);
            return true;
        }

        public IEnumerable<SessionCart> GetBySessionId(string sessionId)
        {
            return GetMany(p => p.SessionId == sessionId);
        }

        public bool DeleteBySessionId(string sessionId)
        {
            DeleteAll(p => p.SessionId == sessionId);
            return true;
        }

    }
}
