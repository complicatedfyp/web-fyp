﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FYPWeb.Repository.Context;
using Model.Entity;

namespace FYPWeb.Repository.Repository
{
    public class CustomerReviewRepository : BaseRepository<CustomerReview>
    {
        public CustomerReviewRepository(AppContext context) : base(context)
        {
        }

        public int GetReviewPageCountByUser(int userId)
        {
            return GetPageCount(p => p.UserId == userId);
        }
        public IEnumerable<CustomerReview> GetReviewsByUserId(int userId,int page = 1, int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetMany(p => p.UserId == userId,page, pageCount);
        }
        public IEnumerable<CustomerReview> GetReviews(int page = 1, int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetAll(page, pageCount);
        }

        public int GetAllReviewsPaginationCount(int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetPageCount(pageCount);
        }

    }
}
