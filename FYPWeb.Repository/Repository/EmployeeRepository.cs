﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FYPWeb.Repository.Context;
using Model.Entity;

namespace FYPWeb.Repository.Repository
{
    public class EmployeeRepository : BaseRepository<Employee>
    {
        public EmployeeRepository(AppContext context) : base(context)
        {
        }
        public Employee GetByIdAndUser(int id, int userId)
        {
            return Get(p => p.UserId == userId && p.Id == id);
        }
        public IEnumerable<Employee> GetEmployees(int page, int pageCount)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetAll(page, pageCount);
        }
        public IEnumerable<Employee> GetEmployees(int userId, int page, int pageCount)
        {
            if (page < 1)
                page = 1;
            if (pageCount < 1)
                pageCount = 10;
            return GetMany(user => user.UserId == userId, page, pageCount);
        }

        public IEnumerable<Employee> GetEmployeesSearch(string parameter, int userId, int page, int pageCount)
        {
            if (page < 1)
                page = 1;
            if (pageCount < 1)
                pageCount = 10;
            return GetMany(user => ((user.Name).Contains(parameter)), page, pageCount);
        }

        public IEnumerable<Employee> GetEmployeesSearch(string parameter, int page, int pageCount)
        {
            if (page < 1)
                page = 1;
            if (pageCount < 1)
                pageCount = 10;
            return GetMany(user => ((user.Name).Contains(parameter)), page, pageCount);
        }

        public int GetEmployeesSearchCount(string parameter, int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetPageCount(user => ((user.Name).Contains(parameter)), pageCount);
        }
        public int GetEmployeesSearchCount(string parameter, int userId, int pageCount)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetPageCount(user => ((user.Name).Contains(parameter)) && user.UserId == userId, pageCount);
        }
        public int GetPaginationCount(int userId, int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetPageCount(pageCount);
        }


        public IEnumerable<Employee> GetEmployeesByPharmacyId(int parameterPharmacyId, int page)
        {
            return GetMany(p => p.PharmacyId == parameterPharmacyId,page);
        }
        public int GetCountEmployeesByPharmacyId(int parameterPharmacyId, int pageSize = 10)
        {
            return GetPageCount(p => p.PharmacyId == parameterPharmacyId, pageSize);
        }
        public IEnumerable<Employee> GetEmployeesByUserId(int parameterPharmacyId, int page)
        {
            return GetMany(p => p.UserId == parameterPharmacyId, page);
        }

        public int GetCountEmployeesByUserId(int parameterPharmacyId, int pageSize = 10)
        {
            return GetPageCount(p => p.UserId == parameterPharmacyId, pageSize);
        }

        public IEnumerable<Employee> GetEmployeesSearchByUserId(int userId,string parameterSearch, int page)
        {
            return GetMany(p => p.UserId == userId && p.Name.Contains(parameterSearch), page);
        }
        public int GetCountEmployeesSearchByUserId(int userId, string parameterSearch, int pageSize = 10)
        {
            return GetPageCount(p => p.UserId == userId && p.Name.Contains(parameterSearch), pageSize);
        }
    }
}
