﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FYPWeb.Repository.Context;
using Model.Entity;

namespace FYPWeb.Repository.Repository
{
    public class PharmacistReviewRepository : BaseRepository<PharmacyReview>
    {
        public PharmacistReviewRepository(AppContext context) : base(context)
        {
        }

        public int GetReviewCountByPharmacy(int pharmacyId)
        {
            return GetCount(p => p.PharmacyId == pharmacyId);
        }
        public int GetReviewPageCountByPharmacy(int pharmacyId,int pageSize = 10)
        {
            return GetPageCount(p => p.PharmacyId == pharmacyId,pageSize);
        }
        public IEnumerable<PharmacyReview> GetReviewsByPharmacy(int pharmacyId,int page = 1, int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetMany(p => p.PharmacyId == pharmacyId,page, pageCount);
        }

        public int GetPaginationCount(int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetPageCount(pageCount);
        }
    }
}
