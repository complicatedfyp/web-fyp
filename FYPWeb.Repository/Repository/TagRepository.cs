﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FYPWeb.Repository.Context;
using Model.Entity;

namespace FYPWeb.Repository.Repository
{
    public class TagRepository : BaseRepository<Tag>
    {
        public TagRepository(AppContext context) : base(context)
        {
        }

        public IEnumerable<int> GetIdsOfTags(IEnumerable<string> tags)
        {
            var list = new List<int>();
            foreach (var tag in tags)
            {
                var d = Get(p => p.Name == tag);
                if (d != null)
                {
                    list.Add(d.Id);
                }
                else
                {
                    var t = new Tag()
                    {
                        Name = tag
                    };
                    Add(t);
                    list.Add(t.Id);
                }
            }
            return list;
        }

        public IEnumerable<Tag> GetTags(IEnumerable<string> tags)
        {
            var list = new List<Tag>();
            foreach (var tag in tags)
            {
                var d = Get(p => p.Name == tag);
                if (d != null)
                {
                    list.Add(d);
                }
                else
                {
                    var t = new Tag()
                    {
                        Name = tag
                    };
                    Add(t);
                    list.Add(t);
                }
            }
            return list;
        }

        public IEnumerable<Tag> GetAllTags(int page = 1,int pageSize = 10)
        {
            return GetAll(page,pageSize);
        }
        public IEnumerable<Tag> GetAllTagsSearch(string parameter,int page = 1,int pageSize = 10)
        {
            return GetMany(p => p.Name.Contains(parameter),page,pageSize);
        }

        public int GetPaginationCount(int pageSize = 10)
        {
            return GetPageCount(pageSize);
        }
        public int GetPaginationSearchCount(string parameter,int pageSize = 10)
        {
            return GetPageCount(p => p.Name.Contains(parameter), pageSize);
        }
    }
}
