﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FYPWeb.Repository.Context;
using Model.Entity;

namespace FYPWeb.Repository.Repository
{
    public class DrugCartRepository : BaseRepository<DrugCart>
    {
        public DrugCartRepository(AppContext context) : base(context)
        {
        }

        public int GetUniqueMedicineCountByUserId(int userId)
        {
            return GetMany(p => p.UserId == userId).GroupBy(p => p.InventoryId).Count();
        }

        public DrugCart GetByMedicineIdAndUserId(int medicineId, int userId)
        {
            return Get(p => p.InventoryId == medicineId && p.UserId == userId);
        }

        public bool DeleteByMedicineIdAndUserId(int medicineId, int userId)
        {
            Delete(p => p.InventoryId == medicineId && p.UserId == userId);
            return true;
        }

        public IEnumerable<DrugCart> GetByUserId(int userId)
        {
            return GetMany(p => p.UserId == userId);
        }

        public bool DeleteByUserId(int userId)
        {
            DeleteAll(p => p.UserId == userId);
            return true;
        }
        public bool DeleteByUserIdAndPharmacyId(int userId,int pharmacyId)
        {
            DeleteAll(p => p.UserId == userId && p.Inventory.PharmacyId == pharmacyId);
            return true;
        }

        public IEnumerable<DrugCart> GetByUserIdAndPharmacyId(int userId,int pharmacyId)
        {
            return GetMany(p => p.UserId == userId && p.Inventory.PharmacyId == pharmacyId);
        }
    }
}
