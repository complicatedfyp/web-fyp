﻿using System.Collections.Generic;
using FYPWeb.Repository.Context;
using Model.Entity;

namespace FYPWeb.Repository.Repository
{
    public class UserRepository : BaseRepository<User>
    {
        public UserRepository(AppContext context) : base(context)
        {
        }


        public bool UserEmailIsAvailable(string email)
        {
            return Get(p => p.Email == email) != null;
        }
        public bool UserNameIsAvailable(string name)
        {
            return Get(p => p.Username == name) != null;
        }

        public User LoginIn(string email,string password,int type)
        {
            return Get(p => p.Email == email && password == p.Password && p.UserRoleId == type && p.IsActive && p.IsVerified);
        }
        public User GetByEmail(string email)
        {
            return Get(p => p.Email.ToLower() == email.ToLower());
        }

        public IEnumerable<User> GetActiveUsers(int page = 1,int pageCount = 10)
        {
            if (pageCount == 0)
                pageCount = 10;
            return GetMany(user => user.IsActive && user.UserRoleId != 1, page, pageCount);
        }

        public IEnumerable<User> GetActiveCustomers(int page = 1, int pageCount = 10)
        {
            if (pageCount == 0)
                pageCount = 10;
            return GetMany(user => user.IsActive && user.UserRoleId == 2, page, pageCount);
        }
        public IEnumerable<User> GetActiveCustomersSearch(string parameter,int page = 1, int pageCount = 10)
        {
            if (pageCount == 0)
                pageCount = 10;
            return GetMany(user => user.IsActive && user.UserRoleId == 2 && ((user.FirstName + " " + user.LastName).Contains(parameter) || user.Username.Contains(parameter)), page, pageCount);
        }
        public IEnumerable<User> GetActivePharmacistSearch(string parameter, int page = 1, int pageCount = 10)
        {
            if (pageCount == 0)
                pageCount = 10;
            return GetMany(user => user.IsActive && user.UserRoleId == 3 && ((user.FirstName + " " + user.LastName).Contains(parameter) || user.Username.Contains(parameter)), page, pageCount);
        }
        public IEnumerable<User> GetActivePharmacist(int page = 1, int pageCount = 10)
        {
            if (pageCount == 0)
                pageCount = 10;
            return GetMany(user => user.IsActive && user.UserRoleId == 3, page, pageCount);
        }
        public IEnumerable<User> GetActiveUsersSearch(string parameter,int page = 1, int pageCount = 10)
        {
            if (pageCount == 0)
                pageCount = 10;
            return GetMany(user => user.IsActive && user.UserRoleId != 1 && ((user.FirstName + " " + user.LastName).Contains(parameter) || user.Email.Contains(parameter) || user.Username.Contains(parameter)), page, pageCount);
        }

        public IEnumerable<User> GetInActiveUsersSearch(string parameter, int page = 1, int pageCount = 10)
        {
            if (pageCount == 0)
                pageCount = 10;
            return GetMany(user => !user.IsActive && user.UserRoleId != 1 && ((user.FirstName + " " + user.LastName).Contains(parameter) || user.Email.Contains(parameter) || user.Username.Contains(parameter)), page, pageCount);
        }

        public int GetInActiveUsersSearchCount(string parameter, int pageCount = 10)
        {
            if (pageCount == 0)
                pageCount = 10;
            return GetPageCount(user => !user.IsActive && user.UserRoleId != 1 && ((user.FirstName + " " + user.LastName).Contains(parameter) || user.Email.Contains(parameter) || user.Username.Contains(parameter)), pageCount);
        }

        public int GetActiveUsersSearchCount(string parameter, int pageCount = 10)
        {
            if (pageCount == 0)
                pageCount = 10;
            return GetPageCount(user => user.IsActive && user.UserRoleId != 1 && ((user.FirstName + " " + user.LastName).Contains(parameter) || user.Email.Contains(parameter) || user.Username.Contains(parameter)), pageCount);
        }

        public int GetActivePaginationCount(int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetPageCount(p => p.IsActive && p.UserRoleId != 1, pageCount);
        }

        public int GetInActivePaginationCount(int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetPageCount(p => !p.IsActive && p.UserRoleId != 1, pageCount);
        }

        public IEnumerable<User> GetInActiveUsers(int page = 1, int pageCount = 10)
        {
            if (pageCount == 0)
                pageCount = 10;
            return GetMany(user => !user.IsActive && user.UserRoleId != 1, page, pageCount);
        }

        public bool SetStatus(int parameterUserId, bool b)
        {
            var user = GetById(parameterUserId);
            if (user != null)
            {
                user.IsActive = b;
                Update(user);
                return true;
            }
            return false;
        }

        public bool SetVerifyStatus(int userId, bool b)
        {
            var user = GetById(userId);
            if (user != null)
            {
                user.IsVerified = b;
                Update(user);
                return true;
            }
            return false;
        }

        public User GetByUsername(string username)
        {
            return Get(p => p.Username.ToLower() == username.ToLower());
        }
        public int GetActivePaginationSearcCount(string parameter,int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetPageCount(user => user.IsActive && user.UserRoleId != 1 && ((user.FirstName + " " + user.LastName).Contains(parameter) || user.Email.Contains(parameter) || user.Username.Contains(parameter)), pageCount);
        }
    }
}
