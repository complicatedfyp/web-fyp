﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FYPWeb.Repository.Context;
using FYPWeb.Repository.RepositoryViewModel;
using Model.Entity;

namespace FYPWeb.Repository.Repository
{
    public class InventoryTrackRepository : BaseRepository<InventoryTrack>
    {
        public InventoryTrackRepository(AppContext context) : base(context)
        {
        }

        public IEnumerable<InventoryTrack> GetDrugsByPharmacyId(int pharmacyId,int page = 1,int pageSize = 10)
        {
            return GetMany(p => p.PharmacyId == pharmacyId).OrderBy(p => p.Id).Skip((page - 1) * pageSize).Take(pageSize);
        }
        public IEnumerable<InventoryTrack> GetDrugsByPharmacyIdSearch(int pharmacyId,string search, int page = 1, int pageSize = 10)
        {
            return GetMany(p => p.PharmacyId == pharmacyId && (p.Drug.Brand.BrandName + " " + p.Drug.DrugName).Contains(search)).OrderBy(p => p.Id).Skip((page - 1) * pageSize).Take(pageSize);
        }
        public int GetDrugsByPharmacyIdCount(int pharmacyId,int pageSize = 10)
        {
            return (int)Math.Ceiling((float)GetMany(p => p.PharmacyId == pharmacyId).GroupBy(p => p.DrugId).Count() / (float)pageSize);
        }

        public IEnumerable<InventoryTrack> GetDrugsByInventory(string parameter = "", int brandId = 0, int pharmacyId = 0, int rating = 0, int min = 0, int max = 0, string form = "", int sortBy = 0, int page = 1, int pageSize = 10)
        {
            if (form == "null")
            {
                form = "";
            }
            var isBrand = brandId > 0;
            var isPharmacy = pharmacyId > 0;
            var isRating = rating > 0;
            var isMin = min > 0;
            var isMax = max > 0;
            var isForm = !string.IsNullOrEmpty(form);
            var list =
                GetMany(
                    p =>
                        (p.Drug.Brand.BrandName + " " + p.Drug.DrugName).Contains(parameter) && (!isBrand || p.Drug.BrandId == brandId) &&
                        (!isForm || p.Drug.DrugForm == form) && (!isPharmacy || p.PharmacyId == pharmacyId) &&
                        (!isRating || p.Drug.DrugReviews.Average(pr => pr.Rating) > rating) &&
                        (!isMin || p.Price >= min) && (!isMax || p.Price <= max));
            DrugRepository.SearchType type = (DrugRepository.SearchType)sortBy;
            if (isPharmacy)
            {
                IOrderedQueryable<IGrouping<int, InventoryTrack>> orderedList = null;
                switch (type)
                {
                    case DrugRepository.SearchType.SortByNone:
                        orderedList = list.OrderBy(p => p.Id).GroupBy(p => p.DrugId)
                        .OrderBy(p => p.Key);
                        break;
                    case DrugRepository.SearchType.SortByName:
                        orderedList = list.OrderBy(p => p.Drug.DrugName).GroupBy(p => p.DrugId)
                        .OrderBy(p => p.FirstOrDefault().Drug.DrugName);
                        break;
                    case DrugRepository.SearchType.SortByNameDesc:
                        orderedList = list.OrderByDescending(p => p.Drug.DrugName).GroupBy(p => p.DrugId)
                        .OrderByDescending(p => p.FirstOrDefault().Drug.DrugName);
                        break;
                    case DrugRepository.SearchType.SortByPrice:
                        orderedList = list.OrderBy(p => p.Price).GroupBy(p => p.DrugId)
                       .OrderBy(p => p.FirstOrDefault().Price);
                        break;
                    case DrugRepository.SearchType.SortByPriceDesc:
                        orderedList = list.OrderByDescending(p => p.Price).GroupBy(p => p.DrugId)
                        .OrderByDescending(p => p.FirstOrDefault().Price);
                        break;
                    case DrugRepository.SearchType.SortByRating:
                        orderedList = list.OrderBy(p => p.Drug.DrugReviews.Average(pr => pr.Rating)).GroupBy(p => p.DrugId)
                        .OrderBy(p => p.FirstOrDefault().Drug.DrugReviews.Average(pr => pr.Rating));
                        break;
                    case DrugRepository.SearchType.SortByRatingDesc:
                        orderedList = list.OrderByDescending(p => p.Drug.DrugReviews.Average(pr => pr.Rating)).GroupBy(p => p.DrugId)
                        .OrderByDescending(p => p.FirstOrDefault().Drug.DrugReviews.Average(pr => pr.Rating));
                        break;
                }
                return
                    orderedList
                        .Skip((page - 1) * pageSize)
                        .Take(pageSize)
                        .Select(p => p.FirstOrDefault());
            }
            IOrderedQueryable<IGrouping<dynamic, InventoryTrack>> orderedList2 = null;
            switch (type)
            {
                case DrugRepository.SearchType.SortByNone:
                    orderedList2 = list.OrderBy(p => p.Id).GroupBy(p => new { p.DrugId, p.PharmacyId })
                    .OrderBy(p => p.Key.DrugId);
                    break;
                case DrugRepository.SearchType.SortByName:
                    orderedList2 = list.OrderBy(p => p.Drug.DrugName).GroupBy(p => new { p.DrugId, p.PharmacyId })
                    .OrderBy(p => p.FirstOrDefault().Drug.DrugName);
                    break;
                case DrugRepository.SearchType.SortByNameDesc:
                    orderedList2 = list.OrderByDescending(p => p.Drug.DrugName).GroupBy(p => new { p.DrugId, p.PharmacyId })
                    .OrderByDescending(p => p.FirstOrDefault().Drug.DrugName);
                    break;
                case DrugRepository.SearchType.SortByPrice:
                    orderedList2 = list.OrderBy(p => p.Price).GroupBy(p => new { p.DrugId, p.PharmacyId })
                   .OrderBy(p => p.FirstOrDefault().Price);
                    break;
                case DrugRepository.SearchType.SortByPriceDesc:
                    orderedList2 = list.OrderByDescending(p => p.Price).GroupBy(p => new { p.DrugId, p.PharmacyId })
                    .OrderByDescending(p => p.FirstOrDefault().Price);
                    break;
                case DrugRepository.SearchType.SortByRating:
                    orderedList2 = list.OrderBy(p => p.Drug.DrugReviews.Average(pr => pr.Rating)).GroupBy(p => new { p.DrugId, p.PharmacyId })
                    .OrderBy(p => p.FirstOrDefault().Drug.DrugReviews.Average(pr => pr.Rating));
                    break;
                case DrugRepository.SearchType.SortByRatingDesc:
                    orderedList2 = list.OrderByDescending(p => p.Drug.DrugReviews.Average(pr => pr.Rating)).GroupBy(p => new { p.DrugId, p.PharmacyId })
                    .OrderByDescending(p => p.FirstOrDefault().Drug.DrugReviews.Average(pr => pr.Rating));
                    break;
            }
            return
                orderedList2
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize)
                    .Select(p => p.FirstOrDefault());
        }
        
        public int GetDrugsByInventoryCount(string parameter, int brandId, int pharmacyId, int rating, int min, int max, string form)
        {
            if (form == "null")
            {
                form = "";
            }
            var isBrand = brandId > 0;
            var isPharmacy = pharmacyId > 0;
            var isRating = rating > 0;
            var isMin = min > 0;
            var isMax = max > 0;
            var isForm = !string.IsNullOrEmpty(form);
            if (isPharmacy)
            {
                return
                    GetGroupByPageCount(
                        p =>
                            (p.Drug.Brand.BrandName + " " + p.Drug.DrugName).Contains(parameter) && (!isBrand || p.Drug.BrandId == brandId) &&
                            (!isForm || p.Drug.DrugForm == form) && (!isPharmacy || p.PharmacyId == pharmacyId) &&
                            (!isRating || p.Drug.DrugReviews.Average(pr => pr.Rating) > rating) &&
                            (!isMin || p.Price >= min) && (!isMax || p.Price <= max), p => new { p.DrugId });//.GroupBy().Select(p => GetByDrugIdPharmacyId(p.Key.DrugId, p.Key.PharmacyId));
            }
            return
                GetGroupByPageCount(
                    p =>
                        p.Drug.DrugName.Contains(parameter) && (!isBrand || p.Drug.BrandId == brandId) &&
                        (!isForm || p.Drug.DrugForm == form) && (!isPharmacy || p.PharmacyId == pharmacyId) &&
                        (!isRating || p.Drug.DrugReviews.Average(pr => pr.Rating) > rating) &&
                        (!isMin || p.Price >= min) && (!isMax || p.Price <= max), p => new { p.DrugId, p.PharmacyId });//.GroupBy().Select(p => GetByDrugIdPharmacyId(p.Key.DrugId, p.Key.PharmacyId));
        }

        public bool UpdateTrack(int pharmacyId, int drugId, int quantity)
        {
            try
            {

                var d = Get(p => p.DrugId == drugId && p.PharmacyId == pharmacyId);
                if (d != null)
                {
                    if (quantity > 0)
                    {
                        d.TotalCount += quantity;
                        d.LastUpdated = DateTime.Now;
                        Update(d);
                        return true;
                    }
                    else
                    {
                        if (-quantity > d.TotalCount)
                        {
                            return false;
                        }
                        d.TotalCount += quantity;
                        d.LastUpdated = DateTime.Now;
                        Update(d);
                        return true;
                    }
                }
                else
                {
                    if (quantity <= 0)
                    {
                        return false;
                    }
                    Add(new InventoryTrack()
                    {
                        DrugId = drugId,
                        LastUpdated = DateTime.Now,
                        PharmacyId = pharmacyId,
                        TotalCount = quantity,
                    });
                    return true;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return false;
        }

        public int GetTotalMedicineCountByPharmacy(int pharmacyId)
        {
            try
            {
                return GetMany(p => p.PharmacyId == pharmacyId).Sum(p => p.TotalCount);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return 0;
        }

        public int GetTotalMedicineCountByPharmacy(int pharmacyId, string parameter)
        {
            try
            {
                return GetMany(p => p.PharmacyId == pharmacyId && (p.Drug.Brand + " " + p.Drug.DrugName).Contains(parameter)).Sum(p => p.TotalCount);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return 0;
        }

        public IEnumerable<InventoryTrack> GetMedicineInventoryByPharmacy(int pharmacyId,int page = 1,int pageSize = 10)
        {
            return GetMany(p => p.PharmacyId == pharmacyId, page, pageSize);
        }
        public IEnumerable<InventoryTrack> GetMedicineInventoryByPharmacy(int pharmacyId,int[] drugsIds, int page = 1, int pageSize = 10)
        {
            return GetMany(p => p.PharmacyId == pharmacyId && drugsIds.Contains(p.DrugId), page, pageSize);
        }
        public int GetMedicineInventoryByPharmacyCount(int pharmacyId, int pageSize = 10)
        {
            return GetPageCount(p => p.PharmacyId == pharmacyId, pageSize);
        }


        public IEnumerable<InventoryTrack> GetMedicineInventoryByPharmacySearch(int pharmacyId, string parameter, int page = 1, int pageSize = 10)
        {
            return GetMany(p => p.PharmacyId == pharmacyId && (p.Drug.Brand.BrandName + " " + p.Drug.DrugName).Contains(parameter), page, pageSize);
        }
        public int GetMedicineInventoryByPharmacySearchCount(int pharmacyId, string parameter, int pageSize = 10)
        {
            return GetPageCount(p => p.PharmacyId == pharmacyId && (p.Drug.Brand.BrandName + " " + p.Drug.DrugName).Contains(parameter), pageSize);
        }

        public IEnumerable<InventoryTrack> GetByDrugId(int id,int page = 1,int pageSize = 10)
        {
            return GetMany(p => p.DrugId == id, page, pageSize);
        }

        public int GetDrugsByPharmacyIdSearchCount(int pharmacyId, string search,int pageSize = 10)
        {
            return GetPageCount(
                p => p.PharmacyId == pharmacyId && (p.Drug.Brand.BrandName + " " + p.Drug.DrugName).Contains(search), pageSize);
        }
    }
}
