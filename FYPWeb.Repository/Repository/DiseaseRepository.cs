﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FYPWeb.Repository.Context;
using Model.Entity;

namespace FYPWeb.Repository.Repository
{
    public class DiseaseRepository : BaseRepository<Disease>
    {
        public DiseaseRepository(AppContext context) : base(context)
        {
        }

        public IEnumerable<Disease> GetDiseases(int page = 1, int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetAll(page, pageCount);
        }

        public IEnumerable<Disease> GetDiseasesSearch(string parameter, int page = 1, int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetMany(user => ((user.DiseaseName).Contains(parameter)), page, pageCount);
        }

        public int GetDiseasesSearchCount(string parameter, int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetPageCount(user => ((user.DiseaseName).Contains(parameter)), pageCount);
        }


        public int GetPaginationCount(int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetPageCount(pageCount);
        }

        public IEnumerable<Disease> GetTopSearchedDiseases(int pageSize = 10)
        {
            return Context.Diseases.OrderByDescending(p => p.PageVisit).Take(pageSize).AsQueryable();
        }

        public IEnumerable<DiseaseCount> PerformAnalysisOnOrders(int id,int count = 15)
        {
            var list = Context.Orders.Where(p => p.UserId == id && p.IsCompleted).OrderByDescending(p => p.CompletedAt)
                .Take(count).ToList().SelectMany(p => p.OrderDetails.Select(pr => pr.Drug.DrugDetail.DrugDetailDiseases.Where(ppr => ppr.IsIndication)));
            return list.SelectMany(p => p.ToList()).GroupBy(p => p.DiseaseId).OrderByDescending(p => p.Count()).Take(5).Select(p => new DiseaseCount()
            {
                Count = p.Count(),
                Disease = p.FirstOrDefault().Disease
            });
        }

        public class DiseaseCount   
        {
            public Disease Disease { get; set; }
            public int Count { get; set; }
        }
    }
}
