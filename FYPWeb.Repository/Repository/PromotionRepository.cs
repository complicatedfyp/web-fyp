﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.Linq;
using FYPWeb.Repository.Context;
using Model.Entity;

namespace FYPWeb.Repository.Repository
{
    public class PromotionRepository : BaseRepository<Promotion>
    {
        public PromotionRepository(AppContext context) : base(context)
        {
        }

        public IEnumerable<Promotion> GetPromotionsByUserId(int userId,int page,int pageSize)
        {
            return GetMany(p => p.Pharmacy.UserId == userId, page, pageSize);
        }
        public IEnumerable<Promotion> GetActivePromotionsByUserId(int userId, int page, int pageSize)
        {
            return GetMany(p => p.Pharmacy.UserId == userId && p.StartDate <= DateTime.Now && p.EndDate >= DateTime.Now, page, pageSize);
        }
        public IEnumerable<Promotion> GetActivePromotionsSearchByUserId(string parameter,int userId, int page, int pageSize)
        {
            return GetMany(p => p.Pharmacy.UserId == userId && p.Name.Contains(parameter), page, pageSize);
        }
        public IEnumerable<Promotion> GetInActivePromotionsSearchByUserId(string parameter, int userId, int page, int pageSize)
        {
            return GetMany(p => p.Pharmacy.UserId == userId && (p.EndDate < DateTime.Now) && p.Name.Contains(parameter), page, pageSize);
        }
        public IEnumerable<Promotion> GetInActivePromotionsByUserId(int userId, int page, int pageSize)
        {
            return GetMany(p => p.Pharmacy.UserId == userId && (p.EndDate < DateTime.Now), page, pageSize);
        }
        public int GetPromotionsByUserIdPaginationCount(int userId, int pageSize)
        {
            return GetPageCount(p => p.Pharmacy.UserId == userId,pageSize);
        }
        public int GetActivePromotionsByUserIdPaginationCount(int userId, int pageSize)
        {
            return GetPageCount(p => p.Pharmacy.UserId == userId && p.EndDate >= DateTime.Now, pageSize);
        }
        public int GetActivePromotionsSearchByUserIdPaginationCount(string parameter,int userId, int pageSize)
        {
            return GetPageCount(p => p.Pharmacy.UserId == userId && p.EndDate >= DateTime.Now && p.Name.Contains(parameter), pageSize);
        }
        public int GetInActivePromotionsSearchByUserIdPaginationCount(string parameter, int userId, int pageSize)
        {
            return GetPageCount(p => p.Pharmacy.UserId == userId && (p.EndDate < DateTime.Now) && p.Name.Contains(parameter), pageSize);
        }
        public int GetInActivePromotionsByUserIdPaginationCount(int userId, int pageSize)
        {
            return GetPageCount(p => p.Pharmacy.UserId == userId && (p.EndDate < DateTime.Now), pageSize);
        }
        public IEnumerable<Promotion> GetPromotionsSearchByUserId(string parameter,int userId, int page, int pageSize)
        {
            return GetMany(p => p.Pharmacy.UserId == userId && p.Name.Contains(parameter), page, pageSize);
        }
        public int GetPromotionsSearchByUserIdPaginationCount(string parameter, int userId, int pageSize)
        {
            return GetPageCount(p => p.Pharmacy.UserId == userId && p.Name.Contains(parameter), pageSize);
        }

        public IEnumerable<Promotion> GetCurrentActivePromotions()
        {
           return GetMany(p => DateTime.Now >= p.StartDate && DateTime.Now <= p.EndDate);
        }

        public IEnumerable<Promotion> GetPromotions(int page, int pageSize)
        {
            return GetAll(page, pageSize);
        }
        public IEnumerable<Promotion> GetActivePromotions(int page, int pageSize)
        {
            return GetMany(p => p.EndDate >= DateTime.Now, page, pageSize);
        }
        public IEnumerable<Promotion> GetActivePromotionsSearch(string parameter,int page, int pageSize)
        {
            return GetMany(p => p.Name.Contains(parameter) && p.EndDate >= DateTime.Now, page, pageSize);
        }
        public IEnumerable<Promotion> GetInActivePromotions(int page, int pageSize)
        {
            return GetMany(p => (p.EndDate < DateTime.Now), page, pageSize);
        }
        public IEnumerable<Promotion> GetInActivePromotionsSearch(string parameter, int page, int pageSize)
        {
            return GetMany(p => (p.EndDate < DateTime.Now) && p.Name.Contains(parameter), page, pageSize);
        }
        public int GetPromotionsPaginationCount(int pageSize)
        {
            return GetPageCount(pageSize);
        }
        public int GetActivePromotionsPaginationCount(int pageSize)
        {
            return GetPageCount(p => p.EndDate >= DateTime.Now, pageSize);
        }
        public int GetInActivePromotionsPaginationCount(int pageSize)
        {
            return GetPageCount(p => p.EndDate <= DateTime.Now, pageSize);
        }
        public IEnumerable<Promotion> GetPromotionsSearch(string parameter,int page, int pageSize)
        {
            return GetMany(p => p.Name.Contains(parameter), page, pageSize);
        }
        public int GetPromotionsSearchCount(string parameter, int pageSize)
        {
            return GetPageCount(p => p.Name.Contains(parameter), pageSize);
        }
        public int GetActivePromotionsSearchCount(string parameter, int pageSize)
        {
            return GetPageCount(p => p.Name.Contains(parameter), pageSize);
        }
        public int GetInActivePromotionsSearchCount(string parameter, int pageSize)
        {
            return GetPageCount(p => p.Name.Contains(parameter) && p.EndDate <= DateTime.Now, pageSize);
        }

        public IEnumerable<Promotion> GetTopActivePromotion()
        {
            return Context.Promotions.Where(p => p.StartDate <=  DateTime.Now && p.EndDate >= DateTime.Now).OrderByDescending(p => p.PercentageOff).Take(5).AsQueryable();
        }
        public IEnumerable<Promotion> GetPromotionsFromNear(float lat, float lng)
        {
            var coord = DbGeography.FromText(
                $"POINT({lng.ToString(CultureInfo.CurrentCulture).Replace(",", ".")} {lat.ToString(CultureInfo.CurrentCulture).Replace(",", ".")})");
            var nearest = (from h in Context.Promotions
                let geo = DbGeography.FromText("POINT(" + h.Pharmacy.ShopLocationY + " " + h.Pharmacy.ShopLocationX + ")")
                where h.Pharmacy.IsApproved && !h.Pharmacy.IsRejected && h.EndDate >= DateTime.Now
                orderby geo.Distance(coord)
                group h by h.PharmacyId into g
                           select g.FirstOrDefault()).OrderByDescending(p => p.PercentageOff).Take(5).ToList();
            return nearest;
        }
    }
}
