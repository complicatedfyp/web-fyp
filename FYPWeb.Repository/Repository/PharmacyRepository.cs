﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.Linq;
using FYPWeb.Repository.Context;
using Model.Entity;
namespace FYPWeb.Repository.Repository
{
    public class PharmacyRepository : BaseRepository<Pharmacy>
    {
        public PharmacyRepository(AppContext context) : base(context)
        {

        }

        public Pharmacy GetByIdAndUser(int id,int userId)
        {
            return Get(p => p.UserId == userId && p.Id == id);
        }
        public IEnumerable<Pharmacy> GetApprovedPharmaciesByUserId(int userId)
        {
            return GetMany(p => p.IsApproved && !p.IsRejected && p.UserId == userId);
        }
        public IEnumerable<Pharmacy> GetAllApprovedPharmacies(int page = 1,int pageSize = 10)
        {
            return GetMany(p => p.IsApproved && !p.IsRejected,page,pageSize);
        }
        public int GetAllApprovedPharmaciesPageCount(int pageSize = 10)
        {
            return GetPageCount(p => p.IsApproved && !p.IsRejected, pageSize);
        }
        public IEnumerable<Pharmacy> GetAllApprovedPharmaciesSearch(string parameter, int page = 1, int pageSize = 10)
        {
            return GetMany(p => p.IsApproved && !p.IsRejected && p.ShopName.Contains(parameter), page, pageSize);
        }
        public int GetAllApprovedPharmaciesSearchPageCount(string parameter, int pageSize = 10)
        {
            return GetPageCount(p => p.IsApproved && !p.IsRejected && p.ShopName.Contains(parameter), pageSize);
        }

        public IEnumerable<Pharmacy> GetPharmacies(int page, int pageCount)
        {
            if (page < 1)
                page = 1;
            if (pageCount < 1)
                pageCount = 10;
            return GetAll(page, pageCount);
        }
        public IEnumerable<Pharmacy> GetPharmacies(int page, int pageCount,string type)
        {
            if (page < 1)
                page = 1;
            if (pageCount < 1)
                pageCount = 10;
            switch (type.ToLower())
            {
                case "rejected":
                    return GetMany(p => p.IsRejected,page, pageCount);
                case "approved":
                    return GetMany(p => p.IsApproved, page, pageCount);
                case "pending":
                    return GetMany(p => !p.IsApproved, page, pageCount);
            }
            return null;
        }
        public IEnumerable<Pharmacy> GetPharmacies(int userId,int page, int pageCount)
        {
            if (page < 1)
                page = 1;
            if (pageCount < 1)
                pageCount = 10;
            return GetMany(user => user.UserId == userId, page, pageCount);
        }
        public IEnumerable<Pharmacy> GetPharmaciesSearch(string parameter,int userId, int page, int pageCount)
        {
            if (page < 1)
                page = 1;
            if (pageCount < 1)
                pageCount = 10;
            return GetMany(user => ((user.ShopName).Contains(parameter)), page, pageCount);
        }
        public IEnumerable<Pharmacy> GetPharmaciesSearch(string parameter, int page, int pageCount)
        {
            if (page < 1)
                page = 1;
            if (pageCount < 1)
                pageCount = 10;
            return GetMany(user => ((user.ShopName).Contains(parameter)), page, pageCount);
        }
        public int GetPharmaciesSearchCount(string parameter, int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetPageCount(user => ((user.ShopName).Contains(parameter)), pageCount);
        }
        public int GetPharmaciesSearchCount(string parameter,int userId, int pageCount)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetPageCount(user => ((user.ShopName).Contains(parameter)) && user.UserId == userId, pageCount);
        }
        public int GetPaginationCount(int userId,int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetPageCount(pageCount);
        }

        public IEnumerable<Pharmacy> GetPharmaciesByStockMedicineId(int id,int stock = 0)
        {
            if (stock < 0)
            {
                stock = 0;
            }
            return
                GetMany(
                    p =>
                        p.InventoryTracks.Any(pr => pr.DrugId == id) &&
                        p.InventoryTracks.Where(pr => pr.DrugId == id).Sum(pr => (int?) pr.TotalCount) >
                        stock);
        }


        public IEnumerable<Pharmacy> GetAllApprovedNearestPharmacies(float lat, float lng,float range = 10000f)
        {
            var coord = DbGeography.FromText(
                $"POINT({lng.ToString(CultureInfo.CurrentCulture).Replace(",", ".")} {lat.ToString(CultureInfo.CurrentCulture).Replace(",", ".")})");
            var nearest = (from h in Context.Pharmacies
                           let geo = DbGeography.FromText("POINT(" + h.ShopLocationY + " " + h.ShopLocationX + ")")
                           where h.IsApproved && !h.IsRejected && geo.Distance(coord) <= range
                           orderby geo.Distance(coord)
                           select h).Take(10).ToList();
            return nearest;
        }
    }
}
