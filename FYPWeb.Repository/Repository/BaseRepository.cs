﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using FYPWeb.Repository.Context;
using FYPWeb.Repository.Infrastructure;
using Model.Entity;

namespace FYPWeb.Repository.Repository
{
    public class BaseRepository<T> : IRepository<T>
        where T : Base
    {
        protected AppContext Context;

        public BaseRepository(AppContext context)
        {
            Context = context;
        }

        public bool Add(T entity)
        {
            Context.Set<T>().Add(entity);
            Context.SaveChanges();
            return true;
        }
        public bool AddRange(List<T> entity)
        {
            foreach (var t in entity)
            {
                Context.Set<T>().Add(t);
            }
            Context.SaveChanges();
            return true;
        }


        public void Delete(Expression<Func<T, bool>> where)
        {
            T entity = Context.Set<T>().Where(@where).FirstOrDefault();
            if (entity != null)
            {
                Context.Set<T>().Remove(entity);
            }
            Context.SaveChanges();
        }

        protected void DeleteAll(Expression<Func<T, bool>> where)
        {
            IEnumerable<T> entity = Context.Set<T>().Where(@where);
            foreach (var t in entity)
            {
                Context.Set<T>().Remove(t);
            }
            Context.SaveChanges();
        }

        public void Delete(T entity)
        {
            Context.Entry(entity).State = EntityState.Deleted;
            Context.SaveChanges();
        }

        public T Get(Expression<Func<T, bool>> where)
        {
            return Context.Set<T>().Where(@where).FirstOrDefault();
        }

        public IEnumerable<T> GetAll()
        {
            return Context.Set<T>().ToList();
        }

        public T GetById(int id)
        {
            return Context.Set<T>().Find(id);
        }
        public bool IsExist(int id)
        {
            return Context.Set<T>().Find(id) != null;
        }
        public IQueryable<T> GetMany(Expression<Func<T, bool>> where)
        {
            return Context.Set<T>().Where(where);
        }

        public void Update(T entity)
        {
            Context.Set<T>().Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
            Context.SaveChanges();
        }

        public IQueryable<T> GetMany(Expression<Func<T, bool>> where, int page, int pageSize = 10,bool isDecending = false)
        {
            return isDecending ? Context.Set<T>().Where(@where).OrderByDescending(p => p.Id).Skip((page - 1) * pageSize).Take(pageSize) : Context.Set<T>().Where(@where).OrderBy(p => p.Id).Skip((page - 1) * pageSize).Take(pageSize);
        }
        public IQueryable<T> GetMany(Expression<Func<T, bool>> where, int page, int pageSize, Expression<Func<T,DateTime>> orderBy,bool isDecending)
        {
            return isDecending ? Context.Set<T>().Where(@where).OrderByDescending(orderBy).Skip((page - 1) * pageSize).Take(pageSize) : Context.Set<T>().Where(@where).OrderBy(orderBy).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public int GetCount(Expression<Func<T, bool>> where)
        {
            return Context.Set<T>().Count(where);
        }
        public int GetGroupByCount(Expression<Func<T, int>> groupBy)
        {
            try
            {
                return Context.Set<T>().GroupBy(groupBy).Count();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return 0;
        }
        public int GetGroupByCount(Expression<Func<T, bool>> where,Expression<Func<T, int>> groupBy)
        {
            return Context.Set<T>().Where(where).GroupBy(groupBy).Count();
        }
        public int GetGroupByCount(Expression<Func<T, bool>> where, Expression<Func<T, dynamic>> groupBy)
        {
            return Context.Set<T>().Where(where).GroupBy(groupBy).Count();
        }
        public int GetPageCount(Expression<Func<T, bool>> where,int pageSize = 10)
        {
            return (int)Math.Ceiling(Context.Set<T>().Count(where) / (float)pageSize);
        }
        public int GetGroupByPageCount(Expression<Func<T, bool>> where, Expression<Func<T, dynamic>> groupBy, int pageSize = 10)
        {
            return (int)Math.Ceiling(Context.Set<T>().Where(where).GroupBy(groupBy).Count() / (float)pageSize);
        }

        public int GetPageCount(int pageSize = 10)
        {
            return (int)Math.Ceiling(Context.Set<T>().Count() / (float)pageSize);
        }
        public IQueryable<T> GetAll(int page, int pageSize = 10)
        {
            return Context.Set<T>().OrderBy(p => p.Id).Skip((page - 1) * pageSize).Take(pageSize);
        }

        public bool IsCheck(Expression<Func<T, bool>> where)
        {
            return Context.Set<T>().Any(where);
        }
    }
}
