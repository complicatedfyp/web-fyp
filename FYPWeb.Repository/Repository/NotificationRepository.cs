﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FYPWeb.Repository.Context;
using Model.Entity;

namespace FYPWeb.Repository.Repository
{
    public class NotificationRepository : BaseRepository<Notification>
    {
        public NotificationRepository(AppContext context) : base(context)
        {
        }

        public IEnumerable<Notification> GetNotificationByUserId(int id,int page = 1,int pageSize = 10)
        {
            return
                Context.Notifications.Where(p => p.ReceiverUserId == id)
                    .OrderByDescending(p => p.CreatedAt)
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize);
        }
        public bool SeenAllByUserId(int id)
        {
            var list = GetMany(p => p.IsSeen == false && p.ReceiverUserId == id).ToList();
            list.ForEach(p => p.IsSeen = true);
            Context.SaveChanges();
            return true;
        }

        public int GetUnSeenCountByUserId(int parameterId)
        {
            return GetCount(p => !p.IsSeen && p.ReceiverUserId == parameterId);
        }

        public bool SetUnSeen(int[] notificationIds)
        {
            try
            {
                var list = GetMany(p => !p.IsSeen && notificationIds.Contains(p.Id)).ToList();
                list.ForEach(p => p.IsSeen = true);
                Context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
            return false;
        }

        public int GetPageCountByUserId(int parse)
        {
            return GetPageCount(p => p.ReceiverUserId == parse);
        }
    }
}
