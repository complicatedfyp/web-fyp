﻿using FYPWeb.Repository.Context;
using Model.Entity;

namespace FYPWeb.Repository.Repository
{
    public class OrderDetailRepository : BaseRepository<OrderDetail>
    {
        public OrderDetailRepository(AppContext context) : base(context)
        {

        }

        public OrderDetail GetByOrderIdDrugId(int orderId, int drugId)
        {
            return Get(p => p.OrderId == orderId && p.DrugId == drugId);
        }
    }
}
