﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FYPWeb.Repository.Context;
using Model.Entity;

namespace FYPWeb.Repository.Repository
{
    public class DrugDetailDiseaseRepository : BaseRepository<DrugDetailDisease>
    {
        public DrugDetailDiseaseRepository(AppContext context) : base(context)
        {

        }

        public bool UpdateAll(List<DrugDetailDisease> listInt, int detailId)
        {
            DeleteAll(p => p.DrugDetailId == detailId);
            return AddRange(listInt);
        }
    }
}
