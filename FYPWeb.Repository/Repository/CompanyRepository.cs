﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FYPWeb.Repository.Context;
using Model.Entity;

namespace FYPWeb.Repository.Repository
{
    public class CompanyRepository : BaseRepository<Company>
    {
        public CompanyRepository(AppContext context) : base(context)
        {

        }

        public IEnumerable<Company> GetCompanies(int page = 1, int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetAll(page, pageCount);
        }

        public IEnumerable<Company> GetCompaniesSearch(string parameter, int page = 1, int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetMany(user => ((user.CompanyName).Contains(parameter) || user.Phone.Contains(parameter) || user.Fax.Contains(parameter)), page, pageCount);
        }

        public int GetCompaniesSearchCount(string parameter, int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetPageCount(user => ((user.CompanyName).Contains(parameter) || user.Phone.Contains(parameter) || user.Fax.Contains(parameter)), pageCount);
        }


        public int GetPaginationCount(int pageCount = 10)
        {
            if (pageCount < 1)
                pageCount = 10;
            return GetPageCount(pageCount);
        }


    }
}
