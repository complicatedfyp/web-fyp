namespace FYPWeb.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Brands",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CompanyId = c.Int(nullable: false),
                        BrandName = c.String(),
                        ImagePath = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: true)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CompanyName = c.String(),
                        WebLink = c.String(),
                        Address = c.String(),
                        Phone = c.String(),
                        Fax = c.String(),
                        ImagePath = c.String(),
                        CityId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cities", t => t.CityId, cascadeDelete: true)
                .Index(t => t.CityId);
            
            CreateTable(
                "dbo.Cities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CityName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Drugs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DrugName = c.String(),
                        BrandId = c.Int(nullable: false),
                        DrugForm = c.String(),
                        DrugDetailId = c.Int(nullable: false),
                        PageVisit = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Brands", t => t.BrandId, cascadeDelete: true)
                .ForeignKey("dbo.DrugDetails", t => t.DrugDetailId, cascadeDelete: true)
                .Index(t => t.BrandId)
                .Index(t => t.DrugDetailId);
            
            CreateTable(
                "dbo.DrugDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        OverView = c.String(),
                        ImageLink = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DrugDetailDiseases",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DrugDetailId = c.Int(nullable: false),
                        DiseaseId = c.Int(nullable: false),
                        IsIndication = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Diseases", t => t.DiseaseId, cascadeDelete: true)
                .ForeignKey("dbo.DrugDetails", t => t.DrugDetailId, cascadeDelete: true)
                .Index(t => t.DrugDetailId)
                .Index(t => t.DiseaseId);
            
            CreateTable(
                "dbo.Diseases",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DiseaseName = c.String(),
                        Description = c.String(),
                        PageVisit = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DrugReviews",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DrugId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        Rating = c.Single(nullable: false),
                        Review = c.String(),
                        PostedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Drugs", t => t.DrugId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.DrugId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        Password = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Address = c.String(),
                        ContactNumber = c.String(),
                        AboutMe = c.String(),
                        Email = c.String(),
                        AuthCode = c.String(),
                        IsVerified = c.Boolean(nullable: false),
                        Gender = c.String(),
                        RegisteredAt = c.DateTime(nullable: false),
                        DateOfBrith = c.DateTime(nullable: false),
                        UserImage = c.String(),
                        UserRoleId = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        Signature = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserRoles", t => t.UserRoleId, cascadeDelete: true)
                .Index(t => t.UserRoleId);
            
            CreateTable(
                "dbo.CustomerReviews",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PharmacyId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        OrderId = c.Int(nullable: false),
                        Rating = c.Single(nullable: false),
                        Review = c.String(),
                        PostedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Orders", t => t.OrderId, cascadeDelete: true)
                .ForeignKey("dbo.Pharmacies", t => t.PharmacyId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.PharmacyId)
                .Index(t => t.UserId)
                .Index(t => t.OrderId);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(),
                        PharmacyId = c.Int(),
                        DeliveryCharges = c.Single(nullable: false),
                        DistanceFromDeliveryLocation = c.Single(nullable: false),
                        PromotionId = c.Int(),
                        ExpectedDeliveryTime = c.DateTime(),
                        FromLocation = c.String(),
                        FromLocationX = c.String(),
                        FromLocationY = c.String(),
                        DeliveryLocation = c.String(),
                        DeliveryLocationX = c.String(),
                        DeliveryLocationY = c.String(),
                        OrderedAt = c.DateTime(nullable: false),
                        IsCompleted = c.Boolean(nullable: false),
                        CompletedAt = c.DateTime(),
                        IsAccepted = c.Boolean(nullable: false),
                        AcceptedAt = c.DateTime(),
                        IsCanceled = c.Boolean(nullable: false),
                        CancelledAt = c.DateTime(),
                        CommentsCancelled = c.String(),
                        IsRejected = c.Boolean(nullable: false),
                        RejectedAt = c.DateTime(),
                        CommentsRejected = c.String(),
                        EmployeeId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Employees", t => t.EmployeeId)
                .ForeignKey("dbo.Pharmacies", t => t.PharmacyId)
                .ForeignKey("dbo.Promotions", t => t.PromotionId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.PharmacyId)
                .Index(t => t.PromotionId)
                .Index(t => t.EmployeeId);
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ContactNumber = c.String(),
                        Address = c.String(),
                        ImageLink = c.String(),
                        PharmacyId = c.Int(),
                        UserId = c.Int(nullable: false),
                        Salary = c.Single(nullable: false),
                        DutyHours = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Pharmacies", t => t.PharmacyId)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.PharmacyId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Pharmacies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(),
                        ShopName = c.String(),
                        ShopAddress = c.String(),
                        ContactPersonName = c.String(),
                        ImagePath = c.String(),
                        ShopTimings = c.String(),
                        ContactNumber = c.String(),
                        ShopLocationX = c.Single(nullable: false),
                        ShopLocationY = c.Single(nullable: false),
                        IsVacationMode = c.Boolean(nullable: false),
                        IsApproved = c.Boolean(nullable: false),
                        IsRejected = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.InventoryTracks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PharmacyId = c.Int(nullable: false),
                        DrugId = c.Int(nullable: false),
                        Price = c.Single(nullable: false),
                        TotalCount = c.Int(nullable: false),
                        LastUpdated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Drugs", t => t.DrugId, cascadeDelete: true)
                .ForeignKey("dbo.Pharmacies", t => t.PharmacyId, cascadeDelete: true)
                .Index(t => t.PharmacyId)
                .Index(t => t.DrugId);
            
            CreateTable(
                "dbo.DrugCarts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InventoryId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.InventoryTracks", t => t.InventoryId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.InventoryId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.SessionCarts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InventoryId = c.Int(nullable: false),
                        SessionId = c.String(),
                        Quantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.InventoryTracks", t => t.InventoryId, cascadeDelete: true)
                .Index(t => t.InventoryId);
            
            CreateTable(
                "dbo.PharmacyReviews",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PharmacyId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        OrderId = c.Int(nullable: false),
                        Rating = c.Single(nullable: false),
                        Review = c.String(),
                        PostedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Orders", t => t.OrderId, cascadeDelete: true)
                .ForeignKey("dbo.Pharmacies", t => t.PharmacyId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.PharmacyId)
                .Index(t => t.UserId)
                .Index(t => t.OrderId);
            
            CreateTable(
                "dbo.Promotions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PharmacyId = c.Int(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Code = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        PercentageOff = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Pharmacies", t => t.PharmacyId, cascadeDelete: true)
                .Index(t => t.PharmacyId);
            
            CreateTable(
                "dbo.OrderDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrderId = c.Int(nullable: false),
                        DrugId = c.Int(),
                        DrugName = c.String(),
                        Quantity = c.Int(nullable: false),
                        Tax = c.Single(nullable: false),
                        Price = c.Single(nullable: false),
                        IsPrescription = c.Boolean(nullable: false),
                        PrescriptionId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Drugs", t => t.DrugId)
                .ForeignKey("dbo.Orders", t => t.OrderId, cascadeDelete: true)
                .ForeignKey("dbo.Prescriptions", t => t.PrescriptionId)
                .Index(t => t.OrderId)
                .Index(t => t.DrugId)
                .Index(t => t.PrescriptionId);
            
            CreateTable(
                "dbo.Prescriptions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        PrescriptionPath = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserRoleName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Recommendations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FromUserId = c.Int(),
                        ToUserId = c.Int(),
                        DrugId = c.Int(),
                        RecommendedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Drugs", t => t.DrugId)
                .ForeignKey("dbo.Users", t => t.FromUserId)
                .ForeignKey("dbo.Users", t => t.ToUserId)
                .Index(t => t.FromUserId)
                .Index(t => t.ToUserId)
                .Index(t => t.DrugId);
            
            CreateTable(
                "dbo.Notifications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SenderUserId = c.Int(),
                        ReceiverUserId = c.Int(nullable: false),
                        PostId = c.Int(),
                        IsSeen = c.Boolean(nullable: false),
                        IsUser = c.Boolean(nullable: false),
                        IsPharmacy = c.Boolean(nullable: false),
                        IsPost = c.Boolean(nullable: false),
                        Body = c.String(),
                        NotificationType = c.Int(nullable: false),
                        Url = c.String(),
                        CreatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PostThreads", t => t.PostId)
                .ForeignKey("dbo.Users", t => t.ReceiverUserId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.SenderUserId)
                .Index(t => t.SenderUserId)
                .Index(t => t.ReceiverUserId)
                .Index(t => t.PostId);
            
            CreateTable(
                "dbo.PostThreads",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(),
                        PostCategoryId = c.Int(nullable: false),
                        Subject = c.String(),
                        Body = c.String(),
                        PostedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PostCategories", t => t.PostCategoryId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.PostCategoryId);
            
            CreateTable(
                "dbo.PostCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CategoryName = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PostReplies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        PostThreadId = c.Int(nullable: false),
                        Body = c.String(),
                        RepliedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PostThreads", t => t.PostThreadId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.PostThreadId);
            
            CreateTable(
                "dbo.TagPostThreads",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TagId = c.Int(nullable: false),
                        PostThreadId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PostThreads", t => t.PostThreadId, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagId, cascadeDelete: true)
                .Index(t => t.TagId)
                .Index(t => t.PostThreadId);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Notifications", "SenderUserId", "dbo.Users");
            DropForeignKey("dbo.Notifications", "ReceiverUserId", "dbo.Users");
            DropForeignKey("dbo.Notifications", "PostId", "dbo.PostThreads");
            DropForeignKey("dbo.PostThreads", "UserId", "dbo.Users");
            DropForeignKey("dbo.TagPostThreads", "TagId", "dbo.Tags");
            DropForeignKey("dbo.TagPostThreads", "PostThreadId", "dbo.PostThreads");
            DropForeignKey("dbo.PostReplies", "UserId", "dbo.Users");
            DropForeignKey("dbo.PostReplies", "PostThreadId", "dbo.PostThreads");
            DropForeignKey("dbo.PostThreads", "PostCategoryId", "dbo.PostCategories");
            DropForeignKey("dbo.Recommendations", "ToUserId", "dbo.Users");
            DropForeignKey("dbo.Recommendations", "FromUserId", "dbo.Users");
            DropForeignKey("dbo.Recommendations", "DrugId", "dbo.Drugs");
            DropForeignKey("dbo.DrugReviews", "UserId", "dbo.Users");
            DropForeignKey("dbo.Users", "UserRoleId", "dbo.UserRoles");
            DropForeignKey("dbo.CustomerReviews", "UserId", "dbo.Users");
            DropForeignKey("dbo.Orders", "UserId", "dbo.Users");
            DropForeignKey("dbo.Orders", "PromotionId", "dbo.Promotions");
            DropForeignKey("dbo.Orders", "PharmacyId", "dbo.Pharmacies");
            DropForeignKey("dbo.OrderDetails", "PrescriptionId", "dbo.Prescriptions");
            DropForeignKey("dbo.Prescriptions", "UserId", "dbo.Users");
            DropForeignKey("dbo.OrderDetails", "OrderId", "dbo.Orders");
            DropForeignKey("dbo.OrderDetails", "DrugId", "dbo.Drugs");
            DropForeignKey("dbo.Employees", "UserId", "dbo.Users");
            DropForeignKey("dbo.Pharmacies", "UserId", "dbo.Users");
            DropForeignKey("dbo.Promotions", "PharmacyId", "dbo.Pharmacies");
            DropForeignKey("dbo.PharmacyReviews", "UserId", "dbo.Users");
            DropForeignKey("dbo.PharmacyReviews", "PharmacyId", "dbo.Pharmacies");
            DropForeignKey("dbo.PharmacyReviews", "OrderId", "dbo.Orders");
            DropForeignKey("dbo.SessionCarts", "InventoryId", "dbo.InventoryTracks");
            DropForeignKey("dbo.InventoryTracks", "PharmacyId", "dbo.Pharmacies");
            DropForeignKey("dbo.DrugCarts", "UserId", "dbo.Users");
            DropForeignKey("dbo.DrugCarts", "InventoryId", "dbo.InventoryTracks");
            DropForeignKey("dbo.InventoryTracks", "DrugId", "dbo.Drugs");
            DropForeignKey("dbo.Employees", "PharmacyId", "dbo.Pharmacies");
            DropForeignKey("dbo.CustomerReviews", "PharmacyId", "dbo.Pharmacies");
            DropForeignKey("dbo.Orders", "EmployeeId", "dbo.Employees");
            DropForeignKey("dbo.CustomerReviews", "OrderId", "dbo.Orders");
            DropForeignKey("dbo.DrugReviews", "DrugId", "dbo.Drugs");
            DropForeignKey("dbo.Drugs", "DrugDetailId", "dbo.DrugDetails");
            DropForeignKey("dbo.DrugDetailDiseases", "DrugDetailId", "dbo.DrugDetails");
            DropForeignKey("dbo.DrugDetailDiseases", "DiseaseId", "dbo.Diseases");
            DropForeignKey("dbo.Drugs", "BrandId", "dbo.Brands");
            DropForeignKey("dbo.Companies", "CityId", "dbo.Cities");
            DropForeignKey("dbo.Brands", "CompanyId", "dbo.Companies");
            DropIndex("dbo.TagPostThreads", new[] { "PostThreadId" });
            DropIndex("dbo.TagPostThreads", new[] { "TagId" });
            DropIndex("dbo.PostReplies", new[] { "PostThreadId" });
            DropIndex("dbo.PostReplies", new[] { "UserId" });
            DropIndex("dbo.PostThreads", new[] { "PostCategoryId" });
            DropIndex("dbo.PostThreads", new[] { "UserId" });
            DropIndex("dbo.Notifications", new[] { "PostId" });
            DropIndex("dbo.Notifications", new[] { "ReceiverUserId" });
            DropIndex("dbo.Notifications", new[] { "SenderUserId" });
            DropIndex("dbo.Recommendations", new[] { "DrugId" });
            DropIndex("dbo.Recommendations", new[] { "ToUserId" });
            DropIndex("dbo.Recommendations", new[] { "FromUserId" });
            DropIndex("dbo.Prescriptions", new[] { "UserId" });
            DropIndex("dbo.OrderDetails", new[] { "PrescriptionId" });
            DropIndex("dbo.OrderDetails", new[] { "DrugId" });
            DropIndex("dbo.OrderDetails", new[] { "OrderId" });
            DropIndex("dbo.Promotions", new[] { "PharmacyId" });
            DropIndex("dbo.PharmacyReviews", new[] { "OrderId" });
            DropIndex("dbo.PharmacyReviews", new[] { "UserId" });
            DropIndex("dbo.PharmacyReviews", new[] { "PharmacyId" });
            DropIndex("dbo.SessionCarts", new[] { "InventoryId" });
            DropIndex("dbo.DrugCarts", new[] { "UserId" });
            DropIndex("dbo.DrugCarts", new[] { "InventoryId" });
            DropIndex("dbo.InventoryTracks", new[] { "DrugId" });
            DropIndex("dbo.InventoryTracks", new[] { "PharmacyId" });
            DropIndex("dbo.Pharmacies", new[] { "UserId" });
            DropIndex("dbo.Employees", new[] { "UserId" });
            DropIndex("dbo.Employees", new[] { "PharmacyId" });
            DropIndex("dbo.Orders", new[] { "EmployeeId" });
            DropIndex("dbo.Orders", new[] { "PromotionId" });
            DropIndex("dbo.Orders", new[] { "PharmacyId" });
            DropIndex("dbo.Orders", new[] { "UserId" });
            DropIndex("dbo.CustomerReviews", new[] { "OrderId" });
            DropIndex("dbo.CustomerReviews", new[] { "UserId" });
            DropIndex("dbo.CustomerReviews", new[] { "PharmacyId" });
            DropIndex("dbo.Users", new[] { "UserRoleId" });
            DropIndex("dbo.DrugReviews", new[] { "UserId" });
            DropIndex("dbo.DrugReviews", new[] { "DrugId" });
            DropIndex("dbo.DrugDetailDiseases", new[] { "DiseaseId" });
            DropIndex("dbo.DrugDetailDiseases", new[] { "DrugDetailId" });
            DropIndex("dbo.Drugs", new[] { "DrugDetailId" });
            DropIndex("dbo.Drugs", new[] { "BrandId" });
            DropIndex("dbo.Companies", new[] { "CityId" });
            DropIndex("dbo.Brands", new[] { "CompanyId" });
            DropTable("dbo.Tags");
            DropTable("dbo.TagPostThreads");
            DropTable("dbo.PostReplies");
            DropTable("dbo.PostCategories");
            DropTable("dbo.PostThreads");
            DropTable("dbo.Notifications");
            DropTable("dbo.Recommendations");
            DropTable("dbo.UserRoles");
            DropTable("dbo.Prescriptions");
            DropTable("dbo.OrderDetails");
            DropTable("dbo.Promotions");
            DropTable("dbo.PharmacyReviews");
            DropTable("dbo.SessionCarts");
            DropTable("dbo.DrugCarts");
            DropTable("dbo.InventoryTracks");
            DropTable("dbo.Pharmacies");
            DropTable("dbo.Employees");
            DropTable("dbo.Orders");
            DropTable("dbo.CustomerReviews");
            DropTable("dbo.Users");
            DropTable("dbo.DrugReviews");
            DropTable("dbo.Diseases");
            DropTable("dbo.DrugDetailDiseases");
            DropTable("dbo.DrugDetails");
            DropTable("dbo.Drugs");
            DropTable("dbo.Cities");
            DropTable("dbo.Companies");
            DropTable("dbo.Brands");
        }
    }
}
