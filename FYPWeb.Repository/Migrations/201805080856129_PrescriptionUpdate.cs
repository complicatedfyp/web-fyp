namespace FYPWeb.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PrescriptionUpdate : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OrderDetails", "PrescriptionId", "dbo.Prescriptions");
            DropIndex("dbo.OrderDetails", new[] { "PrescriptionId" });
            CreateTable(
                "dbo.OrderPrescriptions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrderId = c.Int(nullable: false),
                        PrescriptionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Orders", t => t.OrderId, cascadeDelete: true)
                .ForeignKey("dbo.Prescriptions", t => t.PrescriptionId, cascadeDelete: true)
                .Index(t => t.OrderId)
                .Index(t => t.PrescriptionId);
            
            AddColumn("dbo.Orders", "IsPrescription", c => c.Boolean(nullable: false));
            DropColumn("dbo.OrderDetails", "IsPrescription");
            DropColumn("dbo.OrderDetails", "PrescriptionId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.OrderDetails", "PrescriptionId", c => c.Int());
            AddColumn("dbo.OrderDetails", "IsPrescription", c => c.Boolean(nullable: false));
            DropForeignKey("dbo.OrderPrescriptions", "PrescriptionId", "dbo.Prescriptions");
            DropForeignKey("dbo.OrderPrescriptions", "OrderId", "dbo.Orders");
            DropIndex("dbo.OrderPrescriptions", new[] { "PrescriptionId" });
            DropIndex("dbo.OrderPrescriptions", new[] { "OrderId" });
            DropColumn("dbo.Orders", "IsPrescription");
            DropTable("dbo.OrderPrescriptions");
            CreateIndex("dbo.OrderDetails", "PrescriptionId");
            AddForeignKey("dbo.OrderDetails", "PrescriptionId", "dbo.Prescriptions", "Id");
        }
    }
}
