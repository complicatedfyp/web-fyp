namespace FYPWeb.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SomeUpdates : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "IsPrescriptionSet", c => c.Boolean(nullable: false));
            DropColumn("dbo.OrderDetails", "DrugName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.OrderDetails", "DrugName", c => c.String());
            DropColumn("dbo.Orders", "IsPrescriptionSet");
        }
    }
}
