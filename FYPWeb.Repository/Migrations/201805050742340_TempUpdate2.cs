namespace FYPWeb.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TempUpdate2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Drugs", "Packing", c => c.String());
            AddColumn("dbo.Drugs", "ItemsInPack", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Drugs", "ItemsInPack");
            DropColumn("dbo.Drugs", "Packing");
        }
    }
}
