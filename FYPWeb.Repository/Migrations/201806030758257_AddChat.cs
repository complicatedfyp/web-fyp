namespace FYPWeb.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddChat : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrderId = c.Int(),
                        SenderUserId = c.Int(),
                        RecieverUserId = c.Int(),
                        Content = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Orders", t => t.OrderId)
                .ForeignKey("dbo.Users", t => t.RecieverUserId)
                .ForeignKey("dbo.Users", t => t.SenderUserId)
                .Index(t => t.OrderId)
                .Index(t => t.SenderUserId)
                .Index(t => t.RecieverUserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Messages", "SenderUserId", "dbo.Users");
            DropForeignKey("dbo.Messages", "RecieverUserId", "dbo.Users");
            DropForeignKey("dbo.Messages", "OrderId", "dbo.Orders");
            DropIndex("dbo.Messages", new[] { "RecieverUserId" });
            DropIndex("dbo.Messages", new[] { "SenderUserId" });
            DropIndex("dbo.Messages", new[] { "OrderId" });
            DropTable("dbo.Messages");
        }
    }
}
