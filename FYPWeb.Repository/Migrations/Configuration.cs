using System;
using System.Data.Entity.Migrations;
using FYPWeb.Repository.Context;
using FYPWeb.Services.Util;
using Model.Entity;

namespace FYPWeb.Repository.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<AppContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AppContext context)
        {
            return;
            context.Cities.Add(new City()
            {
                CityName = "Lahore"
            });
            context.Cities.Add(new City()
            {
                CityName = "Multan"
            });
            context.Cities.Add(new City()
            {
                CityName = "Karachi"
            });
            context.SaveChanges();
            var roleAdmin = new UserRole
            {
                UserRoleName = "Admin"
            };
            context.UserRoles.Add(roleAdmin);
            var roleCust = new UserRole
            {
                UserRoleName = "Customer"
            };
            context.UserRoles.Add(roleCust);

            var rolePhar = new UserRole
            {
                UserRoleName = "Pharmacist"
            };
            context.UserRoles.Add(rolePhar);
            context.SaveChanges();
            var user = new User()
            {
                Email = "h.halai0334@gmail.com",
                UserImage = UtilService.DefaultImagePath,
                Address = "",
                AuthCode = "",
                ContactNumber = "",
                DateOfBrith = DateTime.Now,
                FirstName = "Mr",
                LastName = "Admin",
                IsActive = true,
                Gender = "Male",
                IsVerified = true,
                Password = UtilService.EncryptPassword("12345"),
                RegisteredAt = DateTime.Now,
                Username = "admin",
                Signature = "I am the Admin",
                UserRoleId = roleAdmin.Id,
            };
            context.Users.Add(user);
        }
    }
}
