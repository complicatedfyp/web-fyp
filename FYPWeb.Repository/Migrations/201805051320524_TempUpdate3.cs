namespace FYPWeb.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TempUpdate3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pharmacies", "DeliveryCharges", c => c.Single(nullable: false));
            DropColumn("dbo.OrderDetails", "Tax");
        }
        
        public override void Down()
        {
            AddColumn("dbo.OrderDetails", "Tax", c => c.Single(nullable: false));
            DropColumn("dbo.Pharmacies", "DeliveryCharges");
        }
    }
}
