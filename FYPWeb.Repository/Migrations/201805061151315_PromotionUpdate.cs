namespace FYPWeb.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PromotionUpdate : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Promotions", "IsActive");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Promotions", "IsActive", c => c.Boolean(nullable: false));
        }
    }
}
