﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FYPWeb.Repository.RepositoryViewModel
{
    public class InventoryRepositoryViewModel
    {
        public int Id { get; set; }
        public int Total { get; set; }
        public string Name  { get; set; }
    }
}
